local I = require("openmw.interfaces")
return {
    eventHandlers = {
        AB_OpenInv = function(actor)
            I.UI.setMode(I.UI.MODE.Companion, { target = actor })
        end
    }
}
