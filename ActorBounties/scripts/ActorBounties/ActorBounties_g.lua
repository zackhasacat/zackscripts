local types = require("openmw.types")
local acti = require("openmw.interfaces").Activation
local core = require("openmw.core")
local world = require("openmw.world")
local bountyPaper = require("scripts.ActorBounties.ActorBounties_paper")
local storage = require("openmw.storage")

local function activateNPC(object, actor)
    if types.Actor.stats.dynamic.fatigue(object).current <= 0 then
        actor:sendEvent("AB_OpenInv", object)
        object:sendEvent("setItemCheck")
        return false
    end
end
acti.addHandlerForType(types.NPC, activateNPC)
