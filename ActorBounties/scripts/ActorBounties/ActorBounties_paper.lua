local bountyPaper = {}
local core = require("openmw.core")
local world = require("openmw.world")
local types = require("openmw.types")

function bountyPaper.genBountyText(NPC, amount)
    local lines = {}
    local NPCRecord = NPC.type.records[npc.recordId]
    local name = NPCRecord.name
    local race = NPCRecord.race
    local gender = NPCRecord.isMale and "Male" or "Female"
    table.insert(lines, "WANTED: DEAD OR ALIVE")
    table.insert(lines, "The Imperial Legion will pay the amount of " .. tostring(amount) .. " septims for the individual listed below, if alive. The amount paid will be " .. tostring(amount * 0.8) .. " septims if the target is deceased.")
    table.insert(lines, name .. ", " .. gender.. " " .. race)
    table.insert(lines, "Last Known Location: " .. NPC.cell.name)



    local noteStr = '<DIV ALIGN="LEFT"><FONT COLOR="000000" SIZE="2" FACE="Magic Cards"><BR>'
    for index, line in ipairs(lines) do
        noteStr = noteStr .. line .. '<BR><BR><BR>'
    end
    return noteStr, "Bounty: " .. name
end

function bountyPaper.createNote(NPC,amount)
    local noteStr, noteTitle = bountyPaper.genBountyText(NPC,amount)
    local draft = types.Book.createRecordDraft({icon = "icons\\m\\Tx_parchment_02.tga",model = "meshes\\m\\Text_Parchment_02.NIF",name = noteTitle,text = noteStr,isScroll = true})
local newRecord = world.createRecord(draft)
world.createObject(newRecord.id):moveInto(world.players[1])
end

return bountyPaper
