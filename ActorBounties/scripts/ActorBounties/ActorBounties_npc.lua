local I = require("openmw.interfaces")
local core = require("openmw.core")
local async = require('openmw.async')
local self = require('openmw.self')
local nearby = require('openmw.nearby')
local types = require('openmw.types')
local itemCheck = false
local bracerId = "slave_bracer_left"
local lastUpdate = 0
local isCaptive = false
local initPackage = nil
local function onUpdate()
    if isCaptive then
        local targets = I.AI.getTargets("Combat")
        if #targets > 0 then
            I.AI.removePackages("Combat")
        end
    end
end
local function timerCB()
    if types.Actor.inventory(self):countOf(bracerId) > 0 and not isCaptive then
        isCaptive = true
        initPackage = I.AI.getActivePackage().type
        I.AI.startPackage { type = "Follow", target = nearby.players[1] }
        types.Actor.stats.dynamic.fatigue(self).current = 0
    end
end
local function onSave()
    return { isCaptive = isCaptive }
end
local function onLoad(data)
    if data then
        isCaptive = data.isCaptive
    end
end
return {
    engineHandlers = { onUpdate = onUpdate, onSave = onSave, onLoad = onLoad },
    eventHandlers = {
        setItemCheck = function(val)
            async:newUnsavableGameTimer(1, timerCB)
        end
    }
}
