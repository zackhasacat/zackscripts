local self = require("openmw.self")
local myId = "zhac_jiub_hero"
if self.recordId ~= myId then
    return {}
end
local I = require("openmw.interfaces")
local core = require("openmw.core")
local async = require('openmw.async')
local nearby = require('openmw.nearby')
local types = require('openmw.types')
local pursuring = false
local spellAdded = false
local function isARacer(actor)
    if types.Actor.stats.dynamic.health(actor).current <= 0 then
        return false
    end
    if actor.recordId == "cliff racer" or actor.recordId == "cliff racer_test"  or actor.recordId == "cliff racer_blighted" or actor.recordId == "cliff racer_diseased" then
        return true
    else
        return false
    end
end
local function startFlying()
    types.Actor.spells(self):add("zhac_jiub_fly")
end
local function stopFlying()
    
    types.Actor.spells(self):remove("zhac_jiub_fly")
end
local function getRacers()
    local racers = {}
    for i, x in ipairs(nearby.actors) do
        if isARacer(x) then
            local distance = (x.position - self.position):length()
            local navTest = nearby.castNavigationRay(self.position, x.position, {})
            if navTest then
                
            table.insert(racers, { racer = x, distance = distance })
            end
        end
    end
    -- Sort the racers table by distance (ascending order)
    table.sort(racers, function(a, b)
        return a.distance < b.distance
    end)

    return racers
end
local function isPursingCliffRacer()
    local currentPackage = I.AI.getActivePackage()
    if not currentPackage or currentPackage.type ~= "Combat" then
        return false
    else
        return true
    end
end
local function attackRacer(racer)
    pursuring = true
    local distanceToRacer = (racer.position - self.position):length()
    local navTest = nearby.castNavigationRay(self.position, racer.position, {})
    if not navTest then
        print("Can't reach ",racer.id)
    end
   -- startFlying()
    I.AI.startPackage({ type = "Combat", target = racer })
end
local function onUpdate()
    if  I.AI.isFleeing() then
        I.AI.removePackages()	
        return
     end
    if not pursuring then
        local racers = getRacers()
        if racers[1] then
            attackRacer(racers[1].racer)
        end
    elseif not isPursingCliffRacer() then
        local racers = getRacers()
        if racers[1] then
            attackRacer(racers[1].racer)
        else
            pursuring = false
        end
    elseif not pursuring or I.AI.isFleeing() then
       I.AI.removePackages()	

    end
end


return {
    interfaceName = "Jiub",
    interface = {
        stopFlying = stopFlying,
        startFlying = startFlying,
    },
    engineHandlers = {
        onUpdate = onUpdate
    },
    eventHandlers = {
        startFlying = startFlying
    }
}
