local var = 1
local function onUpdate()

end
return{
    eventHandlers = {
    },
    engineHandlers = {
        onUpdate = onUpdate,
        onSave = function()
            return {var = var}
        end,
        onLoad = function(data)
            if not data then return end
            var = data.var
        end,
    }
}