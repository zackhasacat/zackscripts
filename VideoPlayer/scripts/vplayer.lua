

local ambient = require('openmw.ambient')
local async = require("openmw.async")
local function onQuestUpdate(qid,index)
    if index ~= 823 then return end

print(qid)
print("video ended")
async:newUnsavableSimulationTimer(0.1, function()
ambient.playVideo("FMAD2/the curse-2.m4v")

end)
end
local function onFrame()

end
local function videoPlayTest()
    async:newUnsavableSimulationTimer(02, function()
    ambient.playVideo("FMAD2/the curse-1.mp4")

    end)
end
return{
    eventHandlers = {
        videoPlayTest = videoPlayTest,
    },
    engineHandlers = {
        onQuestUpdate = onQuestUpdate,
        onFrame = onFrame,
    }
}