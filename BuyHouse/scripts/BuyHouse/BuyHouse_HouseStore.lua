local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local core = require("openmw.core")
local util = require("openmw.util")
local I = require("openmw.interfaces")
local storage = require("openmw.storage")

--Quests that result in a empty house:
--Gra-Bol's Bounty
local function BuyHouse_QuestUpdate(data)
local questId = data.questId
local stage = data.stage
end
return {
    interfaceName  = "BuyHouse_HouseStore",
    interface      = {
        version = 1,

    },
    engineHandlers = {
    },
    eventHandlers  = {
        BuyHouse_QuestUpdate = BuyHouse_QuestUpdate,
        NPCDeath = NPCDeath,
    }
}
