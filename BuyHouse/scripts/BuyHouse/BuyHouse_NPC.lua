local self = require('openmw.self')
local core = require('openmw.core')
local wasDead = false
local function onUpdate()
    if not wasDead then
    local health = self.type.stats.dynamic.health(self).current
    if health == 0 then
        wasDead = true
        core.sendGlobalEvent("NPCDeath",self.recordId)
    end
    end
end

return { engineHandlers = { onUpdate = onUpdate } }
