local I = require('openmw.interfaces')
local core = require('openmw.core')
local function openDialogMenu(target)

    I.UI.setMode("Dialogue",{target = target})
end
local function openBarterMenu(target)

    I.UI.setMode("Barter",{target = target})
end
local function onQuestUpdate(questId,stage)

core.sendGlobalEvent("BuyHouse_QuestUpdate",{questId = questId,stage = stage})
end
return {
    interfaceName  = "BuyHouse_Merch",
    interface      = {
        version = 1,

    },
    engineHandlers = {
        onQuestUpdate = onQuestUpdate,
        onLoad = onLoad,
    },
    eventHandlers  = {
        openDialogMenu = openDialogMenu,
        openBarterMenu = openBarterMenu
    }
}