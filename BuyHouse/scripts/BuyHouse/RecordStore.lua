local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local core = require("openmw.core")
local util = require("openmw.util")
local storage = require("openmw.storage")

local createdRecords = {}

local function onSave()
return {createdRecords = createdRecords}
end
local function onLoad(data)
if not data then return end
createdRecords = data.createdRecords
end

local function getNamedRecordId(name,type,baseId)
local baseRecord
if createdRecords[name:lower()] then
return createdRecords[name:lower()] 
end
if baseId ~= nil then
baseRecord = type.record(baseId)
end
local recordDraft = type.createRecordDraft({template = baseRecord, name = name})
local record = world.createRecord(recordDraft)
createdRecords[name:lower()] = record.id
return record.id
end


return {
	interfaceName  = "BuyHouse_RecordStore",
	interface      = {
		version = 1,
        getNamedRecordId = getNamedRecordId,

	},
	engineHandlers = {
		onSave = onSave,
		onLoad = onLoad,
	},
	eventHandlers  = {

	}
}
