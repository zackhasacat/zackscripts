local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local core = require("openmw.core")
local util = require("openmw.util")
local I = require("openmw.interfaces")
local storage = require("openmw.storage")

local houseMerchants = {}
local availableHouses = {}
local deadNPCs = {}
local talkingToMerch = false
local talkedToActor
local function findMerch(parentActor)
local findId = houseMerchants[parentActor.recordId]
if not findId then return end
for index, value in ipairs(parentActor.cell:getAll(types.NPC)) do
    if value.id == findId then
        return value
    end
end
end
local function activateNPC(object, actor)
    if houseMerchants[object.recordId] then
        world.mwscript.getGlobalScript("zhac_housemanager", actor).variables.houseGuy = 1
        talkingToMerch = true
        actor:sendEvent("openDialogMenu", object)
        talkedToActor = object
        return false
    else
        world.mwscript.getGlobalScript("zhac_housemanager", actor).variables.houseGuy = -1
        talkingToMerch = false
    end
end
local delay = 0
local function onUpdate()
    if talkingToMerch and delay > 5 then
        world.mwscript.getGlobalScript("zhac_housemanager", actor).variables.houseGuy = -1
        talkingToMerch = false
        delay = 0
    elseif talkingToMerch and delay < 6 then
        delay = delay + 1
    end
end
local function onSave()
    return { houseMerchants = houseMerchants , deadNPCs = deadNPCs}
end

local function onLoad(data)
    if not data then return end
    houseMerchants = data.houseMerchants
    deadNPCs = data.deadNPCs
end
local function makeMerchant(actor)
    local realMerch = world.createObject("ZHAC_HouseMerchant")
    realMerch.enabled = false
    realMerch:setScale(0) 
    realMerch:teleport(actor.cell,actor.position)
    realMerch.enabled = false
    houseMerchants[actor.recordId] = realMerch.id
end
local function NPCDeath(npcId)
    deadNPCs[npcId] = 1
end
local function onItemActive(item)
    if item.recordId == "zhac_marker_shirt" then
        local talkTo = findMerch(talkedToActor)
        world.players[1]:sendEvent("openBarterMenu", talkTo)
        item:remove()
    end
end
acti.addHandlerForType(types.NPC, activateNPC)
return {
    interfaceName  = "BuyHouse_Merch",
    interface      = {
        version = 1,

    },
    engineHandlers = {
        onSave = onSave,
        onLoad = onLoad,
        onUpdate = onUpdate,
        onItemActive = onItemActive
    },
    eventHandlers  = {
        makeMerchant = makeMerchant,
        NPCDeath = NPCDeath,
    }
}
