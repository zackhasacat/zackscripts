local buyHouse = {}
local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local core = require("openmw.core")
local util = require("openmw.util")
local I = require("openmw.interfaces")
local storage = require("openmw.storage")
local doorLocksReplaced = false
local ownershipTransferred = false
local oldFurnitureExteriorMoved = false
local oldFurnitureMoved = false
local useOffsetExt = -1536
local useOffsetInt = 2036
local function onSave()
    return {
        doorLocksReplaced = doorLocksReplaced,
        ownershipTransferred = ownershipTransferred,
        oldFurnitureExteriorMoved = oldFurnitureExteriorMoved,
        oldFurnitureMoved = oldFurnitureMoved
    }
end
local function drawLavaSquares(cell, basePos, countx, county)
    for x = 1, countx, 1 do
        for y = 1, county, 1 do
            local position = util.vector3(basePos.x + ((x - 1) * 512), basePos.y - ((y - 1) * 512), basePos.z)
            world.createObject("in_lava_blacksquare"):teleport(cell, position)
        end
    end
end
local function onLoad(data)
    if not data then
        return
    end
    doorLocksReplaced = data.doorLocksReplaced
    ownershipTransferred = data.ownershipTransferred
    oldFurnitureExteriorMoved = data.oldFurnitureExteriorMoved
    oldFurnitureMoved = data.oldFurnitureMoved
end

local blackListTypes = { [types.Door] = true }
local blackListPrefix = {"in_","t_imp_set","ab_in","^_"}
local function startsWith(str, prefix) --Checks if a string starts with another string
    return string.sub(str, 1, string.len(prefix)) == prefix
end
local function deFurnishCell(cell)
    if cell.name == nil then
        cell = world.getCellByName(cell)
    end
    for index, obj in ipairs(cell:getAll()) do
        local valid = true
        if obj.type == types.Door then 
            valid = false
        else
            for index, value in ipairs(blackListPrefix) do
                if startsWith(obj.recordId,value) then
                    valid = false
                end
            end
        end
        if valid then
            local bbox = obj:getBoundingBox().halfSize
            if bbox.x > 120 or bbox.y > 120 or bbox.z > 120 then
             --    valid = false
            end
            
        end
        if obj.baseType == types.Item then
           valid = true
        end
        if valid then
            if obj.baseType ~= types.Item then
                --print(obj.recordId)
            end
            obj.enabled = false
        end
    end
end
function buyHouse.replaceDoorLocks(cell)
    local key_hlaalo_manor = "key_hlaalo_manor"
    local jsmk_mm_mi_key = types.Miscellaneous.record("jsmk_mm_mi_key")
    if not jsmk_mm_mi_key then return end
    for i, doorRef in ipairs(cell:getAll(types.Door)) do
        types.Lockable.setKeyRecord(doorRef, jsmk_mm_mi_key)
    end
    doorLocksReplaced = true
end

local player = nil

local function giveBuildingKey(cellId)
    local name = "Key to " .. cellId
    local record = I.BuyHouse_RecordStore.getNamedRecordId(name, types.Miscellaneous, "key_nedhelas")
    local giveKey = world.createObject(record)
    giveKey:moveInto(world.players[1])
    return record
end
function buyHouse.transferOwnership(cell)
    if not cell.name then
        cell = world.getCellByName(cell)
    end
    local keyRecord = giveBuildingKey(cell.name)
    for i, ref in ipairs(cell:getAll()) do
        if ref.owner.recordId then
            ref.owner.recordId = nil
        end
        if ref.owner.factionId then
            ref.owner.factionId = nil
        end
        if ref.type.baseType == types.Lockable then
            types.Lockable.setKeyRecord(ref, keyRecord)
        end
        if ref.type.baseType == types.Actor then
            ref.enabled = false
        end
        if ref.type == types.Door and types.Door.isTeleport(ref) then--make sure to also convert the exterior doors
            local targetCell = types.Door.destCell(ref)
            for index, ref2 in ipairs(targetCell:getAll(types.Door)) do
                if types.Door.isTeleport(ref2) and types.Door.destCell(ref2) == cell then
                    types.Lockable.setKeyRecord(ref2, keyRecord)
                    if ref2.owner.recordId then
                        ref2.owner.recordId = nil
                    end
                    if ref2.owner.factionId then
                        ref2.owner.factionId = nil
                    end
                end
            end
        end
    end
end



local function isWhitelisted(object)
    if object.type == types.Light then return false end -- dark_64 is a location marker for some reason
    if object.type == types.Door then return true end -- in_h_trapdoor_01
    local id = object.recordId
    if id:match("^in_hlaalu") then return true end   -- in_hlaalu_hall_end
    if id:match("^t_de_sethla") then return true end -- t_de_sethla_x_win_01
    if id:match("^ex_hlaalu") then return true end   -- ex_hlaalu_win_01
    if id:match("^ab_in_hla") then return true end   -- ab_in_hlaroomfloor
    return false
end


function buyHouse.moveOldFurniture(cell, offset)
    for i, ref in ipairs(cell:getAll()) do
        if not isWhitelisted(ref) then
            ref:teleport(cell, util.vector3(offset, 0, 0) + ref.position)
        end
    end
    oldFurnitureMoved = true
end

return {
    interfaceName  = "BuyHouse",
    interface      = {
        version = 1,
        journalUpdated = journalUpdated,
        drawLavaSquares = drawLavaSquares,

    },
    engineHandlers = {
        onSave = onSave,
        onLoad = onLoad,
    },
    eventHandlers  = {
        transferOwnership = buyHouse.transferOwnership,
        giveBuildingKey = giveBuildingKey,
        deFurnishCell = deFurnishCell
    }
}
