local core = require('openmw.core')
local self = require('openmw.self')
local types = require('openmw.types')

local hasLeveled
local currentMagickaBonus
local totalMagickaBonus
local intelligenceBonus = 10
local function preLevelUpCallback(e)
    hasLeveled = true
end
local tes3 = {}

tes3.mobilePlayer = self
tes3.player = self
tes3.setStatistic = function (data)
    local reference = data.reference
    local name = data.name
    local base = data.base
    local current = data.current

    if current then 
        
        types.Actor.stats.dynamic[name](reference).modifier = current -  types.Actor.stats.dynamic[name](reference).base
    end
    if base then types.Actor.stats.dynamic[name](reference).base = base end
end
local function getPlayerLevel()
    return types.Actor.stats.level(self).current
end
local paused = false
local lastLevel
--event.register("uiActivated", preLevelUpCallback, { filter = "MenuLevelUp"})


local function getBaseIntelligence()
    return types.Actor.stats.attributes.intelligence(self).base
end

local function getCurrentIntelligence()
    return types.Actor.stats.attributes.intelligence(self).base
end
local function getMagickaMultiplier()
    return 1
end

local function finishLevelUp()
    if (hasLeveled == true) then
        hasLeveled = false;
        print("level up")
        if not totalMagickaBonus then
            totalMagickaBonus = 1
        end
        local intAmountToAdd = getBaseIntelligence() / intelligenceBonus;
        if (intAmountToAdd < 1) then
            intAmountToAdd = 1
        end
        totalMagickaBonus = totalMagickaBonus + intAmountToAdd
        local currentMagicMultiplication = getCurrentIntelligence() / getBaseIntelligence()
        local baseMagicka = (getCurrentIntelligence() * getMagickaMultiplier()) +
        totalMagickaBonus
        tes3.setStatistic({ reference = tes3.mobilePlayer, name = "magicka", base = baseMagicka })
        tes3.setStatistic({ reference = tes3.mobilePlayer, name = "magicka", current = baseMagicka *
        currentMagicMultiplication })
       -- tes3.mobilePlayer:updateDerivedStatistics(tes3.mobilePlayer.magicka)
        currentMagickaBonus = totalMagickaBonus
    end
end


--event.register("menuExit", finishLevelUp)

local function onLoad()
    totalMagickaBonus = totalMagickaBonus or 0
    local currentMagicMultiplication = getCurrentIntelligence() / getBaseIntelligence()
    local baseMagicka = (getCurrentIntelligence() * getMagickaMultiplier()) +
    totalMagickaBonus
    tes3.setStatistic({ reference = tes3.mobilePlayer, name = "magicka", base = baseMagicka })
    tes3.setStatistic({ reference = tes3.mobilePlayer, name = "magicka", current = baseMagicka *
    currentMagicMultiplication })
   -- tes3.mobilePlayer:updateDerivedStatistics(tes3.mobilePlayer.magicka)
    currentMagickaBonus = totalMagickaBonus
end
--event.register("loaded", onLoad)


local function onMagicMenuActivated(e)
    if (tes3.mobilePlayer ~= nil and tes3.player ~= nil and totalMagickaBonus ~= nil) then
        local currentMagicMultiplication = getCurrentIntelligence() / getBaseIntelligence()
        local baseMagicka = (getCurrentIntelligence() * getMagickaMultiplier()) +
        totalMagickaBonus
        tes3.setStatistic({ reference = tes3.mobilePlayer, name = "magicka", base = baseMagicka })
        tes3.setStatistic({ reference = tes3.mobilePlayer, name = "magicka", current = baseMagicka *
        currentMagicMultiplication })
        tes3.mobilePlayer:updateDerivedStatistics(tes3.mobilePlayer.magicka)
        currentMagickaBonus = totalMagickaBonus
    end
end
--event.register(tes3.event.menuEnter, onMagicMenuActivated)
return {
    engineHandlers = {
        onFrame = function()
            if core.isWorldPaused() ~= paused then
         
                paused = core.isWorldPaused()
                if lastLevel ~= getPlayerLevel() then
                    hasLeveled = true
                    finishLevelUp()
                    lastLevel = getPlayerLevel()
                end
            end
        end,
        onInit = function()
            if not lastLevel then
                lastLevel = getPlayerLevel()
            end
        end,
        onSave = function ()
            return {
                totalMagickaBonus = totalMagickaBonus,
                lastLevel = lastLevel,
            }
        end,
        onLoad = function (data)
            if data then
                totalMagickaBonus = data.totalMagickaBonus
                lastLevel = data.lastLevel
            end
            if not lastLevel then
                lastLevel = getPlayerLevel()
            end
        end
    }
}
