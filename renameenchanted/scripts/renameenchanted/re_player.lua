local core = require("openmw.core")
local I = require("openmw.interfaces")
local input = require("openmw.input")
local async = require("openmw.async")
local storage = require("openmw.storage")
local ui = require("openmw.ui")

local util = require("openmw.util")
local v2 = util.vector2


local inputWindow
local newName

local function textContent(text, template, color)
    if (template == nil) then
        template = I.MWUI.templates.textHeader
    else
        if (color ~= nil) then
            template.props.textColor = color
        end
    end
    return {
        type = ui.TYPE.Text,
        template = template,
        props = {
            text = tostring(text),
            textSize = 20 * 1,
            arrange = ui.ALIGNMENT.Start,
            align = ui.ALIGNMENT.Start
        }
    }
end
local function boxedTextContent(text, callback, textScale, name)
    if textScale == nil then
        textScale = 1
    end
    return {
        type = ui.TYPE.Container,
        content = ui.content {
            {
                template = I.MWUI.templates.box,
                props = {
                    anchor = util.vector2(0, -0.5)
                },
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = I.MWUI.templates.textNormal,
                        events = { mouseClick = callback },
                        props = {
                            text = text,
                            textSize = (15 * 1) * textScale,
                            align = ui.ALIGNMENT.Center,
                            name = name,
                        }
                    }
                }
            }
        }
    }
end
local function boxedTextEditContent(text, callback, isMultiline)
    local multiLine = false
    local height = 30
    if isMultiline then
        multiLine = true
        height = 200
    end
    return {
        type = ui.TYPE.Container,
        content = ui.content {
            {
                template = I.MWUI.templates.box,
                props = {
                    anchor = util.vector2(0, -0.5),
                    size = util.vector2(400, 10),
                },
                content = ui.content {
                    {
                        type = ui.TYPE.TextEdit,
                        template = I.MWUI.templates.textEditLine,
                        events = { textChanged = callback },
                        props = {
                            text = text,
                            size = util.vector2(1400, height),
                            textSize = 15 * 1,
                            align = ui.ALIGNMENT.Center,
                            multiline = multiLine,
                        }
                    }
                }
            }
        }
    }
end
local function renderTextInput(textLines, existingText, editCallback, OKCallback, OKText)
    if (OKText == nil) then
        OKText = "OK"
    end
    local vertical = 50
    local horizontal = (ui.screenSize().x / 2) - 400

    local vertical = 0
    local horizontal = ui.screenSize().x / 2 - 25
    local vertical = vertical + ui.screenSize().y / 2 + 100

    local content = {}
    for _, text in ipairs(textLines) do
        table.insert(content, textContent(text))
    end
    local textEdit = boxedTextEditCont ent(existingText, async:callback(editCallback))
    local okButton = boxedTextContent(OKText, async:callback(OKCallback))
    table.insert(content, textEdit)
    table.insert(content, okButton)

    return ui.create {
        layer = "Windows",
        template = I.MWUI.templates.boxTransparentThick,
        props = {
            relativePosition = v2(0.5, 0.5),
            anchor = v2(0.5, 0.5),
            --   position = v2(horizontal, vertical),
            vertical = false,
            relativeSize = util.vector2(0.1, 0.1),
            arrange = ui.ALIGNMENT.Center
        },
        content = ui.content {
            {
                type = ui.TYPE.Flex,
                content = ui.content(content),
                props = {
                    horizontal = false,
                    align = ui.ALIGNMENT.Center,
                    arrange = ui.ALIGNMENT.Center,
                    size = util.vector2(400, 10),
                }
            }
        }
    }
end
local function onOK()
    inputWindow:destroy()
    inputWindow = nil
    core.sendGlobalEvent("RE_ItemRename",{newName = newName})
    I.UI.setMode()
end

local function onKeyPress(key)
    if (key.code == input.KEY.LeftCtrl or key.code == input.KEY.RightCtrl) then
        core.sendGlobalEvent("RE_ShiftUpdate", true)
    end
end
local function onKeyRelease(key)
    if (key.code == input.KEY.LeftCtrl or key.code == input.KEY.RightCtrl) then
        local keyPressed = false

        core.sendGlobalEvent("RE_ShiftUpdate", false)
    end
end
local function openRenameWindow(item)
inputWindow = renderTextInput({"What would you like the item to be named?"},item.type.records[item.recordId].name,function(text)newName = text end,onOK,"OK")

end
return {
    engineHandlers = { onKeyPress = onKeyPress, onKeyRelease = onKeyRelease },
    eventHandlers = {openRenameWindow = openRenameWindow}
}
