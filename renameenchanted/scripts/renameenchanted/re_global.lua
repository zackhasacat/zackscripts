local core = require("openmw.core")
local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local storage = require("openmw.storage")

local I = require('openmw.interfaces')

-- Override Use action (global script).
local prefix = "Generated"

local itemToRename
-- Forbid equipping armor with weight > 5
local shiftPressed = false
local function RE_ShiftUpdate(state)
    shiftPressed = state
end
local function itemHandler(item, actor)
    if string.sub(item.recordId, 1, #prefix) == prefix and shiftPressed then
        itemToRename = item
        actor:sendEvent("openRenameWindow", item)
        return false
    end
end
local function RE_ItemRename(data)
    local newName = data.newName
    local newRecordDraft = itemToRename.type.createRecordDraft({ name = newName, template = itemToRename.type.record(
    itemToRename) })
    local newRecord = world.createRecord(newRecordDraft)
    local charge = types.Item.getEnchantmentCharge(itemToRename)
    local itemcount = itemToRename.count
    itemToRename:remove(itemcount)
    itemToRename = nil
    local newItem = world.createObject(newRecord.id, itemcount)
    types.Item.setEnchantmentCharge(newItem,charge)
    newItem:moveInto(world.players[1])
end
I.ItemUsage.addHandlerForType(types.Armor, itemHandler)
I.ItemUsage.addHandlerForType(types.Clothing, itemHandler)
I.ItemUsage.addHandlerForType(types.Book, itemHandler)
I.ItemUsage.addHandlerForType(types.Potion, itemHandler)
I.ItemUsage.addHandlerForType(types.Weapon, itemHandler)


return {
    eventHandlers = {
        RE_ItemRename = RE_ItemRename,
        RE_ShiftUpdate = RE_ShiftUpdate
    }
}
