local CraftingFramework = {}
local I = require("openmw.interfaces")

CraftingFramework.Material = {}
CraftingFramework.MenuActivator = {}
CraftingFramework.Recipe = {}

function CraftingFramework.Recipe.getRecipe(id)
    
end
function CraftingFramework.Material.registerMaterials(this, materials)
    for index, value in ipairs(materials) do
        I.CF_Main.registerMaterial(value)
    end
end

function CraftingFramework.MenuActivator.new(this, data)

    I.CF_Main.registerMenu(data)
end

return CraftingFramework
