local materials = {}
local I = require("openmw.interfaces")
local types = require("openmw.types")
local core = require("openmw.core")
local self = require("openmw.self")
local aux_core = require("scripts.CraftingFramework.aux_core")
local ambient = require('openmw.ambient')
local menus = {}
local function registerMaterial(mat)
    --for index, value in ipairs(mat) do
      --  print(mat.id)
    table.insert(materials, mat)
  --  end
end

local function getRecordFromId(id)
    for key, value in pairs(types) do
        if value.record and value.record(id) then
            return value.record(id)
        end
    end
end
local function registerMenu(menu)
    local recipes = menu.recipes
    menu.recipes = {}
    for index, value in ipairs(recipes) do
        local record = getRecordFromId(value.id)
        if not record then
            
         record = getRecordFromId(value.craftableId)
        end
        if record then
            table.insert(menu.recipes,value)
        end
    end


    table.insert(menus, menu)
    if menu.type == "activate" then
        core.sendGlobalEvent("registerActivator", menu)
    elseif menu.type == "equip" then
        core.sendGlobalEvent("registerActivatorItem",menu)
    end
end
local function getItemCount(id)
    local inv = types.Actor.inventory(self)
    return inv:countOf(id)
end
local function getRecordFromId(id)
    for key, value in pairs(types) do
        if value.record and value.record(id) then
            return value.record(id)
        end
    end
end
local function getItemCountForMat(id)
    --print(id)
    local data = {}
    for index, mat in ipairs(materials) do
        if mat.id == id then

            local count = 0
            for index, value in ipairs(mat.ids) do
                local icount = getItemCount(value)
                table.insert(data,{id = value,count = icount})
                count = count + icount
            end
            return count, data
        end
    end

    local icount = getItemCount(id)
    table.insert(data,{id = id,count = icount})
    return getItemCount(id), data
end
local function getNameForMat(id)
    for index, mat in ipairs(materials) do
        if mat.id == id then
            return mat.name
        end
    end
    local productRecord = getRecordFromId(id)

    if productRecord then
        return productRecord.name
    else
        return "NO NAME"
    end

end
local function generateProcessedData(data)
    local retData = data
    local recipes = data.recipes
    for index, value in ipairs(recipes) do
        local id = value.id
        if value.craftableId then
            id = value.craftableId
        end
        local materials = value.materials
        local productRecord = getRecordFromId(id)
        local notlearned = value.knownByDefault  == false and value.learned ~= true 
        if not productRecord or notlearned then
            
            retData.recipes[index].valid = false
            
       -- print(id .. " is not valid")
        else
            retData.recipes[index].name = productRecord.name
            retData.recipes[index].model = productRecord.model
            retData.recipes[index].icon = productRecord.icon
            retData.recipes[index].valid = true
            for mindex, mat in ipairs(materials) do
                local mid = mat.material
                local count, data = getItemCountForMat(mid)

                retData.recipes[index].materials[mindex].currentCount = count
                retData.recipes[index].materials[mindex].name = getNameForMat(mid)

                retData.recipes[index].materials[mindex].tooltipData = retData.recipes[index].materials[mindex].name .. " - " .. tostring(count) .. "\\" .. mat.count
                retData.recipes[index].materials[mindex].data = data
            end

        end
    end
    return retData
end
local activeData
local function CF_ActivateMenu(id)
    for index, value in ipairs(menus) do
        if value.id:lower() == id:lower() then
            local data = generateProcessedData(value)
            I.CF_Menu.openCraftingMenu(data)
            activeData = value
            return
        end
    end
end
local function playItemSound(obj)

    local soundName = aux_core.getDropSound(obj)
    ambient.playSound(soundName)
    I.CF_Menu.setData(generateProcessedData(activeData))
end
local function getMaterials()
    return materials
end
return {
    interfaceName = "CF_Main",
    interface = {
        registerMaterial = registerMaterial,
        registerMenu = registerMenu,
        getMaterials = getMaterials,
        generateProcessedData = generateProcessedData,
        CF_ActivateMenu = CF_ActivateMenu,
    },
    engineHandlers = {
    },
    eventHandlers = {
        CF_ActivateMenu = CF_ActivateMenu,
        playItemSound = playItemSound,
    }
}
