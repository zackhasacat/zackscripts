local ui = require("openmw.ui")
local I = require("openmw.interfaces")
local async = require("openmw.async")
local self = require("openmw.self")
local util = require("openmw.util")
local core = require("openmw.core")
local input = require("openmw.input")

local scenegraph = require('openmw.scenegraph')
local craftingMenu
local tooltipMenu
local hoveredOverId
local selectedIndex = 1
local processedData
local reloadItems = false
local rttCamera = scenegraph.camera {
    fov = 75,
    near = 1,
    far = 1000,
    resolution = util.vector2(512, 512),
    clearColor = util.color.rgba(0, 0, 0, 1),
    ambientColor = util.color.hex('a0a0a0'),
}
rttCamera.rootNode.children[1] = scenegraph.mesh { path = "meshes\\o\\contain_chest10.nif" }
local headerText = ""
local startIndex = 0
local function lerp(x, x1, x2, y1, y2)
    return y1 + (x - x1) * ((y2 - y1) / (x2 - x1))
end
local searchText = ""
local materialsList = {}
local updateTime = 0
local function createRotation(x, y, z)
    local rotate = util.transform.rotateZ(z)
    local rotatex = util.transform.rotateX(x)
    local rotatey = util.transform.rotateY(y)
    rotate = rotate:__mul(rotatex)
    rotate = rotate:__mul(rotatey)
    return rotate
end

local function renderItem(item, bold, id, tooltipId)
    if not id then id = item end
    local textTemplate = I.MWUI.templates.textNormal
    if hoveredOverId == item or bold then
        textTemplate = I.MWUI.templates.textHeader
    end
    return {
        type = ui.TYPE.Container,
        id = id,
        tooltipId = tooltipId,
        props = {
            --  anchor = util.vector2(-1,0),
            align = ui.ALIGNMENT.Center,
            relativePosition = util.vector2(1, 0.5),
            arrange = ui.ALIGNMENT.Center,
        },
        events = {
        },
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                alignment = ui.ALIGNMENT.Center,
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = textTemplate,
                        props = {
                            text = item,
                            textSize = 10 * 2,
                            relativePosition = util.vector2(0.5, 0.5),
                            arrange = ui.ALIGNMENT.Center,
                            align = ui.ALIGNMENT.Center,
                        }
                    }
                }
            }
        }
    }
end
local function renderItemChoice(itemList, horizontal, vertical, align, anchor)
    local content = {}
    for _, item in ipairs(itemList) do
        local itemLayout = renderItem(item)
        itemLayout.template = I.MWUI.templates.padding
        table.insert(content, itemLayout)
    end
    return ui.create {
        layer = "Notification",
        template = I.MWUI.templates.boxTransparent,
        props = {
            -- relativePosition = v2(0.65, 0.8),
            position = util.vector2(vertical, horizontal),
        },
        content = ui.content {
            {
                type = ui.TYPE.Flex,
                content = ui.content(content),
                props = {
                    vertical = true,
                    arrange = align,
                    align = align,
                }
            }
        }
    }
end
local function mouseMove(mouseEvent, data)
    local id = data.id
    if hoveredOverId ~= id then
        hoveredOverId = id
        I.CF_Menu.renderCraftingMenu()
    end
    if data.tooltipData then
        if tooltipMenu then
            tooltipMenu:destroy()
        end
        local text = data.tooltipData
        tooltipMenu = renderItemChoice({ text }, mouseEvent.position.y, mouseEvent.position.x)
    elseif tooltipMenu then
        tooltipMenu:destroy()
        tooltipMenu = nil
    end
end
local function calculateTextScale()
    local screenSize = ui.screenSize()
    local width = screenSize.x
    local scale = lerp(width, 1280, 2560, 1.3, 1.8)
    local textScaleSetting = 1
    return scale * textScaleSetting
end
local selectedData
local function updateSelectedItem()
    materialsList = {}
    for index, data in ipairs(processedData.recipes) do
        if index == selectedIndex then
            rttCamera.rootNode.children[1] = scenegraph.mesh { path = data.model }
            for index, mat in ipairs(data.materials) do
                table.insert(materialsList, mat)
            end
            print(data.model)
            I.CF_Menu.renderCraftingMenu()
            return
        end
    end
end
local function attemptCraft()
    for index, data in ipairs(processedData.recipes) do
        if index == selectedData.index then
            for mindex, mat in ipairs(data.materials) do
                if mat.currentCount < mat.count then
                    local missing = mat.count - mat.currentCount
                    ui.showMessage("Missing " .. tostring(missing) .. " " .. mat.name)
                    return
                end
            end

            ui.showMessage("You crafted " .. selectedData.name)
            core.sendGlobalEvent("CraftItem", data)
            return
        end
    end
end
local function mouseClick(one, data)
    if data.id == "Cancel" then
        craftingMenu:destroy()
        craftingMenu = nil
        I.UI.setMode()
        return
    elseif data.id == "Craft" then
        attemptCraft()
        return
    end
    if data.index ~= nil then
        selectedIndex = data.index
        selectedData = data.data
        updateSelectedItem()
    end
end

local function renderItemBold(item, bold, id, tooltipData)
    if not id then id = item end
    local textTemplate = I.MWUI.templates.textNormal
    if hoveredOverId == item or bold then
        textTemplate = I.MWUI.templates.textHeader
    end
    return {
        type = ui.TYPE.Container,
        id = id,
        tooltipData = tooltipData,
        props = {
            --  anchor = util.vector2(-1,0),
            align = ui.ALIGNMENT.Center,
            relativePosition = util.vector2(1, 0.5),
            arrange = ui.ALIGNMENT.Center,
        },
        events = {
            mouseMove = async:callback(mouseMove),
            mousePress = async:callback(mouseClick),
        },
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                alignment = ui.ALIGNMENT.Center,
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = textTemplate,
                        props = {
                            text = item,
                            textSize = 10 * 2,
                            relativePosition = util.vector2(0.5, 0.5),
                            arrange = ui.ALIGNMENT.Center,
                            align = ui.ALIGNMENT.Center,
                        }
                    }
                }
            }
        }
    }
end
local function renderListItem(item, index)
    local textTemplate = I.MWUI.templates.textNormal
    if hoveredOverId == item.id then
        textTemplate = I.MWUI.templates.textHeader
    end
    return {
        type = ui.TYPE.Container,
        id = item.id,
        data = item,
        index = index,
        props = {
            --  anchor = util.vector2(-1,0),
            align = ui.ALIGNMENT.Center,
            relativePosition = util.vector2(1, 0.5),
            arrange = ui.ALIGNMENT.Center,
        },
        events = {
            mouseMove = async:callback(mouseMove),
            mousePress = async:callback(mouseClick),
        },
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                alignment = ui.ALIGNMENT.Center,
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = textTemplate,
                        props = {
                            text = "- " .. item.name,
                            textSize = 10 * 2,
                            relativePosition = util.vector2(0.5, 0.5),
                            arrange = ui.ALIGNMENT.Center,
                            align = ui.ALIGNMENT.Center,
                        }
                    }
                }
            }
        }
    }
end
local function flexedItems(content, horizontal)
    if not horizontal then
        horizontal = false
    end
    return ui.content {
        {
            type = ui.TYPE.Flex,
            content = ui.content(content),
            events = {
                mouseMove = async:callback(mouseMove)
            },
            props = {
                horizontal = horizontal,
                align = ui.ALIGNMENT.Start,
                arrange = ui.ALIGNMENT.Start,
                size = util.vector2(800, 1200),
            }
        }
    }
end
local function getPreviewBox()
    local itemTemplate
    itemTemplate = I.MWUI.templates.boxThick

    return {
        type = ui.TYPE.Container,
        events = {},
        template = itemTemplate,
        content = flexedItems({ {
            type = ui.TYPE.Image,
            props = {
                resource = ui.texture {
                    texture = rttCamera.texture
                },
                size = util.vector2(400, 400),
            }
        } })
    }
end
local function selectSearch()

end

local function textChanged(newtext)
    searchText = newtext
    updateTime = core.getRealTime() + 0.5
    reloadItems = true
end
local function boxedTextEditContent(text, callback, isMultiline)
    local multiLine = false
    local height = 30
    if isMultiline then
        multiLine = true
        height = 200
    end
    return {
        type = ui.TYPE.Container,
        content = ui.content {
            {
                template = I.MWUI.templates.boxThick,
                props = {
                    anchor = util.vector2(0, -0.5),
                    size = util.vector2(390, 10),
                },
                content = ui.content {
                    {
                        type = ui.TYPE.TextEdit,
                        template = I.MWUI.templates.textEditLine,
                        events = { textChanged = async:callback(textChanged) },
                        props = {
                            text = text,
                            size = util.vector2(390, height),
                            textSize = 10 * 2,
                            align = ui.ALIGNMENT.Center,
                            multiline = multiLine,
                        }
                    }
                }
            }
        }
    }
end

local function renderButton(text)
    local itemTemplate
    itemTemplate = I.MWUI.templates.boxThick

    return {
        type = ui.TYPE.Container,
        events = {},
        template = itemTemplate,
        content = ui.content { renderItemBold(text) },
    }
end
local function renderItemBoxed(content, size)
    local itemTemplate
    local text
    if not size then
        size = util.vector2(800, 1200)
    end

    itemTemplate = I.MWUI.templates.boxThick

    return {
        type = ui.TYPE.Container,
        events = {},
        template = itemTemplate,
        content = ui.content {
            {
                props = {
                    size = size,
                },
                content = content
            },
        },
    }
end
local craftableCache
local function getCraftableItems(filter)
    local ret = {}
    filter = searchText
    if not startIndex then startIndex = 0 end
    local endIndex = startIndex + 200
    local realIndex = 1
    local validItems = {}

    if craftableCache and not reloadItems then
        validItems = craftableCache
    else
        for index, value in ipairs(processedData.recipes) do
            if filter == "" or string.find(value.name:lower(), filter:lower()) then
                if value.name == "" then
                else
                    table.insert(validItems, { name = value.name, id = value.id, index = index , valid = value.valid})
                end
            end
        end
        craftableCache = validItems
    end
    local lastIndex = 0

    if selectedIndex > #validItems then
        selectedIndex = 0
    end
    if startIndex > #validItems then
        startIndex = 0
    end
    for index, value in ipairs(validItems) do
        if index > startIndex and index < endIndex then
            table.insert(ret, value)
            realIndex = realIndex + 1
        elseif index >= endIndex then
            break
        end
        lastIndex = index
    end
    reloadItems = false
    return ret
end
local function renderSkillBox()
    local content = {}
    table.insert(content, renderItemBold("Skills:", true))
    --local skills = "Conjuration: 32/30"
    --table.insert(content,renderItemBonotlearnedld(skills))
    return renderItemBoxed(flexedItems(content, false), util.vector2(390, 100))
end
local function renderMaterialBox()
    local content = {}
    table.insert(content, renderItemBold("Materials:", true))
    for index, value in ipairs(materialsList) do
        if value.name then
        local id   = value.id
        local text = value.name .. " " .. tostring(value.count)
        table.insert(content, renderItemBold(text, false, id, value.tooltipData))
        end
    end
    return renderItemBoxed(flexedItems(content, false), util.vector2(390, 300))
end

local function renderCraftingMenu()
    if craftingMenu then
        craftingMenu:destroy()
    end
    local size = 10
    local citems = getCraftableItems()
    local citemsContent = {}
    for index, value in ipairs(citems) do
        if value.valid then

        table.insert(citemsContent, renderListItem(value, value.index))
        end
    end
    local content    = {}
    local searchLine = boxedTextEditContent(searchText)
    local itemList   = renderItemBoxed(flexedItems(citemsContent, false), util.vector2(390, 1000))
    local leftBox    = renderItemBoxed(flexedItems({ renderItemBold("Recipes", true), searchLine, itemList }, false),
        util.vector2(400, 1200))
    local buttonRow  = renderItemBoxed(flexedItems({ renderButton("Craft"), renderButton("Cancel") }, true),
        util.vector2(800, 40))

    local skillBox   = renderSkillBox()
    local matBox     = renderMaterialBox()
    local rightBox   = renderItemBoxed(
        ui.content(flexedItems {
            renderItemBoxed(ui.content({ getPreviewBox() }), util.vector2(400, 400)),
            skillBox,
            matBox })
    )
    local headerText = renderItemBold(headerText, true)
    local mainBoxbox = renderItemBoxed(flexedItems({ leftBox, rightBox }, true))
    table.insert(content, headerText)
    table.insert(content, mainBoxbox)
    table.insert(content, buttonRow)
    -- table.insert(content, imageContent(resource, size))
    craftingMenu = ui.create {
        layer = "Windows",
        template = I.MWUI.templates.boxTransparentThick
        ,
        props = {
            anchor = util.vector2(0.5, 0.5),
            relativePosition = util.vector2(0.5, 0.5),
            arrange = ui.ALIGNMENT.Center,
            align = ui.ALIGNMENT.Center,
        },
        content = ui.content {
            {
                type = ui.TYPE.Flex,
                content = ui.content(content),
                props = {
                    horizontal = false,
                    align = ui.ALIGNMENT.Center,
                    arrange = ui.ALIGNMENT.Center,
                    size = util.vector2(800, 1200),
                }
            }
        }
    }
end

local eyeX     = 40
local eyeY     = 40
local eyeZ     = 40

local centerX  = 0
local centerY  = 0
local lastTime = core.getRealTime()
local function onFrame(dt)
    if updateTime ~= 0 and core.getRealTime() > updateTime then
        updateTime = 0
        updateSelectedItem()
    end
    local dt             = lastTime - core.getRealTime()
    local rotationAngle  = 20.0 * dt
    local angleInRadians = math.rad(rotationAngle)

    local newX           = math.cos(angleInRadians) * (eyeX - centerX) -
        math.sin(angleInRadians) * (eyeY - centerY) +
        centerX
    local newY           = math.sin(angleInRadians) * (eyeX - centerX) +
        math.cos(angleInRadians) * (eyeY - centerY) +
        centerY

    eyeX                 = newX
    eyeY                 = newY

    rttCamera.viewMatrix = util.transform.lookAt(util.vector3(eyeX, eyeY, eyeZ),
        util.vector3(centerX, centerY, 0),
        util.vector3(0, 0, 1))
    lastTime             = core.getRealTime()
end
local function setData(data)
    processedData = data
    updateSelectedItem()
    renderCraftingMenu()
end
local function openCraftingMenu(data)
    if data then
        headerText = data.name
        local rec = data.recipes
        processedData = data
    end
    startIndex = 0
    craftableCache = nil
    searchText = ""
    I.UI.setMode("Interface", { windows = {} })
    updateSelectedItem()
    renderCraftingMenu()
end

local function onInputAction(action)
    if not craftingMenu then
        return
    end
    if action == input.ACTION.ZoomIn then
        startIndex = startIndex + 1
    elseif action == input.ACTION.ZoomOut and startIndex > 0 then
        startIndex = startIndex - 1
    end
    updateSelectedItem()
end
return {
    interfaceName = "CF_Menu",
    interface = {
        openCraftingMenu = openCraftingMenu,
        renderCraftingMenu = renderCraftingMenu,
        setData = setData
    },
    engineHandlers = {
        onFrame = onFrame,
        onInputAction = onInputAction
    },
    eventHandlers = {

        UiModeChanged = function(data)
            if not data.newMode and craftingMenu then
                craftingMenu:destroy()
                craftingMenu = nil
            end
        end
    }
}
