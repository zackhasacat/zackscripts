local types = require("openmw.types")
local I = require("openmw.interfaces")
local world = require("openmw.world")
local activateObjects = {}
local useObjects = {}
local acti = require("openmw.interfaces").Activation
local function registerActivator(data)
    local id = data.id
    activateObjects[id:lower()] = true
end
local function registerActivatorItem(data)
    local id = data.id
    useObjects[id:lower()] = true
end
local function activationHandler(obj)
    if activateObjects[obj.recordId] then
        world.players[1]:sendEvent("CF_ActivateMenu", obj.recordId)
        return false
    end
end
local function useHandler(obj)
    if useObjects[obj.recordId] then
        print(obj.recordId)
        world.players[1]:sendEvent("CF_ActivateMenu", obj.recordId)
        return false
    end
end
for index, value in pairs(types) do
    acti.addHandlerForType(value, activationHandler)
    if value.baseType == types.Item then
        I.ItemUsage.addHandlerForType(value, useHandler)
    end
end
local function CraftItem(data)
    local itemToGive = data.id
    if data.craftableId then
        itemToGive = data.craftableId
    end
    for mindex, mat in ipairs(data.materials) do
        local countToRemove = mat.count
        for index, submat in ipairs(mat.data) do
            if countToRemove > 0 then
                local countToRemoveHere = math.min(countToRemove, submat.count)
                countToRemove = countToRemove - countToRemoveHere
                local item = types.Actor.inventory(world.players[1]):find(submat.id)
                if item then
                    item:remove(countToRemoveHere)
                end
            end
        end
    end

    local count = 1
    local newItem = world.createObject(itemToGive, count)
    newItem:moveInto(world.players[1])
    world.players[1]:sendEvent("playItemSound", newItem)
end
return {
    interfaceName = "CF_Main",
    interface = {
    },
    engineHandlers = {
    },
    eventHandlers = {
        registerActivator = registerActivator,
        registerActivatorItem = registerActivatorItem,
        CraftItem = CraftItem
    }
}
