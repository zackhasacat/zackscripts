local CraftingFramework = require("CraftingFramework")
local types = require("openmw.types")
local I = require("openmw.interfaces")
--Register your materials
local materials = {
    {
        id = "metal",
        name = "Metal",
        ids = {
            "ingred_scrap_metal_01"
        }
    }
}
CraftingFramework.Material:registerMaterials(materials)

--Create List of Recipes
local recipes = {
    {
        id = "misc_soulgem_grand",
        materials = {
            {
                material = "metal",
                count = 3,
            },
        }
    },
    {
        id = "gold_100",
        materials = {
            {
                material = "metal",
                count = 1,
            }
        }
    },
}

for index, value in pairs(types) do
    if value.baseType == types.Item and value.records then
        for index, rec in ipairs(value.records) do
            table.insert(recipes, {
                id = rec.id,
                materials = {}
            })
        end
    end
end

--Register your MenuActivator
CraftingFramework.MenuActivator:new {
    name = "Menu I am",
    id = "furn_bannerd_clothing_01_hastie",
    type = "activate",
    recipes = recipes
}
return {engineHandlers = {
    onConsoleCommand = function(mode,cmd)
        if cmd == "itemview" then
            I.CF_Main.CF_ActivateMenu("furn_bannerd_clothing_01_hastie")
        end
    end
}}