local cam, camera, core, self, nearby, types, ui, util, storage, async, input, debug =
    require('openmw.interfaces').Camera, require('openmw.camera'),
    require('openmw.core'), require('openmw.self'),
    require('openmw.nearby'), require('openmw.types'),
    require('openmw.ui'), require('openmw.util'),
    require("openmw.storage"), require("openmw.async"),
    require("openmw.input"),
    require("openmw.debug")

local followers = {}
local crimeType = {

    OT_Theft = 1,              -- Taking items owned by an NPC or a faction you are not a member of
    OT_Assault = 2,            -- Attacking a peaceful NPC
    OT_Murder = 3,             -- Murdering a peaceful NPC
    OT_Trespassing = 4,        -- Picking the lock of an owned door/chest
    OT_SleepingInOwnedBed = 5, -- Sleeping in a bed owned by an NPC or a faction you are not a member of
    OT_Pickpocket = 6          -- Entering pickpocket mode, leaving it, and being detected. Any items stolen are a separate
    -- crime (Theft)
}
local function canReportCrime(actor, victim, followers)
    if actor == self or actor.type ~= types.NPC or types.Actor.isDead(actor) then
        return false
    end

    --need to check for combat here

    --need to check for knockdown

    for index, value in ipairs(followers) do
        if value.id == actor.id then
            return false
        end
    end
    return true
end
local function setCrimeLevel(amount)
    core.sendGlobalEvent("Crime_Level_set", { player = self, amount = amount })
    -- types.Player.setCrimeLevel(self,amount)
end
local function getLOS(actor1, actor2)
    local bb1 = (actor1:getBoundingBox().halfSize.z * 1) * 0.9
    local bb2 = (actor2:getBoundingBox().halfSize.z * 1) * 0.9

    local pos1 = actor1.position + util.vector3(0, 0, bb1)
    local pos2 = actor2.position + util.vector3(0, 0, bb2)

    local rayCast = nearby.castRay(pos1, pos2, { ignore = actor1 })
    if rayCast.hitObject == actor2 then
        print("LOS is true")
        return true
    else
        print("LOS is false")
        print(rayCast.hitObject)
        return false
    end
end
local function startCombat(actor, player, followers)
    types.NPC.testCombat(actor,player)
   -- actor:sendEvent('StartAIPackage', { type = 'Combat', target = self.object })
end
local function getDerivedDisposition(actor)
    return 0
end
local function getFightDispositionBias(disposition)
        local fFightDispMult = core.getGMST("fFightDispMult")
        return ((50 - disposition) * fFightDispMult)
end
local function canActorMoveByZAxis()
    return false
end
local function getAggroDistance(actor,vec1,vec2)
    if not vec1 then
        error("no vec1")
    elseif not vec2 then
        error("no vec2")
    end
    return (vec1 - vec2):length()
end
local function getFightDistanceBias(actor1, actor2)
    local pos1 = actor1.position
    local pos2 = actor2.position
    local d = getAggroDistance(actor1,pos1,pos2)

    local iFightDistanceBase = core.getGMST("iFightDistanceBase")
    local fFightDistanceMultiplier = core.getGMST("fFightDistanceMultiplier")
    return (iFightDistanceBase - fFightDistanceMultiplier * d)
end
local function getPrimaryFaction(actor)
    local fact = nil
    for _, factionId in pairs(types.NPC.getFactions(actor)) do
       fact = factionId
    end
    return fact
end
local function roll0to99()
    local max = 100
    if max > 0 then
        return math.random(0, max - 1)
    else
        return 0
    end
end
local function getFatigueTerm(actor)
    local max = types.Actor.stats.dynamic.fatigue(actor).base + types.Actor.stats.dynamic.fatigue(actor).modifier
    local current = types.Actor.stats.dynamic.fatigue(actor).current

    local normalised = math.floor(max) == 0 and 1 or math.max(0, current / max)

    local fFatigueBase = core.getGMST("fFatigueBase")
    local fFatigueMult = core.getGMST("fFatigueMult")

    return fFatigueBase - fFatigueMult * (1 - normalised)
end
local function setAlarmed(actor, val)
--types.NPC.setAlarmed(actor)
end
local function awarenessCheck(player, actor)
    if not actor.enabled or types.Actor.isDead(actor) then
        return false
    end
    local sneakTerm = 0
    if self.controls.sneak then
        local fSneakSkillMult = core.getGMST("fSneakSkillMult")
        local fSneakBootMult = core.getGMST("fSneakBootMult")
        local sneak = types.NPC.stats.skills.sneak(player).modified
        local luck = types.Actor.stats.attributes.luck(player).modified
        local agility = types.Actor.stats.attributes.agility(player).modified
        local bootWeight = 0
        local myBoot = types.Actor.getEquipment(player, types.Actor.EQUIPMENT_SLOT.Boots)
        if myBoot and types.Actor.isOnGround(player) then
            bootWeight = types.Armor.record(myBoot).weight
        end
        sneakTerm = fSneakSkillMult * sneak + 0.2 * agility + 0.1 * luck + bootWeight * fSneakBootMult
    end
    local fSneakDistBase = core.getGMST("fSneakDistanceBase")
    local fSneakDistMult = core.getGMST("fSneakDistanceMultiplier")

    local pos1 = player.position
    local pos2 = actor.position
    local dist = math.sqrt((pos1.x - pos2.x) ^ 2 + (pos1.y - pos2.y) ^ 2 + (pos1.z - pos2.z) ^ 2)
    local distTerm = fSneakDistBase + fSneakDistMult * dist
    local playerActiveEffects = types.Actor.activeEffects(self)
    local actorActiveEffects = types.Actor.activeEffects(actor)
    local chameleon = playerActiveEffects:getEffect("Chameleon").magnitude
    local invisibility = playerActiveEffects:getEffect("Invisibility").magnitude
    local x = sneakTerm * distTerm * getFatigueTerm(player) + chameleon
    if invisibility > 0 then
        x = x + 100
    end

    local obsAgility = types.Actor.stats.attributes.agility(actor).modified
    local obsLuck = types.Actor.stats.attributes.luck(actor).modified
    local obsBlind = actorActiveEffects:getEffect("Blind").magnitude
    local obsSneak = types.NPC.stats.skills.sneak(actor).modified

    local obsTerm = obsSneak + 0.2 * obsAgility + 0.1 * obsLuck - obsBlind

    -- is ptr behind the observer? (Simplified, as direct equivalent might require complex vector math)
    local fSneakNoViewMult = core.getGMST("fSneakNoViewMult")
    local fSneakViewMult = core.getGMST("fSneakViewMult")
    local y = obsTerm * getFatigueTerm(actor) * fSneakNoViewMult -- Simplification, adjust as needed
    local vec = pos1 - pos2
    local target = x - y
    return roll0to99() >= target
end
local function clamp(value, min, max)
    if value < min then
        return min
    elseif value > max then
        return max
    else
        return value
    end
end
local function reportCrime(victim, cType, factionId, amount)
    if cType == crimeType.OT_Murder and victim then
        --TODO: notify murder
    end
    local disp = 0
    local dispVictim = 0
    if cType == crimeType.OT_Trespassing or cType == crimeType.OT_SleepingInOwnedBed then
        arg = core.getGMST("iCrimeTresspass")
        disp = core.getGMST("iDispTresspass")
        dispVictim = disp
    elseif cType == crimeType.OT_Pickpocket then
        arg = core.getGMST("iCrimePickPocket")
        disp = core.getGMST("fDispPickPocketMod")
        dispVictim = disp
    elseif cType == crimeType.OT_Assault then
        arg = core.getGMST("iCrimeAttack")
        disp = core.getGMST("iDispAttackMod")
        dispVictim = core.getGMST("fDispAttacking")
    elseif cType == crimeType.OT_Murder then
        arg = core.getGMST("iCrimeKilling")
        disp = core.getGMST("iDispKilling")
        dispVictim = disp
    elseif cType == crimeType.OT_Theft then
        disp = core.getGMST("fDispStealing") * amount
        dispVictim = disp
        amount = amount * core.getGMST("fCrimeStealing")
        if amount < 1 then
            amount = 1
        end
    end

    local neighbors = {}
    local myPos = self.position
    local nearbyActors = {}

    --TODO: Need to add the logic to track followers
    local radius = core.getGMST("fAlarmRadius")
    for index, value in ipairs(nearby.actors) do
        if value.type == types.NPC then
            local dist = (value.position - self.position):length()
            local isFollowing = false

            for index, fvalue in ipairs(followers) do
                if value.id == fvalue.id then
                    isFollowing = true
                end
            end
            if dist < radius and not isFollowing then
                table.insert(nearbyActors, value)
            end
        end
    end
    local fight = 0
    local fightVictim = 0
    if cType == crimeType.OT_Trespassing or cType == crimeType.OT_SleepingInOwnedBed then
        fight = core.getGMST("iFightTrespass")
        fightVictim = fight
    elseif cType == crimeType.OT_Pickpocket then
        fight = core.getGMST("iFightPickpocket")
        fightVictim = fight * 4
    elseif cType == crimeType.OT_Assault then
        fight = core.getGMST("iFightAttacking")
        fightVictim = core.getGMST("iFightAttack")
    elseif cType == crimeType.OT_Murder then
        fight = core.getGMST("iFightKilling")
        fightVictim = fight
    elseif cType == crimeType.OT_Theft then
        fight = core.getGMST("fFightStealing")
        fightVictim = fight
    end
    local reported = false
    for index, value in ipairs(nearbyActors) do
        if canReportCrime(value, victim, followers) then
            local alarm = types.Actor.stats.ai.alarm(value).base
            if alarm >= 100 then
                reported = true
                if cType == crimeType.OT_Trespassing then
                    value:sendEvent("CrimeYell", "intruder") --can't really do this yet
                end
            end
        end
    end
    for index, value in ipairs(nearbyActors) do
        if canReportCrime(value, victim, followers) then
            local dispTerm
            local alarm = types.Actor.stats.ai.alarm(value).base
            local alarmTerm = 0.01 * alarm
            local isActorVictim = value == victim
            if isActorVictim then
                dispTerm = dispVictim
            else
                dispTerm = disp
            end
            local isActorGuard = types.NPC.record(value).class == "guard"
            local currentDisposition = getDerivedDisposition(value)
            local isPermanent = false
            local applyOnlyIfHostile = false
            local dispositionModifier = 0

            if cType == crimeType.OT_Theft then
                dispositionModifier = dispTerm * alarmTerm
            elseif cType == crimeType.OT_Pickpocket then
                if alarm >= 100 and isActorGuard then
                    dispositionModifier = dispTerm
                elseif isActorGuard and isActorGuard then
                    isPermanent = true
                    dispositionModifier = dispTerm * alarmTerm
                elseif isActorVictim then
                    isPermanent = true
                    dispositionModifier = dispTerm
                end
            elseif cType == crimeType.OT_Assault then
                if isActorVictim and not isActorGuard then
                    dispositionModifier = dispTerm
                    isPermanent = true
                elseif alarm >= 100 then
                    dispositionModifier = dispTerm
                elseif isActorGuard and isActorGuard then
                    isPermanent = true
                    dispositionModifier = dispTerm * alarmTerm
                else
                    applyOnlyIfHostile = true
                    dispositionModifier = dispTerm * alarmTerm
                end
            end
            local setCrimeId = false
            if isPermanent and dispositionModifier ~= 0 and not applyOnlyIfHostile then
                setCrimeId = true
                dispositionModifier = clamp(dispositionModifier, -currentDisposition, 100 - currentDisposition)
                local baseDispotion --todo: this
            elseif dispositionModifier ~= 0 and not applyOnlyIfHostile then
                setCrimeId = true
                dispositionModifier = clamp(dispositionModifier, -currentDisposition, 100 - currentDisposition)
                local baseDispotion --todo: this
            end
            if isActorGuard and alarm >= 100 then
                setAlarmed(actor, true)
                setCrimeId = true
                --todo: pursue
            else
                -- If Alarm is 0, treat it like 100 to calculate a Fight modifier for a victim of pickpocketing.
                -- Observers which do not try to arrest player do not care about pickpocketing at all.
                if cType == crimeType.OT_Pickpocket and isActorVictim and alarmTerm == 0 then
                    alarmTerm = 1
                elseif cType == crimeType.OT_Pickpocket and not isActorVictim then
                    alarmTerm = 0
                end
                local fightTerm = isActorVictim and fightVictim or fight
                fightTerm = fightTerm + getFightDispositionBias(dispTerm)
                fightTerm = fightTerm + getFightDistanceBias(value, self)
                fightTerm = fightTerm * alarmTerm
                local observerFightRating = types.Actor.stats.ai.fight(value).base
                if observerFightRating + fightTerm > 100 then
                    fightTerm = 100 - observerFightRating
                end
                fightTerm = math.max(0, fightTerm)

                print(value, observerFightRating + (fightTerm))
                if (observerFightRating + fightTerm >= 100) then
                    if (dispositionModifier ~= 0 and applyOnlyIfHostile) then
                        dispositionModifier = clamp(dispositionModifier, -currentDisposition, 100 - currentDisposition)
                        --todo:observerStats.modCrimeDispositionModifier(dispositionModifier);
                    end
                    startCombat(value, self, followers)
                   -- value:sendEvent("setFight",observerFightRating + (fightTerm))
                    setCrimeId = true
                    setAlarmed(value, true)
                end
            end
            if setCrimeId then
                --observerStats.setCrimeId(id);
            end
        end
        if reported then
            setCrimeLevel(math.max(0, types.Player.getCrimeLevel(self) + amount))
            if victim and victim.type == types.NPC then
                local nfactionId = getPrimaryFaction(victim)
                if nfactionId then
                    local playerRank = types.NPC.getFactionRank(self, nfactionId)
                    if playerRank > 0 then
                        types.NPC.expel(self, nfactionId)
                    end
                end
            elseif factionId then
                local playerRank = types.NPC.getFactionRank(self, factionId)
                if playerRank > 0 then
                    types.NPC.expel(self, factionId)
                end
            end
        end
    end
    return false --
end
local function commitCrime(victim, amount, cType, factionId, victimAware)
    if cType == crimeType.OT_Assault then
        victimAware = true
    end
    local nearbyActors = {}

    --TODO: Need to add the logic to track followers
    local radius = core.getGMST("fAlarmRadius")
    for index, value in ipairs(nearby.actors) do
        if value.type == types.NPC then
            local dist = (value.position - self.position):length()
            local isFollowing = false

            for index, fvalue in ipairs(followers) do
                if value.id == fvalue.id then
                    isFollowing = true
                end
            end
            if dist < radius and not isFollowing then
                table.insert(nearbyActors, value)
            end
        end
    end
    if victim ~= nil and ((self.position.x - victim.position.x) ^ 2 + (self.position.y - victim.position.y) ^ 2 + (self.position.z - victim.position.z) ^ 2 > radius * radius) then
        table.insert(nearbyActors, victim)
    end

    local crimeSeen = false
    for index, value in ipairs(nearbyActors) do
        if canReportCrime(value, victim, followers) then
            if (value == victim and victimAware) or (cType == crimeType.OT_Murder and value ~= victim)
                or (getLOS(self, value) and awarenessCheck(self, value))
            then
                if cType == crimeType.OT_Theft then
                    value:sendEvent("CrimeYell", "thief") --can't really do this yet
                end
                crimeSeen = true
            end
        end
    end
    if crimeSeen then
        reportCrime(victim, cType, factionId, amount)
    else
        if cType == crimeType.OT_Assault and victim then
            local reported = false
            if types.NPC.record(victim).class == "guard" then
                reported = reportCrime(victim, cType, nil, amount)
            end
            if not reported then
                startCombat(victim, self)
            end
        end
    end
end

local function stealItem(item)

    local owner = item.owner.recordId
    local victim
    for index, value in ipairs(nearby.actors) do
        if value.recordId == owner then
            victim = value
        end
    end
    local amount = item.type.records[item.recordId].value * item.count
    commitCrime(victim,amount,crimeType.OT_Theft)
end
local delay = 0
local fstage
local function onFrame()
    if delay > 0 then
        delay = delay - 1
        if delay == 0 then
            if fstage == 1 then
                
            core.sendGlobalEvent("bounceBack1")
            else
                core.sendGlobalEvent("bounceBack2")
            end

        end
    end
end
return {

    interfaceName = "CrimeSystem",
    interface = {
        commitCrime = commitCrime,
        stealItem = stealItem,
        crimeType = crimeType,
    },
    engineHandlers ={
        onFrame = onFrame,
        onKeyPress = function (key)
            
    if key.code == input.KEY.V then
        input._injectKeyPressLeft()
        ui.showMessage("M")
    elseif key.code == input.KEY.C then
        input._injectKeyPressRight()
        ui.showMessage("C")
    end
        end
    },
    eventHandlers = {
        bounceBack = function (stage)
            fstage = stage
            if stage == 1 then

                delay = 40
            elseif stage == 2 then
    
                    delay = 40
            end
        end
    }
}
