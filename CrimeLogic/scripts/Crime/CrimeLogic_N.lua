local types    = require("openmw.types")
local core     = require("openmw.core")
local util     = require("openmw.util")
local self     = require("openmw.self")
local I        = require("openmw.interfaces")
local storage  = require("openmw.storage")

local function setFight(amount)
    types.Actor.stats.ai.fight(self).modifier = amount - types.Actor.stats.ai.fight(self).base
end

local function CrimeYell(type)
    types.NPC.crimeYell(self,type)
end

return{
    eventHandlers = {
        setFight = setFight,
        CrimeYell = CrimeYell,
    }
}