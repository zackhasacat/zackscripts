local ui, I = require("openmw.ui"), require("openmw.interfaces")
local v2, util, cam, core, self, nearby, types, Camera, input, storage =
    require("openmw.util").vector2, require("openmw.util"),
    require("openmw.interfaces").Camera, require("openmw.core"),
    require("openmw.self"), require("openmw.nearby"),
    require("openmw.types"), require("openmw.camera"),
    require("openmw.input"), require("openmw.storage")
--Enters static cam mode, in front of player, shows UI to select saved outfits.
--Can switch to outfits, or

local savedOutfits = {}
local initialOutfit
local function getPositionBehind(pos, rot, distance, direction)
    local currentRotation = -rot
    local angleOffset = 0

    if direction == "north" then
        angleOffset = math.rad(90)
    elseif direction == "south" then
        angleOffset = math.rad(-90)
    elseif direction == "east" then
        angleOffset = 0
    elseif direction == "west" then
        angleOffset = math.rad(180)
    else
        error("Invalid direction. Please specify 'north', 'south', 'east', or 'west'.")
    end

    currentRotation = currentRotation - angleOffset
    local obj_x_offset = distance * math.cos(currentRotation)
    local obj_y_offset = distance * math.sin(currentRotation)
    local obj_x_position = pos.x + obj_x_offset
    local obj_y_position = pos.y + obj_y_offset
    return util.vector3(obj_x_position, obj_y_position, pos.z)
end


local function enterOutfitMode()
    Camera.setMode(Camera.MODE.Static)
    Camera.setStaticPosition(getPositionBehind(util.vector3(self.position.x, self.position.y, self.position.z + 80),
        self.rotation:getAnglesZYX(), 150, "south"))
    Camera.setPitch(0)
    Camera.setYaw(self.rotation:getAnglesZYX() + math.rad(180))
    types.Player.setControlSwitch(self, input.CONTROL_SWITCH.Controls, false)
    --  types.Player.setControlSwitch(self, types.Player.CONTROL_SWITCH.Looking, false)
    I.UI.setMode("Interface", { windows = {} })
    initialOutfit = types.Actor.getEquipment(self)
    I.Outfits_Menu.drawOutfitsUI()
end

--Exits outfit mode, returning the player to their original outfit, or whichever outfit they had applied or switched to.
local function exitOutfitMode()
    Camera.setMode(Camera.MODE.FirstPerson)
    types.Player.setControlSwitch(self, input.CONTROL_SWITCH.Controls, true)
    -- types.Player.setControlSwitch(self,types.Player.CONTROL_SWITCH.Looking, true)
    types.Actor.setEquipment(self, initialOutfit)
    I.UI.setMode(nil)
end

--Transmogs all the items the player is wearing to use the appearance of the items in the selected outfit.
--Prompts if you want to apply with invisible overrides for items not included in the outfit.
local function applyOutfitAppearance()

end
--Applies all items from the selected outfit to the player character, and equips them. If they are inside of nearby inventories or world spaces, transports them, and notes their parent container or position/rotation.
--If any equipped items are part of another outfit, checks if they have a place to return to, and puts them there, if needed.
local function applyOutfit(name)

end
--Shows the outfit on the player, but don't give it for real
local function applyOutfitTemp(slot)
    if savedOutfits[slot] then
        types.Actor.setEquipment(self, savedOutfits[slot])
    end
end
--Creates an outfit from the player's current equipment.
local function createOutfit(name)

end

local function saveOutfitToSlot(slot)
    savedOutfits[slot] = initialOutfit
end
return {
    interfaceName = "Outfits_Manage",
    interface = {
        enterOutfitMode = enterOutfitMode,
        exitOutfitMode = exitOutfitMode,
        applyOutfitAppearance = applyOutfitAppearance,
        applyOutfit = applyOutfit,
        createOutfit = createOutfit,
        applyOutfitTemp = applyOutfitTemp,
        saveOutfitToSlot = saveOutfitToSlot,
    },
    eventHandlers = {
    },
    engineHandlers = { onInit = onInit, onLoad = onInit }
}
