local ui, I = require("openmw.ui"), require("openmw.interfaces")
local v2, util, cam, core, self, nearby, types, Camera, input, storage =
    require("openmw.util").vector2, require("openmw.util"),
    require("openmw.interfaces").Camera, require("openmw.core"),
    require("openmw.self"), require("openmw.nearby"),
    require("openmw.types"), require("openmw.camera"),
    require("openmw.input"), require("openmw.storage")
local imageBoxCenter
local currentMenuItem = 1
local menuOptions = {
    "Current Outfit",
    "Outfit 1",
    "Outfit 2",
    "Outfit 3",
    "Outfit 4",
    "Outfit 5",
    "Outfit 6",
    "Outfit 7",
    "Outfit 8",
}
local function calculateTextScale()
return 1
end
local function renderItemBold(item, bold)
    return {
        type = ui.TYPE.Container,
        props = {
            --  anchor = util.vector2(-1,0),
            align = ui.ALIGNMENT.Center,
            relativePosition = util.vector2(1, 0.5),
            arrange = ui.ALIGNMENT.Center,
        },
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                alignment = ui.ALIGNMENT.Center,
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = I.MWUI.templates.textNormal,
                        props = {
                            text = item,
                            textSize = 10 * calculateTextScale(),
                            relativePosition = v2(0.5, 0.5),
                            arrange = ui.ALIGNMENT.Center,
                            align = ui.ALIGNMENT.Center,
                        }
                    }
                }
            }
        }
    }
end
local function textContent(text, template, color)
    if (template == nil) then
        template = I.MWUI.templates.textHeader
    else
        if (color ~= nil) then
           -- template.props.textColor = color
        end
    end
    return {
        type = ui.TYPE.Text,
        template = I.MWUI.templates.textNormal,
        props = {
            text = tostring(text),
            textSize = 20 * calculateTextScale(),
            arrange = ui.ALIGNMENT.Start,
            align = ui.ALIGNMENT.Start
        }
    }
end
local function renderTextWithBox(text, horizontal, vertical, size)
    if (size == nil) then
        size = 8 * 1
    else
        size = size * 1
    end


    local content = {}

    table.insert(content, textContent(text, size))
    return ui.create {
        layer = "HUD",
        template = I.MWUI.templates.boxTransparent,
        props = {
            -- relativePosition = v2(0.65, 0.8),
            --  anchor = v2(-1, -2),
            anchor = util.vector2(0.0, 0.0),
            relativePosition = v2(horizontal, vertical),
            arrange = ui.ALIGNMENT.Center,
            align = ui.ALIGNMENT.Center,
        },
        content = ui.content(content)
    }
end
local function drawOutfitsUI()
    if imageBoxCenter then imageBoxCenter:destroy() end
    imageBoxCenter = renderTextWithBox(menuOptions[currentMenuItem], 0.5, 0.0, 5)
end
local function onKeyPress(key)
    
    if not  imageBoxCenter then 
        if key.code == input.KEY.U then
            I.Outfits_Manage.enterOutfitMode()
        end
        return end
    if key.code == input.KEY.UpArrow then
        if menuOptions[currentMenuItem + 1] then
            currentMenuItem = currentMenuItem + 1
        end
        drawOutfitsUI()
        I.Outfits_Manage.applyOutfitTemp(menuOptions[currentMenuItem])
    elseif key.code == input.KEY.DownArrow then
        if menuOptions[currentMenuItem - 1] then
            currentMenuItem = currentMenuItem - 1
        end
        drawOutfitsUI()
        I.Outfits_Manage.applyOutfitTemp(menuOptions[currentMenuItem])
    elseif key.code == input.KEY.Escape then
        I.Outfits_Manage.exitOutfitMode()
        imageBoxCenter:destroy()
        imageBoxCenter = nil
    elseif key.code == input.KEY.Enter then
        I.Outfits_Manage.saveOutfitToSlot(menuOptions[currentMenuItem ])
        ui.showMessage("Saved Outfit")
    elseif key.code == input.KEY.Enter then
        I.Outfits_Manage.saveOutfitToSlot(menuOptions[currentMenuItem ])
        ui.showMessage("Saved Outfit")
    elseif key.code == input.KEY.U then
        I.Outfits_Manage.exitOutfitMode()
        imageBoxCenter:destroy()
        imageBoxCenter = nil
    end
end
return {
    interfaceName = "Outfits_Menu",
    interface = {
        drawOutfitsUI = drawOutfitsUI,
    },
    eventHandlers = {
        
    },
    engineHandlers = { onInit = onInit, onLoad = onInit, onKeyPress = onKeyPress, }
}
