local ui, I = require("openmw.ui"), require("openmw.interfaces")
local v2, util, cam, core, self, nearby, types, Camera, input, storage =
    require("openmw.util").vector2, require("openmw.util"),
    require("openmw.interfaces").Camera, require("openmw.core"),
    require("openmw.self"), require("openmw.nearby"),
    require("openmw.types"), require("openmw.camera"),
    require("openmw.input"), require("openmw.storage")

return {
    interfaceName = "Outfits_Storage",
    interface = {
    },
    eventHandlers = {
    },
    engineHandlers = { onInit = onInit, onLoad = onInit }
}