local terminalData = {}
local debug = require('openmw.debug')
local core = require("openmw.core")
terminalData.name = "MUNDIS_PowerMenu"

terminalData.items = {

    {text = "Debug: Add Power", output = "Added charges", functions = {
        function ()
            core.sendGlobalEvent("addSoulCount",2)
        end
    }},
}
terminalData.header = 
{
    "Aetheryte Systems",
   "COPYRIGHT 3E 427 ",
    "-Server 6-"
}
--header is centered
terminalData.globalStorage = "MundisData"
terminalData.info = 
{
    "MUNDIS Power Control",
"Current MUNDIS Power: [@chargeCount]",
"MUNDIS Power Enabled: [@MUNDISPowered]",
    "Direct access terminal"
}
--info is left

return terminalData