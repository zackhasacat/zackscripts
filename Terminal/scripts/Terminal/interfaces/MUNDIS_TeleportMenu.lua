local terminalData = {}

terminalData.name = "MUNDIS_TeleportMenu"

terminalData.items = {

    {text = "Manage Power", event = "myEvent",  subMenu = "MUNDIS_PowerMenu"},
    {text = "Select New Teleport Destination", subMenu = "MUNDIS_TeleportDestList"},
    {text = "Select New Timeframe", subMenu = "MUNDIS_TimeMenu"},
    {text = "Engage Teleport"},
}
terminalData.header = 
{
    "Aetheryte Systems",
   "COPYRIGHT 3E 427 ",
    "-Server 6-"
}
--header is centered
terminalData.info = 
{
    "MUNDIS Control",
"Select the system desired.",
    "Direct access terminal"
}
--info is left

return terminalData