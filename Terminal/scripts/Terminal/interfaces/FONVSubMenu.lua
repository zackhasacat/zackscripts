local terminalData = {}

terminalData.name = "FONVSubMenu"

terminalData.items = {

    {text = "Open Door", event = "myEvent", output = "Opening Door", goBack = true},
    {text = "Close System", event = "myEvent", output = "Opening Door", closeTerminal = true},
    {text = "Nest Manage Door System", event = "myEvent", subMenu = "FONVSubMenu"},
}
terminalData.header = 
{
    "ROBCO INDUSTRIES UNIFIED OPERATING SYSTEM",
   "COPYRIGHT 2075-2077 ROBCO INDUSTRIES",
    "-Server 6-"
}
--header is centered
terminalData.info = 
{
    "H&H Tools,  Inc.",
"\"Building the things you need to build a better tomorrow!\"",
    "Employee access terminal"
}
--info is left

return terminalData