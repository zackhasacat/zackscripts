local terminalData = {}

terminalData.name = "NPCManage"

terminalData.filterMode = true
terminalData.header = 
{
    "OpenMW Cheat Menu",
   "COPYRIGHT 2075-2077 ROBCO INDUSTRIES",
    "-Server 6-"
}
--header is centered
terminalData.info = 
{
    "NPC: [NAME],   ID: [ID]",
    "Current Health: [HEALTH], Enabled: [STATE]",
}

terminalData.items = {

    {text = "Kill"},
    {text = "Enter Inventory"},
    {text = "Make Follow"},
    {text = "Disable"},
}
--info is left

return terminalData