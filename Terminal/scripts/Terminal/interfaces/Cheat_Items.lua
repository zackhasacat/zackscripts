local terminalData = {}

terminalData.name = "Cheat_Items"
local core = require("openmw.core")
local self = require("openmw.self")
local types = require("openmw.types")

terminalData.items = {

}
terminalData.filterMode = true
terminalData.header =
{
    "OpenMW Cheat Menu",
    "COPYRIGHT 2075-2077 ROBCO INDUSTRIES",
    "-Server 6-"
}
--header is centered
terminalData.info =
{
    "Teleport Destinations",
}
local itemData
terminalData.onAppear = function(data)
    if not itemData then
        itemData = {}
        for index, type in pairs(types) do
            if type.records and type.baseType == types.Item then
                for index, record in ipairs(type.records) do
                    if record.name ~= nil and record.name ~= "" then
                        
                    local menuItem = {
                        text = record.name,
                        closeTerminal = true,
                        data = { id = record.id },
                        functions = {
                            function(data)
                                core.sendGlobalEvent("ZackUtilsAddItems",
                                {
                                  itemIds = {record.id},
                                  actorId = self.recordId,
                                  equip = false,
                                  actor = self
                                })
                            end
                        }
                    }
                    table.insert(itemData, menuItem)
                    end
                end
            end
        end

        table.sort(itemData, function(a, b)
            return a.text < b.text
        end)
    end

    for i, x in ipairs(itemData) do
        table.insert(data.items,x)
    end
end
terminalData.onBounceBack = function(data, eventData)

end
--info is left

return terminalData
