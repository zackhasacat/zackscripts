local terminalData = {}

terminalData.name = "NPCManage"

terminalData.items = {

    {text = "Teleport", subMenu = "Cheat_Teleport"},
    {text = "Browse Items", subMenu = "Cheat_Items"},
}
terminalData.filterMode = true
terminalData.header = 
{
    "OpenMW Cheat Menu",
   "COPYRIGHT 2075-2077 ROBCO INDUSTRIES",
    "-Server 6-"
}
--header is centered
terminalData.info = 
{
    "NPC Management",
"Select a NPC to mess with",
"[@VAR1]",
"[@VAR2]"
}
--info is left

return terminalData