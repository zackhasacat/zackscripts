local terminalData = {}
local debug = require('openmw.debug')
local core = require("openmw.core")
terminalData.name = "MUNDIS_TimeMenu"

terminalData.items = {

    { text = "Select Timeframe" },
    { text = "Select Timeline" },
    { text = "Select Dimension" },
}
terminalData.header =
{
    "Aetheryte Systems",
    "COPYRIGHT 3E 427 ",
    "-Server 6-"
}
--header is centered
terminalData.globalStorage = "MundisData"
terminalData.info =
{
    "MUNDIS Power Control",
    "Local Time: [@timeDate]",
    "Current Timeline: [@MUNDISPowered]",
    "Current Dimension: "
}

--info is left

return terminalData
