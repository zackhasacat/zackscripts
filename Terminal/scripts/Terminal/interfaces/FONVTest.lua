local terminalData = {}

terminalData.name = "FONVTest"

terminalData.items = {

    {text = "Manage Door System", event = "myEvent", subMenu = "FONVSubMenu"},
    {text = "E-mail From Alan Dalton", event = "myEvent", output = "Selected Email 2"},
    {text = "HR E-mail 07/25/2022", event = "myEvent", output = "Selected Email 3"},
    {text = "HR E-mail 11/08/2023", event = "myEvent", output = "Selected Email 4"},
    {text = "HR E-mail 11/09/2023", event = "myEvent", output = "Selected Email 4"},
    {text = "HR E-mail 11/10/2023", event = "myEvent", output = "Selected Email 4"},
    {text = "HR E-mail 11/11/2023", event = "myEvent", output = "Selected Email 4"},
    {text = "HR E-mail 11/12/2023", event = "myEvent", output = "Selected Email 4"},
    {text = "HR E-mail 11/13/2023", event = "myEvent", output = "Selected Email 4"},
    {text = "HR E-mail 11/14/2023", event = "myEvent", output = "Selected Email 4"},
    {text = "HR E-mail 11/15/2023", event = "myEvent", output = "Selected Email 4"},
    {text = "HR E-mail 11/16/2023", event = "myEvent", output = "Selected Email 4"},
    {text = "HR E-mail 11/17/2023", event = "myEvent", output = "Selected Email 4"},
}
terminalData.filterMode = true
terminalData.header = 
{
    "ROBCO INDUSTRIES UNIFIED OPERATING SYSTEM",
   "COPYRIGHT 2075-2077 ROBCO INDUSTRIES",
    "-Server 6-"
}
--header is centered
terminalData.info = 
{
    "H&H Tools,  Inc.",
"\"Building the things you need to build a better tomorrow!\"",
    "Employee access terminal"
}
--info is left

return terminalData