local terminalData = {}
local I = require("openmw.interfaces")
local core = require("openmw.core")
local self = require("openmw.self")
local storage = require("openmw.storage")
local myModData = storage.globalSection("MundisData")

terminalData.name = "MUNDIS_TeleportDestList"

terminalData.items = {

}
terminalData.header =
{
    "Aetheryte Systems",
    "COPYRIGHT 3E 427 ",
    "-Server 6-"
}
terminalData.filterMode = true
--header is centered
terminalData.info =
{
    "MUNDIS Destination Control",
    "Select the destination desired.",
    "Direct access terminal"
}
terminalData.onAppear = function(data)
    local mdata = myModData:get("MUNDISLocData")
    for i, x in ipairs(mdata) do
        table.insert(data.items,
            {
                text = x.cellData.cellName,
                event = "itemPickedNPC",
                closeTerminal = true,
                data = { posid = x.ID },
                functions = {
                    function(data)
                        core.sendGlobalEvent("checkButtonText", data.data.posid)
                    end
                }
            })
    end
end
--info is left

return terminalData
