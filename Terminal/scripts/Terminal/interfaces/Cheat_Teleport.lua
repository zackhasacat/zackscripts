local terminalData = {}

terminalData.name = "Cheat_Teleport"
local core = require("openmw.core")
local self = require("openmw.self")

terminalData.items = {

}
terminalData.filterMode = true
terminalData.header =
{
    "OpenMW Cheat Menu",
    "COPYRIGHT 2075-2077 ROBCO INDUSTRIES",
    "-Server 6-"
}
--header is centered
terminalData.info =
{
    "Teleport Destinations",
}
local cellData
terminalData.onAppear = function(data)
    if not cellData then

    core.sendGlobalEvent("getCellList", self)
    else
        for i, x in ipairs(cellData) do
            table.insert(data.items,
                {
                    text = x.text,
                    closeTerminal = true,
                    data = x.data,
                    functions = {
                        function(data)
                            core.sendGlobalEvent("COCEvent",{objectToTeleport = self,cellname = x.text})
                        end
                    }
                })
        end
    end
end
terminalData.onBounceBack = function(data, eventData)
    table.sort(eventData, function(a, b)
        return a.text < b.text
    end)
    cellData = eventData

    for i, x in ipairs(eventData) do
        table.insert(data.items,
            {
                text = x.text,
                closeTerminal = true,
                data = x.data,
                functions = {
                    function(data)
                        core.sendGlobalEvent("COCEvent",{objectToTeleport = self,cellname = x.text})
                    end
                }
            })
    end
end
--info is left

return terminalData
