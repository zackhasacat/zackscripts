-- OpenMW Lua Script: Terminal Interface
local core = require("openmw.core")
local input = require("openmw.input")
local ui = require("openmw.ui")
local async = require("openmw.async")
local util = require("openmw.util")
local self = require("openmw.self")
local nearby = require('openmw.nearby')
local I = require("openmw.interfaces")
local terminalData = nil
local function CellChange()
    I.Terminal.resetInterface("NPCManage")
    for index, value in ipairs(nearby.actors) do
        if value.id ~= self.id then
            local actorRecord = value.type.record(value)
            I.Terminal.addItemToInterface({
                interfaceName = "NPCManage",
                item =
                {
                    text = actorRecord.name,
                    event = "itemPickedNPC",
                    closeTerminal = true,
                    data = { id = value.id },
                    functions = {
                        function(data)

                        end
                    }
                }
            })
        end
    end
end
local function itemPickedNPC(data)
    print("Picked")
    local text = data.text
    local id = data.data.id
    for index, value in ipairs(nearby.actors) do
        local actorRecord = value.type.record(value)

        if value.id == id then
            value:sendEvent("setStat", { type = "health", value = 0 })
        end
    end
end
local itemsAdded = false
local function activateMUNDISTerminal(data)
    -- I.Terminal.resetInterface("MUNDIS_TeleportDestList")
    terminalData = data

    I.Terminal.openTerminal("MUNDIS_TeleportMenu")
end
local function getTerminalData()
    return terminalData
end
return {
    interfaceName = "Terminal_TB",
    interface = { getTerminalData = getTerminalData, drawTerminal = drawTerminal },
    engineHandlers = { onKeyPress = onKeyPress, onKeyRelease = onKeyRelease },
    eventHandlers = { CellChange = CellChange, itemPickedNPC = itemPickedNPC, UiModeChanged = UiModeChanged, activateMUNDISTerminal = activateMUNDISTerminal, }
}
