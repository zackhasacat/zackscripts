local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local util = require("openmw.util")
local I = require("openmw.interfaces")
local async = require("openmw.async")
local core = require("openmw.core")
local calendar = require('openmw_aux.calendar')

local activatedObj = false
local activatedBed
local pendingSleep = false
local finishedSleep = false
local isWait = false
local once = true
local function activatorActivation(activator, actor)
    if activator.recordId == "zhac_terminal_screen" then
        

        actor:sendEvent("activateMUNDISTerminal")
    end
end
I.Activation.addHandlerForType(types.Activator, activatorActivation)

local function terminalBounceBack(plr)
    plr:sendEvent("terminalBounceBack")
end

local function getCellList(plr)
    local data = {}
    for index, cell in ipairs(world.cells) do
        if cell and cell.name and cell.name ~= "" then
            table.insert(data,{text = cell.name, data = {gridX = cell.gridX, gridY = cell.gridY, isExterior = cell.isExterior, noSleep = cell:hasTag("NoSleep")}})
        end
    end

    plr:sendEvent("terminalBounceBack",data)
end
return {
    interfaceName = "Terminal",
    interface = {
    },
    engineHandlers = {
    },
    eventHandlers = {
        terminalBounceBack = terminalBounceBack,
        getCellList = getCellList,
    }
}
