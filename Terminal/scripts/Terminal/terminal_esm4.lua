-- OpenMW Lua Script: Terminal Interface
local core = require("openmw.core")
local input = require("openmw.input")
local ui = require("openmw.ui")
local async = require("openmw.async")
local util = require("openmw.util")
local self = require("openmw.self")
local vfs = require('openmw.vfs')
local storage = require('openmw.storage')
local types = require('openmw.types')
local I = require("openmw.interfaces")


local function getLines(str, maxLineLength)
    maxLineLength = maxLineLength or 60  -- Default maximum line length
    local lines = {}
    for line in string.gmatch(str, "[^\r\n]+") do
        while #line > maxLineLength do
            local splitLine = string.sub(line, 1, maxLineLength)
            table.insert(lines, splitLine)
            line = string.sub(line, maxLineLength + 1)
        end
        table.insert(lines, line)
    end
    return lines
end

local function addTerminal(record)
    local terminalData = {}

    terminalData.name = record.id

    terminalData.items = {

    }
    if record.items then
        for index, item in ipairs(record.items) do
            local subMenu
            if #item.subMenu > 0 then
                subMenu = item.subMenu
            end
            if #item.note > 0 then
                subMenu = item.note
            end
            table.insert(terminalData.items, {
                subMenu = subMenu,
                text = item.itemText,
                output = item.resultText
            })
        end
    end
    --    terminalData.filterMode = false
    terminalData.header =
    {
        "ROBCO INDUSTRIES UNIFIED OPERATING SYSTEM",
        "COPYRIGHT 2075-2077 ROBCO INDUSTRIES",
        "-Server 6-"
    }
    --header is centered
    terminalData.info = getLines(record.text)
    --info is left

    return terminalData
end

return { addTerminal = addTerminal }
