-- OpenMW Lua Script: Terminal Interface
local core = require("openmw.core")
local input = require("openmw.input")
local ui = require("openmw.ui")
local async = require("openmw.async")
local util = require("openmw.util")
local self = require("openmw.self")
local vfs = require('openmw.vfs')
local types = require('openmw.types')
local storage = require('openmw.storage')
local I = require("openmw.interfaces")
local SettingsBookPickup = require("scripts.bookpickup.bp_settings")


local esm4Terminal = require("scripts.Terminal.terminal_esm4")
-- Valid interfaces initialization
local validInterfaces = {}
local originalInterfaces = {}
local interfaceTest = require("scripts.Terminal.interfaces.FONVTest")


local calendar = require('openmw_aux.calendar')
I.Settings.registerPage {
    key = "SettingsTerminal",
    l10n = "SettingsTerminal",
    name = "Terminal",
    description = ""
}
local myVariables = {
    VAR1 = "Ashley",
    VAR2 = "Sydney",
    timeDate = calendar.formatGameTime("%c", core.getGameTime())
}

local savedStorages = {}


I.Settings.registerGroup {
    key = "SettingsTerminal",
    page = "SettingsTerminal",
    l10n = "SettingsTerminal",
    name = "Terminal",
    description = "",
    permanentStorage = true,
    settings = {
        {
            key = "foregroundColor",
            renderer = "color",
            name = "Color",
            description = "Press this key while looking at a book to open or close it.",
            default = util.color.rgb(1, 1, 1)
        },
        {
            key = "backgroundColor",
            renderer = "color",
            name = "Color",
            description = "Press this key while looking at a book to open or close it.",
            default = util.color.rgb(0, 0, 0)
        },
    }
}
local settings = storage.playerSection("SettingsTerminal")
--local interfaceTest2 = require("scripts.Terminal.interfaces.FONVSubMenu")
for fileName in vfs.pathsWithPrefix("scripts\\terminal\\interfaces") do
    fileName                                           = string.gsub(fileName, "/", ".")
    -- Remove .lua
    fileName                                           = string.gsub(fileName, "%.lua$", "")
    local interfaceToAdd                               = require(fileName)
    validInterfaces[interfaceToAdd.name]               = interfaceToAdd
    validInterfaces[interfaceToAdd.name].originalItems = {}
    for index, value in ipairs(interfaceToAdd.items) do
        table.insert(validInterfaces[interfaceToAdd.name].originalItems, value)
    end
    originalInterfaces[interfaceToAdd.name] = interfaceToAdd
end
for index, value in ipairs(types.ESM4Terminal.records) do
    local newTerm                           = esm4Terminal.addTerminal(value)

    validInterfaces[value.id]               = newTerm
    validInterfaces[value.id].originalItems = {}
    for index, value in ipairs(newTerm.items) do
        table.insert(validInterfaces[newTerm.name].originalItems, value)
    end
end
for index, value in ipairs(types.ESM4Note.records) do
    local newTerm                           = esm4Terminal.addTerminal(value)

    validInterfaces[value.id]               = newTerm
    validInterfaces[value.id].originalItems = {}
    for index, value in ipairs(newTerm.items) do
        table.insert(validInterfaces[newTerm.name].originalItems, value)
    end
end
validInterfaces[interfaceTest.name] = interfaceTest

local previousTerminal
-- Active interface and texture setup
local activeInterface = nil
local baseTexture = ui.texture { path = "textures\\terminalfull.png" }
local lineTexture = ui.texture { path = "textures\\terminalheader.png" }



local borderTexture = ui.texture { path = "textures\\terminal_border.png" }
local normalTextColor = settings:get("backgroundColor")
local selectedTextColor = settings:get("foregroundColor")
local terminalHeight = 600
local terminalWidth = 800
local typeLine = ""
local subMenuTree = {}
local typeMode = false
-- UI element variables


local function replaceVariables(str, variables)
    local globalStorageId = validInterfaces[activeInterface].globalStorage
    return string.gsub(str, "%[@(.-)%]", function(var)
        local ret = variables[var] or ""
        if globalStorageId then
            if not savedStorages[globalStorageId] then
                savedStorages[globalStorageId] = storage.globalSection(validInterfaces[activeInterface].globalStorage)
            end
            local getValue = savedStorages[globalStorageId]:get(var)
            if getValue then
                ret = tostring(getValue)
            end
        end
        return ret
    end)
end

local element
local borderElement
local bodyItems

local currentLine = 0

local maxLine = 0
local maxItems = 0
local startModifer = 0
-- Function to create a text UI element
local function addItemToInterface(data)
    local interfaceName = data.interfaceName
    local item = data.item
    if validInterfaces[interfaceName] then
        table.insert(validInterfaces[interfaceName].items, item)
        print("Added item")
    end
end
local function resetInterface(name)
    validInterfaces[name].items = {}
    for index, value in ipairs(validInterfaces[name].originalItems) do
        table.insert(validInterfaces[name].items, value)
    end
end
local function createTextElement(text, position, anchor, textColor, textSize)
    return {
        type = ui.TYPE.Text,
        props = {
            position = position,
            anchor = anchor,
            text = replaceVariables(text, myVariables),
            textSize = textSize or 24,
            textColor = textColor,
        }
    }
end

local function createTextEditElement(text, position, anchor, textColor, textSize)
    return {
        type = ui.TYPE.TextEdit,
        props = {
            position = position,
            anchor = anchor,
            textSize = textSize or 24,
            textColor = textColor,
            text = text,
            size = util.vector2(terminalWidth - 40, 20),
        },
        events = {
            textChanged = async:callback(function(text, layout)
                typeLine = (text)
            end),
        }
    }
end

-- Function to show the Terminal Test interface


local filteredItems
local function drawTerminal()
    local i = 1
    bodyItems = {}

    -- Background setup
    local background = {
        {
            type = ui.TYPE.Image,
            props = {
                resource = baseTexture,
                size = util.vector2(terminalWidth, terminalHeight),
                color = normalTextColor
            }
        }
    }

    -- Adding header, info, and items to the background
    local function addItems(items, startPosition, anchor)
        for _, item in ipairs(items) do
            local position = util.vector2(startPosition, 30 * i)
            table.insert(bodyItems, createTextElement(item, position, anchor, selectedTextColor))
            i = i + 1
        end
    end

    addItems(validInterfaces[activeInterface].header, 400, util.vector2(0.5, 0))
    addItems(validInterfaces[activeInterface].info, 20, util.vector2(0.0, 0))

    -- Separator line
    table.insert(bodyItems,
        createTextElement("_______________________________________________________", util.vector2(20, 30 * i),
            util.vector2(0, 0), selectedTextColor))
    i = i + 1

    -- Adding items with selection logic
    local startLine = i
    filteredItems = {}
    local matchingItems = {}
    if validInterfaces[activeInterface].filterMode and typeLine ~= "" then
        for _, value in ipairs(validInterfaces[activeInterface].items) do
            if string.find(value.text:lower(), typeLine:lower()) ~= nil then
                table.insert(matchingItems, value)
            end
        end
    else
        matchingItems = validInterfaces[activeInterface].items
    end

    -- Then, iterate over the filtered list and check against the startModifier
    filteredItems = {}
    for index, value in ipairs(matchingItems) do
        if index >= startModifer then
            table.insert(filteredItems, value)
        end
    end
    if previousTerminal then
        table.insert(filteredItems, { text = "Back", goBack = true })
    end

    -- Calculate the maximum number of items that can be displayed
    local availableSpace = terminalHeight - 50 - (30 * startLine)
    maxItems = math.floor(availableSpace / 30)

    -- Display the filtered items
    for index, item in ipairs(filteredItems) do
        if index > maxItems then break end
        local color = (i == currentLine + startLine) and normalTextColor or selectedTextColor
        if typeMode then
            color = selectedTextColor
        end
        local position = util.vector2(20, 30 * i)
        table.insert(bodyItems, createTextElement("> " .. item.text, position, util.vector2(0, 0), color))
        i = i + 1
    end

    -- Filling space to maintain layout
    while ((30 * i) < terminalHeight - 50) do
        i = i + 1
    end

    -- Typing line
    local typeLineColor = typeMode and normalTextColor or selectedTextColor
    table.insert(bodyItems,
        createTextElement("> " .. typeLine, util.vector2(20, 30 * i), util.vector2(0, 0), typeLineColor))

    -- Adding the line texture

    local overlayLine = typeMode and i or currentLine + startLine
    table.insert(background, {
        type = ui.TYPE.Image,
        props = {
            resource = lineTexture,
            size = util.vector2(terminalWidth, 30),
            position = util.vector2(0, 30 * overlayLine),
            color = selectedTextColor
        }
    })

    -- Inserting all items into the background
    for _, value in ipairs(bodyItems) do
        table.insert(background, value)
    end

    -- Border element
    if not borderElement then
        local borderItems = {
            {
                type = ui.TYPE.Image,
                props = {
                    resource = borderTexture,
                    size = util.vector2(1880 * 0.55, 1350 * 0.55)
                }
            }
        }
        borderElement = ui.create {
            layer = 'Windows',
            type = ui.TYPE.Container,
            content = ui.content(borderItems),
            props = {
                horizontal = false,
                arrange = ui.ALIGNMENT.Center,
                align = ui.ALIGNMENT.Center,
                relativePosition = util.vector2(0.5, 0.5),
                anchor = util.vector2(0.5, 0.5),
                autoSize = true,
            },
        }
    end

    -- Main UI element
    if element then
        element:destroy()
    end
    element = ui.create {
        layer = 'Windows',
        type = ui.TYPE.Container,
        content = ui.content(background),
        events = {
            onKeyPress = async:callback(function(key, layout)
                -- Key press handling logic here
            end),
        },
        props = {
            horizontal = false,
            arrange = ui.ALIGNMENT.Center,
            align = ui.ALIGNMENT.Center,
            relativePosition = util.vector2(0.5, 0.5),
            anchor = util.vector2(0.5, 0.5),
            autoSize = true,
            size = util.vector2(terminalWidth, terminalHeight)
        },
    }
end

local function switchTerminal(terminalName, parentTerminal)
    if terminalName then
        activeInterface = terminalName
    else
        error("No terminal name")
        --  activeInterface = interfaceTest.name
    end
    typeLine = ""
    currentLine = 0
    startModifer = 0
    maxItems = 0
    validInterfaces[activeInterface].items = {}
    for index, value in ipairs(validInterfaces[activeInterface].originalItems) do
        table.insert(validInterfaces[activeInterface].items, value)
    end
    if validInterfaces[activeInterface].onAppear ~= nil then
        validInterfaces[activeInterface].onAppear(validInterfaces[activeInterface])
    end
    drawTerminal()
end
local function openTerminal(terminalName, parentTerminal)
    subMenuTree = {}
    previousTerminal = nil
    I.UI.setMode(I.UI.MODE.Interface, { windows = {} })
    switchTerminal(terminalName)
end

local function onKeyPress(key)
    if not element then return end
    if not typeMode then
        if key.code == input.KEY.UpArrow then
            -- Scroll up within the visible list
            if currentLine > 0 then
                currentLine = currentLine - 1
                drawTerminal()
                -- Scroll up in the overall list
            elseif startModifer > 0 then
                startModifer = startModifer - 1
                drawTerminal()
            end
        elseif key.code == input.KEY.DownArrow then
            -- Scroll down within the visible list
            print(maxItems)
            if currentLine < maxItems - 1 and filteredItems[currentLine + 1] ~= nil then
                currentLine = currentLine + 1
                drawTerminal()
                -- Scroll down in the overall list
            elseif filteredItems[currentLine + 2] ~= nil then
                startModifer = startModifer + 1
                drawTerminal()
            end
        elseif key.code == input.KEY.Enter then
            local activeItem = filteredItems[currentLine + 1]
            if not activeItem then
                return
            end
            local output = activeItem.output
            if output then
                typeLine = output
            end
            if activeItem.event then
                self:sendEvent(activeItem.event, {
                    text = activeItem.text,
                    menu = activeInterface,
                    data = activeItem
                        .data
                })
            end
            if activeItem.functions then
                for index, func in ipairs(activeItem.functions) do
                    func({
                        text = activeItem.text,
                        menu = activeInterface,
                        data = activeItem
                            .data
                    })
                end
            end
            if activeItem.subMenu then
                previousTerminal = activeInterface
                table.insert(subMenuTree, previousTerminal)
                switchTerminal(activeItem.subMenu)
            elseif activeItem.goBack and previousTerminal then
                currentLine = 0
                activeInterface = subMenuTree[#subMenuTree]
                if #subMenuTree > 0 then
                    table.remove(subMenuTree, #subMenuTree)
                end
                if #subMenuTree == 0 then
                    previousTerminal = nil
                end
                switchTerminal(activeInterface)
            end
            if activeItem.closeTerminal then
                element:destroy()
                borderElement:destroy()
                borderElement = nil
                element = nil
                I.UI.setMode()
                return
            end

            if activeItem.functions then
                core.sendGlobalEvent("terminalBounceBack", self)
            end
            drawTerminal()
        end
    else
        print(key.symbol)
        local shiftPressed = key.withShift
        local symbol = key.symbol
        if key.code == input.KEY.Backspace then
            typeLine = string.sub(typeLine, 1, -2)
            drawTerminal()
        elseif key.code == input.KEY.Enter then
            typeMode = not typeMode
            drawTerminal()
        elseif key.symbol then
            if shiftPressed then
                symbol = symbol:upper()
            end
            typeLine = typeLine .. symbol
            drawTerminal()
        end
    end
    if key.code == 227 then
        typeMode = not typeMode
        drawTerminal()
    end
end

local function UiModeChanged(data)
    if not data.newMode and element then
        element:destroy()
        borderElement:destroy()
        borderElement = nil
        element = nil
    end
end
local itemsAdded = false
-- Return statement
--I.Terminal.openTerminal
local function terminalBounceBack(data)
    if data and validInterfaces[activeInterface].onBounceBack then
        validInterfaces[activeInterface].onBounceBack(validInterfaces[activeInterface], data)
    end
    drawTerminal()
end
return {
    interfaceName = "Terminal",
    interface = { openTerminal = openTerminal, drawTerminal = drawTerminal, addItemToInterface = addItemToInterface, resetInterface = resetInterface },
    engineHandlers = { onKeyPress = onKeyPress, onKeyRelease = onKeyRelease },
    eventHandlers = { openTerminal = openTerminal, terminalBounceBack = terminalBounceBack, drawTerminal = drawTerminal, UiModeChanged = UiModeChanged, addItemToInterface = addItemToInterface, resetInterface = resetInterface, activateMUNDISTerminal = activateMUNDISTerminal, }
}
