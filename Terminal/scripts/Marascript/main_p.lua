-- OpenMW Lua Script: Terminal Interface
local core = require("openmw.core")
local input = require("openmw.input")
local ui = require("openmw.ui")
local async = require("openmw.async")
local util = require("openmw.util")
local self = require("openmw.self")
local vfs = require('openmw.vfs')
local storage = require('openmw.storage')
local I = require("openmw.interfaces")

local function init()
    
for name, func in pairs(core) do
    _G[name] = func
end
end

return{
    init = init
}