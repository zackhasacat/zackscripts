local core = require('openmw.core')
local world = require('openmw.world')
local types = require('openmw.types')
local time = require('openmw_aux.time')
local util = require('openmw.util')
local async = require('openmw.async')
local calendar = require('openmw_aux.calendar')
local caster
local target
local function onPlayerAdded()
    --[[
    local craft = {
          id = "something",
          model = "meshes\\m\\Misc_Potion_Quality_01.nif",
          mwscript = "aa_projectiles",
          name = "caster"
          }
          
    local craftcaster = types.Activator.createRecordDraft(craft)
    local ca = world.createRecord(craftcaster)  
    caster = world.createObject( ca.id , 1 )
    ]]
end
local function checkCreate()
if not caster then

  caster = world.createObject("aa_projectilebox", 1 )
  target = world.createObject( "aa_pos" , 1)
end
end

--[[
local function onUpdate()
         local wo = world.cells
          for a, _ in pairs(wo) do
            local pl = wo[a]:getAll(types.Player) -- this is for defining in what cell(s) the change is wanted
             for m ,_ in pairs(pl) do
              if wo[a]:isInSameSpace(pl[1]) then  -- in same space as wanted object
                caster:teleport( wo[a] , util.transform.move(0,0,200):apply(pl[1].position))
                    -- this will move the wanted item along with the ( player in this case ) 
              end
             end
          end
end
]]

local function start(ind)
   print("11 event is fired at  ", calendar.gameTime() )
   checkCreate()
   --target = world.createObject( "aa_pos" , 1)
   --   print("22 target is created in  ", calendar.gameTime() )
      
   target:teleport( ind.player.cell , ind.castrayhitpos )
      print("33 target is moved in  ", calendar.gameTime() )
      
        caster:teleport( ind.player.cell , util.transform.move(0,0,200):apply(ind.player.position))
        print("44 caster is moved in  ", calendar.gameTime() )

        async:newUnsavableSimulationTimer( 0.2 , function()  
          --types.Actor.activeEffects(ind.player):set( 40, core.magic.EFFECT_TYPE.Levitate)
          world.mwscript.getLocalScript(caster).variables.state = 1
          print("55 cast is made in  ", calendar.gameTime() )
          -- casting the spell via wmscript                
        end)

      --[[
       if target then
         async:newUnsavableSimulationTimer( 0.4 , function()  
            target:remove(1)    -- removing the objects   
            print("66 target is removed in  ", calendar.gameTime())
            --caster:remove(1)
         end)
       end                         
       ]]
end

 
return { 
      eventHandlers = { tele = start },
      engineHandlers = { onPlayerAdded = onPlayerAdded } --, onUpdate = onUpdate }
      } 