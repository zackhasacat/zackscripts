local camera = require("openmw.camera")
local nearby = require("openmw.nearby")
local util = require("openmw.util")
local ui = require('openmw.ui')
local core = require('openmw.core')
local self = require('openmw.self')
local time = require('openmw_aux.time')

local function onKeyPress(key)
            if key.symbol == 'v' then
              
              pitch = -(camera.getPitch() + camera.getExtraPitch())
              yaw = (camera.getYaw() + camera.getExtraYaw())     
        
              local xzLen = math.cos(pitch)
              local bor = util.vector3(
                xzLen * math.sin(yaw), -- x
                xzLen * math.cos(yaw), -- y
                math.sin(pitch)        -- z  -- this all angle calculus is from zackhasacat in discord
                ):normalize()        

               local pos = camera.getPosition()
               local rayC = nearby.castRenderingRay(pos, pos + bor * 20000)
               

                  local p = rayC.hitPos 
                  local ce = self
              
                  core.sendGlobalEvent( "tele" ,{ player = ce, castrayhitpos = p })
                  
            end
end


return { engineHandlers = { onKeyPress = onKeyPress } }