local input = require('openmw.input')
local ui = require('openmw.ui')
local I = require('openmw.interfaces')
local storage = require('openmw.storage')
local key = "kick"
local async = require('openmw.async')
input.registerTrigger({ description = "Kick Stuff", name = "kick", l10n = "kick", key = key, })

local function handleKick()
    ui.showMessage("You kicked!")
end
local modData = storage.playerSection("Settings" .. key)
input.registerTriggerHandler(key, async:callback(handleKick))


I.Settings.registerPage {
    key = key,
    l10n = key,
    name = key,
}
I.Settings.registerGroup {
    key = "Settings" .. key,
    page = key,
    l10n = key,
    name = key,
    permanentStorage = true,
    settings = {
        {
            key = "KickButton",
            renderer = "inputBinding",
            name = "Kick Button",
            description =
            "Kick with this",
            default = "h",
            argument = {
                type = "keyboardPress",
                key = key
            }
        },
    }
}

return {
    engineHandlers = {
        onKeyPress = function(key)
            if key.symbol == modData:get("KickButton") then
                input.activateTrigger(key)
            end
        end
    }
}
