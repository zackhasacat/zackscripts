local storage = require('openmw.storage')
local modStorage = storage.playerSection('AutoLoad')

local function onSave()
    print("Saved.", os.time())
    modStorage:set("saveTime", os.time())
end


return
{
    engineHandlers = {
        onSave = onSave,
    }
}
