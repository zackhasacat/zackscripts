local storage = require('openmw.storage')
local modStorage = storage.playerSection('AutoLoad')

local menu = require('openmw.menu')

local function loadMostRecent()
    local timeDate = modStorage:get("saveTime")
    if not modStorage:get("saveTime") then return end
    timeDate = timeDate - 5
    local range = timeDate + 15
    for folder, saveList in pairs(menu.getAllSaves()) do
        for name, save in pairs(saveList) do
            if math.floor(save.creationTime) > timeDate and math.ceil(save.creationTime) < range then
                print(save.creationTime, "valid", folder, name)
                menu.loadGame(folder, name)
            end
        end
    end
end
local function onConsoleCommand(mode, command)
    if command == "load" then
        loadMostRecent()
    end
end
local function onInit()
    if menu.getState() == menu.STATE.NoGame then
        loadMostRecent()
    end
end

return
{
    engineHandlers = {
        onSave = onSave,
        onInit = onInit,
        onConsoleCommand = onConsoleCommand
    }
}
