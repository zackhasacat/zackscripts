THIS MOD REQUIRES FLIGGERTY'S SLAVE MOD

Slave Mod: Here

Von Slave Market 1.5




Ever been running around with 
your slaves, and some of them get killed, and you're frustrated by the limited number of slaves you can have? Worry no more! 
,
This mod adds over 150 slaves, of all races and genders. They are all located at the Von Slave Market near Vos and Tel Vos. Look for the velothi tower.

Over 50 of the slaves are combat slaves, which are level 15, can carry usually double what the normal slave can, and probably 60 of them could kill vivec.


Combat slaves are 5000 gold, while normal household slaves are 1000 gold.

You will not be able to enter the market(legally) if you have a bounty. Different areas of the slave market are gated with Magical forcefields, 
which will automatically open if you are wearing the security glove, which any guard can give you if you don't have. 

If you achieve a bounty while inside the market, your security gauntlet will no longer work. You will have to find a different way to get through the forcefields.

I added a condition to some of the NPC's idle words. Things like "Wheres that slave" should no longer be said by slaves, which was something that bothered me while using Fliggerty's mod.

This new version adds "command spells", and removes the dependency on Westly's head pack, at the cost of less varied faces.

Command spells are spells you can cast on your slave(only your slaves, not slaves that you have not bought. Additionally, they will not work on non-von slaves, for the sake of compatibility.
If requested, I can release a version that works on all slaves.

They work by the enchantment on the slave bracer, and are useful for commanding multiple slaves at once, or if you just consider yourself to be above talking to slaves.

There are 6 command spells, some of which have different ranges. 

The first range will just work on one slave.

The second range, Area, will work on all slaves that are hit by the area affect spell.

The third range, room, will work on all slaves currently loaded.(generally all the owned slaves in the current cell)


"Freeze" will apply a paralyzing effect to your slave, until the spell is cast on the slave again.

"Claim" will, as the name implies, claim the slave for you, as long as you have a key for it. This is the only spell that works on unclaimed slaves.

"Follow" will tell the slave to follow you.

"Execute" will immediatly kill the slave. This is not illegal, as slaves are your property to destroy if you so desire.

"Wait" will make the slave stay where they are until given other orders.

"Wander" will allow the slave to wander around the area.

The Spells can be bought from any slave master.

In the tower of the Market, you will find a sorcerer. He can summon daedra permenently for you(Until they die).
Scamps are 500 gold, Clannfears are 4000 gold, Daedroths are 5500 gold, Winged Twilights are 8000 gold, a dremora(not lord) is 10000 gold, and a golden saint is 12000 gold.

All daedra have Grumpy's companion scripts, like the slaves.
There is only one mesh for the mod, that is for the forcefield.