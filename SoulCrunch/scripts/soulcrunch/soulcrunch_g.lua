local core = require("openmw.core")
local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local storage = require("openmw.storage")
local I = require('openmw.interfaces')
local function itemHandler(item, actor)
  local soul =  types.Miscellaneous.getSoul(item)
  if soul then
    actor:sendEvent("showUseMessage",item)
    return false
  end

end
I.ItemUsage.addHandlerForType(types.Miscellaneous, itemHandler)

return{
    eventHandlers = {
        EatSoulGem = function (gem)
            gem:remove(1)
            
        end
    }
}