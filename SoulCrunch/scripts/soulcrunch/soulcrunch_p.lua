-- OpenMW Lua Script: Terminal Interface
local core = require("openmw.core")
local input = require("openmw.input")
local ui = require("openmw.ui")
local async = require("openmw.async")
local util = require("openmw.util")
local self = require("openmw.self")
local vfs = require('openmw.vfs')
local types = require('openmw.types')
local storage = require('openmw.storage')
local I = require("openmw.interfaces")
local ambient = require('openmw.ambient')

local settings = require("scripts.soulcrunch.settings")
local messageBox = require("scripts.soulcrunch.MessageBox")
local sRechargeEnchantment = core.getGMST("sRechargeEnchantment")
local sMakeEnchantment = core.getGMST("sMake Enchantment")

local openMessageBox 
local eatText = "Eat"
local winName = "Recharge"
local activatedObj
local function restoreMagicka(enchantSkillLevel, soulValue)
    local alpha = settings.getEnchantSkillMultiplier()--1.5 default
    local beta = settings.getSoulValue()--0.5 default
    local magickaRestored = ((enchantSkillLevel / 100) ^ alpha) * (soulValue * beta)
    return magickaRestored
end
local function eatSoulGem(gem)
    local gemCreature = types.Miscellaneous.getSoul(gem)
    local soulValue = types.Creature.record(gemCreature).soulValue
    local magickaToRestore = restoreMagicka(types.NPC.stats.skills.enchant(self).modified, soulValue)
   -- print("To Restore",magickaToRestore, "Soul value",soulValue)
    local base = types.Actor.stats.dynamic.magicka(self).base
    local newValue = types.Actor.stats.dynamic.magicka(self).current + magickaToRestore
    if newValue > base then
        newValue = base
    end
    core.sendGlobalEvent("EatSoulGem",gem)
    ambient.playSound("restoration hit")
    types.Actor.stats.dynamic.magicka(self).current = newValue
end
local function showUseMessage(obj)
    local soul = types.Miscellaneous.getSoul(obj)
    if soul then
        activatedObj = obj
        local gmstOne = core.getGMST("sDoYouWantTo")
        openMessageBox =   messageBox.showMessageBox(winName, { gmstOne }, { sRechargeEnchantment, sMakeEnchantment, eatText })

        return true
    end
    return false
end
return {
    eventHandlers = {
        UiModeChanged = function(data)
            -- print('LMMUiModeChanged to', data.newMode, '(' .. tostring(data.arg) .. ')')


            if not data.newMode and openMessageBox then
                openMessageBox:destroy()
                openMessageBox = nil
            end
        end,
        ButtonClicked = function(data)
            local name = data.name
            local chosenText = data.text
            print(name, chosenText)
            if name == winName then
                if chosenText == sRechargeEnchantment then
                    I.UI.setMode("Recharge", { target = activatedObj })
                elseif chosenText == sMakeEnchantment then
                    I.UI.setMode("Enchanting", { target = activatedObj })
                elseif chosenText == eatText then
                    ui.showMessage("Yum!")
                    eatSoulGem(activatedObj)
                end
            end
        end,
        eatGem = function(obj)
            local inv = types.Actor.inventory(self):getAll(types.Miscellaneous)
            for index, value in ipairs(inv) do
                if showUseMessage(value) then return end
            end
        end,
        showUseMessage = showUseMessage
    }
}
