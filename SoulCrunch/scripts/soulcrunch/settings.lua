
local I = require("openmw.interfaces")

local storage = require("openmw.storage")
local key = "SettingsSoulCrunch"
local modName = "Soul Crunch"
I.Settings.registerPage {
     key = key,
     l10n = key,
     name = modName,
}
local ret = {}
ret.section = storage.playerSection(key)
I.Settings.registerGroup {
     key = key,
     page = key,
     l10n = key,
     name = modName,
     permanentStorage = true,
     settings = {
          {
               key = "enchantSkillMultiplier",
               renderer = "number",
               name = "Enchant Skill Effectiveness Multiplier",
               default = 1.5,
               description = "This setting adjusts the impact of your Enchant skill level on the amount of magicka restored when consuming soul gems. A higher value increases the significance of your Enchant skill."
          },
          {
               key = "soulValueMultiplier",
               renderer = "number",
               name = "Soul Value to Magicka Conversion Rate",
               default = 0.5,
               description = "This parameter determines how the soul value of consumed soul gems is converted into magicka restoration. Increasing this rate makes soul gems more potent, offering more magicka per gem consumed."
          },
     },
    }
     function ret.getEnchantSkillMultiplier()
        return ret.section:get("enchantSkillMultiplier")
    end
     function ret.getSoulValue()
        return ret.section:get("soulValueMultiplier")
    end

    return  ret