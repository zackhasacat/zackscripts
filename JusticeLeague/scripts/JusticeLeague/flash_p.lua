-- OpenMW Lua Script: Terminal Interface
local core = require("openmw.core")
local input = require("openmw.input")
local ui = require("openmw.ui")
local async = require("openmw.async")
local util = require("openmw.util")
local self = require("openmw.self")
local vfs = require('openmw.vfs')
local types = require('openmw.types')
local storage = require('openmw.storage')
local I = require("openmw.interfaces")
local ambient = require('openmw.ambient')
local anim = require('openmw.animation')
local timeScale = 1
local modifiedSpeed = 25
local speed = 1
local myOriginalSpeedBase
local myOriginalSpeedMod
local function enterFlashMode()
    local worldSpeed = 1 / modifiedSpeed
    if timeScale == worldSpeed then
        return --already slowed
    end
    myOriginalSpeedMod = types.Actor.stats.attributes.speed(self).modifier
    myOriginalSpeedBase = types.Actor.stats.attributes.speed(self).base
    types.Actor.stats.attributes.speed(self).modifier = (myOriginalSpeedMod ) * modifiedSpeed
    types.Actor.stats.attributes.speed(self).base = (myOriginalSpeedBase ) * modifiedSpeed
    core.sendGlobalEvent("JL_setTimeScale", worldSpeed)
    timeScale = worldSpeed
end
local function exitFlashMode()
    local worldSpeed = 1
    if timeScale == worldSpeed then
        return --already fast
    end
    types.Actor.stats.attributes.speed(self).modifier = myOriginalSpeedMod
    types.Actor.stats.attributes.speed(self).base = myOriginalSpeedBase
    core.sendGlobalEvent("JL_setTimeScale", worldSpeed)
    timeScale = worldSpeed
end
return {
    engineHandlers = {
        onSave = function()
            exitFlashMode()
        end,
        onUpdate = function()
            if timeScale < 1 then
                anim.setSpeed(self, anim.getActiveGroup(self, 4), modifiedSpeed)
                anim.setSpeed(self, anim.getActiveGroup(self, 3), modifiedSpeed)
                anim.setSpeed(self, anim.getActiveGroup(self, 2), modifiedSpeed)
                anim.setSpeed(self, anim.getActiveGroup(self, 1), modifiedSpeed)
            else

                anim.setSpeed(self, anim.getActiveGroup(self, 4), 1)
                anim.setSpeed(self, anim.getActiveGroup(self, 3), 1)
                anim.setSpeed(self, anim.getActiveGroup(self, 2), 1)
                anim.setSpeed(self, anim.getActiveGroup(self, 1), 1)
            end
        end,
        onKeyPress = function(key)
            if not core.isWorldPaused() then
                if key.code == input.KEY.H then
                    if timeScale < 1 then
                        exitFlashMode()
                    else
                        enterFlashMode()
                    end
                end
            end
        end

    }
}
