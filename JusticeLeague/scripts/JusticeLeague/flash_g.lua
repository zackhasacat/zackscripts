
local core = require("openmw.core")
local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local storage = require("openmw.storage")
local I = require('openmw.interfaces')
return {
    eventHandlers = {
        JL_setTimeScale = function (scale)
            world.setSimulationTimeScale(scale)
        end
    }
}