General:
Days as a Vampire: The number of days that have passed since becoming afflicted with Lycanthropy.
Days Passed: The number of days passed since the game started.(DONE)
Hours Slept: The number of hours slept.
Hours Waiting: The number of hours waited. You can wait almost anywhere, but only if there are no enemies nearby.
Gold Found: The grand total of gold coins found.
Most Gold Carried: The most gold carried at any one time.
Skill Increases: The number of times a skill increases. 
Ingredients Eaten: The number of alchemy ingredients eaten.
Training Sessions: The number of times you have received training from a skill trainer.(DONE)
Books read: The number of books you have read. Simply activating a book will count as reading it.
Barters: The act of one transaction, either buying or selling, with a merchant counts as a barter.
Diseases Contracted: The total number of diseases you have contracted. There are 8 different diseases, all of which can be contracted multiple times.

Quests:
Quests Completed: The grand total of all quests completed.
Main Quests Completed: The number of main quests completed. TBD in total, including TBD optional quests.


Combat:
People Killed: The number of NPCs, both hostile and non-hostile, you have killed.
Animals Killed: The number of animals you have killed. Also includes all types of conjured familiars.
Creatures Killed: The number of creatures you have killed. Animals or undead are not included.
Daedra Killed: The number of Daedra you have killed. Includes Dremora, elemental atronachs and all types of summoned Daedra.
Favorite Weapon: The weapon that is most often used. Limited to melee weapons or bows.
Sneak Attacks: 	The number of sneak attacks performed. A sneak attack is the act of striking a target while remaining undetected.

Magic:
Spells Learned: The number of spells you have learned. Most spells are learned from Spell Merchants.
Favorite Spell: The spell that is most often used.
Favorite School: The school of magic that is most often used. The six schools are Alteration, Conjuration, Destruction, Mysticism, Illusion, and Restoration.

Crafting:
Soul Gems Used: The number of soul gems used to recharge an enchanted weapon only. Soul gems consumed via Enchanting do not count.
Souls Trapped: The number of souls trapped within a soul gem. Souls can only be trapped with the soul trap spell or with a weapon that bears the soul trap enchantment.
Magic Items Made: The number of items enchanted at an arcane enchanter.
Potions Made: The number of potions made.
Potions Used: The number of potions consumed.
Ingredients Harvested: The number of alchemy ingredients harvested from plants.

Crime:
Total Lifetime Bounty: The grand total of all bounties, paid and unpaid, placed upon you. 
Largest Bounty: The largest total bounty you have ever accrued at any one time.
Locks picked: The number of locked doors or containers successfully unlocked via lockpicking.
Items Pickpocketed: The number of items you have pickpocketed from an NPC's inventory.
Times Jailed: The number of times you have been sent to jail.
Days Jailed: The number of days you spend in jail serving your sentence.
Fines Paid: The total amount of gold paid for any bounties you incur.
Fines forgiven: The amount of fines removed via Thieves guild(need better name)
Items Stolen:The number of owned items you have added to your inventory.
Assaults: The number of separate instances of performing an unprovoked attack upon a non-hostile NPC. Typically results in a bounty if witnessed.
Murders: The number of non-hostile NPC deaths you are responsible for. Followers and summoned creatures who kill on your behalf also count towards this statistic.

