## General

  Statistic                  Notes
  -------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Mauls                      The number of NPCs or creatures killed with a melee attack while in the form of a [werewolf](Skyrim:Lycanthropy#Beast_Form "wikilink") or [Vampire Lord](Skyrim:Vampire_Lord "wikilink"). Special finishing moves performed on a target are also counted.
  Werewolf Transformations   The number of times you have used the [Beast Form](Skyrim:Lycanthropy#Beast_Form "wikilink") power.
  Days as a Werewolf         The number of days that have passed since becoming afflicted with [Lycanthropy](Skyrim:Lycanthropy "wikilink").
  Necks Bitten               The number of times you have [fed upon](Skyrim:Vampirism#Feeding "wikilink") an NPC as a [vampire](Skyrim:Vampirism "wikilink").
  Days as a Vampire          The number of days that have passed since becoming afflicted with [Sanguinare Vampiris](Skyrim:Sanguinare_Vampiris "wikilink").
  Locations Discovered       The number of [locations](Skyrim:Places "wikilink") you have discovered. Only locations that are marked on the game map count towards this statistic.
  Dungeons Cleared           The number of dungeons [cleared](Skyrim:Dungeons#Clearing "wikilink"). A dungeon is typically cleared once the boss has been defeated. There are a total of 186 locations that can be cleared, however once a location [respawns](Skyrim:Respawning "wikilink"), it may be cleared again. This means the statistic displayed may go well beyond the dungeon limit.
  Days Passed                The number of days passed since the game started. The game [starts](Skyrim:Unbound "wikilink") on [Morndas, the 17th of Last Seed](Lore:Calendar#Skyrim_Calendar "wikilink"), 4E 201.
  Hours Slept                The number of hours [slept](Skyrim:Sleeping "wikilink"). You can only sleep in an unowned bed and if there are no enemies nearby.
  Hours Waiting              The number of hours [waited](Skyrim:Sleeping#Waiting "wikilink"). You can wait almost anywhere, but only if there are no enemies nearby.
  Standing Stones Found      The number of [Standing Stones](Skyrim:Standing_Stone "wikilink") discovered. There are 13 Standing Stones throughout Skyrim, however visiting a previously discovered standing stone will add to this statistic, making it possible to go well over the original 13. [All-Maker Stones](Skyrim:All-Maker_Stones "wikilink") included in [Dragonborn](Skyrim:Dragonborn "wikilink") are not counted towards this total.
  Gold Found                 The grand total of [gold](Skyrim:Gold#Currency "wikilink") coins found.
  Most Gold Carried          The most gold carried at any one time.
  Chests Looted              The number of [chests](Skyrim:Containers#Chest "wikilink") opened.
  Skill Increases            The number of times a skill increases. There are 18 [skills](Skyrim:Skills "wikilink") in total, each of which can level up to 100. With the [1.9 patch](Skyrim:Patch#Version_1.9 "wikilink"), which includes a [skill reset](Skyrim:Skills#Legendary_Skills "wikilink") option, there is no limit to how many times a skill can be increased.
  Skill Books Read           The number of [skill books](Skyrim:Skill_Books "wikilink") you have read and gained a skill increase from. There are 5 different skill books for each skill and a total of 90 books.
  Food Eaten                 The number of [food](Skyrim:Food "wikilink") items consumed.
  Training Sessions          The number of times you have received training from a [skill trainer](Skyrim:Trainers "wikilink"). There are 59 trainers in total, including add-ons.
  Books Read                 The number of [books](Skyrim:Books "wikilink") you have read. Simply activating a book will count as reading it. Skill books are not counted towards this statistic, but spell tomes are counted (even if the spell is already learned without using the spell tome).
  Horses Owned               The number of [horses](Skyrim:Horses "wikilink") you have obtained. Horses are available for purchase at any [stable](Skyrim:Stables "wikilink") for 1000 gold, in the five major holds and the three Hearthfire houses. [Shadowmere](Skyrim:Shadowmere "wikilink") is also counted if obtained, however [Arvak](Skyrim:Arvak "wikilink") and [Frost](Skyrim:Frost "wikilink") are not. From the Creation Club, the horse from the [Farming](Skyrim:Farming "wikilink") creation counts, as does the Reindeer from the [Saturalia Holiday Pack](Skyrim:Saturalia_Holiday_Pack "wikilink")
  Houses Owned               The number of [houses](Skyrim:Houses "wikilink") you have bought. Only the 5 purchasable houses in the main cities are counted.
  Stores Invested In         The number of [merchants](Skyrim:Merchants "wikilink") who you have invested gold into. Requires the [Investor](Skyrim:Investor "wikilink") perk in the [Speech](Skyrim:Speech "wikilink") skill tree.
  Barters                    The act of one transaction, either buying or selling, with a [merchant](Skyrim:Merchants "wikilink") counts as a barter.
  Persuasions                The number of times a successful [persuasion](Skyrim:Persuasion "wikilink") is performed.
  Bribes                     The number of times a successful [bribe](Skyrim:Bribe "wikilink") is performed.
  Intimidations              The number of times a successful [intimidation](Skyrim:Intimidate "wikilink") is performed.
  Diseases Contracted        The total number of [diseases](Skyrim:Disease "wikilink") you have contracted. There are 8 different diseases, all of which can be contracted multiple times.

## Quest

  scope=\"col\" style=\"\"\|Statistic                Notes
  -------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Dragonborn Quests Completed `{{DB}}`{=mediawiki}   The number of quests completed in the [Dragonborn](Skyrim:Dragonborn "wikilink") [main questline](Skyrim:Dragonborn_Quests "wikilink"). 7 in total.
  Dawnguard Quests Completed `{{DG}}`{=mediawiki}    The number of side quests completed for the [Dawnguard](Skyrim:Dawnguard_(faction)#Quests "wikilink"). 20 in total.
  Quests Completed                                   The grand total of [all quests](Skyrim:Quests "wikilink") completed.
  Misc Objectives Completed                          The number of [miscellaneous quests](Skyrim:Miscellaneous_Quests "wikilink") completed. These are not classed as full quests and many are repeatable.
  Main Quests Completed                              The number of [main quests](Skyrim:Main_Quest "wikilink") completed. 19 in total, including two optional quests.
  Side Quests Completed                              The number of [side quests](Skyrim:Side_Quests "wikilink") completed.
  The Companions Quests Completed                    The number of [primary](Skyrim:Companions#Primary_Quests "wikilink") and side quests completed for the [Companions](Skyrim:Companions "wikilink"). 9 in total. Repeatable [radiant quests](Skyrim:Companions#Radiant_Quests "wikilink") are not included.
  College of Winterhold Quests Completed             The number of [primary](Skyrim:College_of_Winterhold_(faction)#Primary_Quests "wikilink") and [side quests](Skyrim:College_of_Winterhold_(faction)#Optional_Quests "wikilink") completed for the [College of Winterhold](Skyrim:College_of_Winterhold_(faction) "wikilink"). 18 in total.
  Thieves\' Guild Quests Completed                   The number of [primary](Skyrim:Thieves_Guild_(faction)#Primary_Quests "wikilink") quests and [special jobs](Skyrim:Thieves_Guild_(faction)#Special_Job_Quests "wikilink") completed for the [Thieves\' Guild](Skyrim:Thieves_Guild_(faction) "wikilink"). 19 in total.`{{vn}}`{=mediawiki} See [bugs](#Bugs "wikilink").
  The Dark Brotherhood Quests Completed              The number of [primary quests](Skyrim:Dark_Brotherhood#Primary_Quests "wikilink") and [contracts](Skyrim:Dark_Brotherhood#Contract_Quests "wikilink") completed for the [Dark Brotherhood](Skyrim:Dark_Brotherhood "wikilink"). 26 in total, but only 2 possible if you [destroy the Dark Brotherhood](Skyrim:Destroy_the_Dark_Brotherhood! "wikilink"). Repeatable [radiant quests](Skyrim:Dark_Brotherhood#Radiant_Quests "wikilink") are not included.
  Civil War Quests Completed                         The number of quests completed for either side of the [civil war](Skyrim:Civil_War "wikilink") questline. 9 in total.
  Daedric Quests Completed                           The number of [Daedric quests](Skyrim:Daedric_Quests "wikilink") completed. There are 15 in total. Daedric quests are not considered side quests.
  Questlines Completed                               Questlines are typically classed as a series of quests for a joinable [faction](Skyrim:Factions "wikilink"), but it also includes the main questline of Skyrim. 6 in total.

## Combat

  Statistic             Notes
  --------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  People Killed         The number of NPCs, both hostile and non-hostile, you have killed.
  Animals Killed        The number of [animals](Skyrim:Animals "wikilink") you have killed. Also includes all types of [conjured familiars](Skyrim:Conjure_Familiar "wikilink").
  Creatures Killed      The number of [creatures](Skyrim:Monsters "wikilink") you have killed. [Animals](Skyrim:Animals "wikilink") or [undead](Skyrim:Undead "wikilink") are not included.
  Undead Killed         The number of [undead](Skyrim:Undead "wikilink") you have killed. Includes [draugr](Skyrim:Draugr "wikilink"), [ghosts](Skyrim:Ghost "wikilink"), [skeletons](Skyrim:Skeleton "wikilink") and [vampires](Skyrim:Vampire "wikilink") and all types of [conjured](Skyrim:Conjuration#Spells "wikilink") undead, such as [bonemen](Skyrim:Conjure_Boneman "wikilink").
  Daedra Killed         The number of Daedra you have killed. Includes [Dremora](Skyrim:Dremora "wikilink"), elemental [atronachs](Skyrim:Daedra "wikilink") and all types of summoned Daedra.
  Automatons Killed     The number of [Dwarven automatons](Skyrim:Dwarven_Automatons "wikilink") you have defeated. Includes [Dwarven spiders](Skyrim:Dwarven_Spider "wikilink"), [spheres](Skyrim:Dwarven_Sphere "wikilink"), [centurions](Skyrim:Dwarven_Centurion "wikilink") and [ballistae](Skyrim:Dwarven_Ballista "wikilink").
  Favorite Weapon       The [weapon](Skyrim:Weapons "wikilink") that is most often used. Limited to melee weapons or bows.
  Critical Strikes      The number of critical strikes performed. A critical strike is a special attack that, if triggered, deals more damage than a normal blow. It can be performed in both regular combat or in stealth, and can only be achieved with melee weapons or bows. A successful [sneak attack](Skyrim:Sneak#Sneak_Attacks "wikilink") will always result in a critical strike.
  Sneak Attacks         The number of [sneak attacks](Skyrim:Sneak#Sneak_Attacks "wikilink") performed. A sneak attack is the act of striking a target while remaining [undetected](Skyrim:Sneak#Remaining_Undetected "wikilink").
  Backstabs             The number of backstabs performed. A backstab is the act of performing a sneak attack with a one-handed weapon on a target facing away from you. It does not require the [Backstab](Skyrim:Sneak#Backstab "wikilink") perk to trigger.
  Weapons Disarmed      The number of NPCs you have successfully disarmed. Disarms can be achieved with either the [Disarm](Skyrim:Disarm "wikilink") shout directed at a target or by performing a [power bash](Skyrim:Block#Bashing "wikilink") attack with a shield, weapon or torch and the [Disarming Bash](Skyrim:Disarming_Bash "wikilink") perk.
  Brawls Won            The number of [brawls](Skyrim:Brawl "wikilink") you have won against [various NPCs](Skyrim:Fight!_Fight! "wikilink").
  Bunnies Slaughtered   The number of [rabbits](Skyrim:Rabbit "wikilink") you have killed.

## Magic

  Statistic                 Notes
  ------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Spells Learned            The number of [spells](Skyrim:Spells "wikilink") you have learned. A spell tome must be read in order to learn a spell.`{{vn}}`{=mediawiki}
  Favorite Spell            The spell that is most often used.
  Favorite School           The [school of magic](Skyrim:Magic_Overview "wikilink") that is most often used. The five schools are [Alteration](Skyrim:Alteration "wikilink"), [Conjuration](Skyrim:Conjuration "wikilink"), [Destruction](Skyrim:Destruction "wikilink"), [Illusion](Skyrim:Illusion "wikilink"), and [Restoration](Skyrim:Restoration "wikilink").
  Dragon Souls Collected    Souls collected from slain [dragons](Skyrim:Dragon "wikilink"). Souls gain from Miraak in Dragonborn are not counted.`{{vn}}`{=mediawiki}
  Words of Power Learned    The number of Words of Power learned. A Word of Power is defined as one of the three words that make up a full [Dragon Shout](Skyrim:Dragon_Shouts "wikilink"). They can be learned from either [word walls](Skyrim:Word_Wall "wikilink") or individuals such as the [Greybeards](Skyrim:Greybeards "wikilink") or [Durnehviir](Skyrim:Durnehviir_(dragon) "wikilink").
  Words of Power Unlocked   The number of Words of Power unlocked. A Word of Power can only be unlocked when a Dragon Soul is spent to unlock it.
  Shouts Learned            The number of full [Dragon Shouts](Skyrim:Dragon_Shouts "wikilink") learned. A full shout is defined by learning all three Words of Power of a shout.
  Shouts Mastered           The number of full Dragon Shouts unlocked. Shout mastery is defined by learning and unlocking all three Words of Power of a shout.
  Times Shouted             The number of times you have used a Dragon Shout. Using any one of the three available levels of a shout counts as one instance.
  Favorite Shout            The shout that is most often used.

## Crafting

  scope=\"col\" style=\"\"\|Statistic   Notes
  ------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Soul Gems Used                        The number of [soul gems](Skyrim:Soul_Gems "wikilink") used to recharge an enchanted weapon only. Soul gems consumed via [Enchanting](Skyrim:Enchanting "wikilink") do not count.
  Souls Trapped                         The number of souls trapped within a soul gem. Souls can only be trapped with the [soul trap](Skyrim:Soul_Trap_(spell) "wikilink") spell or with a weapon that bears the [soul trap enchantment](Skyrim:Soul_Trap_(effect) "wikilink").
  Magic Items Made                      The number of items [enchanted](Skyrim:Enchanting "wikilink") at an [arcane enchanter](Skyrim:Arcane_Enchanter "wikilink").
  Weapons Improved                      The number of weapons [tempered](Skyrim:Smithing#Improving_Items "wikilink") at a [grindstone](Skyrim:Grindstone "wikilink").
  Weapons Made                          The number of [weapons](Skyrim:Weapons "wikilink") created at a [forge](Skyrim:Forge "wikilink").
  Armor Improved                        The number of [armor](Skyrim:Armor "wikilink") pieces tempered at a [workbench](Skyrim:Workbench "wikilink").
  Armor Made                            The number of armor pieces created at a forge.
  Potions Mixed                         The number of [potions](Skyrim:Potions "wikilink") made at an [alchemy lab](Skyrim:Alchemy_Lab "wikilink"). Strangely, food you cook will also increase this stat.
  Potions Used                          The number of potions consumed.
  Poisons Mixed                         The number of [poisons](Skyrim:Poisons "wikilink") made at an alchemy lab.
  Poisons Used                          The number of times a poison is applied to a weapon. Poisons placed in the pockets of NPCs with the [Poisoned](Skyrim:Poisoned "wikilink") perk do not count towards this statistic.
  Ingredients Harvested                 The number of alchemy [ingredients](Skyrim:Ingredients "wikilink") harvested from plants or collected from certain [passive creatures](Skyrim:Passive_Creatures "wikilink").`{{vn}}`{=mediawiki}
  Ingredients Eaten                     The number of alchemy ingredients eaten.
  Nirnroots Found                       The number of [nirnroot](Skyrim:Nirnroot "wikilink") plants harvested. They can only be found growing in the wild, typically near water. [Crimson nirnroot](Skyrim:Crimson_Nirnroot "wikilink") does not count towards this statistic.
  Wings Plucked                         The number of wings plucked from certain [winged insects](Skyrim:Passive_Creatures#Insects "wikilink"), such as butterflies. Plucking wings always yields two samples, thus this statistic will be an even number.

## Crime

  Statistic                     Notes
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Total Lifetime Bounty         The grand total of all [bounties](Skyrim:Crime#Bounties "wikilink"), paid and unpaid, placed upon you. The [Dark Brotherhood](Skyrim:Dark_Brotherhood "wikilink") quest [To Kill an Empire](Skyrim:To_Kill_an_Empire "wikilink") incurs an unavoidable 1500 gold bounty.
  `<Hold Name>`{=html} Bounty   The bounty that you have accrued in a specific [hold](Skyrim:Holds "wikilink"). This statistic will only appear if you have incurred a bounty and multiple holds can be listed.
  Tribal Orcs Bounty            The bounty that you have accrued within an [Orc stronghold](Skyrim:Orc_Strongholds "wikilink"). This statistic will only appear if you have incurred a bounty. All 4 strongholds track their bounties collectively.
  Largest Bounty                The largest total bounty you have ever accrued at any one time. The largest possible bounty for a single crime is murder or transforming into a [werewolf](Skyrim:Lycanthropy "wikilink") or [Vampire Lord](Skyrim:Vampire_Lord "wikilink"), which yields a 1000 gold bounty.
  Locks Picked                  The number of locked doors or containers successfully unlocked via [lockpicking](Skyrim:Lockpicking "wikilink").
  Pockets Picked                The number of separate instances of performing a [pickpocket](Skyrim:Pickpocket "wikilink") on an NPC.
  Items Pickpocketed            The number of items you have pickpocketed from an NPC\'s inventory.
  Times Jailed                  The number of times you have been [sent to jail](Skyrim:Crime#Going_to_Jail "wikilink").
  Days Jailed                   The number of days you spend in jail [serving your sentence](Skyrim:Crime#Serving_Your_Sentence "wikilink").
  Fines Paid                    The total amount of gold paid for any [bounties](Skyrim:Crime#Paying_Bounty "wikilink") you incur.
  Jail Escapes                  The number of times you have [escaped](Skyrim:Crime#Escaping_Jail "wikilink") jail, including [The Chill](Skyrim:The_Chill "wikilink"). Escaping from [Cidhna Mine](Skyrim:Cidhna_Mine "wikilink") during [No One Escapes Cidhna Mine](Skyrim:No_One_Escapes_Cidhna_Mine "wikilink") quest does not count towards this statistic.
  Items Stolen                  The number of owned items you have added to your inventory. Can later result in being attacked by a group of [hired thugs](Skyrim:Hired_Thug "wikilink").
  Assaults                      The number of separate instances of performing an unprovoked attack upon a non-hostile NPC. Typically results in a bounty if witnessed.
  Murders                       The number of non-hostile NPC deaths you are responsible for. [Followers](Skyrim:Followers "wikilink") and [summoned creatures](Skyrim:Conjure#Spells "wikilink") who kill on your behalf also count towards this statistic.
  Horses Stolen                 The number of owned [horses](Skyrim:Horses "wikilink") mounted. The horse need not actually be stolen to increase this statistic.
  Trespasses                    The number of times you have trespassed. A trespass occurs when entering a locked property, such as a shop, after it has closed.
