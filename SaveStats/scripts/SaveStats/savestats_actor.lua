local I = require("openmw.interfaces")
local util = require("openmw.util")
local self = require("openmw.self")
local types = require("openmw.types")
local nearby = require("openmw.nearby")
local isFightingPlayerTrue = false
local function isFightingPlayer()
    isFightingPlayerTrue = false
    local func = function(param)
        if param.target and param.target.type == types.Player and param.type == "Combat"
        then
            isFightingPlayerTrue = true
        end
    end
    I.AI.forEachPackage(func)
    return isFightingPlayerTrue
end
local function onUpdate()
    local isFighting = isFightingPlayer()

end
return {
    eventHandlers = {
        Died = function()
            if isFightingPlayerTrue then
                print("Player killed me!")
                nearby.players[1]:sendEvent("killedMe",self)
            end
        end,
        StartAIPackage = function (data)
         
        end
    },
    engineHandlers = {
         onUpdate = onUpdate
    }
}