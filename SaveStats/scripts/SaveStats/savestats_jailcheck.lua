local core = require("openmw.core")
local I = require("openmw.interfaces")
local jailTime
local timeInJail = 0
return
{
    eventHandlers = {
        UiModeChanged = function(data)
            if data.newMode == "Jail" then
                jailTime = core.getGameTime()
            elseif not data.newMode and jailTime then
                print("Jail start", jailTime)
                print("Jail end", core.getGameTime())
                local jailDuration = core.getGameTime() - jailTime
                jailTime = nil
                I.SaveStats.mod("TimesJailed",1)
                I.SaveStats.mod("TimeJailed",jailDuration)
                I.SaveStats.updateValues()
                --1000 bounty:
                -- Jail start    119621.22802734
                --Jail end    206021.22802734
            end
        end,
    }
}
