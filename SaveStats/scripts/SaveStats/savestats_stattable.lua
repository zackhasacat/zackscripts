return

{
    General = {
        DaysAsAVampire = {
            Name = "Days as a Vampire",
            Description = "The number of days that have passed since becoming afflicted with Lycanthropy."
        },
        DaysPassed = {
            Name = "Days Passed",
            Description = "The number of days passed since the game started."
        },
        HoursSlept = {
            Name = "Hours Slept",
            Description = "The number of hours slept."
        },
        HoursWaiting = {
            Name = "Hours Waiting",
            Description =
            "The number of hours waited. You can wait almost anywhere, but only if there are no enemies nearby."
        },
        GoldFound = {
            Name = "Gold Found",
            Description = "The grand total of gold coins found."
        },
        MostGoldCarried = {
            Name = "Most Gold Carried",
            Description = "The most gold carried at any one time."
        },
        SkillIncreases = {
            Name = "Skill Increases",
            Description = "The number of times a skill increases."
        },
        IngredientsEaten = {
            Name = "Ingredients Eaten",
            Description = "The number of alchemy ingredients eaten."
        },
        TrainingSessions = {
            Name = "Training Sessions",
            Description = "The number of times you have received training from a skill trainer."
        },
        BooksRead = {
            Name = "Books Read",
            Description = "The number of books you have read. Simply activating a book will count as reading it."
        },
        Barters = {
            Name = "Barters",
            Description = "The act of one transaction, either buying or selling, with a merchant counts as a barter."
        },
        DiseasesContracted = {
            Name = "Diseases Contracted",
            Description =
            "The total number of diseases you have contracted. There are 8 different diseases, all of which can be contracted multiple times."
        }
    },

    Quests = {
        QuestsCompleted = {
            Name = "Quests Completed",
            Description = "The grand total of all quests completed."
        },
        MainQuestsCompleted = {
            Name = "Main Quests Completed",
            Description = "The number of main quests completed. TBD in total, including TBD optional quests."
        }
    },

    Combat = {
        PeopleKilled = {
            Name = "People Killed",
            Description = "The number of NPCs, both hostile and non-hostile, you have killed."
        },
        AnimalsKilled = {
            Name = "Animals Killed",
            Description = "The number of animals you have killed. Also includes all types of conjured familiars."
        },
        CreaturesKilled = {
            Name = "Creatures Killed",
            Description = "The number of creatures you have killed. Animals or undead are not included."
        },
        DaedraKilled = {
            Name = "Daedra Killed",
            Description =
            "The number of Daedra you have killed. Includes Dremora, elemental atronachs and all types of summoned Daedra."
        },
        FavoriteWeapon = {
            Name = "Favorite Weapon",
            Description = "The weapon that is most often used. Limited to melee weapons or bows."
        },
        SneakAttacks = {
            Name = "Sneak Attacks",
            Description =
            "The number of sneak attacks performed. A sneak attack is the act of striking a target while remaining undetected."
        }
    },
    Magic = {
        SpellsLearned = {
            Name = "Spells Learned",
            Description = "The number of spells you have learned. Most spells are learned from Spell Merchants."
        },
        FavoriteSpell = {
            Name = "Favorite Spell",
            Description = "The spell that is most often used."
        },
        FavoriteSchool = {
            Name = "Favorite School",
            Description =
            "The school of magic that is most often used. The six schools are Alteration, Conjuration, Destruction, Mysticism, Illusion, and Restoration."
        }
    },

    Crafting = {
        SoulGemsUsed = {
            Name = "Soul Gems Used",
            Description =
            "The number of soul gems used to recharge an enchanted weapon only. Soul gems consumed via Enchanting do not count."
        },
        SoulsTrapped = {
            Name = "Souls Trapped",
            Description =
            "The number of souls trapped within a soul gem. Souls can only be trapped with the soul trap spell or with a weapon that bears the soul trap enchantment."
        },
        MagicItemsMade = {
            Name = "Magic Items Made",
            Description = "The number of items enchanted at an arcane enchanter."
        },
        PotionsMade = {
            Name = "Potions Made",
            Description = "The number of potions made."
        },
        PotionsUsed = {
            Name = "Potions Used",
            Description = "The number of potions consumed."
        },
        IngredientsHarvested = {
            Name = "Ingredients Harvested",
            Description = "The number of alchemy ingredients harvested from plants."
        }
    },

    Crime = {
        TotalLifetimeBounty = {
            Name = "Total Lifetime Bounty",
            Description = "The grand total of all bounties, paid and unpaid, placed upon you."
        },
        LargestBounty = {
            Name = "Largest Bounty",
            Description = "The largest total bounty you have ever accrued at any one time."
        },
        LocksPicked = {
            Name = "Locks picked",
            Description = "The number of locked doors or containers successfully unlocked via lockpicking."
        },
        ItemsPickpocketed = {
            Name = "Items Pickpocketed",
            Description = "The number of items you have pickpocketed from an NPC's inventory."
        },
        TimesJailed = {
            Name = "Times Jailed",
            Description = "The number of times you have been sent to jail."
        },
        DaysJailed = {
            Name = "Days Jailed",
            Description = "The number of days you spend in jail serving your sentence."
        },
        FinesPaid = {
            Name = "Fines Paid",
            Description = "The total amount of gold paid for any bounties you incur."
        },
        FinesForgiven = {
            Name = "Fines forgiven",
            Description = "The amount of fines removed via Thieves guild (need better name)."
        },
        ItemsStolen = {
            Name = "Items Stolen",
            Description = "The number of owned items you have added to your inventory."
        },
        Assaults = {
            Name = "Assaults",
            Description =
            "The number of separate instances of performing an unprovoked attack upon a non-hostile NPC. Typically results in a bounty if witnessed."
        },
        Murders = {
            Name = "Murders",
            Description =
            "The number of non-hostile NPC deaths you are responsible for. Followers and summoned creatures who kill on your behalf also count towards this statistic."
        }
    }
}
