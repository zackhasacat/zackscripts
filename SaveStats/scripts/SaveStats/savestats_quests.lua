local I = require("openmw.interfaces")
local ui = require("openmw.ui")
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local time = require('openmw_aux.time')
local self = require("openmw.self")
local completedQuests = {}
local function onQuestUpdate(questId, stage)
    local quest = types.Player.quests(self)[questId]
    if not quest then
        return
    end
    if quest.finished and not completedQuests[questId] then
        local questStart = string.sub(questId, 1, 2):lower()
        completedQuests[questId] = true
        I.SaveStats.mod("QuestsCompleted", 1)
        local isMQ = false
        for i = 1, 10, 1 do
            local str = "a" .. tostring(i)
            local bstr = "b" .. tostring(i)
            if questStart == str or questStart == bstr then
                isMQ = true
            end
        end
        if isMQ then
            I.SaveStats.mod("MainQuestsCompleted", 1)
        end
        I.SaveStats.updateValues()
    end
end


return {
    engineHandlers = {
        onQuestUpdate = onQuestUpdate,
        onSave = function()
            return { completedQuests = completedQuests }
        end,
        onInit = function ()
            for i,x in pairs(types.Player.quests(self)) do
                onQuestUpdate(i)
            end
        end,
        onLoad = function(data)
            if data then
                completedQuests = data.completedQuests
            end
        end
    }
}
