local I = require("openmw.interfaces")
local ui = require("openmw.ui")
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local self = require("openmw.self")
local storage = require("openmw.storage")
local time = require('openmw_aux.time')
local playerSection = storage.playerSection("SettingsSaveStats")


local function crimeIncreased(amountIncreased,paused,currentAmount)
    I.SaveStats.mod("TotalLifetimeBounty",amountIncreased)
  --  I.SaveStats.updateValues()
   if currentAmount > I.SaveStats.get("LargestBounty") then
    I.SaveStats.set("LargestBounty",currentAmount)
  --  I.SaveStats.updateValues()
   end
end
local function crimeDecreased(amountDecreased)
    
end
local lastCrime = 0
local function onFrame(dt)
    local currentCrime = types.Player.getCrimeLevel(self)
    if currentCrime > lastCrime then
        local paused = dt == 0
        crimeIncreased(currentCrime - lastCrime,paused,currentCrime)
    elseif currentCrime < lastCrime then
        crimeDecreased(-(lastCrime - currentCrime))
    end
    lastCrime = currentCrime
end
return {
    engineHandlers = {
        onFrame = onFrame
    }
}