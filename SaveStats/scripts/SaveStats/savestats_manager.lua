local I = require("openmw.interfaces")
local ui = require("openmw.ui")
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local time = require('openmw_aux.time')
local startTime = 1.375
local playerSection = storage.playerSection("SettingsSaveStats")

local sectionList = { "AGeneral", "BQuests", "CCombat", "DMagic", "ECrafting", "FCrime", }

local savedStats = {}
--fixed numbers

savedStats.trainingSessions = 0
savedStats.ingredsConsumed = 0
savedStats.potionsConsumed = 0
--crime
savedStats.ItemsStolen = 0
savedStats.max = 0
--

local statsTable = require("scripts.SaveStats.savestats_stattable")

local function set(name, value)
    if not name then
        error("No name!")
    elseif not value then
        error("No value! " .. name)
    end
    savedStats[name] = value
    for i, x in ipairs(sectionList) do
        local realKey = x:sub(2)
        if statsTable[realKey] and statsTable[realKey][name] then
            --      print("Saving ", name, value)
            --       print(name, value, x)
            local section = storage.playerSection("SettingsSaveStats" .. x)
            section:set(name, value)
        end
    end
    -- updateValues()
end
local function get(name)
    return savedStats[name] or 0
end
local function mod(name, value)
    if not savedStats[name] then
        savedStats[name] = 0
    end
    local newValue = get(name) + value
    savedStats[name] = newValue
    for i, x in ipairs(sectionList) do
        local realKey = x:sub(2)
        if statsTable[realKey] and statsTable[realKey][name] then
            local section = storage.playerSection("SettingsSaveStats" .. x)
            section:set(name, newValue)
        end
    end
    -- updateValues()
end

local function divide(num, divisor)
    if num == 0 then
        return 0
    else
        return num / divisor
    end
end
local function updateValues() --set values that are derived
    --General
    set("DaysPassed", math.floor(divide(core.getGameTime(), time.day) - startTime))
    set("DaysJailed", math.floor(divide(get("TimeJailed"), time.day)))
    for key, category in pairs(statsTable) do
        --loop through the table, find all the categories
        for name, setting in pairs(category) do--name is the ID of the setting, settings is the setting data itself
            --loop each category
            for i, x in ipairs(sectionList) do
                --loop through the sections that are re-ordered
            local realKey = key:sub(2)
            if statsTable[realKey] and statsTable[realKey][name] then
                --      print("Saving ", name, value)
                --       print(name, value, x)
                local section = storage.playerSection("SettingsSaveStats" .. x)
                section:set(name, savedStats[name] or 0)
            end
        end
        end
    end
end
-- Forbid increasing destruction skill past 50
I.SkillProgression.addSkillLevelUpHandler(function(skillid, source, options)
    if source == I.SkillProgression.SKILL_INCREASE_SOURCES.Trainer then
        savedStats.TrainingSessions = (savedStats.TrainingSessions or 0) + 1
        updateValues()
    end
end)
local openContainer
local openContainerContent
local function waitToCloseCont(data)
    openContainer = data.container
    openContainerContent = data.contents
end

return {

    interfaceName = "SaveStats",
    interface = {
        updateValues = updateValues,
        set = set,
        mod = mod,
        get = get,
    },
    eventHandlers = {
        waitToCloseCont = waitToCloseCont,
        itemStolen = function(data)
            --print
            print("stolen", data.count)
            mod("ItemsStolen", data.count)
            updateValues()
        end,
        UiModeChanged = function(data)
            -- print('LMMUiModeChanged to', data.newMode, '(' .. tostring(data.arg) .. ')')
            if data.oldMode == "Container" and data.newMode == nil and openContainer then
                local inventory = types.Container.content(openContainer)
                local stolenItems = 0
                for index, value in ipairs(openContainerContent) do
                    local item = inventory:find(value.id)
                    if not item then
                        stolenItems = stolenItems + value.count
                    elseif item and item.count < value.count then
                        stolenItems = stolenItems + (item.count - value.count)
                    end
                end
                mod("ItemsStolen", stolenItems)
                openContainer = nil
                openContainerContent = nil

                updateValues()
            end
        end,

    },
    engineHandlers = {
        onConsume = function(item)
            if item.type == types.Ingredient then
                mod("IngredientsEaten", 1)
            elseif item.type == types.Potion then
                mod("PotionsUsed", 1)
            end
        end,
        onLoad = function(data)
            if data then
                savedStats = data.savedStats
            end
            updateValues()
        end,
        onSave = function()
            return {
                savedStats = savedStats
            }
        end
    },
}
