local I = require("openmw.interfaces")
local ui = require("openmw.ui")
local util = require("openmw.util")
local storage = require("openmw.storage")

local playerSection = storage.playerSection("SettingsSaveStats")
local function createTest()
    I.Settings.registerRenderer('statShower', function(value, set, arg)

        return {
            type = ui.TYPE.Text,
            props = {
                size = util.vector2(arg and arg.size or 150, 30),
                text = tostring(value or 0),
                textColor = util.color.rgb(1, 1, 1),
                textSize = 15,
                textAlignV = ui.ALIGNMENT.End,
            },
            events = {
            },
        }
    end)

end

return {
    engineHandlers = {

        onLoad = function ()
            createTest()
        end,
        onInit = function ()
            createTest()
        end
    }
}