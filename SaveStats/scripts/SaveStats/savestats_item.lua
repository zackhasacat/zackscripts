local self = require("openmw.self")
local core = require("openmw.core")

local function onActivated()
    print("I am activated!")
end
local lastOwner
local lastCell
local lastPosition 
return {
    engineHandlers = {
        onUpdate = function ()
            local currentCell
            local currentOwner = self.owner.recordId

            if currentOwner ~= lastOwner and lastOwner ~= nil then
                core.sendGlobalEvent("itemStolenMenu",self)
                lastOwner = currentOwner
                return
            else
                lastOwner = currentOwner
                return

            end
        end,
        onActivated = onActivated,
    }
}