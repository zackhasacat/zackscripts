local util = require("openmw.util")
local world = require("openmw.world")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local I = require("openmw.interfaces")

local acti = require("openmw.interfaces").Activation
local function isInFaction(id)
    local player = world.players[1]
   -- print(id)
    if types.NPC.isExpelled(player, id) then
        return false
    end
    for i, x in pairs(types.NPC.getFactions(player)) do
        if x == id then
            return true
        end
    end
    return false
end
local preInventory = {}
local function allowedToGrab(obj)

    if obj.owner.recordId then
        return false
    elseif obj.owner.factionId and obj.owner.factionRank and isInFaction(obj.owner.factionId) and types.NPC.getFactionRank(world.players[1], obj.owner.factionId) >= obj.owner.factionRank then
        return true
    elseif obj.owner.factionId then
        return false 
    end
    return true
end
local function activateContainer(container,actor)
    if not allowedToGrab(container) then
    preInventory = {
        
    }
    for index, value in ipairs(types.Container.content(container):getAll()) do
        table.insert(preInventory,{id = value.recordId, count = value.count})
    end
    actor:sendEvent("waitToCloseCont",{container = container, contents = preInventory})
    end
end

--TODO: Find a way to make this work with clicking items in the inventory
local function itemActivate(item,actor)
    if not allowedToGrab(item) then
        actor:sendEvent("itemStolen",{item = item, count = item.count})
 
    end
end
acti.addHandlerForType(types.Container, activateContainer)
for i,x  in pairs(types) do
    if x.baseType == types.Item then
        acti.addHandlerForType(x, itemActivate)
    end
end

local function onItemActive(item)
    if item.owner.recordId or item.owner.factionId then
        item:addScript("scripts/savestats/savestats_item.lua")
    end
end
local function itemStolenMenu(item)
  --  if not allowedToGrab(item) then
        world.players[1]:sendEvent("itemStolen",{item = item, count = item.count})
 
   -- end
end
return {engineHandlers = {
    onItemActive = onItemActive,
    
},
eventHandlers = {
    itemStolenMenu = itemStolenMenu
}

}