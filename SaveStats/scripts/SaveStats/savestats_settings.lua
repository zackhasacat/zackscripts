local I = require("openmw.interfaces")
local ui = require("openmw.ui")
local util = require("openmw.util")
local storage = require("openmw.storage")

local playerSection = storage.playerSection("SettingsSaveStats")

local statsTable = require("scripts.SaveStats.savestats_stattable")

local function runSection(sectionText,sectionKey,xtable)

    local settingsList = {}
    for i, x in pairs(xtable) do
        local sett =    {
            key = i,
            renderer = "statShower",
            name = x.Name,
            description =
          x.Description,
          --  default = 0,
        }
        table.insert(settingsList,sett)
    end
    local section = storage.playerSection( "SettingsSaveStats" .. sectionKey)
    section:setLifeTime(storage.LIFE_TIME.Temporary)
    section:removeOnExit()
    I.Settings.registerGroup {
        key = "SettingsSaveStats" .. sectionKey,
        page = "SaveStats",
        l10n = "SaveStats",
        name =sectionText,
        description = "",
        permanentStorage = false,
        settings = settingsList
    }
end
local function createTest()


    I.Settings.registerPage {
        key = "SaveStats",
        l10n = "SaveStats",
        name = "Save Stats",
        description = "SaveStats"
    }
    runSection("1. General","AGeneral",statsTable.General)
    runSection("2. Quests","BQuests",statsTable.Quests)
    runSection("3. Combat","CCombat",statsTable.Combat)
    runSection("4. Magic","DMagic",statsTable.Magic)
    runSection("5. Crafting","ECrafting",statsTable.Crafting)
    runSection("6. Crime","FCrime",statsTable.Crime)
  
end

createTest()
return {
    engineHandlers = {

        onLoad = function ()
        end,
        onInit = function ()
        end
    }
}