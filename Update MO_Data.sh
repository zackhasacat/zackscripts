#!/bin/bash

# Get the directory of the script
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Change to the script's directory
cd "scripts/ExtRun/"

# Run the Lua script
lua GenMOData.lua "$@"
lua GenShopData.lua "$@"
lua GenHelpData.lua "$@"

# Check the exit code of the Lua script
exit_code=$?
if [ $exit_code -ne 0 ]; then
  echo "Error: Lua script failed with exit code $exit_code"
  exit $exit_code
fi

echo "Script executed successfully."

