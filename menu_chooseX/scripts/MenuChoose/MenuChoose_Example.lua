local ui = require("openmw.ui")
local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local cam = require("openmw.interfaces").Camera
local core = require("openmw.core")
local self = require("openmw.self")
local nearby = require("openmw.nearby")
local types = require("openmw.types")
local Camera = require("openmw.camera")
local camera = require("openmw.camera")
local input = require("openmw.input")
local async = require("openmw.async")
local storage = require("openmw.storage")
local editMode = false
local keys = input.KEY
local ctrl = input.CONTROLLER_BUTTON
local markData = {}
local lastPosData = nil
local menuMode = false
local markCount = 1
local menuWindow = nil -- Define a comparison function to compare the 'numVal' property
local bindingsWin = nil
local menuName = nil
local zutilsUI = require("scripts.luamultimark.zu_ui")
local function getMarkDataLength()
    local count = 0
    for index, value in pairs(markData) do
        count = count + 1
    end
    return count
end
local function optionPicked(optionName, optionLabel)
    print(optionName)
    self:sendEvent("ChooseMenu_Picked",{menuName = menuName,pickedLabel = optionLabel,pickedName = optionName})
end
local itemWindowLocs = {
    TopLeft = "Top Left",
    TopRight = "Top Right",
    BottomLeft = "Bottom Left",
    BottomRight = "Bottom Right",
    Disabled = "Disabled"
}
local function getWindowLocs()
    local ret = {}
    for key, value in pairs(itemWindowLocs) do
        table.insert(ret, value)
    end
    return ret
end

local playerSettings = storage.playerSection("SettingsMultiMark")
local function compareNumVal(a, b)
    return a.numVal < b.numVal
end
local camData = nil
local playerRot = nil
local function setPauseMode(val)
    local timeScale = 1
    if (val) then
        timeScale = 0
        if playerSettings:get("enableCameraFreeze") == true then
            core.sendGlobalEvent("LMM_paralyzePlayer", "Start")
        end
        camData = { yaw = camera.getYaw(), pitch = camera.getPitch(), roll = camera.getRoll(), mode = camera.getMode() }
        -- camera.setMode(camera.MODE.Static)
        -- camera.setRoll(camData.roll)
        --  camera.setPitch(camData.pitch)
        --  camera.setYaw(camData.yaw)
        playerRot = self.rotation
    else
        camera.setMode(camData.mode)
        timeScale = 1
    end
    menuMode = val
    core.sendGlobalEvent("LMM_SetTimeScale", timeScale)
    input.setControlSwitch(input.CONTROL_SWITCH.Controls, not val)
end
local lastWinState = false
local keyBindings = require("scripts.MenuChoose.lmm_bindings")
local function contextCheck(keyBinding, context)
    for index, value in ipairs(keyBinding.context) do
        if value:lower() == context:lower() then
            return true
        end
    end
    return false
end
function keyBindings.getButtonLabel(bindingName, controllerMode)
    local binding = bindingName
    if not binding then return "nothing" end
    if not controllerMode then
        for key, value in pairs(input.KEY) do
            if value == binding.key then
                return key
            end
        end
    else
        for key, value in pairs(input.CONTROLLER_BUTTON) do
            if value == binding.ctrl then
                return key
            end
        end
    end
end

local inputAction = nil
local function checkBinding(kbinding, key, ctrl)
    if kbinding.context ~= nil then
        if not contextCheck(kbinding, menuWindow.context) then
            return false
        end
    end
    if key == kbinding.key or ctrl == kbinding.ctrl or (inputAction == kbinding.inputAction and inputAction ~= nil) then
        return true
    elseif (inputAction == kbinding.inputAction2 and inputAction ~= nil) then
        return true
    else
        return false
    end
end
local function getSelectedMarkIndex()
    local mindex = 1
    if not menuWindow then
        return -1
    end
    local data = menuWindow:getItemAt(menuWindow.selectedPosX, menuWindow.selectedPosY)
    if data then
        for index, value in ipairs(markData) do
            if value == data then
                return mindex
            else
                mindex = mindex + 1
            end
        end
    end
    return -1
end
local function processInput(key, ctrl, str, action)
    local id = ctrl
    if ctrl then
        keyBindings.controllerMode = true
    else
        keyBindings.controllerMode = false
    end
    inputAction = action
    if editMode then
        if checkBinding(keyBindings.finishTextEdit, key, ctrl) or key == keys.Enter then
            editMode = false
            menuWindow.editMode = false
            local line = menuWindow.editLine
            local data = menuWindow:getItemAt(menuWindow.selectedPosX, menuWindow.selectedPosY)
            if data then
                for index, value in ipairs(markData) do
                    if value == data then
                        -- table.remove(markData, index)
                        menuWindow.list = markData
                        data.label = line
                        -- table.insert(markData, data)
                        table.sort(markData, compareNumVal)
                        break
                    end
                end
            end
        elseif key == keys.Backspace then
            menuWindow.editLine = menuWindow.editLine:sub(1, -2)
        else
            local char = str
            if char and char ~= "" then
                if input.isShiftPressed() then
                    char = char:upper()
                end
                menuWindow.editLine = menuWindow.editLine .. char
            end
        end
        menuWindow:reDraw()
        return
    end

    if not menuWindow or editMode then
        lastWinState = menuWindow == nil
        return
    end
    local selectedx = menuWindow.selectedPosX
    local selectedy = menuWindow.selectedPosY
    if (checkBinding(keyBindings.navUp, key, ctrl)) then
        if (menuWindow.listMode) then
            if (menuWindow.selectedPosX > 1 and menuWindow:getItemAt(menuWindow.selectedPosX - 1, selectedy) ~= nil) then
                menuWindow.selectedPosX = selectedx - 1
            else
                if (menuWindow.scrollOffset > 0) then
                    menuWindow.scrollOffset = (menuWindow.scrollOffset - 1)
                end
            end
        end
    elseif (checkBinding(keyBindings.navDown, key, ctrl)) then
        if (menuWindow.listMode) then
            if (selectedx < 9 and menuWindow:getItemAt(selectedx + 1, selectedy) ~= nil) then
                menuWindow.selectedPosX = selectedx + 1
            elseif menuWindow.selectedPosX > menuWindow.rowCountY - 2 and menuWindow:getItemAt(selectedx + 1, selectedy) then
                menuWindow.scrollOffset = (menuWindow.scrollOffset + 1)
            end
        end
    elseif checkBinding(keyBindings.selectMarkOverwrite, key, ctrl) and lastWinState == false then
        local data = menuWindow:getItemAt(selectedx, selectedy)
        local useMark = 0
        local usePosition = -1
        if data then
            for index, value in ipairs(markData) do
                if value == data then
                    table.remove(markData, index)
                    usePosition = index
                    useMark = value.numVal
                    table.sort(markData, compareNumVal)
                    menuWindow.list = markData
                    if not menuWindow:getItemAt(selectedx, selectedy) then
                        menuWindow.selectedPosX = menuWindow.selectedPosX - 1
                    end
                    break
                end
            end
        end
        if playerSettings:get("enableCameraFreeze") == true then
            core.sendGlobalEvent("LMM_paralyzePlayer", "end")
        end
        I.LMM.saveMarkLoc(useMark, true, usePosition)
        menuWindow:destroy()
        print("selectmark")
        if (bindingsWin) then
            bindingsWin:destroy()
            bindingsWin = nil
        end
        menuWindow = nil

        setPauseMode(false)
        return
    elseif tonumber(str) then
        local data = menuWindow:getItemAt(tonumber(str), selectedy)
        if data then
            if playerSettings:get("enableFollowerTP") == true then
                for index, value in ipairs(nearby.actors) do
                    if value.type ~= self.type then
                        value:sendEvent("teleportFollower",
                            { destPos = data.position, destCell = data.cell, destRot = data.rotation })
                    end
                end
            end
            core.sendGlobalEvent("LMM_TeleportToCell",
                {
                    item = self,
                    cellname = data.cell,
                    position = data.position,
                    rotation = data.rotation
                })
        end
        menuWindow:destroy()

        if (bindingsWin) then
            bindingsWin:destroy()
            bindingsWin = nil
        end
        menuWindow:destroy()
        menuWindow = nil
        setPauseMode(false)
        return
    elseif checkBinding(keyBindings.selectMenuItem, key, ctrl) and lastWinState == false and menuWindow.context == "normal" then
        local data = menuWindow:getItemAt(selectedx, selectedy)

        optionPicked(data.label)
        menuWindow:destroy()

        if (bindingsWin) then
            bindingsWin:destroy()
            bindingsWin = nil
        end
        menuWindow:destroy()
        menuWindow = nil
        setPauseMode(false)
        return
    elseif checkBinding(keyBindings.enterEditMode, key, ctrl) and menuWindow:getItemAt(selectedx, selectedy) and not editMode then
        editMode = true

        menuWindow.editMode = true
        menuWindow.editLine = menuWindow:getItemAt(selectedx, selectedy).label
    elseif checkBinding(keyBindings.deleteItem, key, ctrl) then
        local data = menuWindow:getItemAt(selectedx, selectedy)
        if data then
            for index, value in ipairs(markData) do
                if value == data then
                    table.remove(markData, index)


                    table.sort(markData, compareNumVal)
                    menuWindow.list = markData
                    if not menuWindow:getItemAt(selectedx, selectedy) then
                        menuWindow.selectedPosX = menuWindow.selectedPosX - 1
                    end
                    break
                end
            end
        end
    elseif checkBinding(keyBindings.cancelMenu, key, ctrl) or checkBinding(keyBindings.cancelMenuOverwrite, key, ctrl) then
        menuWindow:destroy()
        if (bindingsWin) then
            bindingsWin:destroy()
            bindingsWin = nil
        end
        menuWindow = nil
        setPauseMode(false)
        if playerSettings:get("enableCameraFreeze") == true then
            core.sendGlobalEvent("LMM_paralyzePlayer", "end")
        end
        return
    end
    menuWindow:reDraw()
    lastWinState = menuWindow == nil
end
local lastChange = 0
local passedTime = 0
local function drawListMenu(buttonTable)
    if (bindingsWin) then
        bindingsWin:destroy()
        bindingsWin = nil
    end
    local winLoc = playerSettings:get("InfoWindowLocation")
    local wx = 0
    local wy = 0
    local align = nil
    local anchor = nil
    if winLoc == itemWindowLocs.TopLeft then
        wx = 0
        wy = 0
        align = ui.ALIGNMENT.Start
    elseif winLoc == itemWindowLocs.TopRight then
        wx = 1
        wy = 0
        anchor = util.vector2(1, 0)
        align = ui.ALIGNMENT.End
    elseif winLoc == itemWindowLocs.BottomLeft then
        wx = 0
        wy = 1
        anchor = util.vector2(0, 1)
        align = ui.ALIGNMENT.End
    elseif winLoc == itemWindowLocs.BottomRight then
        wx = 1
        wy = 1
        anchor = util.vector2(1, 1)
        align = ui.ALIGNMENT.Start
    elseif winLoc == itemWindowLocs.Disabled then
        return
    end
    bindingsWin = zutilsUI.renderItemChoice(buttonTable, wx, wy, align, anchor)
end
local function openMenu(lmenuName)
    if menuWindow then
        menuWindow:destroy()
    end
    menuName = lmenuName
    menuWindow = I.MenuChoose.createItemWindow({}, 0.5, 0.5, keyBindings, "normal")
    setPauseMode(true)
    menuWindow:setGridSize(20, 10)
    menuWindow.selected = true
end
local function addMenuItem(menuItem)
    if menuWindow then
        menuWindow:addMenuItem(menuItem)
    end
end
local function openMarkMenu(context)
    if getMarkDataLength() == 0 then
        ui.showMessage("There are no marked locations. Cast the spell mark before casting recall.")
        return
    end
    table.sort(markData, compareNumVal)
    menuWindow = I.LMM_Window.createItemWindow(markData, 0.5, 0.5, keyBindings, context)

    local buttonTable = {}
    local kb = I.LMM.getKeyBindings()
    if kb.controllerMode then
        table.insert(buttonTable, "DPad: Navigate")
    else
        table.insert(buttonTable, "Arrow Keys: Navigate")
    end
    for index, value in pairs(I.LMM.getKeyBindings()) do
        if value and value ~= kb.getButtonLabel and value ~= kb.controllerMode and value.label and contextCheck(value, menuWindow.context) then
            table.insert(buttonTable, value.label .. ": " .. kb.getButtonLabel(value, kb.controllerMode))
        end
    end
    drawListMenu(buttonTable)
    menuWindow:setGridSize(20, 10)
    menuWindow.selected = true
    setPauseMode(true)
end
local function trim(s)
    return s:match '^()%s*$' and '' or s:match '^%s*(.*%S)'
end
local function formatRegion(regionString)
    -- remove the word "region"
    -- capitalize the first letter of each word
    regionString = string.gsub(regionString, "(%a)([%w_']*)", function(first, rest)
        return first:upper() .. rest:lower()
    end)
    -- trim any leading/trailing whitespace
    regionString = trim(regionString)
    return regionString
end
local function getAvailableSlot()
    local i = 1
    table.sort(markData, compareNumVal)
    if markData[1] == nil then
        return 1
    end
    local lowest = markData[1].numVal -- Initialize lowest with the first element's numVal
    for i = 2, getMarkDataLength() do
        if markData[i] and markData[i].numVal < lowest then
            lowest = markData[i].numVal -- Update lowest if a smaller value is found
        end
    end

    return lowest
end
local function getMaxSlots()
    local baseMark = playerSettings:get("baseMarkCount")
    local skillMult = playerSettings:get("levelsPerMark")
    if skillMult == 0 then
        return 800
    end
    local myMyst = types.NPC.stats.skills["mysticism"](self).modified
    local totalSlots = (myMyst / skillMult) + baseMark
    return math.floor(totalSlots)
end
local function saveMarkLoc(useMarkCount, force, usePosition)
    if usePosition == -1 then
        usePosition = nil
    end
    local firstLabel = self.cell.name
    if firstLabel == "" or firstLabel == nil then
        firstLabel = formatRegion(self.cell.region)
    end
    if firstLabel == "" or firstLabel == nil then
        firstLabel = "Wilderness"
    end
    if getMaxSlots() <= 0 then
        ui.showMessage("Your mysticism level is too low to use this spell.")
        return
    elseif getMarkDataLength() >= getMaxSlots() and not force then
        ui.showMessage("Your mysticism level is too low to save another marked location.")
        openMarkMenu("overwrite")
        return
    end

    if not useMarkCount then
        useMarkCount = markCount
    end
    ui.showMessage("Location marked as " .. firstLabel)
    local data = {
        position = self.position,
        rotation = self.rotation,
        cell = self.cell.name,
        label = firstLabel,
        numVal = useMarkCount
    }
    if usePosition then
        table.insert(markData, usePosition,
            data)
    else
        table.insert(markData,
            data)
    end

    table.sort(markData, compareNumVal)
    markCount = markCount + 1
end
local lastAxis = 0
local threshHold = 0.3
local function onUpdate(dt)
    if not markData then
        markData = {}
    end

    if menuMode == true then
        local controllerAxis = input.getAxisValue(input.CONTROLLER_AXIS.LeftY)
        if controllerAxis > threshHold and lastAxis < threshHold then
            processInput(nil, nil, nil, input.ACTION.ZoomIn)
        elseif controllerAxis > -threshHold and lastAxis < -threshHold then
            processInput(nil, nil, nil, input.ACTION.ZoomOut)
        end
        lastAxis = controllerAxis
    end
end
local function createRotation(x, y, z)
    if (core.API_REVISION < 40) then
        return util.vector3(x, y, z)
    else
        local rotate = util.transform.rotateZ(z)
        return rotate
    end
end
local function onLoad(data)
    if data then
        markData = data.markData
        if data.revVer <= 40 and data.revVer ~= core.API_REVISION then
            markData = data.markData
            for index, value in ipairs(markData) do --Apparently this is not needed
                -- value.rotation = createRotation(0,0,0)
            end
        end
        if data.markCount then
            markCount = data.markCount
        end
    end
end
local function onSave()
    if menuWindow then
        menuWindow:destroy()
        if (bindingsWin) then
            bindingsWin:destroy()
            bindingsWin = nil
        end
        menuWindow = nil
    end
    return { markData = markData, markCount = markCount, revVer = core.API_REVISION }
end
local lastPress = 0
local function onControllerButtonPress(ctrl)
    if (core.getRealTime() < lastPress + 0.002) then
        return
    end
    processInput(nil, ctrl)
    lastPress = core.getRealTime()
end
local function onControllerButtonRelease(ctrl)

end
local function onKeyPress(key)
    processInput(key.code, nil, key.symbol)
end
local function onInputAction(action)
    processInput(nil, nil, nil, action)
end
return {
    interfaceName = "MenuChoose_Main",
    interface = {
        version = 1,
        openMenu = openMenu,
        addMenuItem = addMenuItem
    },
    eventHandlers = {
        openMarkMenu = openMarkMenu,
        saveMarkLoc = saveMarkLoc
    },
    engineHandlers = {
        onControllerButtonPress = onControllerButtonPress,
        onKeyPress = onKeyPress,
        onUpdate = onUpdate,
        onSave = onSave,
        onLoad = onLoad,
        onInputAction = onInputAction,
    }
}
