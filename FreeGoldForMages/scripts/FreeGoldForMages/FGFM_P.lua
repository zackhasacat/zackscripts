local ui = require('openmw.ui')
local this = {}
this.eventHandlers = {}
function this.eventHandlers.FGFM_ShowMessage(message)
    ui.showMessage(message)
end

return this