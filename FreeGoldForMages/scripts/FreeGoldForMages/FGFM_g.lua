local world = require('openmw.world')
local interfaces = require('openmw.interfaces')
local types = require('openmw.types')

local chairToReplace = "furn_de_r_chair_03"
local buttonId = "ex_gg_gateswitch_01"
local myCell = "Balmora, Guild of Mages"
local function placeButton(cell, pos, rot)
    local newButton = world.createObject(buttonId)
    newButton:teleport(cell, pos, rot)
end
local function replaceChairWithButton(chair)
    placeButton(chair.cell, chair.position, chair.rotation)
    chair:remove()
end

local function activateButton(obj, actor)
    if obj.cell.name == myCell and obj.recordId == buttonId then
        local rank = types.NPC.getFactionRank(actor, "mages guild")
        if rank and rank > 0 then
            local newGold = world.createObject("gold_001", 1000)
            newGold:moveInto(actor)
            actor:sendEvent("FGFM_ShowMessage","Here's some money!")
        else
            actor:sendEvent("FGFM_ShowMessage","Sorry, mages only!")
        end
        return false
    end
end
interfaces.Activation.addHandlerForType(types.Activator, activateButton)
return {
    engineHandlers = {
        onObjectActive = function(obj)
            if obj.recordId == chairToReplace and obj.cell.name == myCell then
                replaceChairWithButton(obj)
            end
        end
    }
}
