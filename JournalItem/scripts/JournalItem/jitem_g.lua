
local _, world      = pcall(require, "openmw.world")
local I = require("openmw.interfaces")
local types = require("openmw.types")
    local itemID = "zhac_book_journal"
    local mapID = "zhac_book_map_misc"
local acti = require("openmw.interfaces").Activation
local function addItem(obj,itemId)
    local newObj = world.createObject(itemId, 1)
    newObj:moveInto(obj)
     newObj = world.createObject(mapID, 1)
    newObj:moveInto(obj)
end
local ringItem = "zhac_ring_blocker"
local secretGem = "misc_soulgem_secret"
local modAdded = false
local function onModAdded()
    if modAdded then
        return
    end
    addItem(world.players[1],itemID)
    modAdded = true
end

local function onLoad(data)
    if data then 
        modAdded = true
    else
        onModAdded()
    end
end
local function onSave()
    return {modAdded = true}
end
local function onPlayerAdded(player)
    onModAdded()
end
local function pickupItem(item)
    item:moveInto(world.players[1])
end

local function bookActivate(object, actor)
   if object.recordId == itemID then
    world.players[1]:sendEvent("OpenJournal",object)
    return false
   elseif object.recordId == mapID then
 --   world.players[1]:sendEvent("OpenMap",object)
  --  return false

   end
 end
 local function removeRing(skip)
    local soulGem = types.Actor.inventory(world.players[1]):find(secretGem)
    local item = types.Actor.inventory(world.players[1]):find(ringItem)
    if  item then
        item:remove()
      
    end
    if  soulGem then
        soulGem:remove()
    end
    if not item and not soulGem then
        return
    end
    if not skip then
        world.players[1]:sendEvent("refreshInv",true)
    else
        world.players[1]:sendEvent("queueReturn")
    end
 end
 
 local function addRing()

    local gem = world.createObject(secretGem)
    gem:moveInto(world.players[1])
    local eq = types.Actor.getEquipment(world.players[1])
    if eq[types.Actor.EQUIPMENT_SLOT.RightRing] then
        return
    end
    local item = world.createObject(ringItem)
    item:moveInto(world.players[1])
    eq[types.Actor.EQUIPMENT_SLOT.RightRing] = item
    world.players[1]:sendEvent("setEquipJI",eq)
 end
 I.ItemUsage.addHandlerForType(types.Book, bookActivate)
 acti.addHandlerForType(types.Book, bookActivate)
 local function containerActivate(cont,player)
    --removeRing(true)
 end
 local function actorActivated(cont,player)
    if types.Actor.isDead(cont) then
     --   removeRing(false)
    end
 end
 acti.addHandlerForType(types.Container, containerActivate)
 acti.addHandlerForType(types.NPC, actorActivated)
 acti.addHandlerForType(types.Creature, actorActivated)
-- ZackBridge.AddObjectTypeActivationHandler("Book", bookActivate)
return{
    engineHandlers = {
        onLoad = onLoad,
        onSave = onSave,
        onPlayerAdded = onPlayerAdded,
    },
    eventHandlers = {
        pickupItem = pickupItem,
       -- removeRing = removeRing,
     --  addRing = addRing,
    }
}