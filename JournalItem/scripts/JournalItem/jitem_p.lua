local core = require('openmw.core')
local input = require('openmw.input')
local self = require('openmw.self')
local storage = require('openmw.storage')
local ui = require('openmw.ui')
local types = require('openmw.types')
local async = require('openmw.async')
local Actor = require('openmw.types').Actor
local Player = require('openmw.types').Player

local I = require('openmw.interfaces')
local ringItem = "zhac_ring_blocker"
local secretGem = "misc_soulgem_secret"
local invOnly = false

local activatedbookItem
local settings = storage.playerSection('SettingsOMWControls')

local function uiAllowed()
    return Player.getControlSwitch(self, Player.CONTROL_SWITCH.Controls)
end
local bookItem
local function checkNotWerewolf()
    if Player.isWerewolf(self) then
        ui.showMessage(core.getGMST('sWerewolfRefusal'))
        return false
    else
        return true
    end
end

local function hasMap()
    local itemID = "zhac_book_map_misc"
    return types.Actor.inventory(self):countOf(itemID) > 0
end
local function isJournalAllowed()
    -- During chargen journal is not allowed until magic window is allowed
    return I.UI.getWindowsForMode(I.UI.MODE.Interface)[I.UI.WINDOW.Magic]
end
local mapAllowed = false

local function openMap()
    I.UI.setMode('Interface', { windows = { 'Map' } })
end
local function queueReturn()
    async:newUnsavableSimulationTimer(0.1, function()
        --   core.sendGlobalEvent("addRing")
    end)
end
local allowRightClick = true
local function refreshInv(state)
    if not uiAllowed() then return end
    if not allowRightClick then
        return
    end
    if I.UI.getMode() == nil or state == true then
        local count = types.Actor.inventory(self):countOf(ringItem)
        local gemcount = types.Actor.inventory(self):countOf(secretGem)
        --TODO:Check if the player has other gems, if so don't give this
        if count > 0 or gemcount > 0 then
            --  core.sendGlobalEvent("removeRing")
            queueReturn()
            return
        end
        if invOnly then
            if hasMap() then
                I.UI.setMode('Interface', { windows = { "Inventory", "Map" } })
            else
                I.UI.setMode('Interface', { windows = { "Inventory" } })
            end
        elseif hasMap() then
            I.UI.setMode('Interface', { windows = { "Stats", "Magic", "Inventory", 'Map' } })
        else
            I.UI.setMode('Interface', { windows = { "Stats", "Magic", "Inventory" } })
        end
    elseif I.UI.getMode() == I.UI.MODE.Interface or I.UI.getMode() == I.UI.MODE.Container then
        I.UI.removeMode(I.UI.getMode())
    end
end
input.registerTriggerHandler('Inventory', async:callback(function()
    refreshInv()
end))
local function hasBook()
    local itemID = "zhac_book_journal"
    return types.Actor.inventory(self):countOf(itemID) > 0
end
input.registerTriggerHandler('Journal', async:callback(function()
    if not uiAllowed() then return end
    if not hasBook() then
        ui.showMessage("You are not carrying your journal!")
        return
    end
    if I.UI.getMode() == I.UI.MODE.Journal then
        I.UI.removeMode(I.UI.MODE.Journal)
    elseif isJournalAllowed() then
        activatedbookItem = nil
        I.UI.addMode(I.UI.MODE.Journal)
    end
end))

input.registerTriggerHandler('QuickKeysMenu', async:callback(function()
    if not uiAllowed() then return end

    if I.UI.getMode() == I.UI.MODE.QuickKeysMenu then
        I.UI.removeMode(I.UI.MODE.QuickKeysMenu)
    elseif checkNotWerewolf() and Player.isCharGenFinished(self) then
        I.UI.addMode(I.UI.MODE.QuickKeysMenu)
    end
end))

local function onLoad(data)
    --  print("Disabling default UI")
    I.Controls.overrideUiControls(true)

    local ringCount = types.Actor.inventory(self):countOf(ringItem)
    local eq = types.Actor.getEquipment(self)
    if ringCount == 0 and not eq[types.Actor.EQUIPMENT_SLOT.RightRing] and not eq[types.Actor.EQUIPMENT_SLOT.LeftRing] then
        core.sendGlobalEvent("addRing")
    end
end

local function OpenJournal(item)
    if isJournalAllowed() then
        activatedbookItem = item
        I.UI.addMode(I.UI.MODE.Journal)
    end
end
local function setEquipJI(eq)
    local eq = types.Actor.getEquipment(self)
    eq[types.Actor.EQUIPMENT_SLOT.RightRing] = ringItem
    types.Actor.setEquipment(self, eq)
end
return
{
    interfaceName = "JItem",
    interface = {
        setinvOnly = function(state)
            invOnly = state
        end,
        setAllowRightClick = function (state)
            allowRightClick = state
        end
    },
    engineHandlers = {
        onLoad = onLoad,
        onInit = onLoad,

    },
    eventHandlers = {
        OpenJournal = OpenJournal,
        openMap = openMap,
        refreshInv = refreshInv,
        setEquipJI = setEquipJI,
        queueReturn = queueReturn,
    }
}
