************************************************************************
                            Granny Claus' Christmas Gifts
			           by Korana

************************************************************************
-----------
Description
-----------	

This mod adds Santa's "compound" to Solstheim.  While it would ruin your Christmas surprise
to enter the workshop, you can pick up trees in the Christmas Tree lot, and enter Granny Claus'
Cottage for some dinner, drinks, and to open her gifts to you!  You can even meet the reindeer! 

Travel south,and slightly west of the Skaal Village to find Granny's cottage.
(Cell coordinates: -19, 25)

Presents are under the tree and throughout the cottage.  Among the goodies you may find in this mod are:

-Better Bodies Clothing for male and female.  I took a different, more adult costuming, approach
 to most of the outfits.  

-Santa hat helms, with hairs attached for both male and females.

-A red animated cape.

-A black back pack.

-Mittens in red, white, and black.

-Presents with bows, in two sizes.  Each article of clothing is found in a container that is a   present.  The clothing also uses the container mesh as it's GND, so you can in fact place presents   in game.

-Placeable wreaths. There are 5 versions: a plain wreath, a plain wreath with a red bow, a wreath    with ornaments and pine cones, a lit up wreath with glow mapped lights, and a special wreath with    all the goodies (except lights) and also comberrys.

-Glow mapped christmas lights you can place where-ever you like!

-Round ornaments.  They are slightly reflection mapped.  There are 12 versions available.  They are    also placeable.

-2 glass star ornaments.  They are available in silver and gold.  They are reflection mapped and    slightly transparent.  Also placeable.

-4 versions of Christmas stockings.  Also placeable.

-A christmas tea set.

-Several different trees are available, each one placeable in game.  

-Granny will give you a steaming mug of cider each time you speak with her, and it is also available   on the table.  The mug is animated.

-A feast is on the table, just for you...but don't touch Santa's cookies!

-Candy Cane ingrediants.

-Gold candles are also on the table, you may take them if you like...Granny won't mind. 

(NOTE: The gifts that contain decor items, as well as the trees, have unlimited amounts available, just re-enter the cell.)

*REQUIRES TRIBUNAL AND BLOODMOON.  BETTER BODIES RECOMMENDED*

--------------
Script Notes
--------------

The rotation script attached to the items allows them to be positioned and rotated.

The game engine has a slight glitch when certain rotation combinations are used.  These rotation
combinations seem to usually involving rotating upside down and then sideways. Upon loading a saved game, the items may revert back to a previous position. However, none of the items included should need any rotations in those combinations.

Because of how the game engine drops miscellaneous items out of inventory, the back of the wreaths
and stars will face the player.  The wreaths are not double sided for poly count/framerate concerns.
To avoid needing to rotate these via script, simply face the direction you wish the back of the wreath or star to face, and then position it into place.

Glitches may arise in the scripts if you rotate an item in any fashion, and then put it in your inventory, and then later place it in game rotated again.  A good tip to remember is to always reset the item rotation to 0 before picking up items.

The ornaments, wreaths, and stockings will not stack in the player's inventory, or container inventory.  This seems to be because they are scripted.

The trees have no collision on them, due to how the game engine handles miscellaneous items.  On the plus side, it makes decorating the tree a bit easier! 

--------------
Compatibility
--------------

This mod does in fact conflict with Locharnus' Felsaad Revamped.  There will be a land break in the cell, a tree in front of the workshop, and a holly bush in the tree farm.  The tree and bush can be removed by opening the console by clicking the ~ key and typing in "disable" and "setdelete 1".

-------------
Installation
-------------

Extract the included self installing archive directly into your Morrowind folder.

After installing make sure to enable the plugin in the Morrowind Launcer/Data Files menu.

7zip is a free program needed to properly extract the files.  You may download the program here:

http://www.7-zip.org

----------------------------
Credits, Usage, and Thanks
----------------------------

~Lady E for:  The silver trees, the snow men, the lolipops, the deer (textures by Sabregirl-except   Rudolf's nose), and the candy canes.  The ingrediant candy canes are small versions of her mesh.
 Also, the carrots, tomatoes, and potatoes are hers as well.

~Emma for Granny's face.  The mesh was by Rhedd.

~Rhedd for Granny's hair, Allerleirauh for the hair textures,and for the hairs attached to the santa   hats.

~Dongle for the bed.

~Barabus for the fire place.

~Oom Fooyat for the cider mug, teapot, and table runner.

~Karstux for the amazing santa hats.  The hairs attached to them are Rhedds.  All I did was combine  the two meshes.

~Solar for the red shoes attached to the stockings.

~Qarl for the moddeling advice!

~Dracus Draconi, Pekka, and Galsiah for the rotation scripts.

~MJY for the sleds (I resized one), the pie, and the cookies.

~Kagz for the poinsettia, from his flower resource.

~Junkmail for the teddy bear mesh and texture. And also for the male boots.

~MagicNakor for the food meshes (turnip, onion,, as well as www.rocky3d.com/free3d.htm, where   MagicNakor received all  but the chicken mesh from.

~NoLIV for the clothing meshes.  Without her the outfits would not have been possible!

~Derreko for the backpack meshes.

~Gorg for the cape mesh.

~Starcon5 for the bookcase.

~Calishan for the teacups, saucers, and sugar meshes.

~Lingarn for the short skirt.

~Smite and Taddeus for the pudding and corn.  They are from Neccesities of Morrowind.  I originally   used them as NOM food, and later renamed to them to fit the dish.

~Robearberbil for helping me finish the pine cones.

~The mp3 used ("Have Yourself a Merry Little Christmas) is by Shannon Lake.  The music was downloaded  from this webpage, with the title of "New! All Songs 100% "Permission Given" for your personal use   by the original artists!!"

http://www.freechristmasmp3.com/christmasmp3.htm

Present meshes, round ornament meshes, star ornament meshes, wreaths, and christmas light meshes are by me.  The stockings were based roughly by example off a free downloaded file, however, there was no readme attached.  Because of this, I opted to use their poly structure as an example.  That actual downloaded file was not used in the actual making of the stockings mesh.

-------
USAGE:
-------

Feel fee to use any of these items in other projects! Permission is not needed, but I do appreciate a message letting me know...as I get tickled pink when I see any of my projects in use in other projects!  Please, make sure you give the appropriate creators credit as well...and give me credit for anything of mine you used.

Always keep in mind that some of these models by be updated by me or by the other original creators at any time.  When possible, please try to obtain the original download links for any models used.


                   Email:    roxykittyd@adelphia.net

