				Advent Calendar
				By Danae Merlord Remiros Korana TommyKhajiit

Version: 1.0

_________
Contents
_________
1.	Requirements
2.	Description
3.	Permissions
4.	Changelog
5.	Credits

_____________
Requirements
_____________
Morrowind, Tribunal, and Bloodmoon

MGE XE

____________
Description
____________
I highly recommend Just Drop It as well as Perfect Placement so you can place your gifts (some of them are ornaments) nicely.
If you prefer to NOT use these two mods, install the 01 folder: all items have a mwscript that lets you move them.

There's a scroll of Mark and a few potions or Recall so you can travel back with ease.

Requires MGE XE/MWSE
Not Compatible with OpenMW

____________
Permissions
____________
Feel free to borrow the code, the items, the idea.
You can easily edit the date and the gifts in the config.lua file
Do give credit though ;)
__________
Changelog
__________
1.0 Original release for December 2023

________
Credits
________
Merlord for the code
Team Gluhwein 2022 for the hat, shield, gluewein, snowball and jumper models. (All from the excellent Gluhwein 2022 - Seyda Neen) https://www.nexusmods.com/morrowind/mods/52079
Korana for the stocking, fancy xmas tea set, xmas card, xmas lights, and tree ornaments models.
TommyKhajiit for the candles (UV maps fixed by me), plates and napkin models.
Remiros for the pipe model
Bethesda Softworks for developing Morrowind and the Construction Set.
Brucoms for developing the TES3 Readme Generator this readme was made from.
