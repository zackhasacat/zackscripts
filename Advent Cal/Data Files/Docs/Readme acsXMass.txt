STORY
This plugin brings some miscellaneous items for celebrating Christmass. Once activated you'll find a new amulet in your item, equiping this amulet "summons" a new chest. In this new chest you'll find five different Christmass mugs, five different Christmass plates, five different Christmass candles and five different Christmass serviettes. Because it's Christmass, all items comes for free.

REQUIREMENTS
Either Tribunal or Bloodmoon is required for this plugin. At least one of these expansions is necessary for Dracus Dragani's script to function well. This script enables you to place and rotate each item in every position.

INSTALLATION
To install unzip the file to your "Morrowind\Data Files" folder, be sure to select option "use folders names". Start Morrowind, be sure to activate "HallOfRorque" in the "Data Files" section.

CREDITS
Dracus Dragani for giving permission to use his ingenious script.

KNOWN BUGS AND ISSUES
There are no known bugs. This game will not invalidate your savegame.

VERSION HISTORY
11 december 2003, version 1.0

Have fun!

TommyKhajiit
tommy.khajiit@hccnet.nl