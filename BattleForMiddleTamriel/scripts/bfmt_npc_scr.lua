local nearby = require("openmw.nearby")
local self = require("openmw.self")
local core = require("openmw.core")
local types = require("openmw.types")
local I = require("openmw.interfaces")
local selected = false
local function getPlayer()
    for index, value in ipairs(nearby.actors) do
        if value.recordId == "player" then
            return value
        end
    end
end
local function onUpdate()
    if not selected then return end
    if types.Actor.getStance(self) ~= types.Actor.STANCE.Weapon then
        --types.Actor.setStance(self,types.Actor.STANCE.Weapon)
    end
    if not self.controls.run then
        --````self.controls.run = true
    end
end

local isFollowingPlayerTrue = false
local function isFollowingPlayer()
    isFollowingPlayerTrue = false
    local func = function(param) if param.target == getPlayer() and param.type == "Follow" then isFollowingPlayerTrue = true end end
    I.AI.forEachPackage(func)
    return isFollowingPlayerTrue
end
local function setStance(data)
    types.Actor.setStance(self, data.stance)
end
local function setSelected(state)
    if state == true then
        self.controls.run = true
        types.Actor.setStance(self, types.Actor.STANCE.Weapon)
    else
        types.Actor.setStance(self, types.Actor.STANCE.Nothing)
        self.controls.run = false
    end
    selected = state
end
local function onActive()
end
return { engineHandlers = { onActive = onActive, onUpdate = onUpdate },
    eventHandlers = { setStance = setStance, setSelected = setSelected } }
