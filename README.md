This project is my personal workspace, anything contained in here is a major WIP.

You are free to look at it, download it, and use any of these, but please know there is a liklyhood it will not work.

Here is a quick summary of what is contained here:

Advanced Slavery: a mod to allow finer control of slaves, and to enslave enemies such as bandits in caves, once they are defeated enough.

Flying Castle: A flying castle that can move with controls.

Black Soul Gems: A mod that allows soultrapping NPCs. Available here: https://www.nexusmods.com/morrowind/mods/45902 but I am working on updated it for 0.49

Controller Override: This is a mod that I started on a few months ago, I am rewriting it to be hosted here: https://gitlab.com/zackhasacat/controller_mode

Quicktoss: throw a throwable weapon without having to equip it. Going to be integrated in Tweakmaster

Tweakmaster: A pack of lua mods that are too small to release by themselves.

coctest: a project for autocompleting COC in lua for a MR

Dingus: Maxwell the carryable cat.

Mundis: A lua implementation for this mod: https://www.nexusmods.com/morrowind/mods/43805

Zackutils: This is a function library that is removed from here, but mods may depend on it. I've been trying to avoid this. The most up to date version of this library function is here: https://gitlab.com/zackhasacat/zackutils


Note that many of these mods may be non-functional after updates to the OMW lua API.

If anything is not listed here, that means it's not complete enough to be noted at all.