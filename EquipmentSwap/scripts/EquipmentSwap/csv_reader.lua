local vfs = require('openmw.vfs')
--local csv= require("scripts.MoveObjects.Utility.csv_reader")
local function removeNonLetters(inputString)
    if not inputString then
        return nil
    end
    inputString = string.gsub(inputString, "^%s*(.-)%s*$", "%1")
    -- Remove non-letters, underscores, and spaces, but keep numbers
    return string.gsub(inputString, "[^%a%d_%s]", "")
end

local function parseCSV(lines)
    local headers = {}
    local isNumberColumn = {}

    -- Parse headers from the first line
    local headerLine = table.remove(lines, 1)
    for header in headerLine:gmatch("[^,]+") do
        local hd = removeNonLetters(header:lower())
        table.insert(headers, hd)
        end

    -- Determine which columns are numbers based on the second line
    local secondLine = lines[1]
    if secondLine then
        local values = {}
        for value in secondLine:gmatch("[^,]+") do
            table.insert(values, tonumber(value) or value)
        end

        for i, value in ipairs(values) do
           -- isNumberColumn[i] = type(value) == "number"
        end
    end
    local csvTable = {}

    -- Parse data rows using headers as keys and convert numerical values to numbers
    for _, line in ipairs(lines) do
        local row = {}
        local values = {}
        for value in line:gmatch("[^,]+") do
            table.insert(values, value)
        end
        for i, header in ipairs(headers) do
            local fieldValue =removeNonLetters( values[i]) or ""
            if isNumberColumn[i] then
                row[header] = tonumber(fieldValue) or fieldValue
            else
                row[header] = fieldValue
            end
        end
        table.insert(csvTable, row)
    end

    return csvTable
end

local function extractFilename(inputString)
    local startIndex, endIndex = inputString:find(".*/")
    local filenameWithExtension = inputString:sub(endIndex + 1)
    local filename, _ = filenameWithExtension:match("([^%.]+)")
    return filename
end
local function removeSpaces(inputString)
    return inputString:gsub("%s+", "")
end
local function getCSVTables()
    local files = {}
    for fileName in vfs.pathsWithPrefix("csv\\equipmentSwap\\") do
        local lines = {}
        for line in vfs.lines(fileName) do
            table.insert(lines, line)
        end
        local val = parseCSV(lines)
        local name = removeSpaces(extractFilename(fileName))
        files[name] = val
        -- table.insert(files,name,parseCSV(lines))
    end
    return files
end
local function getCSVLines()
    local files = {}
    for fileName in vfs.pathsWithPrefix("csv\\equipmentSwap\\") do
        local lines = {}
        for line in vfs.lines(fileName) do
            table.insert(lines, line)
        end
        table.insert(files, lines)
    end
    return files
end
return { getCSVTables = getCSVTables, getCSVLines = getCSVLines }
