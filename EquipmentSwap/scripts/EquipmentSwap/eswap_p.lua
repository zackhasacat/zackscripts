local cam, camera, core, self, nearby, types, ui, util, storage, async, input, debug =
    require('openmw.interfaces').Camera, require('openmw.camera'),
    require('openmw.core'), require('openmw.self'),
    require('openmw.nearby'), require('openmw.types'),
    require('openmw.ui'), require('openmw.util'),
    require("openmw.storage"), require("openmw.async"),
    require("openmw.input"),
    require("openmw.debug")
local I = require('openmw.interfaces')
local function ES_setEquipment(equip)
    types.Actor.setEquipment(self, equip)

    I.UI.setMode(nil)
    core.sendGlobalEvent("RemoveItemNow")
end
local function OpenAgain()
    I.UI.setMode(I.UI.MODE.Interface)
end
if core.API_REVISION < 30 then
    return false
end
return {
    interfaceName = "eswap",
    interface = {
        version = 31,
    },
    engineHandlers = {
        onInit = onInit,

    },
    eventHandlers = {
        ES_setEquipment = ES_setEquipment,
        OpenAgain = OpenAgain,
    }
}
