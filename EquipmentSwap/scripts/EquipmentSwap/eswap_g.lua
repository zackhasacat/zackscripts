local types    = require("openmw.types")
local acti     = require("openmw.interfaces").Activation
local core     = require("openmw.core")
local world    = require("openmw.world")
local util     = require("openmw.util")
local I        = require("openmw.interfaces")
local storage  = require("openmw.storage")

local csv      = require("scripts.EquipmentSwap.csv_reader")
-- Override Use action (global script).
-- Forbid equipping armor with weight > 5
local csvFiles = {}

csvFiles       = csv.getCSVTables()

local function findSlot(item)
    if (item == nil) then
        return
    end
    --Finds a equipment slot for an inventory item, if it has one,
    if item.type == types.Armor then
        if (types.Armor.records[item.recordId].type == types.Armor.TYPE.RGauntlet) then
            return types.Actor.EQUIPMENT_SLOT.RightGauntlet
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.LGauntlet) then
            return types.Actor.EQUIPMENT_SLOT.LeftGauntlet
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.Boots) then
            return types.Actor.EQUIPMENT_SLOT.Boots
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.Cuirass) then
            return types.Actor.EQUIPMENT_SLOT.Cuirass
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.Greaves) then
            return types.Actor.EQUIPMENT_SLOT.Greaves
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.LBracer) then
            return types.Actor.EQUIPMENT_SLOT.RightGauntlet
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.RBracer) then
            return types.Actor.EQUIPMENT_SLOT.LeftGauntlet
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.RPauldron) then
            return types.Actor.EQUIPMENT_SLOT.LeftGauntlet
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.LPauldron) then
            return types.Actor.EQUIPMENT_SLOT.LeftPauldron
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.RPauldron) then
            return types.Actor.EQUIPMENT_SLOT.RightPauldron
        elseif (types.Armor.records[item.recordId].type == types.Armor.TYPE.Helmet) then
            return types.Actor.EQUIPMENT_SLOT.Helmet
        end
    elseif item.type == types.Clothing then
        if (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Amulet) then
            return types.Actor.EQUIPMENT_SLOT.Amulet
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Belt) then
            return types.Actor.EQUIPMENT_SLOT.Belt
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.LGlove) then
            return types.Actor.EQUIPMENT_SLOT.LeftGauntlet
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.RGlove) then
            return types.Actor.EQUIPMENT_SLOT.RightGauntlet
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Ring) then
            return types.Actor.EQUIPMENT_SLOT.RightRing
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Skirt) then
            return types.Actor.EQUIPMENT_SLOT.Skirt
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Shirt) then
            return types.Actor.EQUIPMENT_SLOT.Shirt
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Shoes) then
            return types.Actor.EQUIPMENT_SLOT.Boots
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Robe) then
            return types.Actor.EQUIPMENT_SLOT.Robe
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Pants) then
            return types.Actor.EQUIPMENT_SLOT.Pants
        end
    elseif item.type == types.Weapon then
        if (item.type.records[item.recordId].type == types.Weapon.TYPE.Arrow or item.type.records[item.recordId].type == types.Weapon.TYPE.Bolt) then
            return types.Actor.EQUIPMENT_SLOT.Ammunition
        end
        return types.Actor.EQUIPMENT_SLOT.CarriedRight
    end
    -- print("Couldn't find slot for " .. item.recordId)
    return nil
end
local itemToRemove
local actorToRemove

local actorToCheck
local function raceCheck(raceName)
    local endsWithTeen = string.sub(raceName, -4) == "teen"
    local endsWithTeens = string.sub(raceName, -5) == "teens"
    
    return endsWithTeen or endsWithTeens
end

local useNewID = {
    boots_for_beasts = function(actor)
        local race = types.NPC.records[actor.recordId].race
        local beastRaces = {
            ar_teen = true,
            argonian = true,
            khajiit = true,
            kh_teen = true
        }
        if beastRaces[race] then
            return beastRaces[race]
        end
        return false
    end,
    argonian_full_helms = function(actor)
        local race = types.NPC.records[actor.recordId].race
        local beastRaces = {
            ar_teen = true,
            argonian = true,
        }
        if beastRaces[race] then
            return beastRaces[race]
        end
        return false
    end,
    kidclothing = function(actor)
        local race = types.NPC.records[actor.recordId].race
        return raceCheck(race)
    end,
}
I.ItemUsage.addHandlerForType(types.Clothing, function(armor, actor)
    actorToCheck = actor
    if not armor then return end
    local record = types.Clothing.record(armor)
    for fileName, file in pairs(csvFiles) do
        local mseNewID = false
        if useNewID[fileName] then
            mseNewID = useNewID[fileName](actor)
            print("Checked" .. fileName)
        else
            print("No check for " .. fileName)
        end
        for key, data in ipairs(file) do
            --print(data.baseid:lower())
            if data.itemtype then
            print(data.itemtype)
            print(data.newid)
            print(data.baseid)
            end
            if data.baseid:lower() == armor.recordId and mseNewID == true  and data.itemtype ~= nil then
                --  print("Match")
                print(data.baseid)
                local check = types.Clothing.records[data.newid]
                if check then
                    local newArmor = world.createObject(tostring(data.newid), 1)
                    newArmor:moveInto(actor)
                    local eq = types.Actor.getEquipment(actor)
                    eq[findSlot(newArmor)] = newArmor.recordId
                    -- armor:remove(1)
                    itemToRemove = armor.recordId
                    actorToRemove = actor
                    actor:sendEvent("ES_setEquipment", eq)
                    return false
                else
                    print("No armor with id " .. data.newid)
                end
            elseif data.newid:lower() == armor.recordId and mseNewID == false and data.itemtype ~= nil then
                --  print("Match")
                print(data.baseid)
                local check = types.Clothing.records[data.newid]
                if check then
                    local newArmor = world.createObject(tostring(data.baseid), 1)
                    newArmor:moveInto(actor)
                    local eq = types.Actor.getEquipment(actor)
                    eq[findSlot(newArmor)] = newArmor.recordId
                    -- armor:remove(1)
                    itemToRemove = armor.recordId
                    actorToRemove = actor
                    actor:sendEvent("ES_setEquipment", eq)
                    return false
                else
                    print("No armor with id " .. data.newid)
                end
            end
        end
    end
end)
I.ItemUsage.addHandlerForType(types.Armor, function(armor, actor)
    actorToCheck = actor
    if not armor then return end
    local record = types.Armor.record(armor)
    for fileName, file in pairs(csvFiles) do
        print(fileName)
        local mseNewID = false
        if useNewID[fileName] then
            mseNewID = useNewID[fileName](actor)
            print("Checked" .. fileName)
        end
        for key, data in ipairs(file) do
            --print(data.baseid:lower())
            if data.baseid:lower() == armor.recordId and mseNewID == true  and data.itemtype == nil then
                --  print("Match")

                local check = types.Armor.records[data.newid]
                if check then
                    local newArmor = world.createObject(tostring(data.newid), 1)
                    newArmor:moveInto(actor)
                    local eq = types.Actor.getEquipment(actor)
                    eq[findSlot(newArmor)] = newArmor.recordId
                    -- armor:remove(1)
                    itemToRemove = armor.recordId
                    actorToRemove = actor
                    actor:sendEvent("ES_setEquipment", eq)
                    return false
                else
                    print("No armor with id " .. data.newid)
                end
            elseif data.newid:lower() == armor.recordId and mseNewID == false and data.itemtype == nil then
                --  print("Match")
                local check = types.Armor.records[data.newid]
                if check then
                    local newArmor = world.createObject(tostring(data.baseid), 1)
                    newArmor:moveInto(actor)
                    local eq = types.Actor.getEquipment(actor)
                    eq[findSlot(newArmor)] = newArmor.recordId
                    -- armor:remove(1)
                    itemToRemove = armor.recordId
                    actorToRemove = actor
                    actor:sendEvent("ES_setEquipment", eq)
                    return false
                else
                    print("No armor with id " .. data.newid)
                end
            end
        end
    end
    print(record.id)
end)
local function RemoveItemNow()
    for index, value in ipairs(types.Actor.inventory(actorToRemove):getAll()) do
        if value.recordId == itemToRemove then
            value:remove(1)
            actorToRemove:sendEvent("OpenAgain")
            return
        end
    end
end
if core.API_REVISION < 30 then
    return false
end
--table.sort(objectTypes, sortByIntCount)
return {
    interfaceName = "eswap",
    interface = {
        version = 1,
        csvFiles = csvFiles,
    },
    eventHandlers = {
        RemoveItemNow = RemoveItemNow
    }
}
