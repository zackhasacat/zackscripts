local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local async = require("openmw.async")
local function validatePosition(position, options)
    local restrictPosZ
    if options then
        restrictPosZ = options.restrictPosZ
    else
        return true
    end
    if restrictPosZ and position.z < restrictPosZ then
        return false
    end
    return true
end
local contentFileType = { desiredFile = 1, removeFile = 2, originalGame = 3, dynamicCreated = 4, otherMod = 5 }
local function getContentFileType(cindex, desiredModIndex, removeModIndex)
    if tostring(cindex) == tostring(desiredModIndex) then
        return contentFileType.desiredFile
    elseif tostring(cindex) == tostring(removeModIndex) then
        return contentFileType.removeFile
    elseif tostring(cindex) == tostring(-1) then
        return contentFileType.dynamicCreated
    else
        return contentFileType.otherMod
    end
end
local function cleanCells(cellList, removeMod, desiredMod, options)
    local restrictPosZ
    if not core.contentFiles.has(removeMod) then
        print("The mod to remove is not installed" .. removeMod)

        return
    end
    if not core.contentFiles.has(desiredMod) then
        print("The desired mod is not installed: " .. desiredMod)

        return
    end
    if options then
        restrictPosZ = options.restrictPosZ
    end
    local removeModIndex = core.contentFiles.indexOf(removeMod) - 1
    local desiredModIndex = core.contentFiles.indexOf(desiredMod) - 1
    print("Using these index",removeModIndex,desiredModIndex)
    if desiredModIndex < removeModIndex then
        error("The desired mod should be below the removed mod in the load order:" .. removeMod .. ", " .. desiredMod)
    end
    local removeCount = 0
    for index, cellName in ipairs(cellList) do
        local cell = world.getCellByName(cellName)
        for index, object in ipairs(cell:getAll()) do
            local objectContentFile = string.match(object.id, "_(.*)")
            local fileType = getContentFileType(objectContentFile, desiredModIndex, removeModIndex)
            if fileType == contentFileType.removeFile and validatePosition(object.position, options) then
                object:remove()
                removeCount = removeCount + 1
            end
        end
    end
    print("Removed this many objects from:",removeMod,removeCount)
end
local function doTheWork()
    cleanCells({ "Caldera, Guild of Mages" }, "beautiful cities of morrowind.esp", "caldera mages guild expanded.esp",
        { restrictPosZ = 400 })
end
return {
    interfaceName  = "CompatTool",
    interface      = {
        version = 1,
        cleanCells = cleanCells,
        doTheWork = doTheWork
    },
    engineHandlers = {
    },
    eventHandlers  = {
    },
}
