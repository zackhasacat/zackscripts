
local self = require("openmw.self")
local types = require("openmw.types")
local core = require("openmw.core")
local I = require("openmw.interfaces")
local prevItems 
local function UiModeChanged(data)
    if not I.TSC.isEligibleForCurse() then
    elseif data.newMode == "Dialogue" then
        if not prevItems then
            prevItems = {}
      for index, value in ipairs(types.Actor.inventory(self):getAll()) do
        table.insert(prevItems,{id = value.recordId,count = value.count})
      end
        end
    elseif data.newMode == "Barter" then
        prevItems = nil
    elseif not data.newMode and prevItems then
        for index, value in ipairs(types.Actor.inventory(self):getAll()) do
            local found = false
            local dropCount = value.count
            for index, prev in ipairs(prevItems) do
                if prev.id == value.recordId then
                    found = true
                    if prev.count < value.count then
                        dropCount = prev.count - value.count
                    end
                end
            end
            if not found and value.recordId ~= "gold_001" then
                core.sendGlobalEvent("dropItem",{item = value, count = dropCount})
            end
          end

        prevItems = nil
    end
end

return {
    eventHandlers = {
        UiModeChanged = UiModeChanged
    },
}



