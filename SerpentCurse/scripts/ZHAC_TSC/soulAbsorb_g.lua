local world = require("openmw.world")
local types = require("openmw.types")
local util = require("openmw.util")
local multiplier = 1
local function getMagicToRestore(value)
    return math.floor(value * multiplier)
end

local function soulAbsorb(data)
    local soulId = data.soulId
    local player = data.player
    local item = data.item

    types.Item.itemData(item).soul = nil--reset the gem
    if not types.Creature.records[soulId] then
        return
    end
    local soulValue = types.Creature.records[soulId].soulValue
    local restoreAmount = getMagicToRestore(soulValue)
    local magicka = types.Actor.stats.dynamic.magicka(player)
    local currentVal = magicka.current
    local newAmount = currentVal + restoreAmount
    if newAmount > magicka.base then
        newAmount = magicka.base
    end
   -- magicka.current = newAmount
    player:sendEvent("setMagickaSA",newAmount)
end

return {
    eventHandlers = {
        soulAbsorb = soulAbsorb
    }
}