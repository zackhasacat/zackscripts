---@diagnostic disable: unused-local
local ui = require("openmw.ui")
local I = require("openmw.interfaces")
--local layers = require("scripts.ControllerInterface.ci_layers")
local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local cam = require("openmw.interfaces").Camera
local core = require("openmw.core")
local types = require("openmw.types")
local ambient = require('openmw.ambient')
local self = require("openmw.self")
local nearby = require("openmw.nearby")
local Camera = require("openmw.camera")
local camera = require("openmw.camera")
local input = require("openmw.input")
local async = require("openmw.async")
local storage = require("openmw.storage")
local Player = require('openmw.types').Player

local lastExtraModifier = 0

local serpCurseId = "zhac_serpent_curse"
local serpBlessId = "zhac_serpent_blessing"
local serpBlessIdAttrib = "zhac_serpent_blessing2"
local serpBonusId = "zhac_serpent_bonus"
local attributes = {
    types.Actor.stats.attributes.strength,
    types.Actor.stats.attributes.intelligence,
    types.Actor.stats.attributes.speed,
    types.Actor.stats.attributes.agility,

}
local attributesBonus = {
    types.Actor.stats.attributes.speed,

}
local skillBonus = {
   [ types.NPC.stats.skills.acrobatics] = 30,
   [ types.NPC.stats.skills.handtohand] = 40,

}
local modifier = 30
local function hasSpell(spellName) --Checks if the player's spellbook contains the specified spell.
    if (spellName.id ~= nil) then spellName = spellName.id end
    for index, value in ipairs(types.Actor.spells(self)) do
        if (value.id == spellName:lower()) then return true end
    end
    return false
end
local modifierAdded = false
local function addModifier(amount) --Removes the specified spell from the player' spellbook.
    --  if hasSpell(spellName) then
    lastExtraModifier = amount
    if modifierAdded then return end
    for index, attrib in ipairs(attributes) do
       -- attrib(self).base = attrib(self).base - modifier
        attrib(self).modifier =  attrib(self).modifier  + modifier + amount 
    end
    modifierAdded = true
    -- types.Actor.spells(self):remove(spellName)
    --   end
end
local function removeModifier() --Removes the specified spell from the player' spellbook.
    --  if hasSpell(spellName) then
    --   end
    if not lastExtraModifier then
        lastExtraModifier = 0
    end
    if not modifierAdded then
        return
    end
    for index, attrib in ipairs(attributes) do
        attrib(self).modifier =  attrib(self).modifier  - ( modifier + lastExtraModifier)
     --   attrib(self).base = attrib(self).base + modifier
    end
    modifierAdded = false
end
local function updateModifier(amount)
    removeModifier()
    addModifier(amount)
end
local modifierAddedBonus = false
local function addModifierBonus() --Removes the specified spell from the player' spellbook.
    --  if hasSpell(spellName) then
    if modifierAddedBonus then return end
    for index, attrib in ipairs(attributesBonus) do
       -- attrib(self).base = attrib(self).base - modifier
        attrib(self).modifier =  attrib(self).modifier  + modifier
    end
    for skill, amount in pairs(skillBonus) do
        skill(self).modifier = skill(self).modifier + amount
    end
    modifierAddedBonus = true
    -- types.Actor.spells(self):remove(spellName)
    --   end
end
local function removeModifierBonus(spellName) --Removes the specified spell from the player' spellbook.
    --  if hasSpell(spellName) then
    --   end
    if not modifierAddedBonus then
        return
    end
    for index, attrib in ipairs(attributesBonus) do
        attrib(self).modifier =  attrib(self).modifier  - modifier
     --   attrib(self).base = attrib(self).base + modifier
    end
    for skill, amount in pairs(skillBonus) do
        skill(self).modifier = skill(self).modifier - amount
    end
    modifierAddedBonus = false
end
return {
    engineHandlers = {
        onSave = function()
            return {
                modifierAdded = modifierAdded,
                modifierAddedBonus = modifierAddedBonus,
                lastExtraModifier = lastExtraModifier,
            }
        end,
        onLoad = function(data)
            if data then
                lastExtraModifier = data.lastExtraModifier or 0
              --  removed = data.removed
              if data.modifierAddedBonus then
                modifierAddedBonus = data.modifierAddedBonus

              end
              if data.modifierAdded then
                modifierAdded = data.modifierAdded
              end
            end
        end

    },
    eventHandlers = {
        updateModifier = updateModifier,
        addModifier = addModifier,
        removeModifier = removeModifier,
        removeModifierBonus = removeModifierBonus,
        addModifierBonus = addModifierBonus,
        UiModeChanged = function(data)
            -- print('LMMUiModeChanged to', data.newMode, '(' .. tostring(data.arg) .. ')')
            local isOutside = self.cell.isExterior
            if not I.TSC.isEligibleForCurse() then
            elseif data.newMode == "LevelUp" and hasSpell(serpBlessIdAttrib)  then
            elseif data.newMode == "Rest" and hasSpell(serpCurseId) then
                ui.showMessage("The curse prevents you from being able to rest.")
                    I.UI.setMode()
                    return
            elseif data.newMode == "Rest" and isOutside then
                ui.showMessage("You are unable to rest outside.")
                I.UI.setMode()
                return
            elseif data.newMode == "Dialogue" then
                core.sendGlobalEvent("removeRing",true)
            elseif data.newMode == "Rest" and hasSpell(serpCurseId)then
                if hasSpell(serpCurseId) then
                    ui.showMessage("The curse prevents you from being able to rest.")
                    I.UI.setMode()
                    return
                end
            elseif not data.newMode then

            end
        end,

    },
}
