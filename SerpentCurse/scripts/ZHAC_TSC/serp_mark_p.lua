local animation = require('openmw.animation')
local self      = require("openmw.self")
local types     = require('openmw.types')
local ui        = require('openmw.ui')
local camera    = require("openmw.camera")
local I         = require("openmw.interfaces")
local nearby    = require("openmw.nearby")
local core      = require("openmw.core")
local markData
local castDone  = false
local distCheck = 100
local function isInFaction(id)
    local player = self
    -- print(id)
    if types.NPC.isExpelled(player, id) then
        return false
    end
    for i, x in pairs(types.NPC.getFactions(player)) do
        if x == id then
            return true
        end
    end
    return false
end
local function allowedToGrab(obj)
    if not types.Item.isCarriable(obj) then
        return false
    end
    if obj.owner.recordId then
        return false
    elseif obj.owner.factionId and obj.owner.factionRank and isInFaction(obj.owner.factionId) and types.NPC.getFactionRank(self, obj.owner.factionId) >= obj.owner.factionRank then
        return true
    elseif obj.owner.factionId then
        return false 
    end
    return true
end
local function findAndTeleportItems()
    local itemsToSave = {}
    local containerToCheck = {}
    for index, obj in ipairs(nearby.items) do
        local dist = (obj.position - self.position):length()
        local underDist = dist < distCheck
        if underDist and allowedToGrab((obj)) then
            core.sendGlobalEvent("placeVFX", obj.position)
            table.insert(itemsToSave, obj)
            print("sending...",obj.recordId)
        end
    end
    for index, obj in ipairs(nearby.containers) do
        local dist = (obj.position - self.position):length()
        local underDist = dist < distCheck
        if underDist and allowedToGrab((obj)) and not obj.type.record(obj).isOrganic then
            core.sendGlobalEvent("placeVFX", obj.position)
            table.insert(containerToCheck, obj)
        end
    end
    for index, obj in ipairs(nearby.actors) do
        local dist = (obj.position - self.position):length()
        local underDist = dist < distCheck
        if underDist and types.Actor.isDead(obj) then
            core.sendGlobalEvent("placeVFX", obj.position)
            table.insert(containerToCheck, obj)
        end
    end
    core.sendGlobalEvent("TeleportItems", { objects = itemsToSave, containers = containerToCheck, position = markData })
end
local wasMarking = false
local castStarted = false
local function onUpdate()

    if not I.TSC.isEligibleForCurse() then
        return
    end
    local currentGroup = animation.getActiveGroup(self, animation.BONE_GROUP.RightArm)
    local completion = 0
    if currentGroup == "spellcast" then
        completion = animation.getCompletion(self, currentGroup)
    end
    if currentGroup == "spellcast" and types.Actor.getSelectedSpell(self) and types.Actor.getSelectedSpell(self).id == "zhac_serpent_itemtp" and completion < 0.75 then
        local activeSpells = types.Actor.activeSpells(self)
        --  local  x,y,z =self.rotation:getAnglesZYX()

        castStarted = true
        if math.deg(camera.getPitch()) < 70 then
            castStarted = false
            animation.cancel(self, currentGroup)
        end
    elseif types.Actor.getSelectedSpell(self) and types.Actor.getSelectedSpell(self).id == "zhac_serpent_itemtp" and castStarted then
        castStarted = false
        print("Done casting")
        findAndTeleportItems()
    else
        castDone = false
    end
    local mark = types.Actor.activeEffects(self):getEffect("mark").magnitude
    if mark > 0 and not wasMarking then
        --ui.showMessage("Mark!")
        markData = { cell = self.cell.id, position = self.position }
        wasMarking = true
    elseif mark > 0 and wasMarking then

    else
        wasMarking = false
    end
end
local function setMagicka(num)
    types.Actor.stats.dynamic.magicka(self).current = num
end
return {
    engineHandlers = {
        onUpdate = onUpdate,
        onLoad = function(data)
            if data then
                markData = data.markData
            end
        end,
        onSave = function()
            return { markData = markData }
        end
    },
    eventHandlers = {
        setMagicka = setMagicka
    }
}
