local ui = require("openmw.ui")
local I = require("openmw.interfaces")
--local layers = require("scripts.ControllerInterface.ci_layers")
local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local cam = require("openmw.interfaces").Camera
local core = require("openmw.core")
local types = require("openmw.types")
local ambient = require('openmw.ambient')
local self = require("openmw.self")
local nearby = require("openmw.nearby")
local Camera = require("openmw.camera")
local camera = require("openmw.camera")
local input = require("openmw.input")
local async = require("openmw.async")
local storage = require("openmw.storage")
local animation = require('openmw.animation')
local Player = require('openmw.types').Player
local curseAnim
local ringItem = "zhac_ring_blocker"
local secretGem = "misc_soulgem_secret"
local serpCurseId = "zhac_serpent_curse"
local serpCurseAdv = "zhac_serpent_curse2"
local serpBlessId = "zhac_serpent_blessing"
local serpBlessIdAttrib = "zhac_serpent_blessing2"
local serpBonusId = "zhac_serpent_bonus"
local lastCurseTime = 0
local maxMoveEncumberance = 60
local advancedCurseLevel = 30
local lastModifier = 0
local isCursed = false
local constants = require("scripts.ZHAC_TSC.constants")
local maxCastEncumberance = 5
local timecheck = 0
local chargenComplete = false
local skipMe = false
local wasRun = false
local wasParal
local timeOfThird = 0
local startZPos = 0
local wasFalling = false
local wasSoul = false
local lastCell

local freeGoldCount = 500
local function hasSpell(spellName) --Checks if the player's spellbook contains the specified spell.
    if (spellName.id ~= nil) then spellName = spellName.id end
    return types.Actor.spells(self)[spellName] ~= nil
end
local function getEquipped(recordId, actor)
    for i, x in pairs(types.Actor.getEquipment(actor)) do
        if x.recordId == recordId then return true end
    end
    return false
end
local function getSpellCount()
    local spells = types.Actor.spells(self)
    local count = 0
    for index, spell in ipairs(spells) do
        if spell.type == core.magic.SPELL_TYPE.Power or spell.type == core.magic.SPELL_TYPE.Spell then
            count = count + 1
        end
    end
    return count
end
--TODO: Curse for having followers
local blessState = 0 --nothing, 1==cursed, 2 == blessed
local bonusState = 0
local function hasSerpentBirthsign()
    local hasEffect = hasSpell("star-curse")

    --return true
    return hasEffect
end
local function isExempt(item)
    if item.recordId == "misc_soulgem_secret" then
        return true
    elseif item.recordId == ringItem then
        return true
    end
    return false
end
local RESDAYNIA_SANCTUARY = "Resdaynia Sanctuary"
local ENTRANCE = RESDAYNIA_SANCTUARY .. ", Entrance"
local POSITION_THRESHOLD = 11318
local function startsWith(inputString, startString)
    return string.sub(inputString, 1, string.len(startString)) == startString
end
local function isInVault(actor, nameOnly)
    local cellName = actor.cell.name
    if cellName == ENTRANCE and not nameOnly then
        if self.position.x > POSITION_THRESHOLD then
            return true
        else
            return false
        end
    end
    if startsWith(cellName, RESDAYNIA_SANCTUARY) then
        return true
    end
    return false
end
local function isEligibleForCurse()
    if isCursed then
        return true
    end
    if skipMe then
        return false
        elseif isInVault(self) then
            return false
    elseif hasSerpentBirthsign() then
        isCursed = true
        return true
    else
        return false
    end
end
local function getItemStacks()
    local count = 0
    local equipped = 0
    for index, value in ipairs(types.Actor.inventory(self):getAll()) do
        if not isExempt(value) then
            count = count + 1
            if getEquipped(value.recordId, self) then
                equipped = equipped + 1
            end
        end
    end
    return count, equipped
end
local function addSpell(SpellId)
    types.Actor.spells(self):add(SpellId)
end
local function isFalling()
    if types.Actor.isOnGround(self) == true then
        return false
    elseif types.Actor.isSwimming(self) then
        return false
    elseif types.Actor.activeEffects(self):getEffect("levitate").magnitude > 0 then
        return false
    else
        return true
    end
end
local function removeSpell(spellName) --Removes the specified spell from the player' spellbook.
    --  if hasSpell(spellName) then
    types.Actor.spells(self):remove(spellName)
    --   end
end
local function getEncumburance(removeFeather)
    local carryWeight = 0
    local totalFeather = 0
    for index, value in pairs(types.Actor.activeEffects(self)) do
        --  print(value.id)
        if value.id == "feather" then
            local mag = value.magnitude
            if removeFeather and (mag > 1 or totalFeather > 0) then
                types.Actor.activeEffects(self):remove(value.id)
                carryWeight = carryWeight - value.magnitude
                totalFeather = totalFeather + value.magnitude
            elseif not removeFeather then
                carryWeight = carryWeight - value.magnitude
            end
            --
            --
        elseif value.id == "burden" then
            carryWeight = carryWeight + value.magnitude
        end
    end
    for index, value in ipairs(types.Actor.inventory(self):getAll()) do
        if not isExempt(value) then
            local count = value.count
            if value.recordId == "gold_001" then
                count = count - freeGoldCount
                if count < 1 then
                    count = 1
                end
            end
            local weight = value.type.record(value).weight * count
            carryWeight = carryWeight + weight
        end
    end
    return carryWeight
end
local omwsettings = storage.playerSection('SettingsOMWControls')

local advancedCurseApplied = false
local function addAdvancedCurse()
    if advancedCurseApplied then
        return
    end
    addSpell(serpCurseAdv)
    advancedCurseApplied = true
    ambient.playSound("destruction hit")
    core.sendGlobalEvent("setAdvanceCurseState", true)
end
local function removeAdvancedCurse()
    if not advancedCurseApplied then
        return
    end
    advancedCurseApplied = false
    removeSpell(serpCurseAdv)
    core.sendGlobalEvent("setAdvanceCurseState", false)
end
local function isFirstPerson()
    if not constants.require3rdPerson then
        return false
    else
        return camera.getMode() == camera.MODE.FirstPerson
    end
end

local function addCurseEffects()
    wasRun = self.controls.run
    addSpell(serpCurseId)
    ambient.playSound("destruction hit")
    addSpell("zhac_serpent_itemTP")
    removeSpell(serpBlessId)
    removeSpell(serpBonusId)
    -- removeSpell(serpBlessIdAttrib)
    self:sendEvent("removeModifier")
    self:sendEvent("removeModifierBonus")

    animation.addVfx(self, "meshes\\e\\magic_hit_poison.NIF", { loop = true, vfxId = "animSerpCurse" })
    curseAnim = true
    bonusState = 0
    if I.JItem then
        I.JItem.setinvOnly(true)
    end
    Player.setControlSwitch(self, Player.CONTROL_SWITCH.Jumping, false)
    Player.setControlSwitch(self, Player.CONTROL_SWITCH.Magic, false)
    if types.Actor.getStance(self) == types.Actor.STANCE.Spell then
        types.Actor.setStance(self, types.Actor.STANCE.Nothing)
    end
end
local function removeCurseEffects()
    removeSpell(serpBlessId)
    --  removeSpell(serpBlessIdAttrib)
    self:sendEvent("removeModifier")
    self:sendEvent("removeModifierBonus")

    removeAdvancedCurse()
    removeSpell(serpCurseId)
    --addSpell(serpBlessIdAttrib)
    lastModifier = I.TSC.getTimeSinceCurse(true)

    self:sendEvent("addModifier", lastModifier)

    self.controls.run = true
    if curseAnim then
        animation.removeVfx(self, 'animSerpCurse')
        curseAnim = nil
    end
    omwsettings:set('alwaysRun', true)
    if I.JItem then
        I.JItem.setinvOnly(false)
    end
    --  print("Added blessing")
    Player.setControlSwitch(self, Player.CONTROL_SWITCH.Jumping, true)
    Player.setControlSwitch(self, Player.CONTROL_SWITCH.Magic, true)
end
local function onUpdate(dt)
    if blessState == 1 and self.controls.run == true then
        self.controls.run = false
    end
    if blessState == 1 and omwsettings:get("alwaysRun") == true then
        omwsettings:set('alwaysRun', false)
    end
    timecheck = timecheck + dt
    if blessState ~= 0 and not isEligibleForCurse() then
        removeCurseEffects()
        blessState = 0
        return
    elseif blessState == 0 and not isEligibleForCurse() then
        return
    elseif blessState > 0 and not isEligibleForCurse() then

    end

    if blessState == 1 then
        if isFirstPerson() then
            camera.setMode(camera.MODE.ThirdPerson)
        end
        if isFalling() then
            wasFalling = true
        else
            if wasFalling then
                wasFalling = false
                types.Actor.stats.dynamic.fatigue(self).current = -1
            end
            -- startZPos = self.position.z
        end
    end
    if timecheck > 0.1 then
        local gem = types.Actor.inventory(self):find(secretGem)
        if gem then
            local soul = types.Item.itemData(gem).soul
            if soul and not wasSoul then
                core.sendGlobalEvent("soulAbsorb", { item = gem, soulId = soul, player = self })
                wasSoul = true
            elseif not soul and wasSoul then
                wasSoul = false
            end
        end
        local cell = self.cell.id
        if cell ~= lastCell and blessState == 1 then
            animation.removeVfx(self, 'animSerpCurse')
            animation.addVfx(self, "meshes\\e\\magic_hit_poison.NIF", { loop = true, vfxId = "animSerpCurse" })
        end
        if not isFirstPerson() then
            timeOfThird = timeOfThird + timecheck
        else
            timeOfThird = 0
        end
        if not chargenComplete and not types.Player.isCharGenFinished(self) then
            return
        elseif not chargenComplete then --chargen not marked as complete, but it is now
            local hasEffect = hasSerpentBirthsign()
            if hasEffect then
                addSpell("zhac_serpent_itemTP")
                skipMe = false
            else
                skipMe = true
                removeSpell(serpBlessId)
                --   removeSpell(serpBlessIdAttrib)
                self:sendEvent("removeModifier")
                removeSpell(serpBonusId)
                self:sendEvent("removeModifierBonus")

                removeSpell(serpCurseId)
            end
            chargenComplete = true
        end
        local check = getEncumburance(true)
        if check > maxMoveEncumberance then
            I.Controls.overrideMovementControls(true)
            self.controls.movement = 0
        else
            I.Controls.overrideMovementControls(false)
        end
        if check > advancedCurseLevel and not advancedCurseApplied then
            addAdvancedCurse()
        elseif hasSpell(serpCurseAdv) and check < advancedCurseLevel then
            removeAdvancedCurse()
        end
        if check > maxCastEncumberance then
            Player.setControlSwitch(self, Player.CONTROL_SWITCH.Magic, false)
            if types.Actor.getStance(self) == types.Actor.STANCE.Spell then
                types.Actor.setStance(self, types.Actor.STANCE.Nothing)
            end
        else
            Player.setControlSwitch(self, Player.CONTROL_SWITCH.Magic, true)
        end
        local stacks, equipped = getItemStacks()
        local stacksEquipped = stacks == equipped
        --  for index, value in ipairs(nearby.actors) do
        --       value:sendEvent("checkForFollow")
        --   end
        if check >= 0.2 and blessState ~= 1 then
            addCurseEffects()
            removeSpell(serpBonusId)
            blessState = 1
            bonusState = 0
            if I.JItem then
                I.JItem.setinvOnly(true)
            end
            --  print("Added Curse")
        elseif check < 0.2 and blessState ~= 2 then
            if blessState == 1 then
                lastCurseTime = core.getGameTime()
            end
            removeCurseEffects()
            addSpell(serpBlessId)
            --addSpell(serpBlessIdAttrib)
            lastModifier = I.TSC.getTimeSinceCurse(true)

            self:sendEvent("addModifier", lastModifier)

            self.controls.run = true
            blessState = 2
        end
        if blessState == 2 then
            if lastModifier ~= I.TSC.getTimeSinceCurse(true) then
                lastModifier = I.TSC.getTimeSinceCurse(true)
                print("updateModifier")
                self:sendEvent("updateModifier", lastModifier)
            end
        end
        if bonusState ~= 1 and blessState == 2 and stacks < 3 and stacksEquipped and timeOfThird > 30 and getSpellCount() < 21 then
            addSpell(serpBonusId)
            self:sendEvent("addModifierBonus")
        else
            removeSpell(serpBonusId)
            self:sendEvent("removeModifierBonus")
        end
        lastCell = cell
        timecheck = 0
    end
end

--input.registerTriggerHandler('Journal', async:callback(function()
--    return true
--end))

local onLoad = function(data)
    if data and data.timeOfThird then
        timeOfThird = data.timeOfThird
    end
    if data and data.isCursed ~= nil then
        isCursed = data.isCursed
    end
    if data and data.advancedCurseApplied then
        advancedCurseApplied = data.advancedCurseApplied
    end
    chargenComplete = data.chargenComplete or false
    if data and data.lastCurseTime then
        lastCurseTime = data.lastCurseTime
    end
    if data and data.skipMe then
        skipMe = true
        removeSpell(serpBlessId)
        --  removeSpell(serpBlessIdAttrib)
        self:sendEvent("removeModifier")
        self:sendEvent("removeModifierBonus")

        removeSpell(serpBonusId)
        removeSpell(serpCurseId)
    else
        if not chargenComplete and not types.Player.isCharGenFinished(self) then
            return
        elseif not chargenComplete then --chargen not marked as complete, but it is now
            local hasEffect = hasSerpentBirthsign()
            if hasEffect then
                skipMe = false
            else
                skipMe = true
                removeSpell(serpBlessId)
                --   removeSpell(serpBlessIdAttrib)
                self:sendEvent("removeModifier")
                self:sendEvent("removeModifierBonus")

                removeSpell(serpBonusId)
                removeSpell(serpCurseId)
            end
            chargenComplete = true
        end
    end
end
local onSave = function()
    return {
        advancedCurseApplied = advancedCurseApplied,
        skipMe = skipMe,
        chargenComplete = chargenComplete,
        timeOfThird = timeOfThird,
        lastCurseTime = lastCurseTime,
        isCursed = isCursed,
    }
end
return {
    interfaceName = "TSC",
    interface = {
        getEncumburance = getEncumburance,
        hasSpell = hasSpell,
        isFalling = isFalling,
        onSave = onSave,
        onLoad = onLoad,
        isEligibleForCurse = isEligibleForCurse, --I.TSC.isEligibleForCurse()
        getTimeSinceCurse = function(getDays)
            local delta = core.getGameTime() - lastCurseTime
            if getDays then
                delta = ((delta / 60) / 60) / 24
            end
            return math.floor(delta)
        end
    },
    eventHandlers = {
        setMagickaSA = function(newAmount)
            local magicka = types.Actor.stats.dynamic.magicka(self)
            magicka.current = newAmount
        end
    },
    engineHandlers = {
        onSave = onSave,
        onLoad = onLoad,
        onUpdate = onUpdate,
        onInputAction = function(key)
            if key == input.ACTION.ToggleSpell then
                if I.UI.getMode() == "Interface" then
                    ui.showMessage("drop")
                    core.sendGlobalEvent("dropAllItems", self)
                end
            end
        end,
    }

}
