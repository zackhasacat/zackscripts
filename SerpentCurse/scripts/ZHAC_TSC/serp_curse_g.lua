local advancedCurseState = false
local world = require("openmw.world")
local types = require("openmw.types")

local function updateActor(actor)
    if actor.recordId == "player" then--_TODOL fix this
        return
    end
    if actor.type ~= types.NPC then
        return
    end
    local disposition = types.NPC.getDisposition(actor, world.players[1])
    local baseDisposition = types.NPC.getBaseDisposition(actor, world.players[1])
    if advancedCurseState == true then
        if baseDisposition > 0 and disposition < 100 then
         --   print("making enemy " .. actor.recordId)
            types.NPC.setBaseDisposition(actor, world.players[1], -100)
           -- local fight = types.Actor.stats.ai.fight(actor).base
           -- if fight < 80 then
            --    actor:sendEvent("runFromPlayer")
           --end
        end
    else
        if baseDisposition == -100 then
--            print("making friend " .. actor.recordId)
            local recordDisposition = types.NPC.records[actor.recordId].baseDisposition
            types.NPC.setBaseDisposition(actor, world.players[1], recordDisposition)
          --  actor:sendEvent("stopRunFromPlayer")
        end
    end
end
local function updateActiveActors()
    for index, actor in ipairs(world.activeActors) do
        updateActor(actor)
    end
end

local function setAdvanceCurseState(state)
    advancedCurseState = state
    updateActiveActors()
end
local function onActorActive(actor)
    if actor.type == types.NPC then
        updateActor(actor)
    end
end
local function dropAllItems(actor)
    for i, x in ipairs(types.Actor.inventory(actor):getAll()) do
        x:teleport(actor.cell,actor.position)
    end
end
return {
    eventHandlers = {
        setAdvanceCurseState = setAdvanceCurseState,
        dropAllItems = dropAllItems,
    },
    engineHandlers = {
        onActorActive = onActorActive,
        onSave = function()
            return { advancedCurseState = advancedCurseState }
        end,
        onLoad = function(data)
            if data then
                advancedCurseState = data.advancedCurseState
            end
        end
    }
}
