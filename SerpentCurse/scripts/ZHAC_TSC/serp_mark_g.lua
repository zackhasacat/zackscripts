local world = require("openmw.world")
local types = require("openmw.types")
local util = require("openmw.util")
local function teleportItem(obj, cell, pos)
    print("teleporting...",obj.recordId)
    local bb = obj:getBoundingBox()
    local zOffset = bb.halfSize.z
    pos = util.vector3(pos.x, pos.y, pos.z + zOffset)
    obj:teleport(cell, pos)
end
local weightmultiplier = 1
local function getItemWeight(stack)
local weight = stack.type.records[stack.recordId].weight
if stack.recordId == "gold_100" then
    weight =  stack.type.records["gold_001"].weight
end
return weight * stack.count
end
local function TeleportItems(data)
    local items = data.objects
    local position = data.position
    local pos = position.position
    local availableMagic = types.Actor.stats.dynamic.magicka(world.players[1]).current
    local cell = world.getCellById(position.cell)
    for index, value in ipairs(data.containers) do
        types.Container.content(value):resolve()
        -- for some reason items in corpses are owned by the dead body
        for index, item in ipairs(types.Container.content(value):getAll()) do
            item.owner.recordId = nil
            item.owner.factionId = nil
            local weight = getItemWeight(item) * weightmultiplier
            print("weight...",weight)
            if availableMagic > weight then
                teleportItem(item, cell, pos)
                availableMagic = availableMagic - weight
            else
                print("Not enough magic for  " .. item.recordId)
            end
        end
    end
    for index, item in ipairs(items) do
        local weight = getItemWeight(item) * weightmultiplier
        if availableMagic > weight then
            teleportItem(item, cell, pos)
            availableMagic = availableMagic - weight
            if not item.parentContainer and item.position then
                world.vfx.spawn(types.Static.records["VFX_MysticismArea"].model, item.position)
            end
        else
            print("Not enough magic for  " .. item.recordId)
        end
    end
    print("Sending items to " .. position.cell)
    world.players[1]:sendEvent("setMagicka", availableMagic)
end
local function dropItem(data)
    teleportItem(data.item, world.players[1].cell, world.players[1].position)
end

return {
    eventHandlers = {
        TeleportItems = TeleportItems,
        dropItem = dropItem,
        placeVFX = function(position)
            -- world.vfx.spawn(types.Static.records["VFX_MysticismArea"].model,position)
        end
    }
}
