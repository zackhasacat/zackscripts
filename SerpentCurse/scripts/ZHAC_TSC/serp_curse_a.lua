local self = require("openmw.self")
local types = require("openmw.types")
local I = require("openmw.interfaces")
local async = require("openmw.async")
local nearby = require("openmw.nearby")
local AI = I.AI
local wasFleeing = false
local unableToFlee = false
local orginalPos
local function stopRunFromPlayer()
    if not wasFleeing then
        return
    end
    if types.Actor.stats.ai.flee(self).modifier == 300 then
        I.AI.removePackages("Combat")
        types.Actor.stats.ai.flee(self).modifier = 0
        
        if orginalPos then
            I.AI.startPackage({type = "Travel",destPosition = orginalPos, isRepeat = false})
            orginalPos = nil
        end
    end
    wasFleeing = false
end
local function runFromPlayer()
    if unableToFlee then
        return
    end
    --TODO: skip if already in combat, or has high fight
    if types.Actor.stats.ai.flee(self).modifier == 300 then
        return
    end
    orginalPos = self.position
    types.Actor.stats.ai.flee(self).modifier = 300
    I.AI.startPackage({type = "Combat",target = nearby.players[1]})
    wasFleeing = true
    async:newUnsavableSimulationTimer(0.1, function()
  
        local isFleeing = I.AI.isFleeing()
       -- print(self.recordId,isFleeing)
        if not isFleeing then
            stopRunFromPlayer()
            unableToFlee = true
        end
    end)
end

return {
    eventHandlers = {
        runFromPlayer = runFromPlayer,
        stopRunFromPlayer = stopRunFromPlayer,
    },
    engineHandlers = {
        onSave = function ()
            return {wasFleeing = wasFleeing,orginalPos = orginalPos, unableToFlee = unableToFlee}
        end,
        onLoad = function (data)
            if data then
                orginalPos = data.orginalPos
                wasFleeing = data.wasFleeing
                unableToFlee = data.unableToFlee
            end
        end
    }
}