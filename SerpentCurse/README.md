# Eryndril’s Curse: A Minimalist’s Struggle

**Eryndril’s Curse** is a mod that forces you to play a bit differently.

Named after the character I playtested it with, the mod will give you a curse if you carry more than **0.2 weight** of items. 

---

## Required Mods
- **Gold Weight**: Makes each piece of gold weigh 0.01.
- **No Sneaky Sleeping**: Prevents sleeping in owned beds, regardless of stealth.
- **Journal and Map Items**: Ties the map and journal to items, restricting access to when they are available.

---

## Recommended Mods (Not Cheating)
- **Banks**, especially **Faction Banks**:  
  Faction Banks adds a bank to each faction, allowing you to have a balance with the Mages Guild, for example. Gold can be stored and automatically transferred to your balance when trading with any faction member.

---

## Mods to Avoid
- **Multimark Mods**

---

## The Curse

The curse activates when you are carrying more than **0.2 weight** (including equipped items). It has the following effects:

1. **Movement Restrictions**:
   - Unable to run.
   - Unable to jump. Falling any distance causes you to collapse.
   - Unable to move if carrying more than **60 weight**, regardless of strength.

2. **Gameplay Restrictions**:
   - Unable to use first-person camera.
   - Feather spell effects are automatically removed.
   - Unable to use the stats or magic menu.
   - NPCs will refuse to serve you when below 50 disposition(Filter these for each type)
   - Unable to cast spells. (Enchantment casting is possible if carrying less than **5 weight**, but must be pre-selected or assigned to a hotkey.)

3. **Visual Effects**:
   - Poison VFX: Green smoke follows you.

4. **Stat Reductions**:
   - Agility, Speed, and Endurance: **-30 each**
   - Strength: **-20**
   - Luck: **-200**
   - Mercantile, Speechcraft, and Personality: **-100** (NPCs dislike you.)

5. **Other Effects**:
   - Weakness to Common Disease: **100%**
   - NPC topics and greetings have a chance to be overly negative and hostile. 

---

## The Blessing

When you carry **0.2 weight or less** (enough for one ring), the curse is lifted, and a blessing is applied. **(200 gold does not count towards this weight.)**

### Blessing Effects:
- **Stat Bonuses**:
  - Strength, Speed, Agility, Intelligence: **+30**
    - This number increases by **1 per cumulative day** you maintain the blessing. (Resets upon gaining the curse.)
- **Additional Effects**:
  - Reflect: **+10**
  - Night Eye: **+10**
  - Ability to soul trap creatures to convert their souls to magicka.

---

## The Bonus Blessing

An additional blessing, referred to as the **Bonus Blessing**, provides further benefits:

### Bonus Blessing Effects:
- Night Eye: **+20**
- Acrobatics: **+30** (Enough to avoid fall damage if base acrobatics is 100.)
- Hand-to-Hand: **+40**

### Requirements:
- Must not carry anything that isn't equipped (e.g., a ring).
- Must be in **third-person view**. *(This rule might not be final.)*
- Must have fewer than **20 spells**.

## The Advanced Curse

When your carry weight is over **30**, you will experience additional negative effects.

### Advanced Curse Effects:
- Posion - 3 points
- Your disposition with anyone who wasn't 100 before the application of the curse, will be reduced to 0.
---

## Strategies to use

Below are some tips on how to go about the game with the restrictions
### Combat
- Use Hand to hand, this works well especially with the bonus buff. Turn on the OpenMW settings that allow strength to factor in.
- Use destruction magic. This will depend on your available magicka pool though
- Use bound weapons. These are weightless, and can be quite effective. I prefer the bound spear.

### Questing
- Place a mark at your questgiver. If your quest involves retrieving an object or objects, cast recall on the items to send them to the questgiver.
---

## Additional Features

To make the challenge slightly easier, the mod includes a **Recall on Target** spell:
- Cast it on the ground directly below you.
- Items you're allowed to pick up nearby will be teleported to your marked location.
- Magicka cost depends on the item's weight, possibly multiplied.

---