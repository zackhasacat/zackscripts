return {



    APIREV = 40,
    clapSound = "Thunderclap",
    closePortalSound = "AB_Thunderclap3",
    openPortalSound = "AB_Thunderclap2",
    baseScale = 0.5,
    scaleDelay = 0.01,
    scaleMultiplier =  0.03,
    exitDistance = 150,
    heightOffset = 363.791580200195,
    followerExitDistance = 200,
    placeDistance = 150,
    defaultSet = "Spell",
    enterDistance = 130,
    magicDivider = 2000,
    notEnoughMagicMessage = "You don't have enough Magicka to open the portal.",
    noOpenPortalsMessage = "There aren't any open portals"
}