-- scripts\testPotionPlayer.lua
local cam = require('openmw.interfaces').Camera
local core = require('openmw.core')
local self = require('openmw.self')
local nearby = require('openmw.nearby')
local types = require('openmw.types')
local ui = require('openmw.ui')

function contains(tableb, value)
     for _, v in ipairs(tableb) do
          if v == value then
               return true
          end
     end
     return false
end
local function onMessageSent(eventData)
     ui.showMessage(eventData)
end

local function onConsoleCommand(mode, command, selectedObject)
     local words = {}


     for word in command:gmatch("%S+") do
          table.insert(words, word)
     end
     local commands = {
          "luapotion",
          "luaarmor",
          "luamisc",
          "luaweap",
          "luaacti",
          "luaclot",
          "luabook",
     }

     -- Use the `contains` function to check if `words[1]` is in the `commands` table


     if  contains(commands, words[1]) then
          core.sendGlobalEvent("TestPotionEvent", {
               words = words,
               player = self,
               base = selectedObject
          })
     end

end

return {
     engineHandlers = {
          onConsoleCommand = onConsoleCommand
     },
     eventHandlers = {
          onMessageSent = onMessageSent
     }
}

