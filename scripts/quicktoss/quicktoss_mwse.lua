local equipdelay = -1
local prevStance = nil
local startTime = 0

local wasOverRiding = false
local prevWeapon = nil

local function findWeapon()
    -- find a thrown weapon. In the future, this will pick the object with the highest damage, or the last equipped thrown weapon.
    local inv = tes3.getPlayerInventory()
    local ret = nil
    for _, item in ipairs(inv) do
        if item.object.objectType == tes3.objectType.weapon and item.object.type == tes3.weaponType.marksmanThrown then
            ret = item
        end
    end
    return ret
end

local function findSlot(item)
    if item.object.objectType == tes3.objectType.armor then
        return tes3.armorSlot[item.object.slot]
    elseif item.object.objectType == tes3.objectType.book then
        return tes3.quickKeySlot[item.object.type]
    elseif item.object.objectType == tes3.objectType.clothing then
        if item.object.type == tes3.clothingType.amulet then
            return tes3.quickKeySlot.amulet
        elseif item.object.type == tes3.clothingType.skirt then
            return tes3.quickKeySlot.skirt
        elseif item.object.type == tes3.clothingType.shirt then
            return tes3.quickKeySlot.shirt
        elseif item.object.type == tes3.clothingType.shoes then
            return tes3.quickKeySlot.shoes
        elseif item.object.type == tes3.clothingType.robe then
            return tes3.quickKeySlot.robe
        end
    elseif item.object.objectType == tes3.objectType.weapon then
        return tes3.quickKeySlot.weapon
    end
    print("Couldn't find slot for " .. item.id)
    return false
end

local function getEquippedInSlot(slot)
    local equip = tes3.mobilePlayer.object.equipment
    for index, itemId in pairs(equip) do
        if index == slot then
            return itemId
        end
    end
    return nil
end

local function equipItem(itemId, unEquip)
    if unEquip == nil then
        unEquip = false
    end
    if unEquip then
        local equip = tes3.mobilePlayer.object.equipment
        equip[itemId] = nil
        tes3.mobilePlayer.object.equipment = equip
        return
    end
    if itemId == nil then
        return nil
    end
    if itemId.object then
        itemId = itemId.object.id
    end
    local inv = tes3.getPlayerInventory()
    local item = inv:findItem(itemId)
    local slot = findSlot(item)
    if slot then
        local equip = tes3.mobilePlayer.object.equipment
        if unEquip then
            equip[slot] = nil
        else
            equip[slot] = item
        end
        tes3.mobilePlayer.object.equipment = equip
    end
end

local function onUpdate(e)
    local input = tes3.getInputState()
    if equipdelay > 0 then
        equipdelay = equipdelay - 1
    elseif equipdelay == 0 then
        if prevWeapon == "nil" then
            equipItem(tes3.quickKeySlot.weapon, true)
        else
            equipItem(prevWeapon)
        end
        prevWeapon = nil
        equipdelay = -1
        if prevStance ~= nil then
            tes3.mobilePlayer:setStance(prevStance)
            prevStance = nil
        end
    end
    if input:isKeyDown(tes3.scanCode.m) then
        local tweapon = findWeapon()
        if tweapon ~= nil then
            if wasOverRiding == false then
                startTime = tes3.getSimulationTime()
                if tes3.mobilePlayer.stance ~= tes3.stance.weapon then
                    prevStance = tes3.mobilePlayer.stance
                    tes3.mobilePlayer:setStance(tes3.stance.weapon)
                end
                prevWeapon = getEquippedInSlot(tes3.quickKeySlot.weapon)
                if prevWeapon == nil then
                    prevWeapon = "nil"
                end
                equipItem(tweapon)
                tes3.player.controls.overrideAttack = true -- disable the built-in combat controls, otherwise tes3.player.controls.attack is not easy to manipulate.
                print("use")
            elseif wasOverRiding == true and startTime > 0 then

            end

            wasOverRiding = true
            tes3.player.controls.attack = 1
        else
            print("No Thrown Weapon")
        end
    end
    if wasOverRiding == true and startTime > 0 and not input:isKeyDown(tes3.scanCode.m) then
        local waitTime = tes3.getSimulationTime() - startTime
        if waitTime > 0.8 then -- Want to make sure to let go only after fully charged
            wasOverRiding = false
            if prevWeapon ~= nil then
                print(prevWeapon)
            end
            equipdelay = 200
            tes3.player.controls.attack = 0
            tes3.player.controls.overrideAttack = false -- turn back on the normal combat controls
            startTime = 0
        else
            print(waitTime)
            tes3.player.controls.attack = 1
        end
    end
end

event.register("simulate", onUpdate)
