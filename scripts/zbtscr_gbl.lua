local util = require("openmw.util")
local world = require("openmw.world")
local core = require("openmw.core")
local types = require("openmw.types")

local function roundToNearestMultipleOf90(angle)
  local remainder = angle % 90
  if remainder >= 45 then
    print("Modified angle from " .. tostring(angle) .. " to " .. tostring(angle + (90 - remainder)))
    return angle + (90 - remainder)
  else
    print("Modified angle from " .. tostring(angle) .. " to " .. tostring(angle - remainder))
    return angle - remainder
  end
end
local function ReplaceItem(oldObject, newObjectId)
  local newObject = world.createObject(newObjectId, 1)
  print("Replaced item " .. oldObject.recordId .. tostring(oldObject.rotation.z))
  local newrot = util.vector3(oldObject.rotation.x, oldObject.rotation.y,
    math.rad(roundToNearestMultipleOf90(math.deg(oldObject.rotation.z))))
  -- newrot = util.vector3(oldObject.rotation.x, oldObject.rotation.y, 270) -- Changed newrot.z to oldObject.rotation.z
  newObject:teleport(oldObject.cell.name, oldObject.position, newrot)
  oldObject.enabled = false
  newObject.enabled = true
end


function degreesToRadians(degrees)
  return degrees * math.pi / 180
end

function rotateObjects(statics, degrees, bookshelf)
  local angle = (degrees)

  -- Calculate the center of rotation
  local center = statics[1]

  -- Rotate each object around the center
  for i = 1, #statics do
    -- Calculate the relative position of the object to the center
    local relativePosition = statics[i] - center

    -- Calculate the new position
    local x = relativePosition.x * math.cos(angle) - relativePosition.y * math.sin(angle)
    local y = relativePosition.x * math.sin(angle) + relativePosition.y * math.cos(angle)
    local position = util.vector3(x + center.x, y + center.y, statics[i].z)
    print(statics[i].recordId)
    print("old posx: " .. statics[i].x .. "new pos " .. position.x)
    print("old posy: " .. statics[i].y .. "new pos " .. position.y)
    -- Calculate the new rotation
    local rotation = util.vector3(0, math.rad(90), math.rad(90) - angle)
    if (i > 1) then
      local bookId = "bk_biographybarenziah1"

      local newBook = world.createObject(bookId, 1)

      -- Move the object
      newBook:teleport(bookshelf.cell.name, position, rotation)
    end
  end
end

function placeBooks(centerX, centerY, centerZ, bookshelf)
  local startZ = centerZ + 15 -- place the books 50 units above the center
  local booklist = {}
  table.insert(booklist, bookshelf.position)

  for i = 1, 10 do                     -- place 10 books
    local px = centerX - 45 + (i - 1) * 10 -- move 10 units to the right for each book
    local py = centerY
    local pz = startZ

    local rx = 0
    local ry = math.rad(90)
    local rz = math.rad(90)
    --newBook = newBook:teleport(bookshelf.cell.name, util.vector3(px, py, pz), util.vector3(rx, ry, rz))

    table.insert(booklist, util.vector3(px, py, pz))
  end

  return booklist
end

function fillBookshelf(bookshelf)
  local origrot = bookshelf.rotation.z
  --local newbooks =  bookshelf:teleport(bookshelf.cell.name,bookshelf.position, util.vector3(0,0,0))
  -- Define the bookshelf position and rotation based on the passed in variable
  local books = placeBooks(bookshelf.position.x, bookshelf.position.y, bookshelf.position.z, bookshelf)

  rotateObjects(books, origrot, bookshelf)
end

local rot = 0

local function ZBTCellChange(eventData)
  local player = eventData.player
  local cell = player.cell
  local statics = cell:getAll(types.Static)
  local players = { player }

  for _, static in ipairs(statics) do
    if types.Static.record(static).model == "meshes\\f\\Furn_De_Bookshelf_02.NIF" and static.enabled then
      ReplaceItem(static, "zbt_De_R_Bookshelf_02")
    end
  end
end



return {
  eventHandlers = {
    ZBTCellChange = ZBTCellChange,
    fillBookshelf = fillBookshelf
  }
}
