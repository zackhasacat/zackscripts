local self = require("openmw.self")
local ui = require("openmw.ui")
local types = require("openmw.types")
local I = require("openmw.interfaces")
local function healthCheckFunc()
    local myHealth = types.Actor.stats.dynamic.health(self).current
    ui.printToConsole(string.format("Your health is %q", myHealth), ui.CONSOLE_COLOR.Info)
end
I.Console.addCommandOverride("healthcheck")
I.Console.addToEnviroment("hcheckfunc", healthCheckFunc)
local function onConsoleCommand(mode, cmd)
    if cmd == "healthcheck" then
        healthCheckFunc()
    end
end

return {
    engineHandlers = {
        onConsoleCommand = onConsoleCommand
    },
}
