local interfaces = require("openmw.interfaces")
local self = require("openmw.self")
local util = require("openmw.util")
local core = require("openmw.core")
local nearby = require("openmw.nearby")

local function setActive(isactive)
    self.mwscript.active = isactive
end
local originalZPos

local function onUpdate()
    if (self.recordId == "airship_crystal_top") then
        --print("update")
        local startPosition = self.position

        local targetPosition = util.vector3(self.position.x, self.position.y, 0)
        local result = nearby.castRay(startPosition, targetPosition, {ignore = self})

        core.sendGlobalEvent("updateAnchorView", util.vector3(result.hitPos.x - 150,result.hitPos.y, result.hitPos.z + 800))
    end
end
local function AnchorTravelDown()
    local startPosition = self.position

    local targetPosition = util.vector3(self.position.x, self.position.y, 0)
    local result = nearby.castRay(startPosition, targetPosition, {ignore = self})
    print(result.hit)
    if (result.hitObject) then
        print(result.hitObject.recordId)
    end
    if (result.hit) then
        print(result.hitPos.z)
    core.sendGlobalEvent("setAnchorLowerPos", result.hitPos.z)
    end
end
return {
    eventHandlers = {
        setActive = setActive,
        returnActivators = returnActivators,
        recieveActivators = recieveActivators,
        AnchorTravelDown = AnchorTravelDown,
    },
    engineHandlers = {
        onUpdate = onUpdate
}
}

