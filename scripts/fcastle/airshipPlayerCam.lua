local ui = require("openmw.ui")
local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local v3 = require("openmw.util").vector3
local util = require("openmw.util")
local cam = require("openmw.interfaces").Camera
local core = require("openmw.core")
local self = require("openmw.self")
local nearby = require("openmw.nearby")
local types = require("openmw.types")
local storage = require("openmw.storage")
local camera = require("openmw.camera")
local input = require("openmw.input")
local ui = require("openmw.ui")
local async = require("openmw.async")
local cameraAngles = {}
local engaged = false
local startFOV
local cameraStorage = storage.globalSection("AirshipCameraStorage")

local currentCameraPos = "FrontView"
local currentCamIndex = 1

local rightStickY
local rightStickX

local function addCameraAngle(name, num, angle, fov, keyName)
    local initpos = cameraStorage:get(string.lower("aa_airship_camerapos_" .. tostring(num)))
    local newangle = {name = name, num = num, pos = initpos, angle = angle, fov = fov}
    table.insert(cameraAngles, newangle)
end
local function addCameraAngleSpecial(name, num, angle, fov, keyName)
    local initpos = cameraStorage:get(name)
initpos = v3(initpos.x,initpos.y,initpos.z + 4300)
print(initpos)
    local newangle = {name = name, num = num, pos = initpos, angle = angle, fov = fov}
    table.insert(cameraAngles, newangle)
end
local function updatePosition(table, keyName)
    for i, subTable in ipairs(cameraAngles) do
        if "aa_airship_camerapos_" .. tostring(cameraAngles[i].num) == keyName or cameraAngles[i].name == keyName then
            cameraAngles[i].pos = cameraStorage:get(keyName)
            if (cameraAngles[i].name == currentCameraPos) then
                if (engaged == false) then
                    return
                end
                camera.setStaticPosition(v3(cameraAngles[i].pos.x,cameraAngles[i].pos.y,cameraAngles[i].pos.z))
                camera.setPitch(cameraAngles[i].angle.x+ (rightStickY * 3))
                camera.setRoll(cameraAngles[i].angle.y)
                camera.setYaw(cameraAngles[i].angle.z + (rightStickX * 5))
            --    camera.setFieldOfView(cameraAngles[i].fov)
            end
            return subTable
        end
    end
end
local function changeCamPos(newname)
    for i, subTable in ipairs(cameraAngles) do
        if newname == cameraAngles[i].name then
            currentCameraPos = newname
            if (cameraAngles[i].name == currentCameraPos) then
                camera.setStaticPosition(cameraAngles[i].pos)
                camera.setPitch(cameraAngles[i].angle.x+ (rightStickY * 3))
                camera.setRoll(cameraAngles[i].angle.y )
                camera.setYaw(cameraAngles[i].angle.z + (rightStickX * 5))
           --     camera.setFieldOfView(cameraAngles[i].fov)
            end
            currentCamIndex = i
            return true
        end
    end
    return false
end


local uposCallback = async:callback(updatePosition)
local function addDefaultAngles()
    addCameraAngle("AnchorView", 4, v3(1.4575151205063, -0, 1.6178362369537), 2)
    addCameraAngle("FrontView", 1, v3(0.5, -0, 3.1230289936066), 2)
    addCameraAngle("Top View", 2, v3(1.5161088705063, -0, 1.59765625), 1.4)
    addCameraAngle("BottomView", 5, v3(-0.41748487949371, -0, 0.052716493606567), 2)
    addCameraAngle("Back View", 3, v3(0.75048387050629, -0, 0.0234375), 2)
    addCameraAngle("Left View", 6, v3(0.55517137050629, -0, 1.56640625), 1.8)
    addCameraAngle("Right View", 7, v3(00.58642137050629, -0, -1.5625), 2)
    addCameraAngleSpecial("AnchorDynamicView", 8, v3(1.4575151205063, -0, 1.6178362369537), 2)
end
local function getAngleByName(name)
    for _, subTable in ipairs(cameraAngles) do
        if subTable.name == name then
            -- Do something with the matching sub-table
            return subTable
        end
    end
end
local function EndControlMode()
    engaged = false
    input.setControlSwitch(input.CONTROL_SWITCH.Controls, true)
    input.setControlSwitch(input.CONTROL_SWITCH.Looking, true)
    ui.showMessage("Movement mode disengaged")
    core.sendGlobalEvent("disablePlayerConstriants")
    camera.setFieldOfView(startFOV)
    camera.setMode(camera.MODE.FirstPerson, false)
end
local function onControllerButtonPress(id)
    if (engaged == false) then
        return
    end
    if (id == input.CONTROLLER_BUTTON.LeftShoulder) then
        if (currentCamIndex > 1) then
            currentCamIndex = currentCamIndex - 1
            changeCamPos(cameraAngles[currentCamIndex].name)
        end
        ui.showMessage("Pressed: Sneak")
    elseif (id == input.CONTROLLER_BUTTON.RightShoulder) then
        ui.showMessage("Pressed: Run")
        if (currentCamIndex < 8) then
            currentCamIndex = currentCamIndex + 1
            changeCamPos(cameraAngles[currentCamIndex].name)
        end
    elseif (id == input.CONTROLLER_BUTTON.Y) then
        core.sendGlobalEvent("toggleAnchor")
    elseif (id == input.CONTROLLER_BUTTON.B) then
        EndControlMode()
        core.sendGlobalEvent("setMoveEast", 0)
        core.sendGlobalEvent("setMoveNorth",0)
    core.sendGlobalEvent("setMoveUp",0)
    end
end
local function onInputAction(id)
    if (engaged == false) then
        return
    end
    if (id == input.ACTION.Sneak) then
        if (currentCamIndex > 1) then
            currentCamIndex = currentCamIndex - 1
            changeCamPos(cameraAngles[currentCamIndex].name)
        end
        ui.showMessage("Pressed: Sneak")
    elseif (id == input.ACTION.Run) then
        ui.showMessage("Pressed: Run")
        if (currentCamIndex < 7) then
            currentCamIndex = currentCamIndex + 1
            changeCamPos(cameraAngles[currentCamIndex].name)
        end
    else
        --   ui.showMessage("Pressed: " .. tostring(id))
    end
end
local function onKeyPress(id)
end
local function StartControlMode()
    ui.showMessage("It's time")
    engaged = true
    core.sendGlobalEvent("enablePlayerConstriants")
    startFOV = camera.getFieldOfView()
    if (#cameraAngles == 0) then
        addDefaultAngles()
    end
    cameraStorage:subscribe(uposCallback)
    input.setControlSwitch(input.CONTROL_SWITCH.Controls, false)
    input.setControlSwitch(input.CONTROL_SWITCH.Looking, false)
    ui.showMessage("Movement mode engaged")

    camera.setMode(camera.MODE.Static, true)
    changeCamPos(currentCameraPos)
end
local currentCamera = 1
local camPosAnchorView
local camPosAnchorAngle = (v3(1.4575151205063, -0, 1.6178362369537))
local camPosTop
local camPosTopAngle = (v3(1.4575151205063, -0, 1.6178362369537))

local camPosFront
local camPosFrontAngle = (v3(0.5, -0, 3.1230289936066))
local function updateCamPositions()
end
local function onUpdate(dt)
    if (engaged == false) then
        return
    end

    local leftStickX = input.getAxisValue(input.CONTROLLER_AXIS.LeftX)
    local leftStickY = input.getAxisValue(input.CONTROLLER_AXIS.LeftY)
     rightStickY = input.getAxisValue(input.CONTROLLER_AXIS.RightY)
     rightStickX = input.getAxisValue(input.CONTROLLER_AXIS.RightX)
changeCamPos(currentCameraPos)

    local TriggerLeft = input.getAxisValue(input.CONTROLLER_AXIS.TriggerLeft)
    local TriggerRight = input.getAxisValue(input.CONTROLLER_AXIS.TriggerRight)
    local zmove = (TriggerLeft + -TriggerRight)
    local minVal = 0.1
    local posMultiplier = 30 + math.floor(getAngleByName("BottomView").pos.z / 1000) * 10

    local leftStickY = input.getAxisValue(input.CONTROLLER_AXIS.LeftY)
    if (leftStickX < minVal and leftStickX > -minVal) then
        leftStickX = 0
    end
    if (leftStickY < minVal and leftStickY > -minVal) then
        leftStickY = 0
    end
    if (leftStickX ~= 0 or leftStickY ~= 0) then

    --print(zmove)
    end
    if (currentCameraPos == "AnchorView" or currentCameraPos == "AnchorDynamicView") then
        core.sendGlobalEvent("setMoveEast", -(leftStickY * posMultiplier))
        core.sendGlobalEvent("setMoveNorth", -(leftStickX * posMultiplier))
    elseif (currentCameraPos == "FrontView") then
        core.sendGlobalEvent("setMoveEast", -(leftStickX * posMultiplier))
        core.sendGlobalEvent("setMoveNorth", (leftStickY * posMultiplier))
    elseif (currentCameraPos == "BottomView") then
        core.sendGlobalEvent("setMoveEast", (leftStickX * posMultiplier))
        core.sendGlobalEvent("setMoveNorth", -(leftStickY * posMultiplier))
    elseif (currentCameraPos == "Back View") then
        core.sendGlobalEvent("setMoveEast", (leftStickX * posMultiplier))
        core.sendGlobalEvent("setMoveNorth", -(leftStickY * posMultiplier))
    elseif (currentCameraPos == "Left View") then
        core.sendGlobalEvent("setMoveEast", -(leftStickY * posMultiplier))
        core.sendGlobalEvent("setMoveNorth", -(leftStickX * posMultiplier))
    elseif (currentCameraPos == "Right View") then
        core.sendGlobalEvent("setMoveEast", (leftStickY * posMultiplier))
        core.sendGlobalEvent("setMoveNorth", (leftStickX * posMultiplier))
    elseif (currentCameraPos == "Top View") then
        core.sendGlobalEvent("setMoveEast", -(leftStickY * posMultiplier))
        core.sendGlobalEvent("setMoveNorth", -(leftStickX * posMultiplier))
    end
    core.sendGlobalEvent("setMoveUp", -(zmove * posMultiplier))
end

return {
    interfaceName = "AirshipCamera",
    interface = {
        version = 1,
        cameraAngles = cameraAngles,
        getAngleByName = getAngleByName
    },
    eventHandlers = {
        sendMessage = sendMessage,
        returnActivators = returnActivators,
        StartControlMode = StartControlMode
    },
    engineHandlers = {
        onConsoleCommand = onConsoleCommand,
        onFrame = onFrame,
        onLoad = onLoad,
        onInputAction = onInputAction,
        onKeyPress = onKeyPress,
        onControllerButtonPress = onControllerButtonPress,
        onUpdate = onUpdate
    }
}

