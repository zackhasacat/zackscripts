local interfaces = require("openmw.interfaces")
local self = require("openmw.self")
local util = require("openmw.util")
local core = require("openmw.core")
local nearby = require("openmw.nearby")
local types = require("openmw.types")

local targetRef = nil
local aggresive = false

local crystal2ref = nil
local crystal3ref = nil
local trackTarget = nil
local playerRef
local crystal2target = nil
local crystal3target = nil

local parentRef = nil
local function doAttack()
    self.mwscript.state = 1
end

local speed = 0.01
local function setTarget(target)
    trackTarget = target
end
local function setSpeed(fpeed)
speed = fpeed
end
local function startAttack()
    aggresive = true
end
local function testFunc()

end
local function findNextTarget()
    local actors = nearby.actors
    for i, actor in ipairs(actors) do
        if (actor.recordId == "player") then
            playerRef = actor
        end
    end
    local closestActors = {}
    for i, actor in ipairs(actors) do
        if (actor.recordId ~= "player" and types.Actor.stats.dynamic.health(actor).current > 0 and actor.position.z > 0) then
            local distance =
                math.sqrt(
                (actor.position.x - playerRef.position.x) ^ 2 + (actor.position.y - playerRef.position.y) ^ 2 +
                    (actor.position.z - playerRef.position.z) ^ 2
            )
            if (distance <= 3000) then -- Only consider actors within 3000 units of the player
                if #closestActors < 3 or distance < closestActors[#closestActors].distance then
                    table.insert(closestActors, {actor = actor, distance = distance})
                    table.sort(
                        closestActors,
                        function(a, b)
                            return a.distance < b.distance
                        end
                    )
                    if #closestActors > 3 then
                        table.remove(closestActors, 4)
                    end
                end
            end
        end
    end
    for i, v in ipairs(closestActors) do
        -- if(i == 1) then
        if (trackTarget == nil) then
            --  print("find for me")
            trackTarget = v.actor
        elseif (crystal2target == nil and trackTarget ~= v.actor and crystal3target ~= v.actor) then
            --  print("find for 2")
            crystal2target = v.actor
            crystal2ref:sendEvent("setTarget", v.actor)
        elseif (crystal3target == nil and trackTarget ~= v.actor and crystal2target ~= v.actor) then
            crystal3target = v.actor
            crystal3ref:sendEvent("setTarget", v.actor)
        -- print("find for 3")
        end
        -- end
    end
    return #closestActors
end
local shootdelay = 0
local function getLinePoints(startPos, endPos, numPointsPerUnitDistance)
    local distance = math.sqrt((endPos.x - startPos.x) ^ 2 + (endPos.y - startPos.y) ^ 2 + (endPos.z - startPos.z) ^ 2)
    local numPoints = math.max(2, math.floor(numPointsPerUnitDistance * distance))
    -- print(distance)
    local linePoints = {}
    for i = 1, numPoints do
        local t = (i - 1) / (numPoints - 1)
        local x = startPos.x + (endPos.x - startPos.x) * t
        local y = startPos.y + (endPos.y - startPos.y) * t
        local z = startPos.z + (endPos.z - startPos.z) * t
        table.insert(linePoints, util.vector3(x, y, z))
    end

    sangle = math.atan2(endPos.y - startPos.y, endPos.x - startPos.x)
    shootdelay = shootdelay + 1
    if (distance < 1 and shootdelay > 8 and trackTarget) then
        doAttack()
        shootdelay = 0
    end
    return linePoints
end

local function targetEliminated(actiname)
    if (actiname == "aa_minicrystal2") then
        crystal2target = nil
    elseif (actiname == "aa_minicrystal3") then
        crystal3target = nil
    end
end

local function onUpdate(dt)
    if
        not (self.recordId == "aa_minicrystal1" or self.recordId == "aa_minicrystal2" or
            self.recordId == "aa_minicrystal3")
     then
        return
    end

    if (targetRef == nil) then
        local actis = nearby.activators
        for i, acti in ipairs(actis) do
            if (acti.recordId == "aa_crystaltarget1" and self.recordId == "aa_minicrystal1") then
                targetRef = acti
            elseif (acti.recordId == "aa_crystaltarget2" and self.recordId == "aa_minicrystal2") then
                targetRef = acti
            elseif (acti.recordId == "aa_crystaltarget3" and self.recordId == "aa_minicrystal3") then
                targetRef = acti
            elseif (acti.recordId == "aa_minicrystal3" and self.recordId == "aa_minicrystal1") then
                crystal3ref = acti
            elseif (acti.recordId == "aa_minicrystal2" and self.recordId == "aa_minicrystal1") then
                crystal2ref = acti
            elseif (self.recordId == "aa_minicrystal3" and acti.recordId == "aa_minicrystal1") then
                parentRef = acti
            elseif (self.recordId == "aa_minicrystal2" and acti.recordId == "aa_minicrystal1") then
                parentRef = acti
            end
        end
    end
    if (trackTarget) then
        local nextPosForTarget =
            getLinePoints(
            self.position,
            util.vector3(trackTarget.position.x, trackTarget.position.y, trackTarget.position.z + 1600),
            speed
        )[2]
        core.sendGlobalEvent("teleportMe", {object = self, pos = nextPosForTarget})
        core.sendGlobalEvent("teleportMe", {object = targetRef, pos = trackTarget.position})
    end
    if
        ((trackTarget == nil or crystal2target == nil or crystal3target == nil) and aggresive and
            self.recordId == "aa_minicrystal1")
     then
        local check = findNextTarget()

        if (trackTarget == nil) then
            local nextPosForTarget =
                getLinePoints(
                self.position,
                util.vector3(playerRef.position.x, playerRef.position.y, playerRef.position.z + 200),
                speed
            )[2]
            core.sendGlobalEvent("teleportMe", {object = self, pos = nextPosForTarget})
            core.sendGlobalEvent("teleportMe", {object = targetRef, pos = nextPosForTarget})
        end
        if(crystal2target == nil) then
            nextPosForTarget =
                getLinePoints(
                crystal2ref.position,
                util.vector3(playerRef.position.x, playerRef.position.y, playerRef.position.z + 300),
                speed
            )[2]
            core.sendGlobalEvent("teleportMe", {object = crystal2ref, pos = nextPosForTarget})
        end
        if(crystal3target == nil) then
            nextPosForTarget =
                getLinePoints(
                crystal3ref.position,
                util.vector3(playerRef.position.x, playerRef.position.y, playerRef.position.z + 400),
                speed
            )[2]
            core.sendGlobalEvent("teleportMe", {object = crystal3ref, pos = nextPosForTarget})
        end
    elseif (trackTarget == nil and parentRef) then
        parentRef:sendEvent("targetEliminated", self.recordId)
    end
    if (trackTarget and types.Actor.stats.dynamic.health(trackTarget).current == 0) then
        trackTarget = nil
    end
end
return {
    eventHandlers = {
        setTarget = setTarget,
        doAttack = doAttack,
        setSpeed = setSpeed,
        targetEliminated = targetEliminated,
        startAttack = startAttack
    },
    engineHandlers = {
        onUpdate = onUpdate
    }
}

