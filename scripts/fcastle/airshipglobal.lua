local util = require("openmw.util")
local aux_util = require("openmw_aux.util")
local world = require("openmw.world")
local core = require("openmw.core")
local types = require("openmw.types")
local Activation = require("openmw.interfaces").Activation
local async = require("openmw.async")
local storage = require("openmw.storage")
local mainhouse = nil
local shipobjects = {}
local loadedData = nil
local dorotate = false
local I = require("openmw.interfaces")

local manualmove = false
local moveNorth = 0
local moveEast = 0
local moveUp = 0
---default pos:
--x 94224
--y 116671
--z 4408
---
local function pizza(box, banana, pizza)

end
local cameraStorage = storage.globalSection("AirshipCameraStorage")
local player = nil
local defaultPos = util.vector3(94224, 116671, 4408)
local currentPos = nil
local sangle = 0
local anchorRef = nil
local playerMover = nil
local anchored = false
local raisedAnchorZ = 0
local function getObjectbyId(id, cell, type)
    if (cell) then
        extractCell = world.getCellByName(cell.name) --The holding cell for the house
        allitems = extractCell:getAll()

        for i, object in ipairs(allitems) do
            if (object.id == id) then
                return object
            end
        end
    end
end

local airshipPath = nil
local playerRelativePos = nil
local playerzooffset = 400
local function buildAirship(targetPos)
    anchored = false
    --the item where we're teleporting things to
    local extractCell = world.getCellByName("AAAirshipStorage")        --The holding cell for the house
    local allitems = extractCell:getAll()
    local extractCell1 = world.getCellByName("AAAirshipStorageLevel1") --The holding cell for the house
    local allitems1 = extractCell1:getAll()
    local extractCell2 = world.getCellByName("AAAirshipStorageLevel2") --The holding cell for the house
    local allitems2 = extractCell2:getAll()
    local extractCell3 = world.getCellByName("AAAirshipStorageLevel3") --The holding cell for the house
    local allitems3 = extractCell3:getAll()
    local extractCell4 = world.getCellByName("AAAirshipStorageLevel4") --The holding cell for the house
    local allitems4 = extractCell4:getAll()
    shipobjects = {}
    local mergedTable = { allitems, allitems2, allitems3, allitems4, allitems1 }
    local skipped = 0
    local existingIDs = {}  -- Table to store existing object IDs

    for x, itemTable in ipairs(mergedTable) do
        for i, object in ipairs(itemTable) do
            local thisPos = object.position + targetPos
            local clone = false
            if object.recordId == "aa_playerlocfixer" then
                playerMover = I.ZackUtilsG.getPlayer()
            end

            -- Check if object ID already exists in shipobjects
            if existingIDs[object.id] then
                clone = true
                skipped = skipped + 1
            else
                existingIDs[object.id] = true  -- Mark the object ID as existing
            end

            if not clone then
                local newobject = world.createObject(object.recordId)
                if (newobject.recordId == "airship_crystal_top") then
                    anchorRef = newobject
                    currentPos = thisPos
                end
                if object.recordId == "aa_playerlocfixer" then
                    playerMover = I.ZackUtilsG.getPlayer()
                end
                if string.sub(object.recordId, 1, string.len("aa_airship_camerapos_")) == "aa_airship_camerapos_" then
                    cameraStorage:set(object.recordId, thisPos)
                end
                table.insert(shipobjects, newobject)
                newobject:teleport(
                    player.cell,
                    thisPos,
                    util.vector3(object.rotation.x, object.rotation.y, object.rotation.z)
                )
            end
        end
    end

    print("teleport ship in finished " .. #shipobjects)
    print("Skipped " .. skipped)
    local dorotate = true
end

local shipDelay = 0.5
local lastTp = 0
local function airshipTeleport(targetPos)
    --print("Teleporting")
    local playerpos = nil
  --  if core.getGameTime() > lastTp + shipDelay then
    --    lastTp = core.getGameTime()
 --   else
        --too soon
      --  return
 --   end
    local playerrot = nil
    local refPos
    local visitedObjects = {}  -- Table to store visited object IDs

    if (#shipobjects == 0) then
    else
        for i, object in ipairs(shipobjects) do
            if (object.recordId == "airship_crystal_top") then
                refPos = object.position
                if (playerRelativePos == nil) then
                    playerRelativePos = player.position - refPos
                    playerRelativePos =
                        util.vector3(playerRelativePos.x, playerRelativePos.y, playerRelativePos.z + playerzooffset)
                end
            end
        end
        for i, object in ipairs(shipobjects) do
            if (object.position == nil) then
                print(object.recordId)
            else
            end
            if (object.recordId == "player") then
                print("Player found")
                --  end

                if (object) then
                    object:teleport(player.cell.name, (object.position - refPos) + targetPos)
                end
            else
                -- Check if object ID is already visited
                local isRepeated = visitedObjects[object.id]
                if isRepeated then
                    print("Skipping repeated object:", object.recordId)
                else
                    visitedObjects[object.id] = true  -- Mark the object ID as visited

                    if string.sub(object.recordId, 1, string.len("aa_airship_camerapos_")) == "aa_airship_camerapos_" then
                        cameraStorage:set(object.recordId, (object.position - refPos) + targetPos)
                    end
                    if (object.recordId == "airship_crystal_top") then
                        currentPos = (object.position - refPos) + targetPos
                    end
                    if (object) then
                        object:teleport(player.cell.name, (object.position - refPos) + targetPos)
                    end
                end
            end
            --teleport everything in the cell in
        end
    end
    if (playerpos ~= nil) then
        print("playerTP")
    else
        print("playerTPX")
        local newplayerRelativePos =
            util.vector3(playerRelativePos.x, playerRelativePos.y, playerRelativePos.z + playerzooffset)
        playerpos = newplayerRelativePos + targetPos
        local playerzdelta = playerpos.z - player.position.z
        player:teleport(player.cell.name, (player.position - refPos) + targetPos)
        -- if (playerzdelta > playerzooffset) then
        print(playerzdelta)
    end
    --table.insert(shipobjects,player)
end


local function getLinePoints(startPos, endPos, numPointsPerUnitDistance)
    local distance = math.sqrt((endPos.x - startPos.x) ^ 2 + (endPos.y - startPos.y) ^ 2 + (endPos.z - startPos.z) ^ 2)
    local numPoints = math.max(2, math.floor(numPointsPerUnitDistance * distance))

    local linePoints = {}
    for i = 1, numPoints do
        local t = (i - 1) / (numPoints - 1)
        local x = startPos.x + (endPos.x - startPos.x) * t
        local y = startPos.y + (endPos.y - startPos.y) * t
        local z = startPos.z + (endPos.z - startPos.z) * t
        table.insert(linePoints, util.vector3(x, y, z))
    end

    sangle = math.atan2(endPos.y - startPos.y, endPos.x - startPos.x)

    return linePoints
end
local npc = nil
local function airshipInit(eventData)
    if (#shipobjects == 0) then
        player = eventData
        player:teleport(player.cell.name, util.vector3(94267, 116989, 4332))
        buildAirship(defaultPos)
    else
        -- playerMover:teleport(player.cell.name, player.position)
        --  playerMover:sendEvent("setActive", 1)
        airshipPath = getLinePoints(currentPos, util.vector3(0, 0, 4408), 10)
    end
end
local function startMoving(v3, speed)
    --   playerMover:teleport(player.cell.name, player.position)
    --   playerMover:sendEvent("setActive", 1)
    airshipPath = getLinePoints(currentPos, v3, speed)
end

local function moveToCell(cellname, speed)
    if (speed == nil) then
        speed = 0.01
    end
    targetob = world.getCellByName(cellname):getAll()[1]
    startMoving(util.vector3(targetob.position.x, targetob.position.y, 5000), speed)
end
local count = 0
local totalTime = 30 -- seconds
local incrementPerSecond = 360 / totalTime

function degreesToRadians(degrees)
    return degrees * math.pi / 180
end

function sign(x)
    return x > 0 and 1 or x < 0 and -1 or 0
end

local function rotateObjectsTargeted(statics, targetPoint)
    -- Calculate the current center of the group

    local currentCenter = util.vector3(0, 0, 0)
    for i = 1, #statics do
        currentCenter = currentCenter + statics[i].position
    end
    currentCenter = currentCenter / #statics

    -- Calculate the current rotation of the group
    local steeringObj = nil
    for i = 1, #statics do
        if statics[i].recordId == "airship_steering" then
            steeringObj = statics[i]
            break
        end
    end
    if not steeringObj then
        return
    end

    local forwardVec = steeringObj.rotation * util.vector3(0, 1, 0)
    local currentDirection = util.vector2(forwardVec.x, forwardVec.y):normalize()
    local currentAngle = math.atan2(currentDirection.y, currentDirection.x)

    -- Calculate the target direction
    local direction = targetPoint - currentCenter
    direction.z = 0
    local targetDirection = util.vector2(direction.x, direction.y):normalize()
    local targetAngle = math.atan2(targetDirection.y, targetDirection.x)

    -- Calculate the angle difference between the current direction and the target direction
    local angleDiff = targetAngle - currentAngle

    -- Adjust angleDiff to be within the range (-pi, pi)
    if angleDiff > math.pi then
        angleDiff = angleDiff - 2 * math.pi
    elseif angleDiff < -math.pi then
        angleDiff = angleDiff + 2 * math.pi
    end

    -- Calculate the new rotation of the group
    local newAngle = currentAngle + angleDiff
    local rotation = util.quaternion.fromEuler(0, 0, newAngle)

    -- Rotate each object around the center
    for i = 1, #statics do
        -- Calculate the relative position of the object to the center
        local relativePosition = statics[i].position - currentCenter

        -- Rotate the relative position around the z-axis
        local rotatedPosition = util.vector2(relativePosition.x, relativePosition.y):rotate(angleDiff)

        -- Calculate the new position
        local position = currentCenter + util.vector3(rotatedPosition.x, rotatedPosition.y, relativePosition.z)

        -- Move the object
        statics[i]:teleport(statics[i].cell, position, rotation)
    end
end

local objectRotations = {}
local totalRotation = 0

local function rotateObjects(degrees)
    local statics = shipobjects
    local angle = degreesToRadians(degrees)
    local recordId = "airship_crystal_top"
    -- Find the object with the specified recordId and calculate its position
    local center = util.vector3(0, 0, 0)
    local centerFound = false
    for i = 1, #statics do
        if statics[i].recordId == recordId then
            center = statics[i].position
            centerFound = true
            break
        end
    end

    if not centerFound then
        -- Object with the specified recordId was not found, return without rotating
        return
    end

    -- Calculate the total offset of all objects from the center
    local totalOffset = util.vector3(0, 0, 0)
    for i = 1, #statics do
        totalOffset = totalOffset + (statics[i].position - center)
    end

    -- Calculate the average position of all objects
    local averagePosition = center + (totalOffset / #statics)

    -- Rotate each object around the average position
    for i = 1, #statics do
        -- Calculate the relative position of the object to the average position
        local relativePosition = statics[i].position - averagePosition
        local x = relativePosition.x * math.cos(angle) - relativePosition.y * math.sin(angle)
        local y = relativePosition.x * math.sin(angle) + relativePosition.y * math.cos(angle)
        local position = util.vector3(x + averagePosition.x, y + averagePosition.y, statics[i].position.z)

        -- Calculate the new rotation
        local rotation = util.vector3(statics[i].rotation.x, statics[i].rotation.y, statics[i].rotation.z - angle)

        

        -- Move the object
        statics[i]:teleport(statics[i].cell.name, position, rotation)
    end
end

local function onSave()
    local uniqueGridPositions = {}

    -- create a new table to store the filtered properties
    local filteredTable = {}

    -- iterate over each object in the shipobjects array
    for i, obj in ipairs(shipobjects) do
        -- create a new table with only the properties you need
        local filteredObj = {
            gridX = obj.cell.gridX,
            gridY = obj.cell.gridY,
            id = obj.id,
            recordId = obj.recordId
        }
        -- add the filtered object to the filteredTable
        table.insert(filteredTable, filteredObj)
    end

    local filteredPlayer = {
        gridX = player.cell.gridX,
        gridY = player.cell.gridY,
        id = player.id,
        recordId = player.recordId
    }
    table.insert(filteredTable, filteredPlayer)
    -- print(aux_util.deepToString(filteredTable,10))
    return { table = filteredTable, raisedAnchorZ = raisedAnchorZ, anchored = anchored }
end
local function onItemActive(item)
    if (player ~= nil) then
        table.insert(shipobjects, item)
    end
end

local function getObjectByWorldPos(tid, x, y)
    local extractCell = world.getExteriorCell((x), (y)) -- The holding cell for the house
    local allitems = extractCell:getAll()
    -- print(aux_util.deepToString(world.activeActors, 10))

    for i, object in ipairs(allitems) do
        local savedId = tostring(object.id)
        local thisId = tostring(tid)

        if object.id == tid then
            --       print("Found for: " .. object.id)
            return object
        end
    end

    -- tid not found in allitems
    -- print("Not Found for: " .. tid)
    return nil
end
local function onActivate(object, actor)
    if (object.recordId == "aa_switch_01") then
        actor:sendEvent("StartControlMode")
        --  playerMover:teleport(player.cell.name, player.position)
    end
end
local function checkForData()
    --  extractCell = world.getExteriorCell(filteredTable[1][1].gridX,filteredTable[1][1].gridY) --The holding cell for the house
    --    print( tostring(extractCell.gridX) .. tostring( extractCell.gridY) .. "is the grid")
    local retobjects = {}
    for i, actor in ipairs(world.activeActors) do
        if (actor.recordId == "player") then
            player = actor
        end
    end
    for i, filteredObj in ipairs(loadedData) do
        --print(filteredObj.id)
        local item = getObjectByWorldPos(loadedData[i].id, loadedData[i].gridX, loadedData[i].gridY)
        if item and string.sub(item.recordId, 1, string.len("aa_airship_camerapos_")) == "aa_airship_camerapos_" then
            print("found" .. item.recordId)

            cameraStorage:set(item.recordId, item.position)
        end
        if (item and item.recordId == "aa_playerlocfixer") then
            --  playerMover = item
            print(item.recordId)
            table.insert(retobjects, item)
        elseif (item and item.recordId == "player") then
            print(item.recordId)
        elseif (item) then
            if (item.recordId == "airship_crystal_top") then
                currentPos = item.position
                --  print(item.recordId)
                anchorRef = item
            end

            table.insert(retobjects, item)
        end
    end
    print("Total found: " .. #retobjects)
    if (player) then
        shipobjects = retobjects
    end
end

local function onLoad(data)
    loadedData = data.table
    raisedAnchorZ = data.raisedAnchorZ
    anchored = data.anchored
end
local anchorDesiredZPos = nil
local function isAnchorLowered()
    return anchored
end
local function setAnchorLowerPos(zval)
    raisedAnchorZ = anchorRef.position.z
    anchored = true
    print(zval .. "is the thng")
    anchorDesiredZPos = tonumber(zval) + 445.0
    -- anchorRef:teleport(anchorRef.cell.name, util.vector3(anchorRef.position.x,anchorRef.position.y,targetPZ), anchorRef.rotation)
end

local function lowerAnchor()
    anchored = true
    anchorRef:sendEvent("AnchorTravelDown")
end
local function raiseAnchor()
    anchorDesiredZPos = raisedAnchorZ
    print(raisedAnchorZ)
    --   anchorRef:teleport(anchorRef.cell.name, util.vector3(anchorRef.position.x,anchorRef.position.y,targetPZ), anchorRef.rotation)
end
local function toggleAnchor()
    if (anchored == true) then
        raiseAnchor()
    else
        lowerAnchor()
    end
end
local inputDelay = 0
local currDelay = 0
local function onUpdate(dt)
    if (airshipPath ~= nil and #airshipPath > 0 and isAnchorLowered() == false) then
        local currentPoint = airshipPath[1]
        airshipTeleport(currentPoint)
        table.remove(airshipPath, 1)
    elseif airshipPath ~= nil and #airshipPath == 0 then
        --  playerMover:sendEvent("setActive", 0)
    elseif manualmove and isAnchorLowered() == false and (moveNorth ~= 0 or moveEast ~= 0 or moveUp ~= 0) then
        local currentPoint = util.vector3(currentPos.x + moveEast, currentPos.y + moveNorth, currentPos.z + moveUp)
        airshipTeleport(currentPoint)
    end
    if (#shipobjects == 0 and loadedData ~= nil) then
        checkForData()
    end
    -- Check if both anchorRef and anchorDesiredZPos variables exist
    if (anchorRef and anchorDesiredZPos) then
        -- Set the amount by which the object will move in the z-direction
        local moveamount = 5

        -- Get the current z-position of the anchorRef object
        local currentZPos = anchorRef.position.z

        -- Calculate the next z-position by adding moveamount to currentZPos
        local nextZPos = currentZPos + moveamount

        -- Print the difference between the desired z-position and the next z-position
        print(anchorDesiredZPos - nextZPos)

        -- If the desired z-position is within the range of current and next z-positions, teleport the object to the desired z-position and set anchorDesiredZPos to nil
        if anchorDesiredZPos > currentZPos and anchorDesiredZPos <= nextZPos then
            -- If the desired z-position is below the range of current and next z-positions, teleport the object to the desired z-position and set anchorDesiredZPos to nil
            -- Teleport the object up to the desired z-position
            anchorRef:teleport(
                anchorRef.cell.name,
                util.vector3(anchorRef.position.x, anchorRef.position.y, anchorDesiredZPos),
                anchorRef.rotation
            )
            if (anchorDesiredZPos == raisedAnchorZ) then
                anchored = false
            end
            -- Reset anchorDesiredZPos to nil since the object has reached the desired z-position
            anchorDesiredZPos = nil
        elseif anchorDesiredZPos < currentZPos and anchorDesiredZPos >= nextZPos then
            -- If the desired z-position is outside the range of current and next z-positions, move the object towards the desired z-position by moveamount
            -- Teleport the object down to the desired z-position

            anchorRef:teleport(
                anchorRef.cell.name,
                util.vector3(anchorRef.position.x, anchorRef.position.y, anchorDesiredZPos),
                anchorRef.rotation
            )
            -- Reset anchorDesiredZPos to nil since the object has reached the desired z-position
            if (anchorDesiredZPos == raisedAnchorZ) then
                anchored = false
            end
            anchorDesiredZPos = nil
        else
            -- If the desired z-position is above the current z-position, move the object up by moveamount
            if anchorDesiredZPos > currentZPos then
                -- If the desired z-position is below the current z-position, move the object down by moveamount
                anchorRef:teleport(
                    anchorRef.cell.name,
                    util.vector3(anchorRef.position.x, anchorRef.position.y, currentZPos + moveamount),
                    anchorRef.rotation
                )
            else
                anchorRef:teleport(
                    anchorRef.cell.name,
                    util.vector3(anchorRef.position.x, anchorRef.position.y, currentZPos - moveamount),
                    anchorRef.rotation
                )
            end
        end
    end

    --  if (dorotate == true and getObjectbyId(shipobjects[1]).cell.name ~= "AAAirshipStorage") then
    --print(count)
    --   rotateObjects(shipobjects, degreesToRadians(count))
    --	if ( count > 360 ) then
    --  dorotate = false
    --	end
    --   end
end
local function disablePlayerConstriants()
    -- -break   playerMover:sendEvent("setActive", 0)
end
local function enablePlayerConstriants()
    ---  playerMover:sendEvent("setActive", 1)
end
local function setMoveNorth(num)
    if (manualmove == false) then
        --  playerMover:sendEvent("setActive", 1)
        manualmove = true
    end
    moveNorth = num
end
local function teleportMe(data)
    object = data.object
    pos = data.pos
    --print(object.recordId)
    object:teleport(object.cell, pos)
end
local function setMoveEast(num)
    if (manualmove == false) then
        --   playerMover:teleport(player.cell.name, player.position)
        --   playerMover:sendEvent("setActive", 1)
        manualmove = true
    end
    moveEast = num
end
local function updateAnchorView(p3)
    cameraStorage:set("AnchorDynamicView", p3)
    --      print(p3.z)
end
local function setMoveUp(num)
    if (manualmove == false) then
        --  playerMover:teleport(player.cell.name, player.position)
        --  playerMover:sendEvent("setActive", 1)
        manualmove = true
    end
    moveUp = num
end
return {
    interfaceName = "Airship",
    interface = {
        version = 1,
        startMoving = startMoving,
        rotateObjects = rotateObjects,
        sangle = sangle,
        moveToCell = moveToCell,
        raiseAnchor = raiseAnchor,
        isAnchorLowered = isAnchorLowered,
        moveNorth = moveNorth,
        setMoveNorth = setMoveNorth,
        setMoveEast = setMoveEast,
        setMoveUp = setMoveUp,
        lowerAnchor = lowerAnchor
    },
    eventHandlers = {
        airshipTeleport = airshipTeleport,
        airshipInit = airshipInit,
        setAnchorLowerPos = setAnchorLowerPos,
        setMoveNorth = setMoveNorth,
        setMoveEast = setMoveEast,
        setMoveUp = setMoveUp,
        toggleAnchor = toggleAnchor,
        teleportMe = teleportMe,
        disablePlayerConstriants = disablePlayerConstriants,
        enablePlayerConstriants = enablePlayerConstriants,
        updateAnchorView = updateAnchorView,
    },
    engineHandlers = {
        onUpdate = onUpdate,
        onLoad = onLoad,
        onSave = onSave,
        onItemActive = onItemActive,
        onActivate = onActivate
    }
}
