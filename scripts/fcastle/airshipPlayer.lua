local ui = require("openmw.ui")
local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local cam = require("openmw.interfaces").Camera
local core = require("openmw.core")
local self = require("openmw.self")
local nearby = require("openmw.nearby")
local input = require("openmw.input")
local types = require("openmw.types")
local Camera = require("openmw.camera")
local camera = require("openmw.camera")


local Actor = require("openmw.types").Actor
local PLAYER_WIDTH = 100
local function renderItem(item)
    return {
        type = ui.TYPE.Container,
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = I.MWUI.templates.textNormal,
                        props = {
                            text = item.recordId,
                        },
                    },
                },
            },
        },
    }
end
local function anglesToV(pitch, yaw)
    local xzLen = math.cos(pitch)
    return util.vector3(
        xzLen * math.sin(yaw), -- x
        xzLen * math.cos(yaw), -- y
        math.sin(pitch) -- z
    )
end
local function getCameraDirData()
    local pos = Camera.getPosition()
    local pitch, yaw

    pitch = -(Camera.getPitch() + Camera.getExtraPitch())
    yaw = (Camera.getYaw() + Camera.getExtraYaw())

    return pos, anglesToV(pitch, yaw)
end
local function isSelf(t)
    return t == self.object
end
local function getObjInCrosshairs()
    local pos, v = getCameraDirData()
    local dist = 200
    local result = nearby.castRenderingRay(pos, pos + v * dist)
    -- Ignore player if in 3rd person
    if result.hitObject and isSelf(result.hitObject) then
        result = nearby.castRenderingRay(result.hitPos + v * PLAYER_WIDTH, result.hitPos + v * (PLAYER_WIDTH + dist))
    end

    -- Get approximated area. Note that this allows you to aim through walls, because we can't distinguish floor and wall

    return result.hitObject
end
local function renderItemBold(item, bold)
    return {
        type = ui.TYPE.Container,
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                alignment = ui.ALIGNMENT.Center,
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = I.MWUI.templates.textHeader,
                        props = {
                            text = item,
                        }
                    }
                }
            }
        }
    }
end
local function renderItem(item, bold)
    return {
        type = ui.TYPE.Container,
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                alignment = ui.ALIGNMENT.Center,
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = I.MWUI.templates.textNormal,
                        props = {
                            text = item,
                        }
                    }
                }
            }
        }
    }
end
local function renderItemChoice(itemList, currentItem)
    local content = {}
    for _, item in ipairs(itemList) do
        if item == currentItem then
			local itemLayout = renderItemBold(item)
            itemLayout.template = I.MWUI.templates.padding
			table.insert(content, itemLayout)
        else
			local itemLayout = renderItem(item)
            itemLayout.template = I.MWUI.templates.padding
        table.insert(content, itemLayout)
        end
    end
    return ui.create {
        layer = 'HUD',
        template = I.MWUI.templates.boxTransparent,
        props = {
            relativePosition = v2(0.65, 0.8),
            anchor = v2(1, 1),
        },
        content = ui.content {
            {
                type = ui.TYPE.Flex,
                content = ui.content(content),
            props = {
                    vertical = true,
                    arrange = ui.ALIGNMENT.Center,
                },

            },
        },
    }
end


-- Define the camera's rotation speed (in degrees per second)
local rotationSpeed = 10
local xcamera = {
    position = {
        x = 0,
        y = 0,
        z = -100
    },
    roll = 0,
    pitch = 0,
    yaw = 0
}
local uithing
local rotateObject
local function onConsoleCommand(mode, command, selectedObject) --test commands
    if command == "luaship" then
        core.sendGlobalEvent("airshipInit", self)
    elseif command == "luaui" then
        uithing = renderItemChoice({"Box"}, "Box")
    elseif command == "lua" then
    camera.setMode(camera.MODE.Static, true)
        xcamera.position.z = self.position.z
        xcamera.position.y = self.position.y
        xcamera.position.x = self.position.x
        rotateObject = selectedObject
    elseif command == "luastop" then
        uithing:destroy()
    end
end
local function sendMessage(eventData)
    --ui.showMessage(eventData)
end
local function recieveActivators(eventdata)
    for i, object in ipairs(nearby.items) do
        if (object.recordId == "zck_histn_misc_houseunified") then --find the house item?
            -- ui.showMessage("This is : " .. tostring(object.position.x))
            core.sendGlobalEvent("eteleportin", object)
        --Send the event for bringing stuff in, with the house item as the reference
        --core.sendGlobalEvent('eteleportOut',object)
        end
    end
end
local function returnActivators(eventdata)
    for i, object in ipairs(nearby.activators) do --Telepor the mod activators
        if (string.find(object.recordId, "zck_histn_act_")) then
            core.sendGlobalEvent("eteleportOut", object)
        --picked up the item
        end
    end
    for i, object in ipairs(nearby.containers) do --Teleport the mod containers
        if (string.find(object.recordId, "zck_histn_con_")) then
            core.sendGlobalEvent("eteleportOut", object)
        end
    end
end
local function onFrame()
    --      renderItemChoice({"Banana","Box","Pizza"},"Box")
    local obj = getObjInCrosshairs()
    if (uithing) then
        uithing:destroy()
    end
    if obj then
      --  uithing = renderItemChoice({"Wooden Door", "to", "Vos"}, "Wooden Door")
		--uithing.layout.content[1].content[2].horizontal = true
    end
end
local function onLoad()
end







local function onUpdate(dt)
    -- Calculate the camera's new position and orientation

if( rotateObject ) then


end
end

return {
    eventHandlers = {
        sendMessage = sendMessage,
        returnActivators = returnActivators,
        recieveActivators = recieveActivators
    },
    engineHandlers = {
        onConsoleCommand = onConsoleCommand,
        onFrame = onFrame,
onUpdate = onUpdate,
    }
}
