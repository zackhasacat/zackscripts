local world = require("openmw.world")
local types = require("openmw.types")
local function sendData()
    local player = nil
    for index, act in ipairs(world.activeActors) do
        if act.type == types.Player then
            player = act
            break
        end
    end
    local cellNames = {}
    for i, cell in ipairs(world.cells) do
        if (cell.name ~= nil and cell.name ~= "") then
            table.insert(cellNames, cell.name)
        end
    end
    player:sendEvent("recieveCellNames", cellNames)
end



return {
    engineHandlers = {
        onPlayerAdded = sendData,
        onLoad = sendData
    }
}
