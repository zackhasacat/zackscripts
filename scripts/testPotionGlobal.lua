-- scripts/testPotionGlobal.lua
local util = require("openmw.util")
local world = require("openmw.world")
local core = require("openmw.core")
local types = require("openmw.types")
local I = require("openmw.interfaces")
local function createPotion(eventData, name, base)
     local potion = {
          name = name,
          weight = 0.1,
          value = 10,
          icon = "icons\\m\\tx_potion_exclusive_01.dds",
          model = "o\\contain_de_chest_01.nif",
          effects = "sadsad" -- types.Potion.record(base).effects
     }
     local ret = types.Potion.createRecordDraft(potion)
     local record = world.createRecord(ret)
     print(record.id)
     local item = world.createObject(record.id, 1)
     item:moveInto(types.Actor.inventory(eventData.player))
end
local function getItemUpSound(item)
     local defaultName = "Item " .. tostring(item.type) .. " Down"
end

local function soundCheck()
     local count = 0
     for index, type in pairs(types) do
          if type.getDropSound ~= nil and type.records ~= nil then
               for index, record in ipairs(type.records) do
                    local object = world.createObject(record.id)

                    local dropSound = type.getDropSound(object):lower()
                    --  local myDropSound = auxCore.getDropSound(object):lower()
                    --   if myDropSound ~= dropSound and object ~= nil then
                    --       print("Mismatch on ", record.id, record.name)
                    --      print("correct", dropSound)
                    --     print("wrong", myDropSound)
                    -- else
                    --     count = count + 1
                    --end
               end
          end
     end
     print("Found ", count, "correct records")
end
local function soundTest()
     for index, type in pairs(types) do
          if type.getDropSound and type.records then
               local record = type.records[1]
               local object = world.createObject(record.id)
               local dropSound = type.getDropSound(object)
               local pickupSound = type.getPickUpSound(object)
               print("")
               print("Pickup:" .. pickupSound)
               print("Drop:" .. dropSound)
               print(type, record.name)
          end
     end
end
local function createClothing(name)
     local rec = {
          name = name,
          template = types.Clothing.records[1],
     }
     local ret = types.Clothing.createRecordDraft(rec)
     local record = world.createRecord(ret)
     local item = world.createObject(record.id, 1)
     item:moveInto(types.Actor.inventory(world.players[1]))
end
local function overRideNPC(data)
     local rec = {
          name = data.name,
          template = types.NPC.record(data.id),
          baseGold = 10000
     }
     for key, value in pairs(data) do
          if not rec[key] then
               --rec[key] = value
          end
     end
     local ret = types.NPC.createRecordDraft(rec)
     local record = world.createRecord(ret)
     local item = world.createObject(record.id, 1)
     item:teleport(world.players[1].cell, world.players[1].position)
end

local function objectICareAboutTeleported(data)
     local obj = data.data1
     local pl = world.players
     local distance = (obj.position - pl[1].position):length()
     if distance > 800 then
          obj.enabled = false
          print("disabled")
          print(tostring(obj.position))
          print(tostring(distance))
     else
          obj.enabled = true
          print("bugmeister enabled")
          print(tostring(obj.position))
     end
end

local function createBlankRecords(name)
     for index, type in pairs(types) do
          if type.createRecordDraft ~= nil then
               local rec = {
                    name = name .. " " .. tostring(type),
                    --template = type.records[1],
                    health = 100,
                    baseGold = 10000
               }
               local ret = type.createRecordDraft(rec)
               local record = world.createRecord(ret)
               local item = world.createObject(record.id, 1)
               print(record.id)
               item:moveInto(types.Actor.inventory(world.players[1]))
          end
     end
end
local function createMisc(name)
     local miscitem = {
          name = name,
          weight = 0.1,
          template = types.Miscellaneous.record("misc_soulgem_grand"),
          model = "meshes\\m\\misc_portal_shard.nif"
     }
     local ret = types.Miscellaneous.createRecordDraft(miscitem)
     local record = world.createRecord(ret)
     local item = world.createObject(record.id, 1)
     item:moveInto(types.Actor.inventory(world.players[1]))
end
local function createActivator(eventData, name)
     local npc = {
          name = name,
          model = "f\\furn_de_table_07.nif"
     }
     local ret = types.Activator.createRecordDraft(npc)
     local record = world.createRecord(ret)
     local object = world.createObject(record.id, 1)
     object:teleport(eventData.player.cell.name, eventData.player.position)
end
local function createBook(eventData, name)
     local rec = {
          name = name,
          model = "m\\text_octavo_01.nif",
          icon = "icons\\m\\tx_octavo_01.tga",
          text = '<DIV ALIGN="CENTER"><FONT COLOR="000000" SIZE="3" FACE="Magic Cards"><BR>\n' ..
              'The Anuad Paraphrased<BR><BR>\n' ..
              '<DIV ALIGN="LEFT"><BR><BR>\n' ..
              'The first ones were brothers: Anu and Padomay. They came into the Void, and Time began.<BR>',
          enchant = "amulet_gaenor_en",
          enchantCapacity = 1,
          mwscript = "",
          weight = 1,
          value = 100,
          isScroll = false,
          skill = "marksman",
     }
     local ret = types.Book.createRecordDraft(rec)
     local record = world.createRecord(ret)
     local item = world.createObject(record.id, 1)
     item:moveInto(types.Actor.inventory(eventData.player))
end
local function copyArmor(armorRec, name)
     local rec = {
          name = name,
          model = armorRec.model,
          icon = armorRec.icon,
          enchant = "amulet_gaenor_en",
          enchantCapacity = 1,
          type = armorRec.type,
          baseArmor = 100,
          health = 100,
          mwscript = "",
          weight = 1,
          value = 100,
          bodyParts = armorRec.bodyParts
     }
     local ret = types.Armor.createRecordDraft(rec)
     local record = world.createRecord(ret)
     local item = world.createObject(record.id, 1)
     item:moveInto(types.Actor.inventory(world.players[1]))
end
local function copyClothing(armorRec, name)
     local rec = {
          name = name,
          model = armorRec.model,
          icon = armorRec.icon,
          enchantCapacity = 1,
          type = armorRec.type,
          mwscript = "",
          weight = 1,
          value = 100,
          bodyParts = armorRec.bodyParts
     }
     local ret = types.Clothing.createRecordDraft(rec)
     local record = world.createRecord(ret)
     local item = world.createObject(record.id, 1)
     item:moveInto(types.Actor.inventory(world.players[1]))
end
local function createArmor(eventData, name)
     local rec = {
          name = name,
          model = "a\\A_M_Chitin_Gauntlet_gnd.nif",
          icon = "icons\\a\\tx_chitin_gauntlet.tga",
          enchant = "amulet_gaenor_en",
          enchantCapacity = 1,
          type = 9,
          baseArmor = 100,
          health = 100,
          mwscript = "",
          weight = 1,
          value = 100,
     }
     local ret = types.Armor.createRecordDraft(rec)
     local record = world.createRecord(ret)
     local item = world.createObject(record.id, 1)
     item:moveInto(types.Actor.inventory(eventData.player))
end
local function createWeapon(eventData, name)
     local rec = {
          name = name,
          model = "w\\w_iron_dagger.nif",
          icon = "icons\\w\\tx_dagger_iron.tga",
          enchant = "amulet_gaenor_en",
          enchantCapacity = 1,
          type = 0,
          speed = 2,
          reach = 1,
          health = 100,
          mwscript = "",
          weight = 1,
          value = 100,
          chopMinDamage = 5,
          chopMaxDamage = 10,
          slashMinDamage = 10,
          slashMaxDamage = 20,
          thrustMinDamage = 20,
          thrustMaxDamage = 30,
     }
     local ret = types.Weapon.createRecordDraft(rec)
     local record = world.createRecord(ret)
     local item = world.createObject(record.id, 1)
     item:moveInto(types.Actor.inventory(eventData.player))
end
local function createLight(sourceLight, name)
     local rec = {
          name = name,
          template = sourceLight,
          isCarriable = true,
          offByDefault = true,
     }
     local ret = types.Light.createRecordDraft(rec)
     local record = world.createRecord(ret)
     local item = world.createObject(record.id, 1)
     item:moveInto(types.Actor.inventory(world.players[1]))
end




local function TestPotionEvent(eventData)
     local command = eventData.words[1]
     local name = eventData.words[2]
     local base = eventData.base
     if command == "luapotion" then
          createPotion(eventData, name, base)
     elseif command == "luamisc" then
          createMisc(eventData, name)
     elseif command == "luaacti" then
          createActivator(eventData, name)
     elseif command == "luabook" then
          createBook(eventData, name)
     elseif command == "luaarmor" then
          createArmor(eventData, name)
     elseif command == "luaweap" then
          createWeapon(eventData, name)
     end
end
local function onLoad()
     -- createClothing("TestTHING")
     -- createClothing("TestTHING")
     --createClothing("TestTHING")
     --createClothing("TestTHING")
end
return {
     interfaceName  = "RecordTest",
     interface      = {
          version = 1,
          copyArmor = copyArmor,
          copyClothing = copyClothing,
          createMisc = createMisc,
          createClothing = createClothing,
          createBlankRecords = createBlankRecords,
          soundTest = soundTest,
          getDropSound = getDropSound,
          getPickupSound = getPickupSound,
          soundCheck = soundCheck,
          createLight = createLight,
     },
     eventHandlers  = {
          TestPotionEvent = TestPotionEvent,
          overRideNPC = overRideNPC,
     },
     engineHandlers = { onLoad = onLoad }
}
