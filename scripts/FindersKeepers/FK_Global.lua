local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local async = require("openmw.async")

local player = nil
local droppedItem = nil
local function onItemActive(item)
    if (core.isWorldPaused() == false) then
        return
    end
    local obDist = I.ZackUtilsG.distanceBetweenPos(item.position, player.position)

    if (obDist > 250) then
        return
    end
    if (item.owner.recordId ~= nil or item.owner.factionId ~= nil) then
        return
    end
    print("Dropped item " .. item.recordId)
    I.ZackUtilsG.showMessage("Dropped item " .. item.recordId)
end
local playerLastCell
local lastEq = nil

local function compareEquip(eq1, eq2)
for key, value in pairs(eq1) do
    if(eq2 == nil or  eq2[key] ~= value) then
        
        return false
    end
end
for key, value in pairs(eq2) do
    if(eq1 == nil or  eq1[key] ~= value) then
        return false
    end
end
return true
end

local function getSerializableEquip(eq)
    local ret = {}
    for key, value in pairs(eq) do
        ret[key] = value.recordId
    end
    return ret
end

local function onUpdate(dt)
    if (player == nil) then
        player = I.ZackUtilsG.getPlayer()
    
    end

    local nowEq = getSerializableEquip(types.Actor.getEquipment(player))
    if (compareEquip(nowEq, lastEq) == false and lastEq ~= nil) then
      --  I.ZackUtilsG.showMessage("Equipment Changed")
    end
    lastEq = getSerializableEquip(types.Actor.getEquipment(player))

    if player.cell ~= playerLastCell then

    end

    playerLastCell = player.cell
end
local function onLoad()
    player = I.ZackUtilsG.getPlayer()
end


return {
    interfaceName  = "DebugModeGlobal",
    interface      = {
        version = 1,
    },
    engineHandlers = {
        onActorActive = onActorActive,
        onPlayerAdded = onPlayerAdded,
        onLoad = onLoad,
        onInit = onLoad,
        onItemActive = onItemActive,
    --    onUpdate = onUpdate,
    },
    eventHandlers  = {
        setSetting = setSetting,
        runMWscriptBridge = runMWscriptBridge,
        COCEvent = COCEvent,
        killAll = killAll,
        DebugActorSwap = DebugActorSwap,
        DoUnlock = DoUnlock,
        setOwner = setOwner,
        setOwnerFaction = setOwnerFaction,
        findAllRefs = findAllRefs,
        moveToId = moveToId,
        onFrame = onFrame,
        purgeMod = purgeMod,
    },
}
