local ui = require("openmw.ui")
local input = require("openmw.input")
local cellNames = nil

local function getCellsThatStartWith(txt)
    local ret = {}
    for index, value in ipairs(cellNames) do
        if string.sub(value, 1, #txt):lower() == txt:lower() then
            table.insert(ret, value)
        end
    end
    return ret
end
local tabCount = 1
local originalLine = nil
local function getEntry(tbl, position)
    local length = #tbl
    if position > length then
        return tbl[length]
    else
        return tbl[position]
    end
end
local function sortStrings(list)
    local size = #list
    local swapped = true

    while swapped do
        swapped = false

        for i = 1, size - 1 do
            if list[i] > list[i + 1] then
                list[i], list[i + 1] = list[i + 1], list[i]
                swapped = true
            end
        end

        size = size - 1
    end
end
local function onKeyPress(key)
    if key.code == input.KEY.Tab then
        local cline = ui.getConsoleInput()
        --Above line returns a valid string only if the console is open, and in lua mode.
        if cline and cline ~= "" then
            if string.sub(cline, 1, 3) == "coc" then
                local searchString = string.sub(cline, 5) -- Remove "coc " from the start
                if (originalLine == nil) then
                    originalLine = searchString
                end
                local valid = getCellsThatStartWith(originalLine)
                sortStrings(valid)
                if (key.withShift and tabCount > 1) then
                    tabCount = tabCount - 1
                    ui.showMessage("Shift Tab Press")
                end
                if next(valid) and valid[1] ~= cline then
                    local nline = getEntry(valid, tabCount)
                    ui.setConsoleInput("coc " .. nline)
                end
                if (not key.withShift and #valid > tabCount) then
                    tabCount = tabCount + 1
                    ui.showMessage("Tab Press")
                end
            end
        end
    elseif key.code == input.KEY.Enter then
        tabCount = 1
        originalLine = nil
    elseif key.code == input.KEY.Backspace then
        tabCount = 1
        originalLine = nil
    end
end


return {
    eventHandlers = {
        recieveCellNames = function(c)
            cellNames = c--takes a table of cell names from a global script
        end
    },
    engineHandlers = {
        onKeyPress = onKeyPress
    }
}
