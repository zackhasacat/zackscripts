local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local self = require("openmw.self")

local lastCell = nil
local function cellChange()


end
local function onUpdate(dt)
  local cell = self.object.cell
  if cell ~= lastCell then
    core.sendGlobalEvent("ZBTCellChange", {
        player = self
    })

	
 -- end
  lastCell = cell
end
end
local function onConsoleCommand(mode, command, selectedObject)
if command == "luabook"  then
core.sendGlobalEvent("fillBookshelf", selectedObject)

end
end
return {
  engineHandlers = {
    onUpdate = onUpdate,
	onConsoleCommand = onConsoleCommand
  }
}