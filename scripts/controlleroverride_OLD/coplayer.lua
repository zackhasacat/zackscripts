local ui = require("openmw.ui")
local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local cam = require("openmw.interfaces").Camera
local core = require("openmw.core")
local self = require("openmw.self")
local nearby = require("openmw.nearby")
local types = require("openmw.types")
local Camera = require("openmw.camera")
local camera = require("openmw.camera")
local input = require("openmw.input")
local async = require("openmw.async")
local storage = require("openmw.storage")
local acti = require("openmw.interfaces").Activation
local playerSelected
local Actor = require("openmw.types").Actor
local myModData = storage.globalSection("MundisData")
local openingActor = false
local PLAYER_WIDTH = 100
local nearbyActualInv
local itemCheck = "ebony_dagger_mehrunes"
local iconsize = 40
local scrollOffsetPlayer = 0
local scrollOffsetTarget = 0
local camyaw
local campitch
local camroll
local skipThisItem = nil
local currentCat = 1
local catTypes = { "All", "Weapon", "Apparel", "Magic", "Misc" }
local locData = {}
local locDataInvMode = {}
local menumode = false
local invMode = false


local actorToActivate = nil
local contToActivate = nil
local ActStartTime = 0


local typeFilter = {
    ["All"] = {},
    ["Weapon"] = { types.Weapon },
    ["Apparel"] = { types.Clothing, types.Armor },
    ["Magic"] = { types.Ingredient, types.Potion },
    ["Misc"] = { types.Apparatus, types.Book, types.Miscellaneous, types.Probe, types.Repair }
}
local function formatNumber(num)
    local threshold = 10000
    local millionThreshold = 1000000

    if num >= millionThreshold then
        local formattedNum = math.floor(num / millionThreshold)
        return string.format("%dm", formattedNum)
    elseif num >= threshold then
        local formattedNum = math.floor(num / threshold)
        return string.format("%dk", formattedNum)
    else
        return tostring(num)
    end
end




local function clickMe(x1, x2)
    -- ui.showMessage("Clicked" .. x2.props.name)
end
local function clickMeStop(x1, x2)
    --   ui.showMessage("Not Clicked")
    I.ControllerMode.upDateinvWins()
end
local function clickMeMove(x1, x2)
    if (x1.button == 1) then
        local xpos = x1.position.x
        local ypos = x1.position.y
        local bname = x2.props.name
        print(xpos, ypos)
        if (invMode) then
            locDataInvMode[bname] = {
                xpos = xpos,
                ypos = ypos
            }
        else
            locData[bname] = {
                xpos = xpos,
                ypos = ypos
            }
        end
    end
end

local protation
local function getScrollOffset(playerSel)
    if (playerSel == nil) then
        if (playerSelected) then
            playerSel = "player"
        end
    end
    if (playerSel == "player") then
        return scrollOffsetPlayer
    else
        return scrollOffsetTarget
    end
end

local function setScrollOffset(scroll, playerSel)
    if (playerSel == nil) then
        if (playerSelected) then
            playerSel = "player"
        end
    end
    if (playerSel == "player") then
        scrollOffsetPlayer = scroll
    else
        scrollOffsetTarget = scroll
    end
end
I.Settings.registerPage {
    key = "ControllerOverRide",
    l10n = "ControllerOverRide",
    name = "ControllerOverRide",
    description = "ControllerOverRide"
}
I.Settings.registerGroup {
    key = "SettingsControllerOverRide",
    page = "ControllerOverRide",
    l10n = "ControllerOverRide",
    name = "ControllerOverRide",
    description = "My Group Description",
    permanentStorage = false,
    settings = {
        {
            key = "OverrideContainers",
            renderer = "checkbox",
            name = "OverrideContainers",
            description = "Text to display when the game starts",
            default = "true"
        }
    }
}
local function getItemAt(x, y, itemList)
    local index = (x - 1) * 10 + y + getScrollOffset()
    if index <= #itemList then
        local item = itemList[index]

        return itemList
    end
    return nil
end

local function renderGridIcon(item, selected)
    local itemIcon = nil
    local resource2

    resource2 = ui.texture({ path = "icons\\selected.tga" })
    local magicIcon = nil
    if (I.ZackUtils.FindEnchant(item) and I.ZackUtils.FindEnchant(item) ~= "") then
        magicIcon = ui.texture({ path = "textures\\menu_icon_magic_mini.dds" })
    end
    local text = ""
    if (item) then
        record = I.ZackUtils.findItemIcon(item)
        if (item.count > 1) then
            text = formatNumber(item.count)
        end
        itemIcon = ui.texture({ path = record })
    end
    if (selected) then
        return {
            type = ui.TYPE.Container,
            props = {
                size = util.vector2(iconsize, iconsize)
            },
            content = ui.content {
                {
                    template = I.MWUI.templates.padding,
                    alignment = ui.ALIGNMENT.Center,
                    content = ui.content {
                        I.ZackUtilsUI_co.imageContent(magicIcon),
                        I.ZackUtilsUI_co.imageContent(itemIcon),
                        I.ZackUtilsUI_co.imageContent(resource2),
                        I.ZackUtilsUI_co.textContent(tostring(text))
                    }
                }
            }
        }
    end
    return {
        type = ui.TYPE.Container,
        props = {
            size = util.vector2(iconsize, iconsize)
        },
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                alignment = ui.ALIGNMENT.Center,
                content = ui.content {
                    I.ZackUtilsUI_co.imageContent(magicIcon),
                    I.ZackUtilsUI_co.imageContent(itemIcon),
                    I.ZackUtilsUI_co.textContent(tostring(text))
                }
            }
        }
    }
end

local selectedx = 2
local selectedy = 2
local selectedInvItem
local selectedItemList
local function renderItemGrid(itemList, currentItem, small, selected, isPlayer)
    local vertical = 80
    local horizontal = (ui.screenSize().x / 2) - 600
    local myData = nil
    local name = "ItemGrid"
    if selected then
        selectedItemList = itemList
    end
    if (small == true) then
        vertical = 80
        horizontal = (ui.screenSize().x / 2) + 200
        name = "ItemGridOther"
        if (invMode) then --shouldn't exist
            myData = locDataInvMode[name]
        else
            myData = locData[name]
        end
        if (myData ~= nil) then
            horizontal = myData.xpos
            vertical = myData.ypos
        end
    else
        if (invMode) then --shouldn't exist
            myData = locDataInvMode[name]
        else
            myData = locData[name]
        end
        if (myData ~= nil) then
            horizontal = myData.xpos
            vertical = myData.ypos
        end
    end
    local content1 = {}
    local content2 = {}
    local counter = 0
    local contents = {}
    for x = 1, 10 do
        local content = {} -- Create a new table for each value of x
        for y = 1, 10 do
            local index = (x - 1) * 10 + y
            if (isPlayer) then
                index = index + getScrollOffset("player")
            else
                index = index + getScrollOffset("")
            end
            --print(tostring(getScrollOffset(isPlayer)))
            --print(index)
            if index <= #itemList then
                local item = itemList[index]
                if (item.recordId == itemCheck) then
                    core.sendGlobalEvent("clearContainerCheck", item)
                    item = nil
                end
                if selectedx == x and selectedy == y and selected then
                    selectedInvItem = item
                    local itemLayout = renderGridIcon(item, true)
                    if (isPlayer and I.ZackUtils.itemIsEquipped(item, self) and selectedInvItem.count == 1) or (isPlayer == false and I.ZackUtils.itemIsEquipped(item, nearbyActualInv)) then
                        itemLayout.template = I.MWUI.templates.box
                    else
                        itemLayout.template = I.MWUI.templates.padding
                    end
                    table.insert(content, itemLayout)
                else
                    local itemLayout = renderGridIcon(item, false)
                    if (isPlayer and I.ZackUtils.itemIsEquipped(item, self) and item.count == 1) or (isPlayer == false and I.ZackUtils.itemIsEquipped(item, nearbyActualInv)) then
                        itemLayout.template = I.MWUI.templates.box
                    else
                        itemLayout.template = I.MWUI.templates.padding
                    end
                    table.insert(content, itemLayout)
                end
            else
                if selectedx == x and selectedy == y and selected then
                    selectedInvItem = nil
                    local itemLayout = renderGridIcon(nil, true)
                    itemLayout.template = I.MWUI.templates.padding
                    table.insert(content, itemLayout)
                else
                    local itemLayout = renderGridIcon(nil, false)
                    itemLayout.template = I.MWUI.templates.padding
                    table.insert(content, itemLayout)
                end
            end
        end
        table.insert(contents, content)
    end

    for _, item in ipairs(itemList) do
        counter = counter + 1
    end
    local table_contents = {} -- Table to hold the generated items

    for index, contentx in ipairs(contents) do
        local item = {
            type = ui.TYPE.Flex,
            content = ui.content(contentx),
            props = {
                size = util.vector2(iconsize, iconsize),
                position = v2(50 * (index - 1), 0.8),
                vertical = false,
                arrange = ui.ALIGNMENT.Center,
            },
            external = {
                grow = iconsize + 15
            }
        }
        table.insert(table_contents, item)
    end
    return ui.create {
        layer = "Windows",
        template = I.MWUI.templates.boxTransparent,
        events = {
            mousePress = async:callback(clickMe),
            mouseRelease = async:callback(clickMeStop),
            mouseMove = async:callback(clickMeMove)
        },
        props = {
            -- relativePosition = v2(0.65, 0.8),
            size = v2(iconsize * 15, iconsize * 15),
            position = v2(horizontal, vertical),
            arrange = ui.ALIGNMENT.Center,
            name = name,
        },
        content = ui.content(table_contents)
    }
end
local function renderItemText(item, bold)
    return {
        type = ui.TYPE.Container,
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                alignment = ui.ALIGNMENT.Center,
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = I.MWUI.templates.textNormal,
                        props = {
                            text = item,
                            arrange = ui.ALIGNMENT.Center
                        }
                    }
                }
            }
        }
    }
end

local function createParentUi(content, horizontal, vertical, name, isHorizontal)
    if (isHorizontal == nil) then
        isHorizontal = false
    end
    local ret = {
        layer = "Windows",
        template = I.MWUI.templates.boxTransparent,
        events = {
            mousePress = async:callback(clickMe),
            mouseRelease = async:callback(clickMeStop),
            mouseMove = async:callback(clickMeMove)
        },
        props = {
            -- relativePosition = v2(0.65, 0.8),
            anchor = v2(0.5, -0.5),
            position = v2(horizontal, vertical),
            vertical = not isHorizontal,
            arrange = ui.ALIGNMENT.Center,
            align = ui.ALIGNMENT.Center,
            name = name
        },
        content = ui.content {
            {
                type = ui.TYPE.Flex,
                content = ui.content(content),
                props = {
                    size = v2((ui.screenSize().x / 2) - 600, 40),
                    horizontal = isHorizontal,
                    align = ui.ALIGNMENT.Center,
                    arrange = ui.ALIGNMENT.Center
                }
            }
        }
    }
    local myData = nil
    if (invMode) then
        myData = locDataInvMode[name]
    else
        myData = locData[name]
    end


    if (myData ~= nil) then
        ret.props = {
            -- relativePosition = v2(0.65, 0.8),
            position = v2(myData.xpos, myData.ypos),
            vertical = not isHorizontal,
            arrange = ui.ALIGNMENT.Center,
            align = ui.ALIGNMENT.Center,
            name = name
        }
    end
    return ui.create(ret)
end

local function anglesToV(pitch, yaw)
    local xzLen = math.cos(pitch)
    return util.vector3(
        xzLen * math.sin(yaw), -- x
        xzLen * math.cos(yaw), -- y
        math.sin(pitch)        -- z
    )
end
local function getCameraDirData()
    local pos = Camera.getPosition()
    local pitch, yaw

    pitch = -(Camera.getPitch() + Camera.getExtraPitch())
    yaw = (Camera.getYaw() + Camera.getExtraYaw())

    return pos, anglesToV(pitch, yaw)
end


local function getObjInCrosshairs( mdist)
    local pos, v = getCameraDirData()

    local dist = 500
    if (mdist ~= nil) then
        dist = mdist
    end

    if (true) then
        return nearby.castRay(pos, pos + v * dist, { })
    end
end
local function renderButtonPromptsList()
    local vertical = 50
    if selected then
        selectedItemList = itemList
    end

    local vertical = 0
    local horizontal = ui.screenSize().x / 2
    local vertical = vertical + ui.screenSize().y / 2 - 100

    local content = {}
    --table.insert(content, I.ZackUtilsUI_co.imageContent(ui.texture({path = "icons\\128w\\A.png"})))
    if (invMode) then
        local dropPos = getObjInCrosshairs(500).hitPos
        if (dropPos == nil) then
            table.insert(content, I.ZackUtilsUI_co.paddedTextContent("A: Drop to floor "))
        else
            table.insert(content, I.ZackUtilsUI_co.paddedTextContent("A: Drop to target"))
        end
        table.insert(content, I.ZackUtilsUI_co.paddedTextContent("Y: Switch to Spells"))
    else
        table.insert(content, I.ZackUtilsUI_co.paddedTextContent("A: Transfer"))
        table.insert(content, I.ZackUtilsUI_co.paddedTextContent("RS Click: Switch Selected Inventory"))

        table.insert(content, I.ZackUtilsUI_co.paddedTextContent("Y: Take All"))
    end
    --table.insert(content, I.ZackUtilsUI_co.imageContent(ui.texture({path = "icons\\128w\\X.png"})))
    table.insert(content, I.ZackUtilsUI_co.paddedTextContent("X: Equip"))
    --table.insert(content, I.ZackUtilsUI_co.imageContent(ui.texture({path = "icons\\128w\\Y.png"})))
    --table.insert(content, I.ZackUtilsUI_co.imageContent(ui.texture({path = "icons\\128w\\Right Stick Click.png"})))

    return createParentUi(content, horizontal, vertical, "ButtonPromptsList", true)
end
local function renderItemCategoriesChooser()
    local vertical = 50
    local horizontal = (ui.screenSize().x / 2) - 400
    if selected then
        selectedItemList = itemList
    end

    local vertical = 0
    local horizontal = ui.screenSize().x / 2
    local vertical = vertical + ui.screenSize().y / 2 + 100

    local content = {}
    for index, value in ipairs(catTypes) do
        table.insert(content, I.ZackUtilsUI_co.boxedTextContent(value, catTypes[currentCat]))
    end
    return createParentUi(content, horizontal, vertical, "CatChooser", true)
end
local function padString(str, len)
    local padding = len - #str
    if padding > 0 then
        return str .. string.rep(" ", padding)
    else
        return str
    end
end
local function roundToTwoDecimalPlaces(num)
    return math.floor(num * 100 + 0.5) / 100
end
local weaponTypeStr = {
    [types.Weapon.TYPE.Arrow] = nil,
    [types.Weapon.TYPE.AxeOneHand] = "Axe, One Handed",
    [types.Weapon.TYPE.AxeTwoHand] = "Axe, Two Handed",
    [types.Weapon.TYPE.BluntOneHand] = "Blunt Weapon, One Handed",
    [types.Weapon.TYPE.BluntTwoClose] = "Blunt Weapon, Two Handed",
    [types.Weapon.TYPE.BluntTwoWide] = "Blunt Weapon, Two Handed",
    [types.Weapon.TYPE.Bolt] = nil,
    [types.Weapon.TYPE.LongBladeOneHand] = "Long Blade, One Handed",
    [types.Weapon.TYPE.LongBladeTwoHand] = "Long Blade, Two Handed",
    [types.Weapon.TYPE.MarksmanBow] = "Marksman",
    [types.Weapon.TYPE.MarksmanCrossbow] = "Marksman",
    [types.Weapon.TYPE.MarksmanThrown] = "Marksman",
    [types.Weapon.TYPE.ShortBladeOneHand] = "Short Blade, One Handed",
    [types.Weapon.TYPE.SpearTwoWide] = "Spear",

}
local function renderItemInventoryInfo(item)
    local vertical = 50
    local horizontal = (ui.screenSize().x / 2)
    if selected then
        selectedItemList = itemList
    end

    local vertical = 0
    local horizontal = ui.screenSize().x / 2
    local vertical = vertical + ui.screenSize().y / 2 - 100

    local content = {}
    --  for _, item in ipairs(itemList) do
    if (item.count > 1) then
        table.insert(content,
            I.ZackUtilsUI_co.paddedTextContent(padString(
                I.ZackUtils.FindGameObjectName(item) .. "(" .. tostring(item.count) .. ")", 0)))
    else
        table.insert(content, I.ZackUtilsUI_co.paddedTextContent(padString(I.ZackUtils.FindGameObjectName(item), 0)))
    end
    --  table.insert(content, I.ZackUtilsUI_co.imageContent(ui.texture({ path = "icons\\weight-xxl.png" })))
    table.insert(content,
        I.ZackUtilsUI_co.paddedTextContent("Weight:" ..
            tostring(roundToTwoDecimalPlaces(I.ZackUtils.findItemWeight(item)))))
    table.insert(content,
        I.ZackUtilsUI_co.paddedTextContent("Value:" ..
            tostring(roundToTwoDecimalPlaces(I.ZackUtils.findItemRecord(item.recordId).value))))

    if (item.type == types.Weapon) then
        print("weapon")
        table.insert(content,
            I.ZackUtilsUI_co.paddedTextContent(weaponTypeStr[I.ZackUtils.findItemRecord(item.recordId).type], 0))

        local dmString = ""
        local record = I.ZackUtils.findItemRecord(item.recordId)
        if (record.chopMaxDamage > 0 and record.slashMaxDamage > 0) then
            dmString = string.format("%sChop: %d-%d, ", dmString, record.chopMinDamage, record.chopMaxDamage)
        elseif (record.chopMaxDamage > 0 and record.slashMaxDamage == 0) then
            if (record.chopMinDamage == record.chopMaxDamage) then
                dmString = string.format("%sAttack: %d, ", dmString, record.chopMinDamage)
            else
                dmString = string.format("%sAttack: %d-%d, ", dmString, record.chopMinDamage, record.chopMaxDamage)
            end
        end
        if (record.slashMaxDamage > 0) then
            dmString = string.format("%sSlash: %d-%d, ", dmString, record.slashMinDamage, record.slashMaxDamage)
        end
        if (record.slashMaxDamage > 0) then
            dmString = string.format("%sThrust: %d-%d, ", dmString, record.thrustMinDamage, record.thrustMaxDamage)
        end

        table.insert(content, I.ZackUtilsUI_co.paddedTextContent(dmString, 0))
    end

    return createParentUi(content, horizontal, 10, "ItemInventoryInfo")
end
local PlayerInv
local OtherInv
local Text1
local Text2
local Text3
local nearbyinv = nil
local function onInputAction(id)
    --ui.showMessage(id .. "AND THE REST")
    if id == input.ACTION.Activate then
        -- ui.showMessage(id .. "AND THE REST")
    end
end
local function sortByItemName(items)
    if true then
        return items
    end

    table.sort(items, function(a, b)
        return a.type.record(a).name < b.type.record(b).name
    end)
end
local function mergeSortedTables(table1, table2)
    local mergedTable = {}
    if (table1 ~= nil) then
        for index, value in ipairs(table1) do
            table.insert(mergedTable, #mergedTable + 1, value)
        end
    end
    if (table1 ~= nil) then
        for index, value in ipairs(table2) do
            table.insert(mergedTable, #mergedTable + 1, value)
        end
    end
    return mergedTable
end

local function filterItems(itemList, skipItem, actor)
    local key = catTypes[currentCat]
    local filter = typeFilter[key]

    local ret = {}
    if (skipItem == nil) then
        skipItem = itemCheck
    else
        skipItem = skipItem.recordId
    end
    for index, value in ipairs(itemList) do
        if (value.recordId == itemCheck) then
            core.sendGlobalEvent("clearContainerCheck", value)
        end
    end
    if (key == "All") then
        for index, value in ipairs(itemList) do
            if (value.recordId ~= skipItem and I.ZackUtils.itemIsEquipped(value, actor) and value.count == 1) then
                table.insert(ret, value)
            end
        end
        for index, value in ipairs(itemList) do
            if (value.recordId ~= skipItem and I.ZackUtils.itemIsEquipped(value, actor) == false) then
                table.insert(ret, value)
            end
        end
        return ret
    end
    for index, value in ipairs(itemList) do
        local valid = false
        for k, type in ipairs(filter) do
            if (value.type == type) then
                valid = true
            end
        end
        if (key == "Magic" and valid == false) then
            if (value.type == types.Weapon or value.type == types.Book or value.type == types.Armor or value.type == types.Clothing) then
                if (value.type.record(value).enchant ~= nil and value.type.record(value).enchant ~= "") then
                    valid = true
                end
            end
        end
        if (value.recordId ~= skipItem and valid and I.ZackUtils.itemIsEquipped(value, actor) and value.count == 1) then
            table.insert(ret, value)
        end
    end
    for index, value in ipairs(itemList) do
        local valid = false
        for k, type in ipairs(filter) do
            if (value.type == type) then
                valid = true
            end
        end
        if (key == "Magic" and valid == false) then
            if (value.type == types.Weapon or value.type == types.Book or value.type == types.Armor or value.type == types.Clothing) then
                if (value.type.record(value).enchant ~= nil and value.type.record(value).enchant ~= "") then
                    valid = true
                end
            end
        end
        if (value.recordId ~= skipItem and valid and I.ZackUtils.itemIsEquipped(value, actor) == false) then
            table.insert(ret, value)
        end
    end

    return ret
end
local function upDateinvWins()
    if (PlayerInv) then
        PlayerInv:destroy()
    end
    if (OtherInv) then
        OtherInv:destroy()
    end
    if (Text1) then
        Text1:destroy()
    end
    if (Text2) then
        Text2:destroy()
    end
    if (Text3) then
        Text3:destroy()
    end
    local inventory = filterItems(types.Actor.inventory(self.object):getAll(), skipThisItem)
    skipThisItem = nil
    if (menumode == false) then
        return
    end
    if (invMode) then
        PlayerInv = renderItemGrid(inventory, nil, false, playerSelected, true)


        Text1 = renderButtonPromptsList(I.ZackUtils.FindGameObjectName(selectedInvItem))
        Text3 = renderItemCategoriesChooser()

        if (selectedInvItem) then
            Text2 = renderItemInventoryInfo((selectedInvItem))
        end
        return
    end
    if (nearbyActualInv) then
        if (openingActor) then
            nearbyinv = filterItems(types.Actor.inventory(nearbyActualInv):getAll(), nearbyActualInv)
        else
            nearbyinv = filterItems(types.Container.content(nearbyActualInv):getAll())
        end
    end
    if (nearbyinv) then
        OtherInv = renderItemGrid(nearbyinv, nil, true, not playerSelected, false)
        PlayerInv = renderItemGrid(inventory, nil, false, playerSelected, true)


        Text1 = renderButtonPromptsList(I.ZackUtils.FindGameObjectName(selectedInvItem))
        Text3 = renderItemCategoriesChooser()

        if (selectedInvItem) then
            Text2 = renderItemInventoryInfo((selectedInvItem))
        end
    end
end
local function updateControlSwitch(enable)
   -- input.setControlSwitch(input.CONTROL_SWITCH.Controls, enable)
    -- input.setControlSwitch(input.CONTROL_SWITCH.Looking, enable)
   -- input.setControlSwitch(input.CONTROL_SWITCH.ViewMode, enable)
   -- I.ZackUtils.PauseWorld(not enable)
end
local function ClickedContainer(container)

    if (ActStartTime > 0 and container == nil) then
        local timeLapsed = core.getRealTime() - ActStartTime
        if (core.isWorldPaused()) then
            --trusting that the object's item opened
            container = contToActivate
            contToActivate = nil
            ActStartTime = 0
            print("Opened")
            
        end
        if (timeLapsed > 1) then
            print("Missed window")
            contToActivate = nil
            ActStartTime = 0
            return
        end
    elseif(container ~= nil) then
        print("Started Timer")
        ActStartTime = core.getRealTime()
        contToActivate = container
        return

    end


    if (container) then
        core.sendGlobalEvent("containerCheck", container)
    end
    selectedy = 1
    selectedx = 1
    paused = true
    openingActor = false
    menumode = true
    --self.rotation = protation
    if (container ~= nil) then
        updateControlSwitch(false)

        invMode = false
        nearbyActualInv = container
        nearbyinv = types.Container.content(container):getAll()
    else
    end
    if (container == nil or #nearbyinv == 0) then
        playerSelected = true
    end

end
local function processInput(key, id, onRelease)
    local keys = input.KEY
    if (menumode == false) then
        if (id == input.CONTROLLER_BUTTON.B and core.isWorldPaused() == false and onRelease == false) then
            
        invMode = true
            ClickedContainer(nil)
            menumode = true
        end

        return
    end
    if ((id == input.CONTROLLER_BUTTON.B or id == input.CONTROLLER_BUTTON.Back) and onRelease == false) then
        updateControlSwitch(true)
        nearbyinv = nil
        nearbyActualInv = nil
        upDateinvWins()
        menumode = false
    end
    if (nearbyActualInv == nil and invMode == false) then
        return
    end
    if (onRelease == false) then
        if (id == input.CONTROLLER_BUTTON.DPadLeft or key == keys.LeftArrow) then
            if (selectedx > 1 and getItemAt(selectedx - 1, selectedy, selectedItemList) ~= nil) then
                selectedx = selectedx - 1
            else
                if (getScrollOffset() > 9) then
                    setScrollOffset(getScrollOffset() - 10)
                end
            end
        elseif (id == input.CONTROLLER_BUTTON.DPadRight or key == keys.RightArrow) then
            if (selectedx < 10 and getItemAt(selectedx + 1, selectedy, selectedItemList) ~= nil) then
                selectedx = selectedx + 1
            else
                setScrollOffset(getScrollOffset() + 10)
            end
        elseif (id == input.CONTROLLER_BUTTON.DPadUp or key == keys.UpArrow) then
            if (selectedy > 1 and getItemAt(selectedx, selectedy - 1, selectedItemList) ~= nil) then
                selectedy = selectedy - 1
            end
        elseif (id == input.CONTROLLER_BUTTON.DPadDown or key == keys.DownArrow) then
            if (selectedy < 10 and getItemAt(selectedx, selectedy + 1, selectedItemList)) then
                selectedy = selectedy + 1
            end
        elseif (id == input.CONTROLLER_BUTTON.LeftShoulder) then
            if (currentCat > 1) then
                currentCat = currentCat - 1
            end
        elseif (id == input.CONTROLLER_BUTTON.RightShoulder) then
            if (currentCat < #catTypes) then
                currentCat = currentCat + 1
            end
        elseif (id == input.CONTROLLER_BUTTON.RightStick or key == keys.Tab) then
            playerSelected = not playerSelected
            selectedy = 1
            selectedx = 1
        elseif (id == input.CONTROLLER_BUTTON.B or key == keys.Escape or id == input.CONTROLLER_BUTTON.Back) then
            updateControlSwitch(true)
            nearbyinv = nil
            nearbyActualInv = nil

            I.ZackUtils.PauseWorld(false)
        elseif ((id == input.CONTROLLER_BUTTON.A or key == keys.Enter) and selectedInvItem) then
            if (invMode) then
              
            else
                local cellname = self.cell.name
                if (playerSelected) then
                    core.sendGlobalEvent(
                        "doMoveInto",
                        {
                            item = selectedInvItem.id,
                            inv = nearbyActualInv.id,
                            host = self.id,
                            cell = cellname,
                            hostisplayer = true,
                            openingActor = openingActor
                        }
                    )
                else
                    core.sendGlobalEvent(
                        "doMoveInto",
                        {
                            item = selectedInvItem.id,
                            inv = self.id,
                            host = nearbyActualInv.id,
                            cell = cellname,
                            hostisplayer = false,
                            openingActor = openingActor
                        }
                    )
                end
            end
        elseif (id == input.CONTROLLER_BUTTON.Y or key == keys.End) then --take all
            local cellname = self.cell.name
            for _, iitem in ipairs(nearbyinv) do
                core.sendGlobalEvent(
                    "doMoveInto",
                    {
                        item = iitem.id,
                        inv = self.id,
                        host = nearbyActualInv.id,
                        cell = cellname,
                        hostisplayer = false,
                        openingActor = openingActor
                    }
                )
            end

            updateControlSwitch(true)
            nearbyinv = nil
            nearbyActualInv = nil
        elseif (id == input.CONTROLLER_BUTTON.X and selectedInvItem or key == keys.Home) then
            local cellname = self.cell.name
            if (playerSelected) then
                equip = types.Actor.equipment(self)
                if (equip[I.ZackUtils.findSlot(selectedInvItem)] == selectedInvItem) then --already wearing this, take it off
                    equip[I.ZackUtils.findSlot(selectedInvItem)] = nil
                else
                    equip[I.ZackUtils.findSlot(selectedInvItem)] = selectedInvItem
                end
                types.Actor.setEquipment(self, equip)
            else --will figure this out later
            end
        end
    else
        if ((id == input.CONTROLLER_BUTTON.A or key == keys.Enter) and selectedInvItem) then
            if (invMode and menumode) then
                local dropPos = getObjInCrosshairs(500).hitPos
                if (dropPos == nil) then
                    dropPos = self.position
                end
                local halfSize = selectedInvItem:getBoundingBox().halfSize.z
                local centerPos = selectedInvItem:getBoundingBox().center.z
                if (halfSize > 10000) then
                    halfSize = 0
                end
                if (centerPos > 10000) then
                    centerPos = 0
                end
                local found = false
                for index, value in ipairs(types.Actor.inventory(self):getAll()) do
                    if (value == selectedInvItem) then
                        found = true
                    end
                end
                if(I.ZackUtils.itemIsEquipped(selectedInvItem,self) or selectedInvItem.recordId == "gold_001" ) then
                    found = false
                end
                if (found) then
                    dropPos = util.vector3(dropPos.x, dropPos.y, dropPos.z + halfSize - centerPos)
                    skipThisItem = selectedInvItem
                    I.ZackUtils.teleportItemToCell(selectedInvItem, self.cell.name, dropPos,
                        util.vector3(0, 0, self.rotation.z))
                end

            end
        end
    end
    upDateinvWins()
end
local function onKeyPress(key)
    core.sendGlobalEvent("setControllerMode",false)
    processInput(key.code, -1, false)
end

local function onControllerButtonPress(id)
    core.sendGlobalEvent("setControllerMode",true)
    processInput(-1, id, false)
end
local function onControllerButtonRelease(id)
    --ui.showMessage(id .. "AND THE REST")

    processInput(-1, id, true)
end
local updatetime = 0.0
local function forwardSettings()
    local playerSettings = storage.playerSection("SettingsControllerOverRide")

    core.sendGlobalEvent("setOverrideContainers", playerSettings:get("OverrideContainers"))
end
local count = 0
local function onFrame(dt)
    if(ActStartTime > 0 ) then
        ClickedContainer()
    end
    if (invMode == true and core.isWorldPaused() == false and count > 50) then
        menumode = false
        upDateinvWins()
        count = 0
    elseif (invMode == true and core.isWorldPaused() == false and count < 51) then
        count = count + 1
    end
    updatetime = updatetime + dt
    if (paused == true) then
        -- camera.setPitch(campitch)
        --print(campitch)
        -- camera.setYaw(camyaw)
        -- camera.setRoll(camroll)
    end
    --print(updatetime)
    if (updatetime > 0.3) then
        upDateinvWins()
        updatetime = 0
        forwardSettings()
    end
    if (I.ZackUtils == nil) then
        print "nil"
    end
end


local paused = false
local function ClickedActor(actor)
    if (actor) then
        --   core.sendGlobalEvent("containerCheck", container)
    end
    selectedy = 1
    selectedx = 1
    camyaw = camera.getYaw()
    campitch = camera.getPitch()
    camroll = camera.getRoll()
    protation = self.rotation
    paused = true
    openingActor = true
    menumode = true
    --self.rotation = protation
    if (actor ~= nil) then
        updateControlSwitch(false)

        invMode = false
        nearbyActualInv = actor
        nearbyinv = types.Actor.inventory(actor):getAll()
    else
    end
    if (actor == nil or #nearbyinv == 0) then
        playerSelected = true
    end

end

local function onActive()
    updateControlSwitch(true)
    print("active")
end


local function onSave()
    return {
        locData = locData,
        locDataInvMode = locDataInvMode
    }
end

local function onLoad(data)
    if (data ~= nil) then
        locData = data.locData
        locDataInvMode = data.locDataInvMode
    end
end

return {
    interfaceName = "ControllerMode",
    interface = {
        version = 1,
        upDateinvWins = upDateinvWins
    },
    eventHandlers = {
        sendMessage = sendMessage,
        returnActivators = returnActivators,
        ClickedContainer = ClickedContainer,
        upDateinvWins = upDateinvWins,
        ClickedActor = ClickedActor,
    },
    engineHandlers = {
        onConsoleCommand = onConsoleCommand,
        onFrame = onFrame,
        onActive = onActive,
        onControllerButtonPress = onControllerButtonPress,
        onInputAction = onInputAction,
        onSave = onSave,
        onKeyPress = onKeyPress,
        onLoad = onLoad,
        onControllerButtonRelease = onControllerButtonRelease,
    }
}
