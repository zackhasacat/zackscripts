local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local itemCheck = "ebony_dagger_mehrunes"
local thingToMove = nil
local playerObject
local owner = nil
local stage
local overrideContainers
local controllerMode = false


local function setControllerMode(mode)
controllerMode = mode
end
local function containerActivate(object, actor)
    if(controllerMode == false) then
        return true
    end
    playerObject = actor
    --print(overrideContainers)
    if (overrideContainers) then
        local actorToActivate = object
        ActStartTime = core.getRealTime()
        playerObject:sendEvent("ClickedContainer", actorToActivate)
        return true
    else
        return true
    end
end
local function actorActivate(object, actor)
    if(controllerMode == false) then
        return true
    end
    if (types.Actor.stats.dynamic.health(object).current == 0) then
        actor:sendEvent("ClickedActor", object)
        --ActStartTime = core.getRealTime()
        return true
    end
end
local function setOverrideContainers(booltoUse)
    overrideContainers = booltoUse
end
acti.addHandlerForType(types.Container, containerActivate)
acti.addHandlerForType(types.Creature, actorActivate)
acti.addHandlerForType(types.NPC, actorActivate)
local function containerCheck(container)
    local item = world.createObject(itemCheck, 1)
    item = item:moveInto(types.Container.content(container))

    container.cell:getAll(types.Player)[1]:sendEvent("upDateinvWins", nil)
end
local function clearContainerCheck(container)
    container:remove()
    return nil
end
local listenForItem = false
local function doMoveInto(data)
    owner = nil
    itemid = data.item
    cellname = data.cell
    inv = data.inv
    host = data.host
    actor = data.openingActor
    --container we are moving it from
    hostisplayer = data.hostisplayer
    --print("Item:" .. itemid)
    -- print("Cell:" .. cellname)
    --print("inv:" .. inv)
    --print("Host:" .. host)
    --print("HIP:" .. tostring(hostisplayer))
    stage = 0
    local hostInventory
    local targetInventory
    local itemToMove
    local stealing = false
    local cell = world.getCellByName(cellname)
    playerObject = cell:getAll(types.Player)[1]
    if (hostisplayer) then --need to look through cell
        --host and item is found
        for _, item in ipairs(types.Actor.inventory(cell:getAll(types.Player)[1]):getAll()) do
            if (item.id == itemid) then
                itemToMove = item
            end
        end
        if (actor) then
            for _, cont in ipairs(cell:getAll(types.NPC)) do
                --	print("move try" .. inv .. cont.id)
                if (cont.id == inv) then
                    itemToMove:moveInto(types.Actor.inventory(cont))
                    --print("move try")
                end
            end
        else
            for _, cont in ipairs(cell:getAll(types.Container)) do
                if (cont.id == inv) then
                    itemToMove:moveInto(types.Container.content(cont))
                    owner = cont.owner.recordId
                end
                --print("move cont")
            end
        end
    else
        if (actor) then
            for _, cont in ipairs(cell:getAll(types.NPC)) do
                if (cont.id == host) then
                    for _, item in ipairs(types.Actor.inventory(cont):getAll()) do
                        if (item.id == itemid) then
                            itemToMove = item
                        end
                    end
                end
            end
        else
            for _, cont in ipairs(cell:getAll(types.Container)) do
                if (cont.id == host) then
                    owner = cont.owner.recordId
                    if (owner) then
                        stealing = true
                    end
                    for _, item in ipairs(types.Container.content(cont):getAll()) do
                        if (item.id == itemid) then
                            itemToMove = item
                        end
                    end
                end
            end
        end
        if (itemToMove and not stealing) then
            itemToMove:moveInto(types.Actor.inventory(cell:getAll(types.Player)[1]))
        elseif (itemToMove) then
            listenForItem = true
            itemToMove:teleport(cell, cell:getAll(types.Player)[1].position, cell:getAll(types.Player)[1].rotation)
            thingToMove = itemToMove
            --print(itemToMove.id)
        end
    end

    cell:getAll(types.Player)[1]:sendEvent("upDateinvWins", nil)
end
local function onFrame()
 
end
local function onUpdate()

end
local function onItemActive(item)
    if listenForItem then
        thingToMove = item
        thingToMove.owner.recordId = owner
        listenForItem = false
        thingToMove:activateBy(playerObject)

    end
end
return {
    eventHandlers = {
        doMoveInto = doMoveInto,
        doEquip = doEquip,
        returnActivators = returnActivators,
        ClickedContainer = ClickedContainer,
        CLickedActor = ClickedActor,
        containerCheck = containerCheck,
        clearContainerCheck = clearContainerCheck,
        setOverrideContainers = setOverrideContainers,
        setControllerMode = setControllerMode,
    },
    engineHandlers = { onUpdate = onUpdate, onItemActive = onItemActive }
}
