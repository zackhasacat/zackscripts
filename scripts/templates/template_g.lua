local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local async = require("openmw.async")
local acti = require("openmw.interfaces").Activation


return {
    interfaceName  = "",
    interface      = {
      version = 1,
    },
    engineHandlers = {
      onActorActive = onActorActive,
      onPlayerAdded = onPlayerAdded,
      onLoad = onLoad,
      onUpdate = onUpdate,
      onSave = onSave,
    },
    eventHandlers  = {
    },
  }
  