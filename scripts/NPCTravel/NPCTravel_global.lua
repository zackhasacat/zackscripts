local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local playerSettings = storage.globalSection("SettingsNPCTravel")

local cityCells = {}
local cityActors = {}

local function startsWith(str, prefix)
    return string.sub(str, 1, string.len(prefix)) == prefix
end

local function findAllResidents()
    local cellList = {}
    for index, cell in ipairs(world.cells) do
        local cellName = cell.name
        if startsWith(cellName, world.players[1].cell.name) then
            table.insert(cellList, cell)
            print(cellName)
            local actors = cell:getAll(types.NPC)
            for index, npc in ipairs(actors) do
                table.insert(cityActors, npc)

            end
        end
    end
    return cellList
end
local function wanderCity()
    for index, actor in ipairs(cityActors) do

        actor:sendEvent("StartAIPackage",{type="Wander",distance = 300})
    end

end
local function aggroCity()
    for i, actor in ipairs(cityActors) do
        actor:sendEvent("setCrazy",true)
        local target = nil
        local minDistance = math.huge -- Initialize with a large value
        local myPos = actor.position
        for k, actorT in ipairs(cityActors) do
            if actorT ~= actor and actorT.recordId ~= "player" and actor.recordId ~= "player" then
                local targetPos = actorT.position
                local distance = (targetPos - myPos):length() -- Calculate distance using vector length
                if distance < minDistance then
                    minDistance = distance
                    target = actorT
                end
            end
        end
        if target ~= nil then
            actor:sendEvent("StartAIPackage", {type = "Combat", target = target})
        end
    end
end
local function convergeCity(position)
    for index, actor in ipairs(cityActors) do

        actor:sendEvent("StartAIPackage",{type="Travel",destPosition   = position})
    end

end
local function evacuateCity()

    for index, actor in ipairs(cityActors) do
        if (actor.cell.isExterior == false) then
            for x, door in ipairs(actor.cell:getAll(types.Door)) do
                if (types.Door.destCell(door) ~= nil and
                    types.Door.destCell(door).isExterior) then
                    actor:teleport(types.Door.destCell(door),
                                   types.Door.destPosition(door),
                                   types.Door.destPosition(door))
                                   actor:sendEvent("StartAIPackage",{type="Wander",distance = 300})
                    break
                end
            end
        end
    end

end


local function getAllResidents() return cityActors end
local function onInit() cityCells = findAllResidents() end
local function onLoad() cityCells = findAllResidents() end

return {
    interfaceName = "NPCTravel",
    interface = {
        version = 1,
        findAllResidents = findAllResidents,
        getAllResidents = getAllResidents,
        evacuateCity = evacuateCity,
        wanderCity = wanderCity,
        convergeCity = convergeCity,
        aggroCity = aggroCity,
    },
    engineHandlers = {
        onActorActive = onActorActive,
        onPlayerAdded = onPlayerAdded,
        onLoad = onLoad,
        onInit = onInit
    },
    eventHandlers = {
        setSetting = setSetting,
        runMWscriptBridge = runMWscriptBridge,
        COCEvent = COCEvent,
        killAll = killAll,
        DebugActorSwap = DebugActorSwap
    }
}
