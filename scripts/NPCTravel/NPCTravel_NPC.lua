local storage = require("openmw.storage")
local self = require("openmw.self")
local types = require("openmw.types")

local playerSettings = storage.globalSection("SettingsDebugMode")
local util = require("openmw.util")
local core = require("openmw.core")
local nearby = require("openmw.nearby")
local I = require("openmw.interfaces")
local crazy = false
local function testFunc() print("Testing " .. self.recordId) end
local function onUpdate()

    if (crazy) then
        if I.AI.getActiveTarget("Combat") == nil or types.Actor.stats.dynamic.health(I.AI.getActiveTarget("Combat")).current == 0 then
            I.AI.removePackages()
            local target = nil
            local minDistance = math.huge -- Initialize with a large value
            local myPos = self.position
            for k, actorT in ipairs(nearby.actors) do
                if actorT ~= self and actorT.recordId ~= "player"  and actorT.id ~= self.id and  types.Actor.stats.dynamic.health(actorT).current > 0 then
                    local targetPos = actorT.position
                    local distance = (targetPos - myPos):length() -- Calculate distance using vector length
                    if distance < minDistance then
                        minDistance = distance
                        target = actorT
                    end
                end
            end
            if target ~= nil then
                self:sendEvent("StartAIPackage",
                               {type = "Combat", target = target})
            else
                print("No target for",self.recordId)
            end

        end

    end
end
local function findSlot(item)
    if item.type == types.Armor then
    if (types.Armor.records[item.recordId].type == types.ArmorTYPE.RGauntlet 	) then
        return types.Actor.EQUIPMENT_SLOT.RightGauntlet
    elseif (types.Armor.records[item.recordId].type == types.ArmorTYPE.LGauntlet 	) then
        return types.Actor.EQUIPMENT_SLOT.LeftGauntlet
    end
    elseif item.type == types.Book then
        return types.Book.records[item.recordId].enchant
    elseif item.type == types.Clothing then
        if (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Amulet) then
            return types.Actor.EQUIPMENT_SLOT.Amulet
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Skirt) then
            return types.Actor.EQUIPMENT_SLOT.Skirt
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Shirt) then
            return types.Actor.EQUIPMENT_SLOT.Shirt
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Shoes) then
            return types.Actor.EQUIPMENT_SLOT.Boots
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Robe) then
            return types.Actor.EQUIPMENT_SLOT.Robe
        end
    elseif item.type == types.Weapon then
        return types.Actor.EQUIPMENT_SLOT.CarriedRight
    end
    print("Couldn't find slot for " .. item.recordId)
    return false
end
local function equipItems(itemTable)
    local inv = types.Actor.inventory(self)

    local equip = types.Actor.getEquipment(self)
    for index, itemId in ipairs(itemTable) do
        local item = inv:find(itemId)
        local slot = findSlot(item)
        if (slot) then equip[slot] = item end
    end

    types.Actor.setEquipment(self, equip)
end
local function setCrazy(val) 
   core.sendGlobalEvent("ZackUtilsAddItems",{itemIds={"sunder","wraithguard"},equip=true,actor=self})
   types.NPC.stats.skills.bluntweapon(self).base = 1000 
   types.NPC.stats.skills.heavyarmor(self).base = 1000 
    crazy = val 

end

return {
    interfaceName = "NPCTravel",
    interface = {version = 1, test = test, sendToPos = sendToPos},

    engineHandlers = {onInit = onInit, onLoad = onInit, onUpdate = onUpdate},
    eventHandlers = {
        testFunc = testFunc,
        setStat = setStat,
        setEquipment = setEquipment,
        setBadItems = setBadItems,
        equipItems = equipItems,
        setCrazy = setCrazy,
        equipItems = equipItems,
    }
}
