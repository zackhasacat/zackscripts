local cam, camera, core, self, nearby, types, ui, util, storage, async, input,
zackUtils, debug = require('openmw.interfaces').Camera, require('openmw.camera'),
    require('openmw.core'), require('openmw.self'),
    require('openmw.nearby'), require('openmw.types'),
    require('openmw.ui'), require('openmw.util'),
    require("openmw.storage"), require("openmw.async"),
    require("openmw.input"),
    require("scripts.ZackUtils.PlayerInterface").interface,
    require("openmw.debug")
local ambient = require('openmw.ambient')

local constants = require('scripts.omw.mwui.constants')
local UIInterface = require("scripts.FactionBanks.UIPlayerInterface")

local BankUi = nil
local enabled = false
local innerWindowSize = util.vector2(700, 250)
local v2 = util.vector2
local lastActivatedActor
local activeFaction
local player
local I = require('openmw.interfaces')
local currentSelectedIndex = 0
local textDisabled = {
    type = ui.TYPE.Text,
    props = {
        textSize = constants.textNormalSize,
        textColor = util.color.rgb(10, 10, 10)
    },
}

local function CloseBankWin()
    I.UI.removeMode(I.UI.getMode())
end

local function closeWindow(openDialog)
    if (BankUi ~= nil) then
        BankUi:destroy()
        enabled = false
    end
    if openDialog then
        --  I.UI.setMode("Dialogue", { target = lastActivatedActor })
    end
end
local function resetBoldOptions(index)
    currentSelectedIndex = index
    --    boldOptions = {}
    -- if index and BankData[index] then
    --    boldOptions[index] = true
    --end
    --
end

local function hoverOption(data, data2)
    local index
    if data2 then
        index = (data2.index)
    end
    resetBoldOptions(index)
end

--[[
    local table = {
        renderOption(1, "Deposit 10 gold", "Line1Text", clickOption),
        renderOption(2, "Deposit 100 gold", "Line2Text", clickOption),
        renderOption(3, "Deposit 1000 gold", "Line3Text", clickOption),
        renderOption(4, "Deposit 10000 gold", "Line4Text", clickOption),
        renderOption(5, "Deposit All gold", "Line5Text", clickOption),
        renderOption(6, "Deposit Half gold", "Line6Text", clickOption),
    }
    if withdraw then
        table = {
            renderOption(1, "Withdraw 10 gold", "Line1Text", clickOption),
            renderOption(2, "Withdraw 100 gold", "Line2Text", clickOption),
            renderOption(3, "Withdraw 1000 gold", "Line3Text", clickOption),
            renderOption(4, "Withdraw 10000 gold", "Line4Text", clickOption),
            renderOption(5, "Withdraw All gold", "Line5Text", clickOption),
            renderOption(6, "Withdraw Half gold", "Line6Text", clickOption),
        }
    end
    --]]
local function clickOption(index, data2)
    local index
    if data2 then
        index = (data2.index)
    end
    local playerGold = types.Container.content(self):countOf("gold_001")
    local bankGold = I.FactionBankData.getBankBalance(activeFaction)
    local button = data2.props.text
    local shiftPressed = false
    if input.isKeyPressed(input.KEY.LeftShift) then
        shiftPressed = true
    elseif input.isKeyPressed(input.KEY.RightShift) then
        shiftPressed = true
    end
    local newPlayerGold = 0
    local sound = "item gold up"
    local bookSound = "item book up"
    if shiftPressed then
        sound = bookSound
    end
    if button == "Deposit 10 gold" then
        newPlayerGold = I.FactionBankData.depositToBank(activeFaction, 10)
    elseif button == "Deposit 100 gold" then
        newPlayerGold = I.FactionBankData.depositToBank(activeFaction, 100)
    elseif button == "Deposit 1000 gold" then
        newPlayerGold = I.FactionBankData.depositToBank(activeFaction, 1000)
    elseif button == "Deposit 10000 gold" then
        newPlayerGold = I.FactionBankData.depositToBank(activeFaction, 10000)
    elseif button == "Deposit Half gold" then
        newPlayerGold = I.FactionBankData.depositToBank(activeFaction, math.floor(playerGold / 2))
    elseif button == "Deposit All gold" then
        newPlayerGold = I.FactionBankData.depositToBank(activeFaction, playerGold)
    elseif button == "Withdraw 10 gold" then
        newPlayerGold = I.FactionBankData.withdrawFromBank(activeFaction, 10, shiftPressed)
    elseif button == "Withdraw 100 gold" then
        newPlayerGold = I.FactionBankData.withdrawFromBank(activeFaction, 100, shiftPressed)
    elseif button == "Withdraw 1000 gold" then
        newPlayerGold = I.FactionBankData.withdrawFromBank(activeFaction, 1000, shiftPressed)
    elseif button == "Withdraw 10000 gold" then
        newPlayerGold = I.FactionBankData.withdrawFromBank(activeFaction, 10000, shiftPressed)
    elseif button == "Withdraw Half gold" then
        newPlayerGold = I.FactionBankData.withdrawFromBank(activeFaction, math.floor(bankGold / 2), shiftPressed)
    elseif button == "Withdraw All gold" then
        newPlayerGold = I.FactionBankData.withdrawFromBank(activeFaction, bankGold, shiftPressed)
    end
    if shiftPressed then
        ambient.playSound(sound)
        I.FactionBanksUI.renderBankOptions(lastActivatedActor, newPlayerGold)
    elseif playerGold ~= newPlayerGold and newPlayerGold ~= nil then
        ambient.playSound(sound)

        I.FactionBanksUI.renderBankOptions(lastActivatedActor, newPlayerGold)
    end
    --    closeWindow()
end
local function renderOption(index, text, _, clickCallback)
    local lineText = text or "Line"
    local template = I.MWUI.templates.textHeader


    return {
        type = ui.TYPE.Text,
        template = template,
        events = {
            mouseMove = async:callback(hoverOption),
            mousePress = async:callback(clickCallback)
        },
        index = index,
        props = {
            text = lineText,
            textSize = 15,
            align = ui.ALIGNMENT.Start,
            anchor = util.vector2(0, -index * 1.0 + 1),
            arrange = ui.ALIGNMENT.End,
            size = util.vector2(180, 20),
            autoSize = false
        }
    }
end


-- Repeat clickOption function for other options (clickOptionTwo, clickOptionThree, ...)

local function boxedTextContentTable(withdraw)
    local table = {
        renderOption(1, "Deposit 10 gold", "Line1Text", clickOption),
        renderOption(2, "Deposit 100 gold", "Line2Text", clickOption),
        renderOption(3, "Deposit 1000 gold", "Line3Text", clickOption),
        renderOption(4, "Deposit 10000 gold", "Line4Text", clickOption),
        renderOption(5, "Deposit Half gold", "Line6Text", clickOption),
        renderOption(6, "Deposit All gold", "Line5Text", clickOption),
    }
    if withdraw then
        table = {
            renderOption(1, "Withdraw 10 gold", "Line1Text", clickOption),
            renderOption(2, "Withdraw 100 gold", "Line2Text", clickOption),
            renderOption(3, "Withdraw 1000 gold", "Line3Text", clickOption),
            renderOption(4, "Withdraw 10000 gold", "Line4Text", clickOption),
            renderOption(5, "Withdraw Half gold", "Line6Text", clickOption),
            renderOption(6, "Withdraw All gold", "Line5Text", clickOption),
        }
    end
    local content = {
        type = ui.TYPE.Container,
        props = {
            anchor = util.vector2(0.5, 0.5),
            size = util.vector2(400, 400),
            autoSize = false
        },
        content = ui.content {
            {
                template = I.MWUI.templates.box,
                events = {
                    --  mouseMove = async:callback(I.BankWindow.hoverNone),
                    -- mouseClick = async:callback(I.BankWindow.hoverNone)
                },
                props = {
                    anchor = util.vector2(0, -0.5),
                    size = util.vector2(400, 400),
                    autoSize = false
                },
                content = ui.content(table)
            },

        }
    }

    return content
end

local function textContentLeft(text)
    return {
        type = ui.TYPE.Text,
        template = I.MWUI.templates.textNormal,
        props = {
            relativePosition = v2(0.5, 0.5),
            text = tostring(text),
            arrange = ui.ALIGNMENT.Start,
            align = ui.ALIGNMENT.Start
        }
    }
end

local function textContentCenter(text)
    return {
        type = ui.TYPE.Text,
        template = I.MWUI.templates.textNormal,
        props = {
            relativePosition = v2(0.5, 0.5),
            text = tostring(text),
            arrange = ui.ALIGNMENT.Center,
            align = ui.ALIGNMENT.Center
        }
    }
end

local function boxedTextContentEnd(text, callback)
    return {
        type = ui.TYPE.Container,
        events = {
            mousePress = async:callback(callback)
        },
        props = {
            anchor = util.vector2(0, -0.5),
            align = ui.ALIGNMENT.End,
            arrange = ui.ALIGNMENT.End,
        },
        content = ui.content {
            {
                template = I.MWUI.templates.box,
                props = {
                    anchor = util.vector2(0, -0.5),
                    align = ui.ALIGNMENT.End,
                    arrange = ui.ALIGNMENT.End,
                },
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = I.MWUI.templates.textNormal,
                        props = {
                            text = text,
                            textSize = 15,
                            align = ui.ALIGNMENT.End,
                            arrange = ui.ALIGNMENT.End,
                        }
                    }
                }
            }
        }
    }
end


local function lerp(x, x1, x2, y1, y2)
    return y1 + (x - x1) * ((y2 - y1) / (x2 - x1))
end
local function calculateTextScale()
    local screenSize = ui.layers[1].size
    local width = screenSize.x
    local scale = lerp(width, 1280, 2560, 1.3, 1.8)
    local textScaleSetting = 1
    return scale * textScaleSetting
end
local function openUI()
    local UI = UIInterface.interface.renderItemChoice
end
local function textContent(text, template, color)
    if (template == nil) then
        template = I.MWUI.templates.textHeader
    else
        if (color ~= nil) then
            template.props.textColor = color
        end
    end
    return {
        type = ui.TYPE.Text,
        template = template,
        props = {
            text = tostring(text),
            textSize = 20 * calculateTextScale(),
            arrange = ui.ALIGNMENT.Start,
            align = ui.ALIGNMENT.Start
        }
    }
end
local function renderBankOptions(BankActor, playerGold, resetUI)
    if resetUI then
        I.UI.setMode("Interface", { windows = {} })
    end
    if not playerGold then
        playerGold = types.Container.content(self):countOf("gold_001")
        I.UI.setMode("Interface", { windows = {} })
    end
    lastActivatedActor = BankActor
    activeFaction = types.NPC.getFactions(BankActor)[1]
    if not activeFaction then
        error("No faction found")
    end
    if not I.UI.getMode() then
        I.UI.setMode("Interface", { windows = {} })
    end
    if (BankActor ~= nil) then
        enabled = true
    end
    if (enabled == false) then
        return
    end
    if (BankUi ~= nil) then
        BankUi:destroy()
    end
    if (self.object ~= nil) then
        player = self.object
    end
    if (OKText == nil) then
        OKText = "OK"
    end
    if (core.isWorldPaused() == false) then
        return
    end

    local content = {}
    local content2 = {}
    local content3 = {}
    local content4 = {}

    --resetBoldOptions()

    table.insert(content, textContent(""))
    -- table.insert(content, textContentLeft("Select destination"))

    local textEdit = boxedTextContentTable(false)
    local textEdit2 = boxedTextContentTable(true)
    local cancelButton = boxedTextContentEnd("Close", function()
        closeWindow()
        self:sendEvent("CloseBankWin")
        return
    end)
    local boxButton = boxedTextContentEnd("Safe Deposit Box", function()
        closeWindow()
        core.sendGlobalEvent("openDepositBox", activeFaction)
        return
    end)
    local voucherButton = boxedTextContentEnd("Deposit Voucher", function()
        local deposited = I.FactionBankData.depositVouchers(activeFaction)
        if deposited > 0 then
            local bookSound = "item book up"
            ambient.playSound(bookSound)
            I.FactionBanksUI.renderBankOptions(lastActivatedActor)
        end
        return
    end)
    local goldText = textContentLeft("Gold: " .. tostring(playerGold))

    table.insert(content2, textEdit)
    table.insert(content2, textEdit2)
    local factionName = core.factions.records[activeFaction].name
    local balanceText = textContentCenter(factionName ..
        " Balance: " .. tostring(I.FactionBankData.getBankBalance(activeFaction)))

    table.insert(content3, balanceText)
    table.insert(content3, cancelButton)
    table.insert(content3, boxButton)
    table.insert(content3, voucherButton)

    table.insert(content4, goldText)

    BankUi = ui.create {
        layer = "Windows",
        template = I.MWUI.templates.boxTransparentThick,
        props = {
            relativePosition = v2(0.5, 0.5),
            anchor = v2(0.5, 0.5),
            vertical = false,
            relativeSize = util.vector2(0.5, 0.8),
            arrange = ui.ALIGNMENT.Center
        },
        content = ui.content {
            {
                type = ui.TYPE.Text,
                template = I.MWUI.templates.textHeader,
                props = {
                    anchor = v2(-5, -0),
                    relativePosition = v2(0.5, 0.5),
                    text = tostring("Bank"),
                    arrange = ui.ALIGNMENT.Start,
                    align = ui.ALIGNMENT.Center
                },
            },
            {
                type = ui.TYPE.Flex,
                content = ui.content(content),
                props = {
                    anchor = v2(0.0, -0.0),
                    horizontal = true,
                    autoSize = false,
                    align = ui.ALIGNMENT.Start,
                    arrange = ui.ALIGNMENT.Start,
                    size = innerWindowSize
                }
            },
            {
                type = ui.TYPE.Flex,
                content = ui.content(content4),
                props = {
                    anchor = v2(-0.01, -0.01),
                    horizontal = true,
                    autoSize = false,
                    align = ui.ALIGNMENT.Start,
                    arrange = ui.ALIGNMENT.End,
                    size = innerWindowSize
                }
            },
            {
                type = ui.TYPE.Flex,
                content = ui.content(content3),
                props = {
                    anchor = v2(0.0, -0.0),
                    horizontal = true,
                    autoSize = false,
                    align = ui.ALIGNMENT.End,
                    arrange = ui.ALIGNMENT.End,
                    size = innerWindowSize
                }
            },
            {
                type = ui.TYPE.Flex,
                content = ui.content(content2),
                props = {
                    horizontal = true,
                    anchor = v2(0, 0.05),
                    autoSize = false,
                    align = ui.ALIGNMENT.Center,
                    arrange = ui.ALIGNMENT.Start,
                    size = innerWindowSize
                }
            },
        }
    }
end
local function OpenBankMenu()
    local balanceCheck = I.FactionBankTemp.exitTempMode()
    if lastActivatedActor then
        renderBankOptions(lastActivatedActor, balanceCheck, true)
    end
end
return {
    interfaceName = "FactionBanksUI",
    interface = {
        renderBankOptions = renderBankOptions,
    },
    eventHandlers = {
        CloseBankWin  = CloseBankWin,
        OpenBankMenu  = OpenBankMenu,
        UiModeChanged = function(data)
            if data and data.arg and data.arg then
                lastActivatedActor = data.arg
            end


            if not data.newMode then
                if BankUi then
                    closeWindow()
                end
            else
                if true == true then
                    --
                    --closeWindow(true)
                elseif data and data.arg and data.arg then
                    lastActivatedActor = data.arg
                end
            end
        end
    },

}
