local self = require("openmw.self")

return {
    engineHandlers = {
        onUpdate = function ()
            self.controls.movement = 0
        end
    }
}