--Draw menu with text  enterer, select if scroll or not, source book, insert newline, new page, first line settings 

local ui = require('openmw.ui')
local I = require('openmw.interfaces')
local async = require('openmw.async')
local util = require('openmw.util')
local v2 = util.vector2
local function boxedTextEditContent(text, callback,textScale,width)
    if textScale == nil then
        textScale = 1
    end
    if width == nil then
        width = 400
    end
    return {
        type = ui.TYPE.Container,
        content = ui.content {
            {
                template = I.MWUI.templates.box,
                props = {
                    anchor = util.vector2(0, -0.5),
                    size = util.vector2(400, 10),
                },
                content = ui.content {
                    {
                        type = ui.TYPE.TextEdit,
                        template = I.MWUI.templates.textEditLine,
                        events = { textChanged = callback },
                        props = {
                            text = text,
                            size = util.vector2(width, 60),
                            textAlignH = 15,
                            textSize =( 25),
                            align = ui.ALIGNMENT.Center,
                            multiLine = true
                        }
                    }
                }
            }
        }
    }
end
local function renderTextInput(textLines, existingText, editCallback, OKCallback, OKText)
    if (OKText == nil) then
        OKText = "OK"
    end
    local vertical = 50
    local horizontal = (ui.screenSize().x / 2) - 400

    local vertical = 0
    local horizontal = ui.screenSize().x / 2 - 25
    local vertical = vertical + ui.screenSize().y / 2 + 100

    local content = {}
    for _, text in ipairs(textLines) do
        table.insert(content, I.ZackUtilsUI.textContent(text))
    end
    local textEdit = boxedTextEditContent(existingText, async:callback(editCallback))
    local okButton = I.ZackUtilsUI.boxedTextContent(OKText, async:callback(OKCallback))
    table.insert(content, textEdit)
    table.insert(content, okButton)

    return ui.create {
        layer = "Windows",
        template = I.MWUI.templates.boxTransparentThick,
        props = {
            -- relativePosition = v2(0.65, 0.8),
            --  anchor = v2(-1, -2),
            position = v2(horizontal, vertical),
            vertical = false,
            relativeSize = util.vector2(0.1, 0.1),
            arrange = ui.ALIGNMENT.Center
        },
        content = ui.content {
            {
                type = ui.TYPE.Flex,
                content = ui.content(content),
                props = {
                    horizontal = false,
                    align = ui.ALIGNMENT.Center,
                    arrange = ui.ALIGNMENT.Center,
                    size = util.vector2(400, 10),
                }
            }
        }
    }
end
local function drawBookWriter(bookRecord)
return renderTextInput({""},"",nil,nil,"OK")
end

return{
    eventHandlers = {
drawBookWriter = drawBookWriter
    }
}