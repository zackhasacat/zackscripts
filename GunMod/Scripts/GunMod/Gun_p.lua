local ui = require("openmw.ui")
local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local cam = require("openmw.interfaces").Camera
local core = require("openmw.core")
local self = require("openmw.self")
local nearby = require("openmw.nearby")
local types = require("openmw.types")
local Camera = require("openmw.camera")
local camera = require("openmw.camera")
local input = require("openmw.input")
local async = require("openmw.async")
local storage = require("openmw.storage")
local editMode = false
local keys = input.KEY
local gunmode = false
local ctrl = input.CONTROLLER_BUTTON
local thirdGunHeld = false
local ammoReturn = 0
local markData = {}
local lastPosData = nil
local menuMode = false
local markCount = 1
local disarmSTATE = {waitToReturn = 1, justRemoved = 2, normal = 3}
local recallWindow = nil -- Define a comparison function to compare the 'numVal' property
local bindingsWin = nil
local hitUi = ui.texture({ path = "images/scop.png" })
local scopeElement = nil
local function onLoad()

end
local function isThirdPerson()
if camera.getMode() == camera.MODE.ThirdPerson or camera.getMode() == camera.MODE.Preview then
    return true
else
    return false
end
end
local function imageContent(resource, size)
    return {
        type = ui.TYPE.Image,
        props = {
            resource = resource,
            size = util.vector2(ui.screenSize().x, ui.screenSize().y)
            -- relativeSize = util.vector2(1,1)
        }
    }
end
local function drawUI()
    if scopeElement then
        scopeElement:destroy()
    end

    scopeElement = ui.create {
        layer = "HUD",
        template = I.MWUI.templates.box,
        props = {
            -- relativePosition = v2(0.65, 0.8),
            --  anchor = v2(-1, -2),
            anchor = util.vector2(0.5, 0.5),
            relativePosition = v2(0.5, 0.51),
            arrange = ui.ALIGNMENT.Center,
            align = ui.ALIGNMENT.Center,
        },
        content = ui.content({
            imageContent(hitUi) })
    }
end
local heldWeapon = nil
local desiredFov = 1
local gunFov = 0.3
local function enterGunMode()
    drawUI()
    input.setControlSwitch(input.CONTROL_SWITCH.ViewMode, false)
    local equip = types.Actor.getEquipment(self)
    heldWeapon = equip[types.Actor.EQUIPMENT_SLOT.CarriedRight]
    equip[types.Actor.EQUIPMENT_SLOT.CarriedRight] = nil
    types.Actor.setEquipment(self, equip)
    types.Actor.setStance(self, types.Actor.STANCE.Nothing)
    desiredFov = gunFov
end
local function exitGunMode()
    if heldWeapon then
        local equip = types.Actor.getEquipment(self)
        equip[types.Actor.EQUIPMENT_SLOT.CarriedRight] = heldWeapon
        types.Actor.setStance(self, types.Actor.STANCE.Weapon)
        types.Actor.setEquipment(self, equip)
        heldWeapon = nil
    end
    input.setControlSwitch(input.CONTROL_SWITCH.ViewMode, true)
    desiredFov = 1
    if scopeElement then
        scopeElement:destroy()
        scopeElement = nil
    end
end
local LmbWasPressed = false
local RmbWasPressed = false
local function anglesToV(pitch, yaw)
    local xzLen = math.cos(pitch)
    return util.vector3(
        xzLen * math.sin(yaw), -- x
        xzLen * math.cos(yaw), -- y
        math.sin(pitch)        -- z
    )
end
local function getCameraDirData()
    local pos = Camera.getPosition()
    local pitch, yaw

    pitch = -(Camera.getPitch() + Camera.getExtraPitch())
    yaw = (Camera.getYaw() + Camera.getExtraYaw())

    return pos, anglesToV(pitch, yaw)
end
local function startsWith(str, prefix)
    return string.sub(str, 1, string.len(prefix)) == prefix
end

local function getPosInCrosshairs(mdist)
    local pos, v = getCameraDirData()

    local dist = 500
    if (mdist ~= nil) then
        dist = mdist
    end

    return pos + v * dist
end
local function isObjectInTable(obj, table)
    for i, value in ipairs(table) do
        if value == obj then
            return true
        end
    end
    return false
end
local useRenderingRay = false
local function getObjInCrosshairs(ignoreOb, mdist, force)
    --ignoreOb is the object we are ignoring
    --mdist is the maximum distance we will search for
    if ignoreOb and type(ignoreOb) ~= "table" then
        ignoreOb = { ignoreOb }
    end
    local pos, v = getCameraDirData() --this function gets the position of the camera, and the angle of the camera

    local dist = 500
    if (mdist ~= nil) then
        dist = mdist
    end

    if (ignoreOb and not force) then --current functionality, have to do normal cast ray. This isn't great for placing items, since it will hit the hitbox of a bookshelf, not the shelf.
        return nearby.castRay(pos, pos + v * dist, { ignore = ignoreOb[1]

        })
    else
        if (ignoreOb) then
            if useRenderingRay then
                local ret = nearby.castRenderingRay(getPosInCrosshairs(20), pos + v * dist)
                local hpos = ret.hitPos
                if (ret.hitPos == nil) then
                    hpos = pos + v * dist
                end
                local previousXdist = I.ZackUtilsAA.distanceBetweenPos(getPosInCrosshairs(20), hpos)
                while true do
                    local res = nearby.castRenderingRay(pos, pos + v * dist)
                    if not res.hit or not isObjectInTable(res.hitObject, ignoreOb) then
                        return res
                    end
                    step = 128
                    for i = 1, 4 do
                        local backwardRay = nearby.castRenderingRay(pos + v * step, pos)
                        if not backwardRay.hit or isObjectInTable(backwardRay.hitObject, ignoreOb) then
                            break -- no other objects between `pos` and `pos + step * v`
                        else
                            step = step / 2
                        end
                    end
                    pos = pos + v * step
                    dist = dist - step
                end
                return ret
            else
                -- local ret = nearby.castRay(getPosInCrosshairs(20), pos + v * dist,{ignore = ignoreOb})
                return nearby.castRay(pos, pos + v * dist, { ignore = ignoreOb[1]

                })
            end
        end
        return nearby.castRenderingRay(pos, pos + v * dist)
    end
end
local fovIncrement = 0.08
local hitTime = 0
local weaponNormal = "zhac_gun_svd1"
local weaponFlash = "zhac_gun_svd2"
local frameWait = 0
local dropStage = 0
local function getPositionBehind(pos, rot, distance, direction)
    local currentRotation = -rot
    local angleOffset = 0

    if direction == "north" then
        angleOffset = math.rad(90)
    elseif direction == "south" then
        angleOffset = math.rad(-90)
    elseif direction == "east" then
        angleOffset = 0
    elseif direction == "west" then
        angleOffset = math.rad(180)
    else
        error("Invalid direction. Please specify 'north', 'south', 'east', or 'west'.")
    end

    currentRotation = currentRotation - angleOffset
    local obj_x_offset = distance * math.cos(currentRotation)
    local obj_y_offset = distance * math.sin(currentRotation)
    local obj_x_position = pos.x + obj_x_offset
    local obj_y_position = pos.y + obj_y_offset
    return util.vector3(obj_x_position, obj_y_position, pos.z)
end
local function getViewpoint(pos, object)
    local offset = 0
    if object.type ~= types.Player then
        offset = object:getBoundingBox().halfSize.z
    else
        offset = (object:getBoundingBox().halfSize.z)
    end
    return util.vector3(pos.x, pos.y, pos.z + offset)
end
local function shootGun()
    core.sendGlobalEvent("playSoundInScr", 1)
    local cameraMode = camera.getMode()
    if cameraMode == camera.MODE.FirstPerson then
       -- if scopeElement then
            local hit = getObjInCrosshairs(self, 1000000)
            local hitPos = hit.hitPos
            hit = hit.hitObject
            if hit then
                if hit.type == types.NPC or hit.type == types.Creature then
                    hit:sendEvent("killActor")
                end
            end
            core.sendGlobalEvent("killActor", hitPos)
       -- else

        --end
    elseif (isThirdPerson()) and not scopeElement then
        local target = getPositionBehind(self.position, self.rotation:getAnglesZYX(), 100000, "south")
        local viewPoint = getPositionBehind(self.position, self.rotation:getAnglesZYX(), 10, "south")
        local rayCast = nearby.castRay(getViewpoint(viewPoint, self),target,{ignore = self})
        
        local hitPos = rayCast.hitPos
        local hit = rayCast.hitObject
        if hit then
            if hit.type == types.NPC or hit.type == types.Creature then
                hit:sendEvent("killActor")
            end
        end
        core.sendGlobalEvent("killActor", hitPos)
    end
end
local heldAmmo = nil
local disarmWait = 0
local function removeAmmo()
   local equip = types.Actor.getEquipment(self)
   if equip[types.Actor.EQUIPMENT_SLOT.Ammunition]  ~= nil then
            heldAmmo = equip[types.Actor.EQUIPMENT_SLOT.Ammunition] 
   end
          equip[types.Actor.EQUIPMENT_SLOT.Ammunition] = nil

            types.Actor.setEquipment(self, equip)
end
local function returnAmmo()
   local equip = types.Actor.getEquipment(self)
          equip[types.Actor.EQUIPMENT_SLOT.Ammunition] = heldAmmo
        

            types.Actor.setEquipment(self, equip)
end
local function onUpdate(dt)
    if dt == 0 then
        return
    end
    if ammoReturn > 0 and dropStage == disarmSTATE.waitToReturn then
        ammoReturn = ammoReturn - 1
        if ammoReturn == 0 then
            print("Putting Ammo Back")
            returnAmmo()
            dropStage = 0
        end
    end

    local RmbPress = input.isMouseButtonPressed(3)
    local LmbPress = input.isMouseButtonPressed(1)
    local stance = types.Actor.getStance(self)

    if RmbPress and not scopeElement and gunmode and camera.getMode() == camera.MODE.FirstPerson then
        --enterGunMode()
    elseif not RmbPress and scopeElement then
        exitGunMode()
        return
    end
    if LmbPress and hitTime <= 0 and scopeElement then

        shootGun()
        hitTime = 0.1
    elseif LmbPress and hitTime > 0 then
        hitTime = hitTime - dt
    elseif LmbPress and not LmbWasPressed and stance == types.Actor.STANCE.Weapon and not scopeElement   then
        local equip = types.Actor.getEquipment(self)
        if equip[types.Actor.EQUIPMENT_SLOT.CarriedRight] and equip[types.Actor.EQUIPMENT_SLOT.CarriedRight].recordId == weaponNormal then
            heldWeapon = equip[types.Actor.EQUIPMENT_SLOT.CarriedRight]
            local tempWeapon = nil
            for index, value in ipairs(types.Actor.inventory(self):getAll(types.Weapon)) do
                if value.recordId == weaponFlash then
                    equip[types.Actor.EQUIPMENT_SLOT.CarriedRight] = value
                    break
                end
            end
            core.sendGlobalEvent("playSoundInScr", 1)
            print("Swap")
            frameWait = 10
            shootGun()
            types.Actor.setEquipment(self, equip)
        else
            print("Error")
        end
    elseif RmbPress and not RmbWasPressed and stance == types.Actor.STANCE.Weapon and not scopeElement  then
        thirdGunHeld = true
    elseif( thirdGunHeld == true and RmbPress == true ) then
        self.controls.use = 1
    elseif dropStage == disarmSTATE.justRemoved then
        self.controls.use = 1
        if dropStage == disarmSTATE.justRemoved then
            if disarmWait > 0 then
                disarmWait = disarmWait - 1
                if disarmWait == 0 then
                    print("Frame wait over")
                    disarmWait = 20
                    dropStage = disarmSTATE.waitToReturn
                end
            end
        end
    elseif thirdGunHeld and not RmbPress and RmbWasPressed then
        ammoReturn = 20
        print("Fire?")
        removeAmmo()
        dropStage = disarmSTATE.justRemoved
        disarmWait = 30
        self.controls.use = 1
        thirdGunHeld = false

    else
        self.controls.use = 0
    end
    if dropStage > 0 and dropStage == disarmSTATE.waitToReturn then
        
    end
    if heldWeapon and not scopeElement and frameWait == 0 then
        local equip = types.Actor.getEquipment(self)
        equip[types.Actor.EQUIPMENT_SLOT.CarriedRight] = heldWeapon
        types.Actor.setEquipment(self, equip)
        heldWeapon = nil
    elseif frameWait > 0 then
        frameWait = frameWait - 1
    end
    local fov = camera.getFieldOfView()
    if fov > desiredFov then
        if fov - fovIncrement < desiredFov then
            camera.setFieldOfView(desiredFov)
        else
            camera.setFieldOfView(fov - fovIncrement)
        end
    elseif fov < desiredFov then
        if fov + fovIncrement > desiredFov then
            camera.setFieldOfView(desiredFov)
        else
            camera.setFieldOfView(fov + fovIncrement)
        end
    end
    LmbWasPressed = LmbPress
    RmbWasPressed = RmbPress
end
local function enterGunControls()

   -- input.setControlSwitch(input.CONTROL_SWITCH.Fighting, false)
 --   input.setControlSwitch(input.CONTROL_SWITCH.Magic, false)
    --  if(controllerMode == false) then
    gunmode = true
    input.setControlSwitch(input.CONTROL_SWITCH.Controls, false)
    --  end
   -- camera.setMode(camera.MODE.FirstPerson)
   -- types.Actor.setStance(self, types.Actor.STANCE.Nothing)
    I.ControlsGun.overrideMovementControls(false)
    I.ControlsGun.takeControlOfActor(self, true)
    I.Controls.overrideUiControls(true)
    I.Controls.overrideMovementControls(true)
    print("Enter gun controls")
end
local function exitGunControls()
 --   input.setControlSwitch(input.CONTROL_SWITCH.Fighting, true)
  --  input.setControlSwitch(input.CONTROL_SWITCH.Magic, true)
    --  if(controllerMode == false) then
    input.setControlSwitch(input.CONTROL_SWITCH.Controls, true)
    gunmode = false
    --  end
 --   camera.setMode(camera.MODE.FirstPerson)
--    types.Actor.setStance(self, types.Actor.STANCE.Nothing)
    I.ControlsGun.overrideMovementControls(true)
    I.ControlsGun.takeControlOfActor(self, true)
    I.Controls.overrideUiControls(false)
    I.Controls.overrideMovementControls(false)
    print("exit gun controls")
end
local function onInputAction(id)
    local zoomIncrement = fovIncrement / 3

    if id == input.ACTION.ZoomOut and not scopeElement and gunmode and camera.getMode() == camera.MODE.FirstPerson then
        enterGunMode()
    elseif id == input.ACTION.ZoomIn and scopeElement and desiredFov - zoomIncrement > 0 then
        desiredFov = desiredFov - zoomIncrement
    elseif id == input.ACTION.ZoomOut and scopeElement then
        desiredFov = desiredFov + zoomIncrement
    elseif id == input.ACTION.ToggleWeapon then
        local equip = types.Actor.getEquipment(self)
        local stance = types.Actor.getStance(self)
        if equip[types.Actor.EQUIPMENT_SLOT.CarriedRight] and equip[types.Actor.EQUIPMENT_SLOT.CarriedRight].recordId == weaponNormal then
            if stance == types.Actor.STANCE.Weapon then
         
                print("Put Away Weapon")
                exitGunControls()
            else

                print("Draw Weapon")
                enterGunControls()

                
            end
        end
    end
end
local function DebugLoaded()
        I.DebugMode.addOutCommand("givegun",function() 
            core.sendGlobalEvent("addItemCommand", {
                id = weaponNormal,
                count = 1,
                target = self
            })
            core.sendGlobalEvent("addItemCommand", {
                id = weaponFlash,
                count = 1,
                target = self
            })
            core.sendGlobalEvent("addItemCommand", {
                id = "TACTICAL_BULLET",
                count = 100,
                target = self
            })
            return "Added gun"
        end
        )
end
return {
    interfaceName = "GunMod",
    interface = {
        version = 1,
        drawUI = drawUI,
        enterGunMode = enterGunMode,
        exitGunMode = exitGunMode,
        removeAmmo = removeAmmo,
    },
    eventHandlers = {
        openMarkMenu = openMarkMenu,
        saveMarkLoc = saveMarkLoc,
        DebugLoaded = DebugLoaded,
    },
    engineHandlers = {
        onControllerButtonPress = onControllerButtonPress,
        onKeyPress = onKeyPress,
        onFrame = onUpdate,
        onSave = onSave,
        onLoad = onLoad,
        onInputAction = onInputAction,
    }
}
