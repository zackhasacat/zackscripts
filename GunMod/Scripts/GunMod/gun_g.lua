local core = require("openmw.core")
local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local storage = require("openmw.storage")
local AlembicId = nil
local CalcinatorId = nil
local MortarPestleId = nil
local RetortId = nil

local function playSoundInScr(id)
    world.mwscript.getGlobalScript("ZHAC_GunMod_SoundScript").variables.soundplay = id
         
end
local function killActor(pos)
    local newObject = world.createObject("zhac_explodespell_act"):teleport(world.players[1].cell,pos)
end
return {
    interfaceName  = "GunMod",
    interface      = {
        version = 1,
        
    },
    engineHandlers = {
        onUpdate = onUpdate
    },
    eventHandlers  = {
        playSoundInScr = playSoundInScr,
        killActor = killActor,
    }
}
