--Here, we monitor ai states. If the NPC is attacking the player, if they are following the player or not.

local NPCStates = { normalNPC = 1, followingPlayer = 2, fightingPlayer = 3, dead = 4 }

local nearby = require("openmw.nearby")
local self = require("openmw.self")
local core = require("openmw.core")
local types = require("openmw.types")
local util = require("openmw.util")
local I = require("openmw.interfaces")
local isDead = false
local function killActor()
    if types.Actor.stats.dynamic.health(self).current > 0 and not isDead then
    types.Actor.stats.dynamic.health(self).current = 0 
    isDead = true
    core.sendGlobalEvent("addCrimeLevel",1000)
    end
end
return {
    interfaceName = "GunMod",
    interface = { isFightingPlayer = isFightingPlayer },
    engineHandlers = { onUpdate = onUpdate, onActive = onActive, onSave = onSave, onLoad = onLoad },
    eventHandlers = { killActor = killActor, reportPackages = reportPackages, }
}
