local types    = require("openmw.types")
local acti     = require("openmw.interfaces").Activation
local core     = require("openmw.core")
local world    = require("openmw.world")
local util     = require("openmw.util")
local I        = require("openmw.interfaces")
local storage  = require("openmw.storage")


local function stackBooks(cell,position)
    local bookList = cell:getAll(types.Book)
    local currPos = position
    for index, book in ipairs(bookList) do
      local thickness = book:getBoundingBox().halfSize.z * 2
        
      local heightOffset = book:getBoundingBox().halfSize.x -- +(book:getBoundingBox().halfSize.x * 1)
    --  local frontOffset = book:getBoundingBox().center.y + book:getBoundingBox().halfSize.y

      currPos = currPos + util.vector3(0, thickness, 0)
      
      local usePos = currPos - util.vector3(0, 0, heightOffset)
      
      book:teleport(cell, usePos, util.transform.rotateX(math.rad(-90)))
    end
end

return{
    interfaceName = "HestaturBuddy",
    interface = {
        stackBooks = stackBooks,
    },
    eventHandlers = {
    }
}