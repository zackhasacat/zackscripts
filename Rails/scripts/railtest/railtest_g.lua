local world = require("openmw.world")
local util = require("openmw.util")
local core = require("openmw.core")
local railway = {
    "0xe00000a", "0xe00000b", "0xe000008", "0xe0000ad", "0xe00000c", "0xe000009"
}
local railwayObjects = {}
local currentPoint = 0
local points = {}
local function getRailwayObjects()
    railwayObjects = {}
    for i, x in ipairs(railway) do
        table.insert(railwayObjects, world.getObjectByFormId("FormId:" .. x))
    end
end
local function getRailLength(obj)

    return 512 -- obj:getBoundingBox().halfSize.x * 2

end
local function getRailZPos(obj)
    return obj.position.z + (obj:getBoundingBox().halfSize.z)
end

local function getRailAngle(obj)
    local z, y, x = obj.rotation:getAnglesZYX()
    return z
end
local function getCurvedRailPath(obj, totalPoints, xoffsetX, xoffsetY)
    local centerPos = obj.position -- Center position of the rail (vector3)
    local angle = -getRailAngle(obj) -- Rail rotation in radians (inverted if necessary)
    local zPos = getRailZPos(obj) -- Fixed vertical position for the rail

    -- Get the bounding box's half size
    local halfSize = obj:getBoundingBox().halfSize
    local halfX = halfSize.x
    local halfY = halfSize.y

    -- Define the start and end points
    local startPoint = {x = 0, y = halfY} -- Center of the top side
    local endPoint = {x = halfX, y = 0} -- Center of the right side

    -- Define the control point (adjust for curve height)
    local controlPoint = {x = halfX / 2, y = halfY / 2} -- Midway between start and end

    -- Rotate the points by the rail's rotation angle
    local function rotatePoint(point)
        local rotatedX = point.x * math.cos(angle) - point.y * math.sin(angle)
        local rotatedY = point.x * math.sin(angle) + point.y * math.cos(angle)
        return {x = rotatedX + centerPos.x, y = rotatedY + centerPos.y}
    end

    -- Transform all points into world space
    startPoint = rotatePoint(startPoint)
    endPoint = rotatePoint(endPoint)
    controlPoint = rotatePoint(controlPoint)

    -- Generate the Bézier curve path
    local path = {}
    for i = 0, totalPoints - 1 do
        local t = i / (totalPoints - 1)

        -- Quadratic Bézier formula
        local x =
            (1 - t) ^ 2 * startPoint.x + 2 * (1 - t) * t * controlPoint.x + t ^
                2 * endPoint.x
        local y =
            (1 - t) ^ 2 * startPoint.y + 2 * (1 - t) * t * controlPoint.y + t ^
                2 * endPoint.y

        -- Create the position (use fixed z position)
        local position = util.vector3(x, y, zPos)
        table.insert(path, position)
    end

    return path
end
local function reverseTable(tbl)
    local reversed = {}
    for i = #tbl, 1, -1 do table.insert(reversed, tbl[i]) end
    return reversed
end
local function getRailPositions(obj, numPoints)
    local length = getRailLength(obj) -- Length of the rail
    local centerPos = obj.position -- Center position (vector3)
    local angle = -getRailAngle(obj) -- Invert the angle to correct the direction

    -- Calculate half the length of the rail
    local halfLength = length / 2

    -- Precompute cosine and sine of the inverted angle for rotation
    local cosAngle = math.cos(angle)
    local sinAngle = math.sin(angle)

    -- Calculate the starting point (world coordinates)
    local startX = centerPos.x - halfLength * cosAngle
    local startY = centerPos.y - halfLength * sinAngle

    -- Calculate the step size along the rail
    local step = length / (numPoints - 1)

    -- Table to hold positions
    local positions = {}

    -- Generate positions along the rail
    for i = 0, numPoints - 1 do
        local distance = i * step -- Distance from the start
        -- Offset in local coordinates
        local offsetX = distance * cosAngle
        local offsetY = distance * sinAngle

        -- Retrieve the vertical z position for this point
        local zPos = getRailZPos(obj)

        -- New position in world coordinates
        local newPosition = util.vector3(startX + offsetX, startY + offsetY,
                                         zPos -- Use the dynamically retrieved z position
        )
        table.insert(positions, newPosition)
    end

    return reverseTable(positions)
end
local lastPoint
local controlOffset = -200
local wasCurve = false
local function getQuarterCircleRail(startPoint, endPoint, numPoints,startRotation, targetRotationDelta)
    local midPoint = {
        x = (startPoint.x + endPoint.x) / 2,
        y = (startPoint.y + endPoint.y) / 2,
        z = (startPoint.z + endPoint.z) / 2,
    }

    -- Offset the midpoint to create a control point for the curve
    local dx = endPoint.x - startPoint.x
    local dy = endPoint.y - startPoint.y
    local length = math.sqrt(dx * dx + dy * dy)

    -- Normalized perpendicular vector
    local offsetX = -dy / length * controlOffset
    local offsetY = dx / length * controlOffset

    local controlPoint = {
        x = midPoint.x + offsetX,
        y = midPoint.y + offsetY,
        z = midPoint.z, -- Optional: Add a Z offset if needed
    }

    -- Table to hold positions with rotations
    local positionsWithRotations = {}

    -- Generate points along the curve using a quadratic Bézier formula
    for i = 0, numPoints - 1 do
        local t = i / (numPoints - 1)

        -- Quadratic Bézier interpolation for position
        local invT = 1 - t
        local x = invT^2 * startPoint.x + 2 * invT * t * controlPoint.x + t^2 * endPoint.x
        local y = invT^2 * startPoint.y + 2 * invT * t * controlPoint.y + t^2 * endPoint.y
        local z = invT^2 * startPoint.z + 2 * invT * t * controlPoint.z + t^2 * endPoint.z

        -- Gradually interpolate rotation
        local yaw = startRotation + t * targetRotationDelta -- Linear interpolation for yaw
        -- Add pitch and roll if needed (set to 0 here for simplicity)

        -- Add position and rotation to the table
        table.insert(positionsWithRotations, {
            position = util.vector3(x,y,z),
            rotation = yaw
        })
    end

    return positionsWithRotations

end
local function createRotation(x, y, z)
    local rotate = util.transform.rotateZ(z)
    local rotatex = util.transform.rotateX(x)
    local rotatey = util.transform.rotateY(y)
    rotate = rotate:__mul(rotatex)
    rotate = rotate:__mul(rotatey)
    return rotate
end
local lastRotation
local function railwayTest(xoffsetX, xoffsetY)
    getRailwayObjects()
    points = {}
    currentPoint = 0
    for i, x in ipairs(railwayObjects[1].cell:getAll()) do
        if x.recordId == "ab_in_minehandcar" then x:remove() end
    end
    for i, x in ipairs(railwayObjects) do
        if x.recordId == "ab_in_minerail256" then
            local posList = getRailPositions(x, 40)
            for ii, pos in ipairs(posList) do
                local itemTest = world.createObject("ab_in_minehandcar")
                itemTest:setScale(2)
                local rotation = createRotation(0,0,x.rotation:getAnglesZYX() + math.rad(90))
                itemTest:teleport(x.cell, pos,rotation)
                table.insert(points, pos)
                if wasCurve then
                    local startPoint = pos
                    local endPoint = lastPoint

                    print(startPoint, endPoint)
                    local startRotation 
                    local posList = getQuarterCircleRail(startPoint, endPoint,
                                                         40,lastRotation:getAnglesZYX(),math.rad(90))
                    for ii, posData in ipairs(posList) do
                        local itemTest =
                            world.createObject("ab_in_minehandcar")
                        itemTest:setScale(2)
                        print(posData.rotation )
                        local rotation = createRotation(0,0,posData.rotation + math.rad(-90) )

                        itemTest:teleport(x.cell, posData.position,rotation)
                        table.insert(points, pos)
                    end
                    wasCurve = false
                end
                lastPoint = pos
                lastRotation = rotation
            end
        else
            wasCurve = true

        end
    end
end
local nextTime = 0
local function onUpdate()
    if #points > 0 and currentPoint < #points then
        if core.getRealTime() > nextTime then
            currentPoint = currentPoint + 1
            local point = points[currentPoint]
            if point then
                world.players[1]:teleport(world.players[1].cell, point)
            end
            nextTime = core.getRealTime() + 0.01
        end
    end
end
return {
    interfaceName = "Rail",
    interface = {
        getRailwayObjects = getRailwayObjects,
        railwayTest = railwayTest,
        getRailLength = getRailLength
    },
   -- engineHandlers = {onUpdate = onUpdate}
}
