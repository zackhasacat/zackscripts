local camera = require('openmw.camera')
local core = require('openmw.core')
local input = require('openmw.input')
local util = require('openmw.util')
local self = require('openmw.self')
local types = require('openmw.types')
local ui = require('openmw.ui')
local async = require('openmw.async')
local nearby = require('openmw.nearby')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')
local Door = types.Door
local TalkedToNPC = nil
local activeCellList = nil
local selectedCat = nil
local destDoor = nil
local function onFrame(dt)
    core.sendGlobalEvent("HFG_onFrame", dt)
end
local MenuLevels = { CatSelect = 1, DestSelect = 2 }

local menuOptions = {}
local selectedItem = 1
local menuLevel = 0
local catData = {
    house = { display = "A House", id = "house" },
    inn = { display = "An Inn", id = "inn" },
    shop = { display = "A Shop", id = "shop" },
    guild = { display = "A Guild", id = "guild" }
}
local function genMenu()
    local lines = {}
    for i = 1, 20, 1 do
        table.insert(lines, " ") --spacing
    end
    if menuLevel == MenuLevels.CatSelect then
    table.insert(lines, "Sure, I'd be happy to help you find your way. What kind of place are you trying to find?")
    elseif menuLevel == MenuLevels.DestSelect then
        table.insert(lines, "Okay, now which of these sites are you looking for?")
        
    end
    for i = 1, 8, 1 do
        table.insert(lines, " ") --spacing
    end
    for index, opt in ipairs(menuOptions) do
        if index == selectedItem then
            table.insert(lines, "    --" .. opt)
        else
            table.insert(lines, "     -" .. opt)
        end
    end
    return lines
end
local function drawMenu()
    for index, line in ipairs(genMenu()) do
        ui.showMessage(line)
    end
end
local function onKeyPress(k)
    if menuLevel == 0 then
        return
    end
    if k.code == input.KEY.UpArrow then
        if selectedItem > 1 then
            selectedItem = selectedItem - 1
            drawMenu()
        end
        if selectedItem > #menuOptions then
            selectedItem = #menuOptions
        else
            print(selectedItem,#menuOptions)
        end
    elseif k.code == input.KEY.DownArrow then
        
        if #menuOptions == 0 then
            error("No valid cells!")
        end
        if #menuOptions > selectedItem then
            selectedItem = selectedItem + 1
            drawMenu()
        end
    elseif k.code == input.KEY.Enter then
        if menuLevel == MenuLevels.CatSelect then
            selectedCat = menuOptions[selectedItem]
            menuLevel = MenuLevels.DestSelect
            selectedItem = 1
            menuOptions = {}
            for index, value in pairs(catData) do
                if selectedCat == value.display then
                    selectedCat = value
                end
            end
            for index, cellItem in ipairs(activeCellList) do
                if cellItem.cat == selectedCat.id then
                    table.insert(menuOptions,cellItem.displayName)
                end
            end
            if #menuOptions == 0 then
                error("No valid cells!")
            end
            drawMenu()
        elseif menuLevel == MenuLevels.DestSelect then
            core.sendGlobalEvent("setGoodbye")
            menuLevel = 0
            local destCellName = nil
            local activeCell = nil
            for index, value in ipairs(activeCellList) do
                if value.displayName == menuOptions[selectedItem] then
                    destCellName = value.cellName
                    activeCell = value
                end
            end
            for index, door in ipairs(nearby.doors) do
                if Door.isTeleport(door)  and Door.destCell(door).name == destCellName then
                    local destPoint = activeCell.exitPositions[1]--nearby.findRandomPointAroundCircle(door.position,150)
                    local  stat, nav = nearby.findPath(destPoint,self.position)
                    if stat == nearby.FIND_PATH_STATUS.PartialPath and true == false  then
                        for i = 1, 30, 1 do
                            local destPoint = nearby.findRandomPointAroundCircle(door.position,150)
                            stat, nav = nearby.findPath(destPoint,self.position)
                            if stat == nearby.FIND_PATH_STATUS.Success then
                                break
                            end
                        end
                    end
                    if stat == nearby.FIND_PATH_STATUS.Success or stat == nearby.FIND_PATH_STATUS.PartialPath then
                        destDoor = door
                        TalkedToNPC:sendEvent('StartAIPackage', {type='Escort', destPosition=destPoint,target = self})
                        break

                    else
                        print("no path to door!",door.id)
                    end
                end
            end
        end
    end
end
local function startsWith(str, prefix)
    local prefixStart, prefixEnd = string.find(prefix, "[,:]")
    if prefixStart then
        prefix = string.sub(prefix, 1, prefixStart - 1)
    end
    return string.sub(str, 1, string.len(prefix)) == prefix
end
local function findNearCells()
    local destCells = {}
    for index, door in ipairs(nearby.doors) do
        if Door.isTeleport(door) and door.cell.name ~= nil and door.cell.name ~= "" and startsWith(door.cell.name, self.cell.name) then
            destCells[Door.destCell(door).name] = Door.destCell(door).name
        end
    end
    return destCells
end
local function startDialogMode(data)
    TalkedToNPC = data.actor
    menuOptions = {}
    selectedItem = 1
    for index, value in pairs(catData) do
        table.insert(menuOptions, value.display)
    end
    menuLevel = MenuLevels.CatSelect

    drawMenu()
    core.sendGlobalEvent("processCellList", findNearCells())
end
local function cellListReturn(list)
    activeCellList = list
end
return {
    interfaceName = "DNAirship",
    interface = {
        version = 1
    },
    engineHandlers = {
        onFrame = onFrame,
        onKeyPress = onKeyPress,
    },
    eventHandlers = {
        startDialogMode = startDialogMode,
        cellListReturn = cellListReturn
    }
}
