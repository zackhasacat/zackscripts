local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local async = require("openmw.async")
local activatedNPC = nil
local function onUpdate(dt)

end
local function HFG_onFrame(dt)
    local scr = world.mwscript.getGlobalScript("zhac_hguards_main", world.players[1])
    local diaState = scr.variables.diastage
    if diaState == 1 then
        scr.variables.diastage = 0
        if activatedNPC == nil then
            error("No talked to actor")
        end
        world.players[1]:sendEvent("startDialogMode", { actor = activatedNPC })
        print("Talked to NPC")
    end
end

local function onActivate(object, actor)
    if object.type == types.NPC then
        activatedNPC = object
    end
end
local houseNames = {"house","manor","shack"}
local function containsHouseOrManor(str)
    str = string.lower(str)
    for index, value in ipairs(houseNames) do
        if string.find(str, value) then
            return true
        end
    end
    return false
end
local shopClasses = { "trader", "clothier", "pawnbroker" }
local function getTextAfterLastComma(str)
    local lastCommaIndex = string.find(str, "[^,]*$")
    if lastCommaIndex then
      return string.sub(str, lastCommaIndex + 1)
    else
      return str
    end
  end
  local Door = types.Door
local function processCellList(list)
    local ret = {}
    for index, value in pairs(list) do
        local cell = world.getCellByName(index)
        local cellCat = "other"
        local exitPos = {}
        for index, act in ipairs(cell:getAll(types.NPC)) do
            local class = types.NPC.record(act).class
            if class == "publican" then
                cellCat = "inn"
                break
            else
                for index, value in ipairs(shopClasses) do
                    if class == value then
                        cellCat = "shop"
                    end
                end
            end
        end
        for index, act in ipairs(cell:getAll(types.Door)) do
            if Door.isTeleport(act)  and Door.destCell(act).isExterior then
                table.insert(exitPos,Door.destPosition(act))
            end
        end
        if cellCat == "other" then
            if containsHouseOrManor(cell.name) then
                cellCat = "house"
            end
            if getTextAfterLastComma(cell.name) == "Guild of Mages" or getTextAfterLastComma(cell.name) == "Guild of Fighters"then
                cellCat = "guild"
            end
        end
        table.insert(ret,{cat = cellCat,cellName = cell.name,displayName = getTextAfterLastComma(cell.name),exitPositions = exitPos})
    end
    world.players[1]:sendEvent("cellListReturn",ret)
end
local function setGoodbye()

    local scr = world.mwscript.getGlobalScript("zhac_hguards_main", world.players[1])
     scr.variables.goodbyetime = 1
end
return {
    interfaceName  = "SlaveScript",
    interface      = {
        version = 1,
        CompShare = CompShare,
        makeSlave = makeSlave,
    },
    engineHandlers = {
        onActorActive = onActorActive,
        onPlayerAdded = onPlayerAdded,
        onActivate = onActivate,
        onUpdate = onUpdate,
    },
    eventHandlers  = {
        HFG_onFrame = HFG_onFrame,
        processCellList = processCellList,
        setGoodbye = setGoodbye,
    },
}
