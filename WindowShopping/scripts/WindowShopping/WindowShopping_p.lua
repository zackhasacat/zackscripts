local ambient = require('openmw.ambient')
return {
    interfaceName  = "WindowShopping",
    interface      = {
        version = 1,
    },
    engineHandlers = {
        
    },
    eventHandlers  = {
        WS_playSound = function(sound) 
            ambient.playSound(sound)
        end
    }
}
