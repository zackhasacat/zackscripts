local core = require("openmw.core")
local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local storage = require("openmw.storage")
local core_aux = require("openmw_aux.core")
local useClasses = { ["enchanter service"] = 1, clothier = 2, ["trader service"] = 1 }

local borrowedItems = {}
local borrowCell = nil
local isHoldingBorrowed = false

local activatedContainer = nil
local containerState = 0

local function getOwnerClass(object)
    if not object.owner.recordId then
        return nil
    else
        local class = types.NPC.record(object.owner.recordId).class
        return class
    end
end
local function getInventory(object)
    --Gets the inventory of an object, actor or container.
    if (object.type == types.NPC or object.type == types.Creature or object.type ==
            types.Player) then
        return types.Actor.inventory(object)
    elseif (object.type == types.Container) then
        return types.Container.content(object)
    end
end

local function itemActivate(object, actor)
    local class = getOwnerClass(object)
    if class and useClasses[class] then
        table.insert(borrowedItems,
            {
                itemId = object.id,
                recordId = object.recordId,
                originalCell = object.cell.name,
                originalPos = object.position,
                originalRot = object.rotation,
                count = object.count,
                owner = object.owner.recordId,
                value = object.type.records[object.recordId].value
            })
        isHoldingBorrowed = true
        borrowCell = object.cell.name
        actor:sendEvent("WS_playSound",core_aux.getPickupSound(object))
        object:moveInto(types.Actor.inventory(world.players[1]))
        return false
    else
        return true
    end
end
local borrowedItemsContainer = nil
local function itemIsEquipped(item, actor)
    --Checks if item record is equipped on the specified actor
    if (actor.type ~= types.NPC and actor.type ~= types.Creature and actor.type ~=
            types.Player) then
        print("invalid type")
        return false
    end
    for slot = 1, 17 do
        if (types.Actor.equipment(actor, slot)) then
            if (item.id == types.Actor.equipment(actor, slot).id) then
                return true
            end
        end
    end
    return false
end

local function getBorrowedItemData(id)
    for index, value in ipairs(borrowedItems) do
        if value.itemId == id then
            return value
        end
    end
end
local function openBorrowedItemsContainer(sourceCell, ownerID)
    containerState = 0
    if borrowedItemsContainer then return end

    borrowedItemsContainer = world.createObject("de_r_drawers_01_redwr1")
    borrowedItemsContainer:teleport(world.players[1].cell,world.players[1].position)
    for index, contObject in ipairs(sourceCell:getAll()) do
        local inv = getInventory(contObject)
        if inv then
            for index, item in ipairs(inv:getAll()) do
                if contObject.type == types.NPC and itemIsEquipped(item, contObject) then
                else
                    table.insert(borrowedItems,
                        {
                            itemId = item.id,
                            recordId = item.recordId,
                            originalCell = sourceCell.name,
                            originalContainer = contObject.id,
                            count = item.count,
                            owner = ownerID,
                            value = item.type.records[item.recordId].value
                        })
                    isHoldingBorrowed = true
                    borrowCell = contObject.cell.name
                    item:moveInto(inv)
                end
            end
        end
    end
    borrowedItemsContainer:activateBy(world.players[1])
end
local function returnItems()
    for index, value in ipairs(borrowedItems) do
        local spl = types.Actor.inventory(world.players[1]):find(value.recordId)
        if spl then
            if spl.count > value.count then
                spl = spl:split(value.count)
            end

            spl:teleport(value.originalCell, value.originalPos, value.originalRot)
            spl.owner.recordId = value.owner
        end
    end
    isHoldingBorrowed = false
    borrowedItems = {}
    borrowCell = nil
end
local function onSave()
    return { borrowedItems = borrowedItems, isHoldingBorrowed = isHoldingBorrowed, borrowCell = borrowCell }
end
local function onLoad(data)
    if data then
        borrowedItems = data.borrowedItems
        isHoldingBorrowed = data.isHoldingBorrowed
        borrowCell = data.borrowCell
    end
end
local function doorActivate(object, actor)
    if types.Door.destCell(object) and #borrowedItems > 0 then
        returnItems()
    end
end
local function containerActivate(object, actor)
    local class = getOwnerClass(object)
    if class and useClasses[class] then
        activatedContainer = object
        openBorrowedItemsContainer(object.cell, object.owner.recordId)
        return false
    else
        return true
    end
end
local function onItemActive(item)
    if not item.owner.recordId then
        for index, value in ipairs(borrowedItems) do
            if item.recordId == value.recordId then
                if item.count > value.count then
                    item = item:split(value.count)
                end

                item:teleport(value.originalCell, value.originalPos, value.originalRot)
                item.owner.recordId = value.owner
                table.remove(borrowedItems, index)
                if #borrowedItems == 0 then
                    isHoldingBorrowed = false
                end
                return
            end
        end
    end
end
local function getObjectInCellById(id, cell)
    for index, value in ipairs(cell:getAll()) do
        if value.type == types.Container or value.type == types.NPC then
            if value.id == id then
                return value
            end
        end
    end
end
local function onUpdate()
    if isHoldingBorrowed then
        if world.players[1].cell.name ~= borrowCell then
            returnItems()
        end
    end
    if activatedContainer and borrowedItemsContainer and containerState > 10 then
        print("Container closed")
        local sourceInv = getInventory(borrowedItemsContainer)
        if not sourceInv then return end
        for index, item in ipairs(sourceInv) do
            local itemData = getBorrowedItemData(item.id)
            local targetCont = getObjectInCellById(itemData.originalContainer, world.players[1].cell)
            item:moveInto(getInventory(targetCont))
        end
        activatedContainer = nil
        borrowedItemsContainer:remove()
        borrowedItemsContainer = nil
    elseif activatedContainer and borrowedItemsContainer then
        containerState = containerState + 1
    end
end
for index, type in pairs(types) do
    if type.baseType == types.Item then
        acti.addHandlerForType(type, itemActivate)
    end
end
acti.addHandlerForType(types.Door, doorActivate)
acti.addHandlerForType(types.Container, containerActivate)
return {
    interfaceName  = "WindowShopping",
    interface      = {
        version = 1,
        returnItems = returnItems,
    },
    engineHandlers = {
        onItemActive = onItemActive,
        onSave = onSave,
        onLoad = onLoad,
        onUpdate = onUpdate,
    },
    eventHandlers  = {
        returnItems = returnItems,
    }
}
