local ui, I = require("openmw.ui"), require("openmw.interfaces")
local v2, util, cam, core, self, nearby, types, Camera, input, storage =
    require("openmw.util").vector2, require("openmw.util"),
    require("openmw.interfaces").Camera, require("openmw.core"),
    require("openmw.self"), require("openmw.nearby"),
    require("openmw.types"), require("openmw.camera"),
    require("openmw.input"), require("openmw.storage")

    local ambient = require('openmw.ambient')
    local async = require('openmw.async')
local sceneGraph = require("openmw.scenegraph")
local function anglesToV(pitch, yaw)
    local xzLen = math.cos(pitch)
    return util.vector3(xzLen * math.sin(yaw), -- x
        xzLen * math.cos(yaw),                 -- y
        math.sin(pitch)                        -- z
    )
end
local playerSettings = storage.playerSection("SettingsSwitchableScriptures")
local bookData = {}
local openByDefault = {}
local lastUpdateCell
local function getCameraDirData(sourcePos)
    local pos = sourcePos
    local pitch, yaw

    pitch = -(Camera.getPitch() + Camera.getExtraPitch())
    yaw = (Camera.getYaw() + Camera.getExtraYaw())

    return pos, anglesToV(pitch, yaw)
end
local function getObjInCrosshairs(ignoreOb, mdist, alwaysPost, sourcePos) --Gets the object the player is looking at. Does not work in third person.
    if not sourcePos then
        sourcePos = Camera.getPosition()
    end
    local pos, v = getCameraDirData(sourcePos)

    local dist = 500
    if (mdist ~= nil) then dist = mdist end

    if (ignoreOb) then
        local ret = nearby.castRay(pos, pos + v * dist, { ignore = ignoreOb })
        return ret
    else
        local ret = nearby.castRenderingRay(pos, pos + v * dist)
        local destPos = (pos + v * dist)
        if (alwaysPost and ret.hitPos == nil) then
            return { hitPos = destPos }
        end
        return ret.hitObject, destPos
    end
end
local function isBookSupported(book)
    if book.type == types.Book then return true end
    return false --will go a bit farther later
end
local function setBookState(book, state, sct)
    if sct then
        bookData[book.id] = state
        ambient.playSound("book open")
        sct.activeIndex = state
        return
    end
    --ui.showMessage("Toggling book " ..book.recordId)
    for _, socket in ipairs(book.renderingNode.sockets) do
        if socket.type == sceneGraph.NODE_TYPE.SwitchNode and socket.name == 'BookSwitch' then
            
            bookData[book.id] = state
            socket.activeIndex = state
            return
        end
    end
    --sceneGraph.nodeTest(book)
end
local function toggleBookReferenceClosed(book)
    --ui.showMessage("Toggling book " ..book.recordId)
    for _, socket in ipairs(book.renderingNode.sockets) do
        if socket.type == sceneGraph.NODE_TYPE.SwitchNode and socket.name == 'BookSwitch' then
            setBookState(book, socket.activeIndex == 1 and 2 or 1, socket)
            return
        end
    end
    --sceneGraph.nodeTest(book)
end
local function bookUpdate(book)
    if bookData[book.id] then
        setBookState(book, bookData[book.id])
    end
end
local function updateNearbyBooks()
    if lastUpdateCell == self.cell then
        return
    end
    for index, value in ipairs(nearby.items) do
        if value.type == types.Book then
            bookUpdate(value)
        end
    end
    lastUpdateCell = self.cell
end
local function onKeyPress(key)
    if key.symbol:lower() == playerSettings:get("keyPress"):lower() then
       -- local target = getObjInCrosshairs(nil, 500, false)
        if target and isBookSupported(target) then
        --    toggleBookReferenceClosed(target)
        end
    end
end

input.registerTrigger {
    key = "SwitchScript",
    l10n = 'OMWControls',
    name = "Switch Scriptures",
    description ="When you do this, it switches the book.",
}
input.registerTriggerHandler('SwitchScript', async:callback(function()
    local target = getObjInCrosshairs(nil, 500, false)
    if target and isBookSupported(target) then
        toggleBookReferenceClosed(target)
    end
end))

return {
    engineHandlers = {
        onKeyPress = onKeyPress,

        onSave = function()
            return { bookData = bookData }
        end,
        onLoad = function(data)
            if not data then return end
            if not data.bookData then return end
            bookData = data.bookData
            updateNearbyBooks()
        end,
    },
    eventHandlers = {BookUpdate = bookUpdate}
}
