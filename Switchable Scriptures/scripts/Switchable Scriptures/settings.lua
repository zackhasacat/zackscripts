local I = require("openmw.interfaces")
local input = require("openmw.input")
local async = require("openmw.async")

local status, result = pcall(function()
  require("openmw.scenegraph")
end)

if not status then
I.Settings.registerPage {
    key = "SettingsSwitchableScriptures",
    l10n = "SettingsSwitchableScriptures",
    name = "Switchable Scriptures - ERROR",
    description = "The Scenegraph library wasn't found. This prevents the mod from working. You'll need to download a build from gitlab.com, linked below."
}
I.Settings.registerGroup {
     key = "SettingsSwitchableScriptures",
     page = "SettingsSwitchableScriptures",
     l10n = "SettingsSwitchableScriptures",
     name = "Switchable Scriptures",
     description = "",
     permanentStorage = true,
     settings = {
        {
             key = "downloadLink",
             renderer = "textLine",
             name = "Copy this Link",
             description = "Copy this address into your address bar to download the right build of OpenMW",
             default = "gitlab.com/OpenMW/openmw/-/merge_requests/3155"
        },
     }
}
return
end
input.bindAction('Activate', async:callback(function(dt, use, sneak, run)
     -- while sneaking, only activate things while holding the run binding
     return use and (run or not sneak)
 end), { 'Sneak', 'Run' })

I.Settings.registerPage {
     key = "SettingsSwitchableScriptures",
     l10n = "SettingsSwitchableScriptures",
     name = "Switchable Scriptures",
     description = ""
}
I.Settings.registerGroup {
     key = "SettingsSwitchableScriptures",
     page = "SettingsSwitchableScriptures",
     l10n = "SettingsSwitchableScriptures",
     name = "Switchable Scriptures",
     description = "",
     permanentStorage = true,
     settings = {
        {
             key = "keyPress",
             renderer = "textLine",
             name = "Book Switch Key",
             description = "Press this key while looking at a book to open or close it.",
             default = "b"
        },
        {
             key = "testThing",
             renderer = "inputBinding",
             name = "Book Switch Key",
             description = "Press this key while looking at a book to open or close it.",
               default = "X",
            argument = {
               type = "keyboardPress",
               key = "SwitchScript"
           },
        },
     }
}
