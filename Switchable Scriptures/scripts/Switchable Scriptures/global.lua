local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local async = require("openmw.async")


local function onItemActive(item)
if item.type == types.Book then
world.players[1]:sendEvent("BookUpdate",item)
end
end

return{engineHandlers = {onItemActive = onItemActive}}