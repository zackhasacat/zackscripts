local nearby = require("openmw.nearby")
local I = require("openmw.interfaces")
local self = require("openmw.self")
local storage = require("openmw.storage")

I.Settings.registerPage {
    key = "NoRest",
    l10n = "NoRest",
    name = "No Rest",
    description = "Disables resting without a bed. Also disables waiting, unless it is enabled later."
}

I.Settings.registerGroup {
    key = "SettingsNoRest",
    page = "NoRest",
    l10n = "NoRest",
    name = "No Rest",
    description = "",
    permanentStorage = true,
    settings = {
        {
            key = "enableRestMod",
            renderer = "checkbox",
            name = "Enable Mod",
            description =
            "If enabled, the mod will stop you from sleeping away from a bed.",
            default = true
        },
        {
            key = "AllowWaiting",
            renderer = "checkbox",
            name = "Allow Waiting",
            description =
            "If enabled, you'll be able to wait in public areas.",
            default = false
        },
        {
            key = "AllowSleepingInteriors",
            renderer = "checkbox",
            name = "Allow Sleeping Indoors",
            description =
            "If enabled, you'll be able to sleep in indoor areas without a bed.",
            default = false
        }

    }
}

local modSettings = storage.playerSection("SettingsNoRest")
local function distanceBetweenPos(vector1, vector2)
    --Quick way to find out the distance between two vectors.
    --Very similar to getdistance in mwscript
    local dx = vector2.x - vector1.x
    local dy = vector2.y - vector1.y
    local dz = vector2.z - vector1.z
    return math.sqrt(dx * dx + dy * dy + dz * dz)
end
local function allowedToSleep()
    if not modSettings:get("enableRestMod") then
        return true
    end
    for index, acti in ipairs(nearby.activators) do
        local record = acti.type.record(acti)
        local scr = record.mwscript
        local dist = distanceBetweenPos(self.position, acti.position)
        local isBed = record.name == "Bed" or scr == "bed_standard"
        if isBed and dist < 250 then
            return true
        end
    end
    if not self.cell.isExterior and not self.cell:hasTag("QuasiExterior") and modSettings:get("AllowSleepingInteriors") == true then
        return true --allowed to sleep indoors
    end
    if self.cell:hasTag("NoSleep") and modSettings:get("AllowWaiting") then
        return true --allowed to wait
    end

    return false
end
return {
    eventHandlers = {

        UiModeChanged = function(data)
            if data.newMode == "Rest" then
                if not allowedToSleep() then
                    I.UI.setMode()
                end
            end
        end
    }
}
