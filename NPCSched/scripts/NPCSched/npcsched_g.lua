local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local core = require("openmw.core")
local util = require("openmw.util")
local storage = require("openmw.storage")
local timetool = require("scripts.NPCSched.time")
local knownNPCs = {}
local lockPreset = {
    normal = { lock = 21, unlock = 7 }
}
local lockData = {

}
local function onActorActive(actor)
    if knownNPCs[actor.id] ~= nil then
        --already have data about this actor, probably don't need to do anything
    else

    end
end
local function onLoad(data)
    if not data then return end

    knownNPCs = data.knownNPCs
end
local function trackCells()
    for index, cell in ipairs(world.cells) do
        if cell:hasTag("NoSleep") and not cell.isExterior then
            local knownOwners = {}
            local lastOwner = nil
            local foundOwners = 0
            for index, obj in ipairs(cell:getAll()) do
                if obj.owner.recordId then
                    if not knownOwners[obj.owner.recordId] then
                        knownOwners[obj.owner.recordId] = obj.owner.recordId
                        lastOwner = obj.owner.recordId
                        foundOwners = foundOwners + 1
                    end
                end
            end
        if foundOwners == 1 then
            print("Found for " .. cell.name,"Owner", lastOwner)
        end
    end
    end
end
local function findNPCHouse(actor)
    local cellName = actor.cell.name
    local actorname = actor.type.records[actor.recordId].name
    for index, value in ipairs(world.cells) do
        if string.find(value.name:lower(), actorname:lower()) ~= nil then
            print(value.name)
        end
    end
end
--When a door is loaded, check where it goes. If it is a interior cell, fron an exterior, we should be able to lock it, if the home owner is the right class
--Check who owns the items in the home. If commoner, they are a commoner so it should be locked at night at least, and the door should be owned by the homeowner.
local function onSave()
    return { knownNPCs = knownNPCs }
end
return {
    interfaceName  = "NPCSched",
    interface      = {
        version = 1,
        findNPCHouse = findNPCHouse,
        trackCells = trackCells,
    },
    engineHandlers = {
        onSave = onSave,
        onLoad = onLoad,
        onActorActive = onActorActive
    },
    eventHandlers  = {
    }
}
