local AlembicId = nil
local CalcinatorId = nil
local MortarPestleId = nil
local RetortId = nil

local tempAlembic = nil
local tempCalcinator = nil
local tempMortar = nil
local tempRetort = nil

local ZackBridge = require("scripts.TabletopAlchemy.zackbridge")


local mwse = require("openmw.core") == nil

if(mwse) then
    ZackBridge = require("TabletopAlchemy.scripts.TabletopAlchemy.zackbridge")


end

local function ownerCheck(object)
    if (ZackBridge.getOwnerId(object) ~= nil) then
        return false
    elseif (ZackBridge.getOwnerId(object) ~= nil) then
        return false
    end
    return true
end

local itemsTable = nil
local function sortIntoPlayer()
    local itemsTable = {} -- Table to store the item information

    local allConts = ZackBridge.getObjectsInCell(ZackBridge.getPlayer().cell,"Container")
    for x, cont in ipairs(allConts) do
        if (ownerCheck(cont)) then
            local ingreds = ZackBridge.getInventoryTable(cont)

            for i, ingred in ipairs(ingreds) do
                local recordId = ingred.recordId -- Record ID of the object
                local count = ingred.count       -- Number of objects
                local contId = cont.id           -- ID of the container

                -- Save the item information to the table
                local item = {
                    recordId = recordId,
                    count = count,
                    containerId = contId
                }
                ZackBridge.TransferItem(ZackBridge.getPlayer(), ingred)
                table.insert(itemsTable, item)
            end
        end
    end

    return itemsTable -- Return the table containing the item information
end
local function returnFromPlayer()
    local allConts = ZackBridge.getObjectsInCell(ZackBridge.getPlayer().cell,"Container")

    local ingreds = ZackBridge.getInventoryTable(ZackBridge.getPlayer(), "Ingredient")

    for c, ingred in ipairs(ingreds) do
        local recordId = ingred.recordId -- Record ID of the object
        local count = ingred.count       -- Number of objects
        local contId = ""                -- ID of the container


        local returnToContainer = false --this will determine if we need to return this ingredient to a container

        for i, tableItem in ipairs(itemsTable) do
            local trecordId = tableItem.recordId -- Record ID of the object
            local tcount = tableItem.count       -- Number of objects
            local tcontId = tableItem.containerId
            --  print("Trying to move", tcount, trecordId, tcontId)
            if (trecordId == recordId) then
                for x, cont in ipairs(allConts) do
                    if (cont.id == tcontId and ingred.count > 0) then
                        if (tcount < ingred.count) then --if we are moving less than the ingredients in this container
                            ZackBridge.TransferItem(cont, ZackBridge.SplitStack(tcount))

                            break
                        else
                            ZackBridge.TransferItem(cont, ingred)

                            break
                        end
                        table.remove(itemsTable, i) -- Remove the table item
                    end
                end
            end
        end
    end
    itemsTable = nil
end

local function checkForApparus(objectList, originObject)
    local originQuality = ZackBridge.getObjectRecordData(originObject).quality
    AlembicId = nil
    CalcinatorId = nil
    MortarPestleId = nil
    RetortId = nil
    for x, appar in ipairs(objectList) do
        if (ownerCheck(appar) == true) then --can't use someone else's set
            local quality = ZackBridge.getObjectRecordData(appar).quality
            local type = ZackBridge.getObjectRecordData(appar).type
            if (quality == originQuality and type == ZackBridge.getConst("Alembic")) then
                AlembicId = appar.recordId
            end
            if (quality == originQuality and type == ZackBridge.getConst("Calcinator")) then
                CalcinatorId = appar.recordId
            end
            if (quality == originQuality and type == ZackBridge.getConst("MortarPestle")) then
                MortarPestleId = appar.recordId
            end
            if (quality == originQuality and type == ZackBridge.getConst("Retort")) then
                RetortId = appar.recordId
            end
        end
    end

    if (AlembicId == nil or CalcinatorId == nil or MortarPestleId == nil or RetortId == nil) then
        return false
    end
    return true
end
local waitCount = -1
local function onUpdate(dt)
    if (waitCount > -1) then
        waitCount = waitCount + 1
        if (waitCount > 10) then
            returnFromPlayer()
            ZackBridge.RemoveItem(AlembicId,1, ZackBridge.getPlayer())
            ZackBridge.RemoveItem(CalcinatorId,1, ZackBridge.getPlayer())
            ZackBridge.RemoveItem(MortarPestleId,1, ZackBridge.getPlayer())
            ZackBridge.RemoveItem(RetortId,1, ZackBridge.getPlayer())
            waitCount = -1
        end
    end
end
local function isCloseEnough(value1, value2, decimalPlaces)
    decimalPlaces = 3
    local factor = 10 ^ decimalPlaces
    local roundedValue1 = math.floor(value1 * factor + 0.5) / factor
    local roundedValue2 = math.floor(value2 * factor + 0.5) / factor
    return roundedValue1 == roundedValue2
end



local function apparatusActivate(object, actor)
   local player = actor
   print("Activated")
    if (ZackBridge.isWorldPaused() == true) then
        --let the user pick up the apparatus if in their inventory. This is pointless right now since it's not called in that case, but this future proofs things.
        return true
    end
    if (ownerCheck(object) == false) then
        --can't cook with someone else's apparatus.
        return true
    end
    local allApparatus = ZackBridge.getObjectsInCell(object.cell, "Apparatus")
    local valid = checkForApparus(allApparatus, object)
    --print(valid)
    if (valid) then
        tempAlembic = ZackBridge.AddItem(AlembicId, 1, player)
        tempCalcinator = ZackBridge.AddItem(CalcinatorId, 1, player)
        tempMortar = ZackBridge.AddItem(MortarPestleId, 1, player)
        tempRetort = ZackBridge.AddItem(RetortId, 1, player)
        itemsTable = sortIntoPlayer()
        local holdingCell = ZackBridge.getObjectsInCell("_AHA_HoldingCell")
        waitCount = 0
         print(#holdingCell)
        for x, appar in ipairs(holdingCell) do
            local decimalPlaces = 1 -- You can adjust the decimal places as per your requirement

            if (isCloseEnough(ZackBridge.getObjectRecordData(tempAlembic).quality, 1.5) and appar.recordId == "zhac_aha_gm") then
                appar:teleport(player.cell, player.position)
                --  print("tp")
            end
            if (isCloseEnough(ZackBridge.getObjectRecordData(tempAlembic).quality, 1.2) and appar.recordId == "zhac_aha_m") then
                appar:teleport(player.cell, player.position)
            end
            if (isCloseEnough(ZackBridge.getObjectRecordData(tempAlembic).quality, 1.0) and appar.recordId == "zhac_aha_j") then
                appar:teleport(player.cell, player.position)
            end
            if (isCloseEnough(ZackBridge.getObjectRecordData(tempAlembic).quality, 0.5) and appar.recordId == "zhac_aha_a") then
                appar:teleport(player.cell, player.position)
            end
        end
    end
    return false
end

ZackBridge.AddObjectTypeActivationHandler("Apparatus", apparatusActivate)
ZackBridge.AddEngineHandler("onUpdate", onUpdate)
print("Loaded TTA")
return
