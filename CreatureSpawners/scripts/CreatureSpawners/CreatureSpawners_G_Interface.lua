local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local markerID = "furn_mudcave_pool00"
local CLL = types.LevelledCreature.record
local customLvlRecordData = {}

math.randomseed(os.time())
local function doNotSpawn(chance)
  local rand = math.random()
  print(rand)
  return rand < chance
end
local function getRandomItemFromList(list)
  local randomIndex = math.random(1, #list)
  local ret = { id = list[randomIndex].id, level = list[randomIndex].level }
  return ret
end
local function onSave()
  return { customLvlRecordData = customLvlRecordData }
end
local function onLoad(data)
  if data then
    customLvlRecordData = data.customLvlRecordData
  end
end
local function addActorToList(creatureId, minLevel, listId)
  local record = CLL(listId)
  creatureId = creatureId:lower()
  if record then
    if not customLvlRecordData[listId:lower()] then
      customLvlRecordData[listId:lower()] = {}
      table.insert(customLvlRecordData[listId:lower()], { id = creatureId, level = minLevel })
    else
      local found = false
      for index, value in ipairs( customLvlRecordData[listId:lower()]) do
        if value.id == creatureId:lower() then
          found = true
        end
      end
      if not found then
        
      table.insert(customLvlRecordData[listId:lower()], { id = creatureId, level = minLevel })
      end
    end
  end
end
local function getCreatureList(listId)
  local record = CLL(listId)

  local ls = {}
    for index, value in ipairs(record.creatures) do
      table.insert(ls, value)
    end
    if customLvlRecordData[listId:lower()] then
      for index, value in ipairs(customLvlRecordData[listId:lower()]) do
        table.insert(ls, value)
      end
    end
    return ls
end
local function getActorToSpawn(listId, level)
  local record = CLL(listId)
  local skipSpawn = doNotSpawn(record.chanceNone)
  if skipSpawn then return nil end
  if not level then
    level = types.Actor.stats.level(world.players[1]).current
  end
  local ls = getCreatureList(listId)
  local lvlLs = {}
  for index, value in ipairs(ls) do
      if value.level <= level then
        table.insert(lvlLs,value)
      end
  end
  if #lvlLs == 0 then
    return nil
  end
  return getRandomItemFromList(lvlLs).id
end
local function placeNewSpawner(listId, position, cell, scale)
  I.CreatureSpawners_World.placeNewSpawner(listId, position, cell, scale)
--core.sendGlobalEvent("placeNewSpawner",{listId = listId, position = position, cell = cell,scale = scale})
end
return {
  interfaceName  = "CreatureSpawners",
  interface      = {
    addActorToList = addActorToList,
    getCreatureList = getCreatureList,
    doNotSpawn = doNotSpawn,
    placeNewSpawner = placeNewSpawner,
    getActorToSpawn = getActorToSpawn,
    version = 1,
  },
  engineHandlers = {
    onObjectActive = onObjectActive,
    onLoad = onLoad,
    onSave = onSave
  },
  eventHandlers  = {
    itemSwapEvent = itemSwapEvent,
  },
}
