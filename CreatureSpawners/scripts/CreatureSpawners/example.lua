local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local world = require("openmw.world")
local records = types.LevelledCreature.records


--Add fargoth to every levelled list
for index, record in ipairs(records) do
    --    I.CreatureSpawners.addActorToList("fargoth",0,record.id)
end


I.CreatureSpawners.placeNewSpawner("ex_shore_mudcrab", util.vector3(-64329.515625, 41754.75390625, 352.97271728515625),
    world.getExteriorCell(-8, 5))
--Place a new spawner at the specified position



I.CreatureSpawners.addActorToList("galbedir", 0, "ex_shore_mudcrab")
--Add Galbedir to the mudcrab list



--Test with placeatpc ex_shore_mudcrab 1 0 0
