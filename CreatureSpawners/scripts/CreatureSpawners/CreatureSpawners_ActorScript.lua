
local self = require("openmw.self")
local core = require("openmw.core")
local function onInactive()
core.sendGlobalEvent("ActorInactive",self.object)
end
return {
    interfaceName = "BountyCancel",
    interface = { isFightingPlayer = isFightingPlayer },
    engineHandlers = { onInactive = onInactive, onActive = onActive, onSave = onSave, onLoad = onLoad },
    eventHandlers = { checkCrimeWitness = checkCrimeWitness, reportPackages = reportPackages, }
}
