local I = require("openmw.interfaces")

local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local markerID = "furn_mudcave_pool00"
local spawnerObjectData = {}
local spawnedCreatureData = {}
local actorScrPath = "scripts/CreatureSpawners/CreatureSpawners_ActorScript.lua"
local purgeDone = false
local function distanceBetweenPos(vector1, vector2)
    --Quick way to find out the distance between two vectors.
    --Very similar to getdistance in mwscript
    local dx = vector2.x - vector1.x
    local dy = vector2.y - vector1.y
    local dz = vector2.z - vector1.z
    return math.sqrt(dx * dx + dy * dy + dz * dz)
end
local function findMyActor(obj)
    --Note that this does not work for spawners that already spawned and the actor moved
    for index, crea in ipairs(obj.cell:getAll(types.Creature)) do
        --local dist = distanceBetweenPos(obj.position,crea.position )
        --Find any Creatures that already spawned
        local samePos = crea.position == obj.position
        if samePos then
            return crea
        end
    end
    for index, crea in ipairs(obj.cell:getAll(types.NPC)) do
        --local dist = distanceBetweenPos(obj.position,crea.position )
        local samePos = crea.position == obj.position
        --Find any NPCs that already spawned
        if samePos then
            return crea
        end
    end
end
local function placeNewSpawner(listId, position, cell, scale)
    if not scale then
        scale = 1
    end
    local newObj = world.createObject(markerID)
    local newPos = util.vector3(position.x, position.y, position.z - 10000)
    newObj:teleport(cell, newPos)
    if newObj.scale > 0 then
        newObj:setScale(0)
    end
    spawnerObjectData[newObj.id] = {
        --originalSpawnerID = obj.id,
        originalSpawnerRecordID = listId,
        scale = scale,
        spawnPosition = position,
        spawnedCreatureId = nil,
        spawnerObjectId = newObj.id
    }
    return spawnerObjectData[newObj.id]
end
local function placeNewSpawnerEvent(data)
    placeNewSpawner(data.listId,data.position,data.cell,data.scale)
end
local function handleSpawnerReplace(obj)
    obj.enabled = false
    if spawnerObjectData[obj.id] then return false end
    local data = placeNewSpawner(obj.recordId, obj.position, obj.cell, obj.scale)
    spawnerObjectData[obj.id] = { newObj = data.spawnerObjectId }
    obj:remove()
    return true
end
local function creaturePurge()
    for index, cell in ipairs(world.cells) do
        -- if cell.isExterior then
        for index, obj in ipairs(cell:getAll()) do
            if obj.type == types.Creature then
               -- obj:remove()
            elseif obj.type == types.LevelledCreature then
                -- obj.enabled = false
                handleSpawnerReplace(obj)
            end
        end
        -- end
    end
    purgeDone = true
end
local function onPlayerAdded()
    if not purgeDone then
       -- creaturePurge()
       --This is not worth it, will really make your save bigger
    end
end
local function respawnActor(markerObj)
    local markerObjId = markerObj.id
    local data = spawnerObjectData[markerObjId]
    if spawnerObjectData[markerObjId].spawnedCreatureId ~= nil then
        --delete the old creature
        print("Already spawned!")
        return
        --spawnedCreatureData[data.spawnedCreatureId] = nil
    end
    local newCreatureID = I.CreatureSpawners.getActorToSpawn(data.originalSpawnerRecordID)

    if newCreatureID ~= nil then
        local newCreature = world.createObject(newCreatureID)
        local newId = newCreature.id
        if not newId or newId == "" then
            error("ID is nul!")
        end
        if data.scale ~= 1 and data.scale ~= nil then
            newCreature:setScale(data.scale)
        end
        newCreature:addScript(actorScrPath)
        newCreature:teleport(markerObj.cell, data.spawnPosition)
        spawnedCreatureData[newId] = {
            parentSpawner = markerObjId,
            currentCellName = markerObj.cell.name,
            ccellX = markerObj.cell.gridX,
            ccellY = markerObj.cell.gridY
        }
        spawnerObjectData[markerObjId].spawnedCreatureId = newId
    end
    spawnerObjectData[markerObjId].lastSpawn = core.getGameTime()
end
local function onObjectActive(obj)
    if obj.type == types.Activator and spawnerObjectData[obj.id] then
        obj:setScale(0)
        if spawnerObjectData[obj.id].spawnedCreatureId == nil then
            respawnActor(obj)
        end
    elseif obj.type == types.LevelledCreature and handleSpawnerReplace(obj) == true then
        local crea = findMyActor(obj)
        if crea then
            --  print("Found Creature" ,crea.id)
            crea:remove() --remove the normal creature so we can place our own
        else
            --  print("No creature for me")
        end
    end
end

local function getActorParentID(actor)
    if spawnedCreatureData[actor.id] then
        print("parentObj", spawnerObjectData[spawnedCreatureData[actor.id].parentSpawner].spawnedCreatureId)
        return (spawnedCreatureData[actor.id].parentSpawner)
    else
        print("parentObj", spawnedCreatureData[actor.id].parentSpawner)
        print("Actor not saved")
        return nil
    end
end
local function onSave()
    return { spawnerObjectData = spawnerObjectData, spawnedCreatureData = spawnedCreatureData, purgeDone = purgeDone }
end
local function onLoad(data)
    if data then
        spawnerObjectData = data.spawnerObjectData
        purgeDone = data.purgeDone
        spawnedCreatureData = data.spawnedCreatureData
        if not spawnedCreatureData then spawnedCreatureData = {} end
    end
end
local function ActorInactive(actor)
    local data = spawnedCreatureData[actor.id]
    if not data then
        error("Unsaved creature")
    end
    spawnedCreatureData[actor.id] = {
        parentSpawner = spawnedCreatureData[actor.id].parentSpawner,
        currentCellName = actor.cell.name,
        ccellX = actor.cell.gridX,
        ccellY = actor.cell.gridY
    }
end
return {
    interfaceName  = "CreatureSpawners_World",
    interface      = {
        creaturePurge = creaturePurge,
        getActorParentID = getActorParentID,
        version = 1,
        placeNewSpawner = placeNewSpawner,
    },
    engineHandlers = {
        onObjectActive = onObjectActive,
        onLoad = onLoad,
        onSave = onSave,
        onPlayerAdded = onPlayerAdded,
    },
    eventHandlers  = {
        ActorInactive = ActorInactive,
        placeNewSpawnerEvent = placeNewSpawnerEvent,
    },
}
