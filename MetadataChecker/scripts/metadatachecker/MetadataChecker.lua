local vfs = require('openmw.vfs')
local function sanatizeIndex(inputString)
    local replacedString = string.gsub(inputString, "[. ]", "_")
    return replacedString
end
local function parseMetadata(metadataText)
    local metadata = {}
    local currentSection = nil

    for line in metadataText:gmatch("[^\r\n]+") do
        -- Ignore lines starting with #
        if not line:match("^%s*#") then
            local section = line:match("^%[([^%]]+)%]$")
            if section then
                currentSection =sanatizeIndex(section) 
                metadata[currentSection] = {}
            elseif currentSection then
                local key, value = line:match("^%s*([%w%-_]+)%s*=%s*(.+)$")
                if key and value then
                    key =sanatizeIndex(key) 
                    local trimmedValue = value:match('^%s*(.-)%s*$')
                    local arrayValues = {}
                    for arrayValue in trimmedValue:gmatch('"([^"]+)"') do
                        table.insert(arrayValues, arrayValue)
                    end
                    -- If it's an array with a single value, store as single value
                    if #arrayValues == 1 then
                        metadata[currentSection][key] = arrayValues[1]
                    else
                        -- If it's an array with multiple values, store as array
                        metadata[currentSection][key] = #arrayValues > 0 and arrayValues or trimmedValue
                    end
                end
            end
        end
    end

    return metadata
end
local function endsWith(str, ending)
    if #str < #ending then
        return false
    else
        return ending == "" or str:sub(-#ending) == ending
    end
end

local function getTOMLLines()
    local files = {}
    for fileName in vfs.pathsWithPrefix("") do
        if endsWith(fileName:lower(),".toml") then
            local key = fileName
        local lines = {}
        local lineStr = ""
        for line in vfs.lines(fileName) do
            lineStr = lineStr .. ""
            table.insert(lines,line)
        end
        local tabe = parseMetadata(table.concat(lines, "\n"))
        if tabe.package and tabe.package.name then
            key = tabe.package.name
        end
        files[sanatizeIndex(key)] = tabe
        end
    end
    return files
end
local function getModLoadable(modName)

end
local metadataText = [[
# Data Files/mymod-metadata.toml

# Holds basic information about the mod
[package]
name = "My Mod"
description = "A description of the mod"
homepage = "https://www.nexusmods.com/morrowind/mods/49658"
repository = "https://github.com/jhaakma/skoomaesthesia"
authors = ["Merlord", "Greatness7"]
version = "7.8.9"

# MWSE specific information about this mod
[tools.mwse]
lua-mod = "mer.myMod"
load-order = 100
wait-until-initialized = true

# Dependencies are checked on `initialized` and warn the player if any are missing
[dependencies]
  # Assets can be directories or loose folders
  assets = [
      "Sound\\mySounds",
      "Textures\\tex.dds",
      "Meshes\\mesh.nif"
  ]

# These will only throw an error if the assets are missing AND the BSA is inactive
[dependencies.archives]
"TR_Data.bsa" = [
    "Textures\\TR"
]

# Check MWSE version using build number (the number that comes after "v2.1.0" in MWSE.log)
[dependencies.mwse]
buildnumber = 2907

# Check MGE XE version
[dependencies.mge-xe]
version = ">=0.15.0"

# mwse-module can be a folder or lua file, anywhere in /mods, /lib etc
[dependencies.mods."My Lua mod"]
mwse-module = "mer.myLuaMod"

# Check if a plugin (esp or esm) is active
[dependencies.mods."OAAB Data"]
plugin = "OAAB_Data.esm"

# Finds the version from the metadata file of the dependency
# If url is provided, a download button will appear if the dependency isn't met
[dependencies.mods."My Other Mod"]
version = ">=7.8.9"
url = "https://www.nexusmods.com/morrowind/mods/52116"

# Check if user has specific MCP feature enabled or disabled
[dependencies.mcp.features]
# The following MCP feature needs to be enabled
creatureVoiceoverEnable = true
# The mod is uncompatible with the following MCP feature,
# it needs to be disabled for the mod to work properly
arrowDenocker = false
]]
return getTOMLLines()