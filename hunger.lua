local self = require('openmw.self')
local types = require('openmw.types')
local ui = require('openmw.ui')
local core = require('openmw.core')
local async = require('openmw.async')

local nextHungerTick = core.getGameTime() + 3600
local hungerPercent = 100

local function getTrueFatigueModified()	--gets the player's true current max fatigue using stats
	local STR = types.Player.stats.attributes["strength"](self).modified
	local END = types.Player.stats.attributes["endurance"](self).modified
	local AGL = types.Player.stats.attributes["agility"](self).modified
	local WIL = types.Player.stats.attributes["willpower"](self).modified
	local fMod = STR + END + AGL + WIL
	return fMod
end



local function calcHungerPercent() --calculated hunger percentage based on current stats
	local baseFatigue = types.Player.stats.dynamic["fatigue"](self).base
	local modFatigue = types.Player.stats.dynamic["fatigue"](self).modifier
	local maxFatigue = baseFatigue + modFatigue
	
	return maxFatigue / getTrueFatigueModified() * 100
end


local function setFatigueMax(fatiguePercent) --sets the current max fatigue based on a percent of the true fatigue max
	if fatiguePercent > 100 then
		fatiguePercent = 100
	elseif fatiguePercent < 1 then
		fatiguePercent = 1
	end
	
	local currentFatigue = types.Player.stats.dynamic["fatigue"](self).current
	local maxFatigue = types.Player.stats.dynamic["fatigue"](self).base + types.Player.stats.dynamic["fatigue"](self).modifier
	local newMaxFatigue = getTrueFatigueModified() * fatiguePercent / 100
	local newFatigue = newMaxFatigue * (currentFatigue/maxFatigue)
	
	types.Player.stats.dynamic["fatigue"](self).base = newMaxFatigue
	types.Player.stats.dynamic["fatigue"](self).current =  newFatigue
	
	hungerPercent = calcHungerPercent()
end


local function inList(list, val)
	local valFound = 0
	for i, current in pairs(list) do
		if current == val then
			valFound = 1
		end
	end
	return valFound	
end

local function showHungerPercent(foodAdd)
	if(foodAdd > 0) then
		ui.showMessage("+" .. foodAdd .. "%: " .. "Hunger = " .. math.floor(hungerPercent+.5) .. "%")
	else
		ui.showMessage("Hunger = " .. math.floor(hungerPercent+.5) .. "%")
	end
end

local function onConsume(item)	
	local itemID = item.recordId
	local foodValue = types.Ingredient.records[itemID].value
	local foodWeight = types.Ingredient.records[itemID].weight
	local foodName = types.Ingredient.records[itemID].name
	local hungerValue =(foodWeight * 0.25)+(1/foodWeight*foodValue/1000) 
	hungerValue  = math.floor((hungerValue *1000)+0.5)/10
	
	local foodItems = {"ingred_ash_yam_01", "ingred_bread_01", "ingred_comberry_01", "ingred_crab_meat_01", 
	"ingred_hackle-lo_leaf_01", "ingred_hound_meat_01", "food_kwama_egg_02", "ingred_marshmerrow_01", "ingred_rat_meat_01",
	"ingred_saltrice_01", "ingred_scrib_jelly_01", "ingred_scrib_jerky_01", "ingred_scuttle_01", "food_kwama_egg_01", "ingred_wickwheat_01"
	}
		
	if inList(foodItems, itemID) == 1 then
		setFatigueMax(hungerPercent + hungerValue)
		showHungerPercent(hungerValue)
	end	
end


local function onKeyPress(key)
	if key.symbol == 'h' then		
		showHungerPercent(0)
	end
end

local function onUpdate(dt)	
	if (math.floor(hungerPercent) ~= math.floor(calcHungerPercent())) then --player has altered their fatigue somehow, reset it
		setFatigueMax(hungerPercent)
	end
	
	if nextHungerTick <= core.getGameTime() then				
		local totalTicks = math.floor((core.getGameTime() - nextHungerTick)/3600)
		for i = 0, totalTicks, 1 do
			setFatigueMax(hungerPercent - 4) --4%/hr = 96% hunger per day
		end
		showHungerPercent(0)
		nextHungerTick = core.getGameTime() + 3600 --1 hour ticks (60 seconds * 60 minutes)
	end
end

local function onInit(initData)
	ui.showMessage("INIT")
	nextHungerTick = core.getGameTime() + 3600
	hungerPercent = 100
end

local function onLoad(saveData, initData)
	ui.showMessage("LOAD")
	nextHungerTick = saveData.nextHungerTick
	hungerPercent = saveData.hungerPercent
end

local function onSave()
	return {nextHungerTick = nextHungerTick, hungerPercent = hungerPercent}
end

return {
    engineHandlers = {
	onKeyPress = onKeyPress,
    onConsume = onConsume,
	onUpdate = onUpdate,
	onInit = onInit,
	onLoad = onLoad,
	onSave = onSave
    }
}