local util = require("openmw.util")
local world = require("openmw.world")
local core = require("openmw.core")
local types = require("openmw.types")

local function roundToNearestMultipleOf90(angle)
  local remainder = angle % 90
  if remainder >= 45 then
	print("Modified angle from " ..tostring(angle) .. " to " .. tostring(angle + (90 - remainder)))
    return angle + (90 - remainder)
  else
	print("Modified angle from " ..tostring(angle) .. " to " .. tostring(angle - remainder))
    return angle - remainder
  end
end
local function ReplaceItem(oldObject, newObjectId)
  local newObject = world.createObject(newObjectId, 1)
  print("Replaced item " .. oldObject.recordId .. tostring(oldObject.rotation.z))
  local newrot = util.vector3( oldObject.rotation.x, oldObject.rotation.y,math.rad(roundToNearestMultipleOf90(math.deg(oldObject.rotation.z)))) 
  -- newrot = util.vector3(oldObject.rotation.x, oldObject.rotation.y, 270) -- Changed newrot.z to oldObject.rotation.z
  newObject:teleport(oldObject.cell.name, oldObject.position, newrot)
  oldObject.enabled = false
  newObject.enabled = true
end


function degreesToRadians(degrees)
  return degrees * math.pi / 180
end

function rotateObjects(statics, degrees)
  local angle = degreesToRadians(degrees)

  -- Calculate the center of rotation
  local center = util.vector3(0, 0, 0)
  for i=1,#statics do
    center = center + statics[i].position
  end
  center = center / #statics

  -- Rotate each object around the center
  for i=1,#statics do
    -- Calculate the relative position of the object to the center
    local relativePosition = statics[i].position - center
    
    -- Calculate the new position
    local x = relativePosition.x * math.cos(angle) - relativePosition.y * math.sin(angle)
    local y = relativePosition.x * math.sin(angle) + relativePosition.y * math.cos(angle)
    local position = util.vector3(x + center.x, y + center.y, statics[i].position.z)

    -- Calculate the new rotation
    local rotation = util.vector3(statics[i].rotation.x, statics[i].rotation.y, statics[i].rotation.z - angle)

    -- Move the object
    statics[i]:teleport(statics[i].cell.name, position, rotation)
  end
end











local rot = 0

local function ZBTCellChange(eventData)
  local player = eventData.player
  local cell = player.cell
  local statics = cell:getAll()
  local players = {player }
local mergedTable = {}
for i, v in ipairs(statics) do
  table.insert(mergedTable, v)
end
for i, v in ipairs(players) do
  table.insert(mergedTable, v)
end

-- print the merged table
for i, v in ipairs(mergedTable) do
  --print(i, v)
end
  rot = rot + 0.01
  if(rot > 360) then
  rot = 0
  end
  
rotateObjects(mergedTable,math.rad(rot))
--  for _, static in ipairs(statics) do
 --   if types.Static.record(static).model == "meshes\\f\\Furn_De_Bookshelf_02.NIF" and static.enabled then
     -- ReplaceItem(static, "zbt_De_R_Bookshelf_02")
  --  end
 -- end
end



return {
  eventHandlers = {
    ZBTCellChange = ZBTCellChange
  }
}