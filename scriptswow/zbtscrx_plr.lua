local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local self = require("openmw.self")

local lastCell = nil
local function cellChange()


end
local dttotal = 0
local function onUpdate(dt)
dttotal = dttotal + dt
if(dttotal > 0.025) then
  local cell = self.object.cell
  --if cell ~= lastCell then
    core.sendGlobalEvent("ZBTCellChange", {
        player = self
    })
	dttotal = 0
  lastCell = cell
end
	
 -- end
end

return {
  engineHandlers = {
    onUpdate = onUpdate
  }
}