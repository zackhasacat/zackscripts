
local types = require("openmw.types")
local world = require('openmw.world')
local function isCharGenFinished()
    if not types.Player.isCharGenFinished then
        return true
    else
        return types.Player.isCharGenFinished(world.players[1])
    end
end
local function getPlayerName()

    return world.players[1].type.record(world.players[1]).name
end
local function getPlayerBirthSign()

    return types.Player.getBirthSign(world.players[1])
end
local function isTestChar()
    if getPlayerBirthSign() == "" and isCharGenFinished() then
        return true
    else
        return false
    end
end

local function isConsoleBlocked()
    local playerName = getPlayerName()
    if not playerName then
        --print("No player record")
        return false
    elseif isTestChar() then
        --print("Player is named player")
        return false
    else
        --print("Player is not named player")
        return true
    end
end
return {
    interfaceName = "ConsoleCheck",
    interface = {
        isConsoleBlocked = isConsoleBlocked
    },
}
